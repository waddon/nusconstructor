<?php

use Illuminate\Database\Seeder;
use App\SchoolType;

class SchooltypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolType::create([
                    'name_schooltype'=>'Навчальний заклад',
                ]);
        SchoolType::create([
                    'name_schooltype'=>'Видавництво',
                ]);
    }
}
