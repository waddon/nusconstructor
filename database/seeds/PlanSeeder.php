<?php

use Illuminate\Database\Seeder;
use App\Plan;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::create([
                    'name_plan'=>'Базовий навчальний план закладу початкової освіти з українською мовою навчання',
                    'id_cikl'=>1,
                    'id_ptype'=>1,
                    'content_plan'=>'{"0":{"type_subject":"2","name_subject":"Мовно-літературна","program":"0","name_program":"","galuzes":{"0":{"galuz":"1","classes":{"0":{"class":"1","hours":"245"},"1":{"class":"2","hours":"280"}}}}},"1":{"type_subject":"2","name_subject":"Математична","program":"0","name_program":"","galuzes":{"0":{"galuz":"3","classes":{"0":{"class":"1","hours":"315"},"1":{"class":"2","hours":"350"}}}}}}',
                ]);
        Plan::create([
                    'name_plan'=>'Базовий навчальний план закладу початкової освіти з навчанням мовою корінного народу або національної меншини',
                    'id_cikl'=>2,
                    'id_ptype'=>1,
                    'content_plan'=>'{"0":{"type_subject":"2","name_subject":"Мовно-літературна","program":"0","name_program":"","galuzes":{"0":{"galuz":"1","classes":{"0":{"class":"3","hours":"245"},"1":{"class":"4","hours":"280"}}}}},"1":{"type_subject":"2","name_subject":"Математична","program":"0","name_program":"","galuzes":{"0":{"galuz":"3","classes":{"0":{"class":"3","hours":"315"},"1":{"class":"4","hours":"350"}}}}}}',
                ]);
        Plan::create([
                    'name_plan'=>'Типовий план на базі 1 базового',
                    'id_parent'=>1,
                    'id_cikl'=>1,
                    'id_ptype'=>2,
                    'content_plan'=>'{"0":{"type_subject":"1","name_subject":"Українська мова","program":"1","name_program":"Мовна програма","galuzes":{"0":{"galuz":"1","classes":{"0":{"class":"1","hours":"6"},"1":{"class":"2","hours":"7"}}}}},"1":{"type_subject":"1","name_subject":"Математика","program":"2","name_program":"Програма з математики","galuzes":{"0":{"galuz":"3","classes":{"0":{"class":"1","hours":"8"},"1":{"class":"2","hours":"9"}}}}},"2":{"type_subject":"2","name_subject":"Порахуй","program":"3","name_program":"Інтегрована програма","galuzes":{"0":{"galuz":"1","classes":{"0":{"class":"1","hours":"1"},"1":{"class":"2","hours":"1"}}},"1":{"galuz":"3","classes":{"0":{"class":"1","hours":"1"},"1":{"class":"2","hours":"1"}}}}}}',
                ]);
        Plan::create([
                    'name_plan'=>'Типовий план для циклу 3-4 класи',
                    'id_parent'=>2,
                    'id_cikl'=>2,
                    'id_ptype'=>2,
                    'content_plan'=>'{"0":{"type_subject":"1","name_subject":"Українська мова","program":"4","name_program":"Українська мова","galuzes":{"0":{"galuz":"1","classes":{"0":{"class":"3","hours":"7"},"1":{"class":"4","hours":"8"}}}}},"1":{"type_subject":"1","name_subject":"Математика","program":"5","name_program":"Математика","galuzes":{"0":{"galuz":"3","classes":{"0":{"class":"3","hours":"9"},"1":{"class":"4","hours":"10"}}}}}}',
                ]);

    }
}



