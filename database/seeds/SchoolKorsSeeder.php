<?php

use Illuminate\Database\Seeder;
use App\SchoolKor;

class SchoolKorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolKor::create([
                    'id_school'=>101,
                    'id_galuz'=>1,
                    'key_schoolkor'=>'1 ХРЕН 12',
                    'name_schoolkor'=>'Равным образом постоянный количественный рост и сфера нашей активности играет важную роль в формировании позиций',

                ]);
        SchoolKor::create([
                    'id_school'=>101,
                    'id_galuz'=>1,
                    'key_schoolkor'=>'Железная бабочка - 13',
                    'name_schoolkor'=>'Идейные соображения высшего порядка, а также укрепление и развитие структуры представляет собой интересный эксперимент',

                ]);
        SchoolKor::create([
                    'id_school'=>101,
                    'id_galuz'=>2,
                    'key_schoolkor'=>'7 Волосатое стекло',
                    'name_schoolkor'=>'Таким образом реализация намеченных плановых заданий способствует подготовки и реализации форм развития',

                ]);
        SchoolKor::create([
                    'id_school'=>101,
                    'id_galuz'=>3,
                    'key_schoolkor'=>'ДИНАМИТ',
                    'name_schoolkor'=>'Задача организации, в особенности же постоянный количественный рост и сфера нашей активности позволяет выполнять важные задания',

                ]);
    }
}
