<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create([
                    'id_status'=>1,
                    'name_status'=>'Редагуємий',
                ]);
        Status::create([
                    'id_status'=>2,
                    'name_status'=>'Підготовлено до узгодження без перевірки',
                ]);
        Status::create([
                    'id_status'=>3,
                    'name_status'=>'Перевірено та підготовлено до узгодження',
                ]);
        Status::create([
                    'id_status'=>4,
                    'name_status'=>'Підготовлено до узгодження',
                ]);
        Status::create([
                    'id_status'=>5,
                    'name_status'=>'Узгоджено на рівні закладу',
                ]);
        Status::create([
                    'id_status'=>6,
                    'name_status'=>'Напралено в МОН на узгодження',
                ]);
        Status::create([
                    'id_status'=>7,
                    'name_status'=>'Узгоджено на рівні МОН',
                ]);
        Status::create([
                    'id_status'=>8,
                    'name_status'=>'Повернуто на доопрацювання',
                ]);
        Status::create([
                    'id_status'=>9,
                    'name_status'=>'Архів',
                ]);
    }
}
