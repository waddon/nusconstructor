<?php

use Illuminate\Database\Seeder;
use App\Create;

class CreateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Create::create([
                    'id_create'=>1,
                    'name_create'=>'Креатор',
                ]);
        Create::create([
                    'id_create'=>2,
                    'name_create'=>'Модіфікатор',
                ]);
    }
}
