<?php

use Illuminate\Database\Seeder;
use App\Tool;

class ToolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tool::create([
                    'name_tool'=>'Спостереження',
                ]);
        Tool::create([
                    'name_tool'=>'Завдання',
                ]);
        Tool::create([
                    'name_tool'=>'Опитування',
                ]);
    }
}
