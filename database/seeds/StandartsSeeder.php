<?php

use Illuminate\Database\Seeder;
use App\Riven;
use App\Cikl;
use App\SchoolClass;
use App\Galuz;

class StandartsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Riven::create([
                    'name_riven'=>'Молодша школа',
                ]);
        Riven::create([
                    'name_riven'=>'Середня школа',
                ]);
        Riven::create([
                    'name_riven'=>'Старша школа',
                ]);
        Cikl::create([
                    'id_riven'=>1,
                    'name_cikl'=>'Цикл 1-2',
                    'marking_cikl'=>'2',
                ]);
        Cikl::create([
                    'id_riven'=>1,
                    'name_cikl'=>'Цикл 3-4',
                    'marking_cikl'=>'4',
                ]);
        SchoolClass::create([
                    'id_cikl'=>1,
                    'name_class'=>'1 клас',
                ]);
        SchoolClass::create([
                    'id_cikl'=>1,
                    'name_class'=>'2 клас',
                ]);
        SchoolClass::create([
                    'id_cikl'=>2,
                    'name_class'=>'3 клас',
                ]);
        SchoolClass::create([
                    'id_cikl'=>2,
                    'name_class'=>'4 клас',
                ]);
        Galuz::create([
                    'name_galuz'=>'Мовно-літературна',
                    'marking_galuz'=>'МОВ',
                    'color_galuz'=>'#ff4a49',
                    'text_galuz'=>'<h2>Рідномовна освіта (українська мова та літератури, мови та літератури корінних народів та національних меншин)</h2><p><strong>Мета:</strong>розвиток здатності спілкуватися українською мовою для духовного, культурного й національного самовияву, послуговуватися нею в особистому і суспільному житті, у міжкультурному діалозі, бачити її передумовою життєвого успіху; плекання здатності спілкуватися рідною мовою (якщо вона не українська); формування шанобливого ставлення до культурної спадщини; збагачення емоційно-чуттєвого досвіду.</p><p><strong>Загальні цілі:</strong></p><p>Учень/учениця:</p><p>1) взаємодіє з іншими усно, сприймає і використовує інформацію для досягнення життєвих цілей в різних комунікативних ситуаціях;</p><p>2) аналізує, інтерпретує, критично оцінює інформацію в текстах різних видів, медіатекстах та використовує її; сприймає художній текст як засіб збагачення особистого емоційно-чуттєвого досвіду;</p><p>3) висловлює думки, почуття та ставлення, взаємодіє з іншими письмово та в режимі онлайн, дотримується норм літературної мови;</p><p>4) досліджує індивідуальне мовлення – своє та інших, використовує це для власної мовної творчості, спостерігає за мовними явищами, аналізує їх.</p>',
                ]);
        Galuz::create([
                    'name_galuz'=>'Іншомовна',
                    'marking_galuz'=>'ІНМО',
                    'color_galuz'=>'#e9775c',
                    'text_galuz'=>'Мета і цілі',
                ]);
        Galuz::create([
                    'name_galuz'=>'Математична',
                    'marking_galuz'=>'МАО',
                    'color_galuz'=>'#195dc2',
                    'text_galuz'=>'Мета і цілі',
                ]);
        Galuz::create([
                    'name_galuz'=>'Природнича',
                    'marking_galuz'=>'ПРО',
                    'color_galuz'=>'#6be1c9',
                    'text_galuz'=>'Мета і цілі',
                ]);
        Galuz::create([
                    'name_galuz'=>'Технологічна',
                    'marking_galuz'=>'ТЕО',
                    'color_galuz'=>'#7f5ea5',
                    'text_galuz'=>'Мета і цілі',
                ]);
        Galuz::create([
                    'name_galuz'=>'Інформативна',
                    'marking_galuz'=>'ІФО',
                    'color_galuz'=>'#4b717e',
                    'text_galuz'=>'Мета і цілі',
                ]);
        Galuz::create([
                    'name_galuz'=>'Соціальна і здоров`язбережна',
                    'marking_galuz'=>'СЗО',
                    'color_galuz'=>'#fae453',
                    'text_galuz'=>'Мета і цілі',
                ]);
        Galuz::create([
                    'name_galuz'=>'Фізкультурна',
                    'marking_galuz'=>'ФІО',
                    'color_galuz'=>'#7da7d9',
                    'text_galuz'=>'Мета і цілі',
                ]);
        Galuz::create([
                    'name_galuz'=>'Громадянська та історична',
                    'marking_galuz'=>'ГІО',
                    'color_galuz'=>'#67a1a5',
                    'text_galuz'=>'Мета і цілі',
                ]);
        Galuz::create([
                    'name_galuz'=>'Мистецька',
                    'marking_galuz'=>'МИО',
                    'color_galuz'=>'#bd49c6',
                    'text_galuz'=>'Мета і цілі',
                ]);


    }
}
