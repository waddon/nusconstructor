<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::create([
                    'name_menu'=>'Top Line',
                ]);
        Menu::create([
                    'name_menu'=>'Toggle Navigation',
                ]);
    }
}
