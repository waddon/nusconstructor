<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
                    'id_galuz'=>1,
                    'name_subject'=>'Українська мова',
                ]);
        Subject::create([
                    'id_galuz'=>1,
                    'name_subject'=>'Українська література',
                ]);
        Subject::create([
                    'id_galuz'=>2,
                    'name_subject'=>'Російська мова',
                ]);
        Subject::create([
                    'id_galuz'=>2,
                    'name_subject'=>'Угорська мова',
                ]);
        Subject::create([
                    'id_galuz'=>3,
                    'name_subject'=>'Математика',
                ]);
        Subject::create([
                    'id_galuz'=>3,
                    'name_subject'=>'Алгебра',
                ]);
        Subject::create([
                    'id_galuz'=>3,
                    'name_subject'=>'Геометрія',
                ]);
        Subject::create([
                    'id_galuz'=>4,
                    'name_subject'=>'Природознавство',
                ]);
        Subject::create([
                    'id_galuz'=>5,
                    'name_subject'=>'Технології',
                ]);
        Subject::create([
                    'id_galuz'=>6,
                    'name_subject'=>'Інформатика',
                ]);
        Subject::create([
                    'id_galuz'=>7,
                    'name_subject'=>'БЖД',
                ]);
        Subject::create([
                    'id_galuz'=>8,
                    'name_subject'=>'Фізкультура',
                ]);
        Subject::create([
                    'id_galuz'=>9,
                    'name_subject'=>'Історія',
                ]);
        Subject::create([
                    'id_galuz'=>10,
                    'name_subject'=>'Малювання',
                ]);
        Subject::create([
                    'id_galuz'=>10,
                    'name_subject'=>'Спів',
                ]);
    }
}
