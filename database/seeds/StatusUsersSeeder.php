<?php

use Illuminate\Database\Seeder;
use App\StatusUser;

class StatusUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusUser::create([
                    'id_statususer'=>1,
                    'name_statususer'=>'Діючий',
                ]);
        StatusUser::create([
                    'id_statususer'=>2,
                    'name_statususer'=>'Подано заявку',
                ]);
        StatusUser::create([
                    'id_statususer'=>3,
                    'name_statususer'=>'Відмінено або виключено',
                ]);
    }
}
