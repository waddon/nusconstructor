<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // $this->call(SchoolsSeeder::class);

        $this->call(ProgramSeeder::class);
        $this->call(PtypeStypeSeeder::class);
        $this->call(StatusesSeeder::class);
        $this->call(PlanSeeder::class);
        $this->call(CreateSeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(ZorsSeeder::class);
        $this->call(KorsSeeder::class);
        $this->call(ZmistsSeeder::class);
        $this->call(SchoolUsersSeeder::class);
        $this->call(StatusUsersSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(StandartsSeeder::class);
        $this->call(SchoolKorsSeeder::class);
        $this->call(UmeniesSeeder::class);
        $this->call(NotesSeeder::class);
        $this->call(GaluznotesSeeder::class);
        $this->call(ToolsSeeder::class);
        $this->call(SubjectsSeeder::class);
        $this->call(PackagesSeeder::class);
        $this->call(SchooltypesSeeder::class);
        $this->call(MenusSeeder::class);
        $this->call(MenuItemsSeeder::class);

    }
}
