<?php

use Illuminate\Database\Seeder;
use App\SchoolUser;

class SchoolUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolUser::create([
                    'id_school'=>101,
                    'id_user'=>1,
                    'id_statususer'=>1
                ]);
        SchoolUser::create([
                    'id_school'=>101,
                    'id_user'=>2,
                    'id_statususer'=>1
                ]);
        SchoolUser::create([
                    'id_school'=>101,
                    'id_user'=>4,
                    'id_statususer'=>1
                ]);
        SchoolUser::create([
                    'id_school'=>101,
                    'id_user'=>5,
                    'id_statususer'=>2
                ]);
    }
}
