<?php

use Illuminate\Database\Seeder;
use App\MenuItem;

class MenuItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MenuItem::create([
                    'id_menu'=>1,
                    'name_menuitem'=>'<i class="fa fa-files-o" aria-hidden="true"></i>',
                    'url_menuitem'=>'home',
                    'sort_menuitem'=>1,
                ]);
        MenuItem::create([
                    'id_menu'=>1,
                    'name_menuitem'=>'<i class="fa fa-folder" aria-hidden="true"></i>',
                    'url_menuitem'=>'user-programs-list',
                    'paramurl_menuitem'=>'user',
                    'sort_menuitem'=>2,
                ]);
        MenuItem::create([
                    'id_menu'=>1,
                    'name_menuitem'=>'<i class="fa fa-pie-chart" aria-hidden="true"></i>',
                    'url_menuitem'=>'#',
                    'sort_menuitem'=>3,
                ]);
        MenuItem::create([
                    'id_menu'=>1,
                    'name_menuitem'=>'<i class="fa fa-bell" aria-hidden="true"></i>',
                    'url_menuitem'=>'school-package-list',
                    'sort_menuitem'=>4,
                ]);
        MenuItem::create([
                    'id_menu'=>1,
                    'name_menuitem'=>'<i class="fa fa-user" aria-hidden="true"></i>',
                    'url_menuitem'=>'school-users',
                    'sort_menuitem'=>5,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'name_menuitem'=>'Головна панель',
                    'url_menuitem'=>'home',
                    'icon_menuitem'=>'general',
                    'sort_menuitem'=>6,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'name_menuitem'=>'Глосарій',
                    'url_menuitem'=>'glossary',
                    'icon_menuitem'=>'glossary',
                    'sort_menuitem'=>7,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'name_menuitem'=>'Новини',
                    'url_menuitem'=>'news',
                    'icon_menuitem'=>'news',
                    'sort_menuitem'=>8,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'name_menuitem'=>'СТАНДАРТ',
                    'url_menuitem'=>'#',
                    'sort_menuitem'=>9,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'name_menuitem'=>'ОСВІТНІ ПРОГРАМИ',
                    'url_menuitem'=>'#',
                    'sort_menuitem'=>10,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'name_menuitem'=>'ЗАКЛАД ОСВІТТИ',
                    'url_menuitem'=>'#',
                    'sort_menuitem'=>11,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'name_menuitem'=>'КОНСТРУКТОР',
                    'url_menuitem'=>'#',
                    'sort_menuitem'=>12,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'name_menuitem'=>'ДОКУМЕНТИ',
                    'url_menuitem'=>'#',
                    'sort_menuitem'=>13,
                ]);

        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>9,
                    'name_menuitem'=>'Опис',
                    'url_menuitem'=>'all',
                    'paramurl_menuitem'=>'description',
                    'icon_menuitem'=>'description',
                    'sort_menuitem'=>14,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>9,
                    'name_menuitem'=>'Рівні',
                    'url_menuitem'=>'rivens',
                    'icon_menuitem'=>'rivens',
                    'sort_menuitem'=>15,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>9,
                    'name_menuitem'=>'Галузі',
                    'url_menuitem'=>'galuzes',
                    'icon_menuitem'=>'galuzes',
                    'sort_menuitem'=>16,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>9,
                    'name_menuitem'=>'Обовязкові результати навчання',
                    'url_menuitem'=>'zors',
                    'icon_menuitem'=>'zors',
                    'sort_menuitem'=>17,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>9,
                    'name_menuitem'=>'Базові навчальні плани',
                    'url_menuitem'=>'all',
                    'paramurl_menuitem'=>'basicplans',
                    'icon_menuitem'=>'basic-plans',
                    'sort_menuitem'=>18,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>9,
                    'name_menuitem'=>'Оцінювання',
                    'url_menuitem'=>'all',
                    'paramurl_menuitem'=>'appraisal',
                    'icon_menuitem'=>'appraisal',
                    'sort_menuitem'=>19,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>9,
                    'name_menuitem'=>'Матеріали',
                    'url_menuitem'=>'materials',
                    'icon_menuitem'=>'materials',
                    'sort_menuitem'=>20,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>10,
                    'name_menuitem'=>'Пояснювальна записка',
                    'url_menuitem'=>'all',
                    'paramurl_menuitem'=>'note',
                    'icon_menuitem'=>'explanatory',
                    'sort_menuitem'=>21,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>10,
                    'name_menuitem'=>'Очікувані результати навчання',
                    'url_menuitem'=>'kors',
                    'icon_menuitem'=>'kors',
                    'sort_menuitem'=>22,
                ]);

        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>11,
                    'name_menuitem'=>'Діючі документи',
                    'url_menuitem'=>'#',
                    'icon_menuitem'=>'active',
                    'sort_menuitem'=>23,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>11,
                    'name_menuitem'=>'Очікувані результати навчання закладу освіти',
                    'url_menuitem'=>'school-kors',
                    'icon_menuitem'=>'archive',
                    'sort_menuitem'=>24,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>23,
                    'name_menuitem'=>'Навчальні плани',
                    'url_menuitem'=>'user-plans-list',
                    'paramurl_menuitem'=>'active',
                    'icon_menuitem'=>'',
                    'sort_menuitem'=>25,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>23,
                    'name_menuitem'=>'Навчальні програми',
                    'url_menuitem'=>'user-programs-list',
                    'paramurl_menuitem'=>'active',
                    'icon_menuitem'=>'',
                    'sort_menuitem'=>26,
                ]);

        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>12,
                    'name_menuitem'=>'Навчальні плани',
                    'url_menuitem'=>'#',
                    'icon_menuitem'=>'plans',
                    'sort_menuitem'=>27,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>12,
                    'name_menuitem'=>'Навчальні програми',
                    'url_menuitem'=>'#',
                    'icon_menuitem'=>'programs',
                    'sort_menuitem'=>28,
                ]);

        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>27,
                    'name_menuitem'=>'Базові навчальні плани',
                    'url_menuitem'=>'user-plans-list',
                    'paramurl_menuitem'=>'basic',
                    'sort_menuitem'=>29,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>27,
                    'name_menuitem'=>'Типові навчальні плани',
                    'url_menuitem'=>'user-plans-list',
                    'paramurl_menuitem'=>'typical',
                    'sort_menuitem'=>30,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>27,
                    'name_menuitem'=>'Навчальні плани закладу освіти',
                    'url_menuitem'=>'user-plans-list',
                    'paramurl_menuitem'=>'school',
                    'sort_menuitem'=>31,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>28,
                    'name_menuitem'=>'Типові навчальні програми',
                    'url_menuitem'=>'user-programs-list',
                    'paramurl_menuitem'=>'typical',
                    'sort_menuitem'=>32,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>28,
                    'name_menuitem'=>'Навчальні програми закладу освіти',
                    'url_menuitem'=>'user-programs-list',
                    'paramurl_menuitem'=>'school',
                    'sort_menuitem'=>33,
                ]);


        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>13,
                    'name_menuitem'=>'Закони',
                    'url_menuitem'=>'laws',
                    'icon_menuitem'=>'laws',
                    'sort_menuitem'=>34,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>13,
                    'name_menuitem'=>'Положення',
                    'url_menuitem'=>'regulations',
                    'icon_menuitem'=>'regulations',
                    'sort_menuitem'=>35,
                ]);
        MenuItem::create([
                    'id_menu'=>2,
                    'parent_menuitem'=>13,
                    'name_menuitem'=>'Методичні рекомендації',
                    'url_menuitem'=>'guidelines',
                    'icon_menuitem'=>'guidelines',
                    'sort_menuitem'=>36,
                ]);
    }
}
