<?php

use Illuminate\Database\Seeder;
use App\Ptype;
use App\Stype;

class PtypeStypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ptype::create([
                    'id_ptype'=>1,
                    'name_ptype'=>'Базовий',
                ]);
        Ptype::create([
                    'id_ptype'=>2,
                    'name_ptype'=>'Типовий',
                ]);
        Ptype::create([
                    'id_ptype'=>3,
                    'name_ptype'=>'Закладу освіти',
                ]);
        Ptype::create([
                    'id_ptype'=>4,
                    'name_ptype'=>'Користувацький',
                ]);
        Stype::create([
                    'id_stype'=>1,
                    'name_stype'=>'Предмет',
                ]);
        Stype::create([
                    'id_stype'=>2,
                    'name_stype'=>'Інтегрований предмет',
                ]);
    }
}
