<?php

use Illuminate\Database\Seeder;
use App\Umenie;

class UmeniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Umenie::create([
                    'key_zor'=>'1.1',
                    'name_umenie'=>'Сприймаю усну інформацію',
                ]);
        Umenie::create([
                    'key_zor'=>'1.2',
                    'name_umenie'=>'Перетворюю усну інформацію',
                ]);
        Umenie::create([
                    'key_zor'=>'1.3',
                    'name_umenie'=>'Виокремлюю інформацію',
                ]);
        Umenie::create([
                    'key_zor'=>'1.4',
                    'name_umenie'=>'Аналізую та інтерпретую усну інформацію',
                ]);
        Umenie::create([
                    'key_zor'=>'1.5',
                    'name_umenie'=>'Оцінюю усну інформацію',
                ]);
        Umenie::create([
                    'key_zor'=>'1.6',
                    'name_umenie'=>'Висловлюю і захищаю власні погляди',
                ]);
        Umenie::create([
                    'key_zor'=>'1.7',
                    'name_umenie'=>'Використовую словесні й несловесні засоби під час представлення власних думок',
                ]);
        Umenie::create([
                    'key_zor'=>'1.8',
                    'name_umenie'=>'Регулюю свій емоційний стан',
                ]);
        Umenie::create([
                    'key_zor'=>'2.1',
                    'name_umenie'=>'Обираю тексти для читання',
                ]);
        Umenie::create([
                    'key_zor'=>'2.2',
                    'name_umenie'=>'Сприймаю текст',
                ]);
        /* математика */
        Umenie::create([
                    'id_galuz'=>3,
                    'key_zor'=>'1.1',
                    'name_umenie'=>'Розпізнаю серед життєвих ситуацій ті, які розвязуються математичним шляхом',
                ]);
        Umenie::create([
                    'id_galuz'=>3,
                    'key_zor'=>'1.2',
                    'name_umenie'=>'Аналізую, оцінюю, дані та звязки між ними для розвязання математичних ситуацій',
                ]);
        Umenie::create([
                    'id_galuz'=>3,
                    'key_zor'=>'1.3',
                    'name_umenie'=>'Проводжу найпростіші логічні операції: розуміння понять, порівняння, узагальнення, класифікація, доказ',
                ]);
    }
}
