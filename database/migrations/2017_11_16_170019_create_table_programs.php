<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrograms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->bigIncrements('id_program');
            $table->string('name_program')->default('');
            $table->bigInteger('id_school')->default(0);
            $table->bigInteger('id_user')->default(0);
            $table->bigInteger('id_ptype')->default(0);
            $table->bigInteger('id_plan')->default(0);
            $table->bigInteger('id_subject')->default(0);
            $table->bigInteger('id_stype')->default(1);
            $table->bigInteger('id_cikl')->default(1);
            $table->bigInteger('id_create')->default(1);
            $table->bigInteger('id_status')->default(1);
            $table->double('hours_program',8,2)->default(0);
            $table->text('content_program');
            $table->text('users_program');
            $table->text('galuzes_program');
            $table->timestamps();
            $table->timestamp('blocked_at')->nullable();
            $table->string('session_program')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
