<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('');
            $table->string('second_name')->default('');
            $table->string('patronymic')->default('');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('thumb')->default('');
            $table->bigInteger('id_role')->default('1');
            $table->bigInteger('id_region');
            $table->bigInteger('id_area');
            $table->bigInteger('id_locality');
            $table->bigInteger('id_school')->default('0');
            $table->bigInteger('id_position');
            $table->string('phone', 12)->default('');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_users');
    }
}
