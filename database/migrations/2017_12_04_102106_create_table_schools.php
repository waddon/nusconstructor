<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('id_school');
            $table->string('name_school',255)->default('');
            $table->string('region_school')->default('');
            $table->string('adress_school',255)->default('');
            $table->bigInteger('supervisor_school')->default('0');
            $table->bigInteger('id_statusschool')->default('1');
            $table->bigInteger('id_schooltype')->default('1');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
