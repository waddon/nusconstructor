<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSchoolUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schoolusers', function (Blueprint $table) {
            $table->bigIncrements('id_schooluser');
            $table->bigInteger('id_school')->default('1');
            $table->bigInteger('id_user')->default('1');
            $table->bigInteger('id_statususer')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schoolusers');
    }
}
