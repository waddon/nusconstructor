<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteColumnsFromUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('table_users', function (Blueprint $table) {
            $table->dropColumn(['id_region', 'id_area', 'id_locality']);
            $table->bigInteger('id_position')->default('0')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('table_users', function (Blueprint $table) {
            $table->bigInteger('id_region');
            $table->bigInteger('id_area');
            $table->bigInteger('id_locality');
            $table->bigInteger('id_position')->change();
        });
    }
}
