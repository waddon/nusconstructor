<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSchoolkors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schoolkors', function (Blueprint $table) {
            $table->bigIncrements('id_schoolkor');
            $table->bigInteger('id_school')->default('1');
            $table->bigInteger('id_galuz')->default('1');
            $table->string('key_schoolkor',255)->default('');
            $table->string('name_schoolkor',255)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schoolkors');
    }
}
