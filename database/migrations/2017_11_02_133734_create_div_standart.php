<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivStandart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rivens', function (Blueprint $table) {
            $table->bigIncrements('id_riven');
            $table->string('name_riven')->default('');
        });
        Schema::create('cikls', function (Blueprint $table) {
            $table->bigIncrements('id_cikl');
            $table->bigInteger('id_riven')->default('1');
            $table->string('name_cikl')->default('');
            $table->string('marking_cikl')->default('');
        });
        Schema::create('classes', function (Blueprint $table) {
            $table->bigIncrements('id_class');
            $table->bigInteger('id_cikl')->default('1');
            $table->string('name_class')->default('');
        });
        Schema::create('galuzes', function (Blueprint $table) {
            $table->bigIncrements('id_galuz');
            $table->string('name_galuz')->default('');
            $table->string('marking_galuz')->default('');
            $table->string('color_galuz')->default('');
            $table->text('text_galuz');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rivens');
        Schema::dropIfExists('cikls');
        Schema::dropIfExists('classes');
        Schema::dropIfExists('galuzes');
    }
}
