<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackadgeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id_package');
            $table->string('name_package',255)->default('');
            $table->bigInteger('id_school')->default('0');
            $table->bigInteger('id_status')->default('1');
            $table->bigInteger('approve_user')->default('0');
            $table->bigInteger('plancount_package')->default('0');
            $table->text('plans_package');
            $table->text('vam_package');
            $table->text('critical_package');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
