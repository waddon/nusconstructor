<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id_page');
            $table->string('name_page');
            $table->string('slug_page');
            $table->string('layout_page')->default('');
            $table->longText('content_page');
            $table->timestamps();
        });

        Schema::create('cats', function (Blueprint $table) {
            $table->bigIncrements('id_cat');
            $table->string('name_cat');
            $table->string('slug_cat');
        });

        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id_post');
            $table->string('name_post');
            $table->string('slug_post');
            $table->string('thumb_post')->default('');
            $table->bigInteger('id_cat')->default('1');
            $table->string('layout_post')->default('');
            $table->string('link_post')->default('');
            $table->boolean('download_post')->default(false);
            $table->text('excerpt_post');
            $table->longText('content_post');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
        Schema::dropIfExists('cats');
        Schema::dropIfExists('posts');
    }
}
