<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creates', function (Blueprint $table) {
            $table->bigIncrements('id_create');
            $table->string('name_create')->default('');
        });
        Schema::create('statuses', function (Blueprint $table) {
            $table->bigIncrements('id_status');
            $table->string('name_status')->default('');
        });
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id_plan');
            $table->string('name_plan')->default('');    // Название
            $table->bigInteger('id_parent')->default(0); // Базовый план с которым проверять
            $table->bigInteger('id_cikl')->default(0);   // Цикл
            $table->bigInteger('id_ptype')->default(0);  // Тип плана (базовый, типичный, группы или пользовательский)
            $table->bigInteger('id_school')->default(0); // Идентификатор группы
            $table->bigInteger('id_user')->default(0);   // Идентификатор пользователя
            $table->bigInteger('id_status')->default(1); // Текущий статус плана (редактируемый, на согласовании, утвержденный, архив и т.д.)
            $table->bigInteger('id_create')->default(0); // Тип создание (Креатор или Модификатор) - от этого зависят возможности редактирования
            $table->bigInteger('id_check')->default(0);  // Проверка плана
            $table->text('content_plan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creates');
        Schema::dropIfExists('statuses');
        Schema::dropIfExists('plans');
    }
}
