<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuitems', function (Blueprint $table) {
            $table->bigIncrements('id_menuitem');
            $table->bigInteger('id_menu')->default('1');
            $table->bigInteger('parent_menuitem')->default('0');
            $table->string('name_menuitem',255)->default('');
            $table->string('url_menuitem',255)->default('');
            $table->string('paramurl_menuitem',255)->default('');
            $table->bigInteger('sort_menuitem')->default('0');
            $table->string('icon_menuitem',255)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menuitems');
    }
}
