<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGaluznotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galuznotes', function (Blueprint $table) {
            $table->bigIncrements('id_galuznote');
            $table->bigInteger('id_galuz')->default('1');
            $table->bigInteger('id_cikl')->default('1');
            $table->text('text_galuznote');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galuznotes');
    }
}
