<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableZmists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zmists', function (Blueprint $table) {
            $table->bigIncrements('id_zmist');
            $table->bigInteger('id_galuz')->default('1');
            $table->string('name_zmist')->default('');
            $table->string('marking_zmist')->default('');
            $table->text('text_zmist');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zmists');
    }
}
