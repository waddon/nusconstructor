<?php
    function ukr_date($string=''){
        $string = str_replace('Sunday', 'Неділля', $string);
        $string = str_replace('Monday', 'Понеділок', $string);
        $string = str_replace('Tuesday', 'Вівторок', $string);
        $string = str_replace('Wednesday', 'Середа', $string);
        $string = str_replace('Thursday', 'Четвер', $string);
        $string = str_replace('Friday', 'П`ятниця', $string);
        $string = str_replace('Sunday', 'Суббота', $string);

        $string = str_replace('January', 'січня', $string);
        $string = str_replace('February', 'лютого', $string);
        $string = str_replace('March', 'березня', $string);
        $string = str_replace('April', 'квітня', $string);
        $string = str_replace('May', 'травня', $string);
        $string = str_replace('June', 'червня', $string);
        $string = str_replace('July', 'липня', $string);
        $string = str_replace('August', 'серпня', $string);
        $string = str_replace('September', 'вересня', $string);
        $string = str_replace('October', 'жовтня', $string);
        $string = str_replace('November', 'листопада', $string);
        $string = str_replace('December', 'грудня', $string);
        return $string;
    }
