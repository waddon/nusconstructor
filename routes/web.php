<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/* Admin area */
Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'middleware' => ['auth', 'canDashboard']], function() {
    Route::get('/', ['as' => 'dashboard', 'uses' => 'AdminController@index']);
    Route::group(['middleware' => ['permission:edit_users']], function () {
        Route::resource('users', 'UserController');
        Route::resource('roles', 'RoleController', ['except' => 'show']);
        Route::resource('permissions', 'PermissionController', ['except' => 'show']);
    });
    Route::group(['middleware' => ['permission:edit_menu']], function () {
        Route::resource('menus', 'MenuController', ['except' => 'show']);
        Route::resource('menuitems', 'MenuItemController', ['except' => 'show']);
    });
    Route::group(['middleware' => ['permission:edit_content']], function () {
        Route::resource('posts', 'PostController', ['except' => 'show']);
        Route::resource('categories', 'CategoryController', ['except' => 'show']);
        Route::resource('posts', 'PostController', ['except' => 'show']);
        Route::resource('pages', 'PageController', ['except' => 'show']);
        Route::resource('levels', 'LevelController', ['except' => 'show']);
        Route::resource('cycles', 'CycleController', ['except' => 'show']);
        Route::resource('classes', 'SchoolClassController', ['except' => 'show']);
        Route::resource('notes', 'NoteController', ['except' => 'show']);
        Route::resource('schools', 'SchoolController', ['except' => 'show']);
        Route::post('schools/filter', ['as' => 'schools.filter', 'uses' => 'SchoolController@filter']);
        Route::resource('schooltypes', 'SchoolTypeController', ['except' => 'show']);
        Route::resource('creates', 'CreateController', ['except' => 'show']);
        Route::resource('tools', 'ToolController', ['except' => 'show']);
        Route::resource('statuses', 'StatusController', ['except' => 'show']);
        Route::resource('ptypes', 'PTypeController', ['except' => 'show']);
        Route::resource('stypes', 'STypeController', ['except' => 'show']);
    });
    Route::group(['middleware' => ['permission:edit_branch']], function () {
        Route::resource('branches', 'BranchController', ['except' => 'show']);
        Route::resource('subjects', 'SubjectController', ['except' => 'show']);
        Route::resource('expected-outcomes', 'ExpectedOutcomeController', ['except' => 'show']);
        Route::resource('content-lines', 'ContentLineController', ['except' => 'show']);
        Route::resource('specific-outcomes', 'SpecificOutcomeController', ['except' => 'show']);
        Route::resource('skills', 'SkillController', ['except' => 'show']);
        Route::resource('branchnotes', 'BranchNoteController', ['except' => 'show']);
    });
    Route::get('approve', ['as' => 'approve', 'uses' => 'ApproveController@index']);
    Route::post('approve/filter', ['as' => 'approve.filter', 'uses' => 'ApproveController@filter']);
    Route::post('approve/package_approve', ['as' => 'approve.package_approve', 'uses' => 'ApproveController@package_approve']);
});

Route::get('/',    ['as'=>'home', 'uses'=>'GeneralController@index']);
Route::get('/404', ['as'=>'notfound', 'uses'=>'GeneralController@pagenotfound']);
Route::get('/401', ['as'=>'unauthorized', 'uses'=>'GeneralController@unauthorized']);

Route::get('/search', ['as'=>'search', 'uses'=>'SearchController@index']);

/* Глоссарий и новости и прочие посты */
Route::get('/glossary/{later?}', ['as'=>'glossary', 'uses'=>'GeneralController@glossary']);
Route::get('/news', ['as'=>'news', 'uses'=>'GeneralController@news']);
Route::get('/materials', ['as'=>'materials', 'uses'=>'GeneralController@materials']);
Route::get('/laws', ['as'=>'laws', 'uses'=>'GeneralController@laws']);
Route::get('/regulations', ['as'=>'regulations', 'uses'=>'GeneralController@regulations']);
Route::get('/guidelines', ['as'=>'guidelines', 'uses'=>'GeneralController@guidelines']);



/* Стандарт */
Route::get('/rivens',   ['as'=>'rivens', 'uses'=>'GeneralController@rivens']);
Route::get('/galuzes',  ['as'=>'galuzes', 'uses'=>'GeneralController@galuzes']);
Route::get('/zors',     ['as'=>'zors', 'uses'=>'GeneralController@zors']);
Route::get('/get_zors', ['as'=>'get_zors', 'uses'=>'GeneralController@get_zors']); /* ajax */
Route::get('/kors',     ['as'=>'kors', 'uses'=>'GeneralController@kors']);
Route::get('/get_kors', ['as'=>'get_kors', 'uses'=>'GeneralController@get_kors']); /* ajax */
Route::get('/get_post', ['as'=>'get_post', 'uses'=>'PostController@get_post']);   /* ajax */
Route::get('/note',     ['as'=>'note', 'uses'=>'GeneralController@note']);
Route::get('/get_note', ['as'=>'get_note', 'uses'=>'GeneralController@get_note']);   /* ajax */
Route::post('/get_posts',['as'=>'get_posts', 'uses'=>'SearchController@get_posts']);   /* ajax */


/* Школа */
Route::get('/school/users',    ['as'=>'school-users', 'uses'=>'SchoolController@schoolusers', 'middleware'=>'auth']);
Route::get('/school/users-status',    ['as'=>'school-users-status', 'uses'=>'SchoolController@schoolusersstatus', 'middleware'=>'auth']);
Route::get('/school/kors',    ['as'=>'school-kors', 'uses'=>'SchoolController@kors', 'middleware'=>'auth']);
Route::post('/school/kors-delete',    ['as'=>'school-kors-delete', 'uses'=>'SchoolController@korsdelete', 'middleware'=>'auth']);
Route::post('/school/kors-save',    ['as'=>'school-kors-save', 'uses'=>'SchoolController@korssave', 'middleware'=>'auth']);

Route::get('/school/package', ['as'=>'school-package-list', 'uses'=>'SchoolController@package_list', 'middleware'=>'auth']);
Route::post('/school/package-save',    ['as'=>'school-package-save', 'uses'=>'SchoolController@package_save', 'middleware'=>'auth']);
Route::post('/school/package-get',     ['as'=>'school-package-get',  'uses'=>'SchoolController@package_get',  'middleware'=>'auth']);
Route::post('/school/package-delete',  ['as'=>'school-package-delete', 'uses'=>'SchoolController@package_delete', 'middleware'=>'auth']);
Route::post('/school/package-approve', ['as'=>'school-package-approve', 'uses'=>'SchoolController@package_approve', 'middleware'=>'auth']);


/* Конструктор */
Route::get( '/plans/list/{id_list?}', ['as'=>'user-plans-list', 'uses'=>'PlanController@plans_list','middleware'=>'plansmiddle']);
Route::get( '/plans/view/{id_plan?}', ['as'=>'user-plan-view', 'uses'=>'PlanController@plan_view','middleware'=>'planmiddle']);
Route::get( '/plans/edit/{id_plan?}', ['as'=>'user-plans-edit', 'uses'=>'PlanController@plans_edit','middleware'=>'planeditmiddle']);
Route::post('/plans/save',            ['as'=>'user-plans-save', 'uses'=>'PlanController@plans_save']);
Route::get( '/plans/delete',          ['as'=>'user-plans-delete', 'uses'=>'PlanController@plans_delete']);
Route::get( '/plans/check',           ['as'=>'user-plans-check', 'uses'=>'PlanController@plans_check']);
Route::get ('/plans/get-all',         ['as'=>'user-plans-get-all', 'uses'=>'PlanController@plans_get_all']);  /* ajax */


Route::get( '/plans/new-from-basic',  ['as'=>'user-plans-new-from-basic', 'uses'=>'PlanController@new_plan_from_basic']); /* ajax */
Route::get( '/plans/new-from-typical',['as'=>'user-plans-new-from-typical', 'uses'=>'PlanController@new_plan_from_typical']); /* ajax */

Route::post( '/plans/perevirka',       ['as'=>'user-plans-perevirka', 'uses'=>'PlanController@plans_perevirka']);

Route::post('/plans/popup',           ['as'=>'user-plans-popup', 'uses'=>'PlanController@plan_popup']);




Route::get( '/programs/list/{id_list?}',    ['as'=>'user-programs-list', 'uses'=>'ProgramController@programs_list','middleware'=>'plansmiddle']);
Route::get( '/programs/edit/{id_program?}', ['as'=>'user-program-edit', 'uses'=>'ProgramController@program_edit','middleware'=>'programeditmiddle']);
Route::get( '/programs/view/{id_program?}', ['as'=>'user-program-view', 'uses'=>'ProgramController@program_view','middleware'=>'programmiddle']);
Route::post('/programs/save',               ['as'=>'user-program-save', 'uses'=>'ProgramController@program_save','middleware'=>'programmiddle']);
Route::get( '/programs/free',               ['as'=>'user-program-free', 'uses'=>'ProgramController@program_free','middleware'=>'programmiddle']);
Route::get( '/programs/change',             ['as'=>'user-program-change-editor', 'uses'=>'ProgramController@program_change_editor','middleware'=>'programmiddle']);
Route::get( '/programs/is_open',            ['as'=>'user-program-is-open', 'uses'=>'ProgramController@program_is_open','middleware'=>'programmiddle']);
Route::post('/programs/save-users', ['as'=>'user-program-save-users', 'uses'=>'ProgramController@program_save_users','middleware'=>'programmiddle']);
Route::post('/programs/create-new',         ['as'=>'user-program-create-new', 'uses'=>'ProgramController@program_create_new']);
Route::post('/programs/delete',             ['as'=>'user-program-delete', 'uses'=>'ProgramController@program_delete']);
Route::get ('/programs/get-all',            ['as'=>'user-program-get-all', 'uses'=>'ProgramController@program_get_all']);  /* ajax */
Route::post('/programs/new-from-typical',   ['as'=>'user-programs-new-from-typical', 'uses'=>'ProgramController@new_program_from_typical', 'middleware'=>'auth']); /* ajax */
Route::post('/programs/popup',              ['as'=>'user-programs-popup', 'uses'=>'ProgramController@program_popup']);

/* User */

Route::get( '/user/profile',            ['as'=>'user-profile', 'uses'=>'UserController@profile', 'middleware'=>'auth']);
Route::post('/user/profile/save',       ['as'=>'user-profile-save', 'uses'=>'UserController@profile_save', 'middleware'=>'auth']);
Route::get( '/user/profile/get-schools',['as'=>'user-profile-get-schools', 'uses'=>'UserController@get_schools']);
Route::post('/user/profile/add-entry',  ['as'=>'user-profile-add-entry', 'uses'=>'UserController@add_entry', 'middleware'=>'auth']);

Route::get('/{parametr1?}/{parametr2?}', ['as'=>'all', 'uses'=>'GeneralController@all']);