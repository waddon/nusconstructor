<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Riven as Level;

class Cikl extends Model
{
    protected $table = 'cikls';
    public $primaryKey  = 'id_cikl';
    public $timestamps = FALSE;

    public function getCycleLevel()
    {
        $level = Level::find($this->id_riven);

        return $level->name_riven;
    }
}