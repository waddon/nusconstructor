<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolUser extends Model
{
    protected $table = 'schoolusers';
    public $primaryKey  = 'id_schooluser';    
    public $timestamps = FALSE;
}
