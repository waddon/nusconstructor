<?php

namespace App;

use App\SchoolUser;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'table_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'name', 'second_name', 'patronymic', 'id_role', 'id_position', 'phone', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    }


    public function hasNewNotifications()
    {
        $user = User::find($this->id);
        if( $user->hasRole('director') ) {
            $count = SchoolUser::where('id_school', $this->id_school)->where('id_statususer', 2)->count();
            return $count;
        } else {
            return false;
        }
    }
}