<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cikl as Cycle;

class SchoolClass extends Model
{
    protected $table = 'classes';
    public $primaryKey  = 'id_class';
    public $timestamps = FALSE;

    public function getClassCycle()
    {
    	$cycle = Cycle::find($this->id_cikl);

        return $cycle->name_cikl;
    }
}