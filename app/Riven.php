<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Riven extends Model
{
    protected $table = 'rivens';
    public $primaryKey  = 'id_riven';
    public $timestamps = FALSE;
}
