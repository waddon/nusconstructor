<?php

namespace App;

use App\Galuz as Branch;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';
    public $primaryKey  = 'id_subject';    
    public $timestamps = FALSE;

    public function getSubjectBranch()
    {
    	$branch = Branch::find($this->id_galuz);

    	return $branch;
    }
}
