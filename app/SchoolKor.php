<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolKor extends Model
{
    protected $table = 'schoolkors';
    public $primaryKey  = 'id_schoolkor';    
    public $timestamps = FALSE;
}
