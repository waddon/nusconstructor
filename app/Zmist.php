<?php

namespace App;

use App\Galuz as Branch;
use Illuminate\Database\Eloquent\Model;

class Zmist extends Model
{
    protected $table = 'zmists';
    public $primaryKey  = 'id_zmist';    
    public $timestamps = FALSE;

    public function getContentLineBranch()
    {
    	$branch = Branch::find($this->id_galuz);

    	return $branch;
    }
}
