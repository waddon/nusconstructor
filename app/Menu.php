<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Request;
use App\MenuItem;

class Menu extends Model
{
    protected $table = 'menus';
    protected $primaryKey = 'id_menu';
    public $timestamps = FALSE;

    public static function getMenu($attr=[])
    {
        $title=[1=>'Головна панель', 2=>'Документи користувача', 4=>'Формування пакетів документів',5=>'Користувачі навчального закладу'];
        $id_menu = isset($attr['id_menu']) ? (int)$attr['id_menu'] : 0;
        $class = isset($attr['class']) ? (string)$attr['class'] : '';
        $id = isset($attr['id']) ? (string)$attr['id'] : '';
        $menu = Menu::where('id_menu', $id_menu)->first();
        if ($menu) {
            $menuitems = MenuItem::where('id_menu', $id_menu)->orderBy('sort_menuitem', 'asc', 'id_menuitem', 'asc')->get();

            $result = '<ul class="' . $class . '" id="' . $id . '">';

            foreach ($menuitems as $key => $menuitem) {
                if ($menuitem->url_menuitem != '#'){
                    if ($menuitem->paramurl_menuitem) {
                        $route = route($menuitem->url_menuitem, $menuitem->paramurl_menuitem);
                    } else {
                        $route = route($menuitem->url_menuitem);
                    }
                    
                } else {
                    $route = '#';
                }

                if (Request::url() == $route ) {
                    $active = 'active';
                } else {
                    $active = '';
                }
                $title2 = isset($title[$menuitem->id_menuitem]) ? ' title="'.$title[$menuitem->id_menuitem].'"' : '';
                $result .= '<li class="' . $active . '" '.$title2.'><a href="' . $route . '">' . $menuitem->name_menuitem . '</a>';
                $result .= '</li>';
            }

            $result .= "</ul>";
        } else {
            $result = false;
        }

        return $result;
    }


    public static function getToggleMenu($attr=[])
    {
        $result = false;
        $obj_menu = (object)[];
        $arr_menu = [];
        $id_menu = isset($attr['id_menu']) ? (int)$attr['id_menu'] : 0;
        $class = isset($attr['class']) ? (string)$attr['class'] : '';
        $menu = Menu::where('id_menu', $id_menu)->first();        
        if ($menu) {
            $menuitems = MenuItem::where('id_menu', $id_menu)->where('parent_menuitem', 0)->orderBy('sort_menuitem', 'asc', 'id_menuitem', 'asc')->get();
            foreach ($menuitems as $menuitem) {
                if ($menuitem->url_menuitem != '#'){
                    if ($menuitem->paramurl_menuitem) {
                        $route = route($menuitem->url_menuitem, $menuitem->paramurl_menuitem);
                    } else {
                        $route = route($menuitem->url_menuitem);
                    }
                    
                } else {
                    $route = '#';
                }

                $obj_menu->{$menuitem->id_menuitem} = (object)['id_menuitem'=>$menuitem->id_menuitem, 'name_menuitem'=>$menuitem->name_menuitem, 'icon_menuitem'=>$menuitem->icon_menuitem, 'url_menuitem'=>$route, 'subcount'=>0, 'active'=>'', 'toggle_menuitem'=>false, 'sub'=>(object)[]];
                if (Request::url() == $route ) {$obj_menu->{$menuitem->id_menuitem}->active = 'active';}
                $arr_menu[] = $menuitem->id_menuitem;
            }

            /* Второй уровень */
                $menuitems = MenuItem::whereIn('parent_menuitem', $arr_menu )->orderBy('sort_menuitem', 'asc', 'id_menuitem', 'asc')->get();
                $arr_menu = [];
                foreach ($menuitems as $menuitem) {
                    if ($menuitem->url_menuitem != '#'){
                        if ($menuitem->paramurl_menuitem) {
                            $route = route($menuitem->url_menuitem, $menuitem->paramurl_menuitem);
                        } else {
                            $route = route($menuitem->url_menuitem);
                        }
                        
                    } else {
                        $route = '#';
                    }
                    $obj_menu->{$menuitem->parent_menuitem}->sub->{$menuitem->id_menuitem} = (object)['id_menuitem'=>$menuitem->id_menuitem, 'name_menuitem'=>$menuitem->name_menuitem, 'icon_menuitem'=>$menuitem->icon_menuitem, 'url_menuitem'=>$route, 'subcount'=>0, 'active'=>'', 'toggle_menuitem'=>false, 'sub'=>(object)[]];
                    if (Request::url() == $route ) {
                        $obj_menu->{$menuitem->parent_menuitem}->sub->{$menuitem->id_menuitem}->active = 'active';
                        $obj_menu->{$menuitem->parent_menuitem}->toggle_menuitem = true;
                    }
                    $obj_menu->{$menuitem->parent_menuitem}->subcount++;
                    $arr_menu[] = $menuitem->id_menuitem;
                }

            //dump($arr_menu);

            /* Третий уровень */
                $sql = "SELECT t1.*, t2.parent_menuitem AS grandparent_menuitem FROM menuitems AS t1 LEFT JOIN menuitems AS t2 ON t1.parent_menuitem = t2.id_menuitem WHERE t1.parent_menuitem IN (" . implode(",", $arr_menu) . ") ORDER BY t1.sort_menuitem, t1.id_menuitem";
                $menuitems = DB::select( $sql );
                foreach ($menuitems as $menuitem) {
                    if ($menuitem->url_menuitem != '#'){
                        if ($menuitem->paramurl_menuitem) {
                            $route = route($menuitem->url_menuitem, $menuitem->paramurl_menuitem);
                        } else {
                            $route = route($menuitem->url_menuitem);
                        }
                        
                    } else {
                        $route = '#';
                    }
                    $obj_menu->{$menuitem->grandparent_menuitem}->sub->{$menuitem->parent_menuitem}->sub->{$menuitem->id_menuitem} = (object)['id_menuitem'=>$menuitem->id_menuitem, 'name_menuitem'=>$menuitem->name_menuitem, 'icon_menuitem'=>$menuitem->icon_menuitem, 'url_menuitem'=>$route, 'subcount'=>0, 'active'=>'', 'toggle_menuitem'=>false, 'sub'=>(object)[]];

                    if (Request::url() == $route ) {
                        $obj_menu->{$menuitem->grandparent_menuitem}->sub->{$menuitem->parent_menuitem}->sub->{$menuitem->id_menuitem}->active = 'active';
                        $obj_menu->{$menuitem->grandparent_menuitem}->toggle_menuitem = true;
                    }
                    $obj_menu->{$menuitem->grandparent_menuitem}->sub->{$menuitem->parent_menuitem}->subcount++;

                }

            /* Отображение */

            $result = '<ul class="' . $class . '">';
            foreach ($obj_menu as $menuitem1) {
                if ($menuitem1->subcount == 0){
                    $result .= '<li class="sidebar-item-2 '.$menuitem1->active.'"><a href="'.$menuitem1->url_menuitem.'" ><span class="icon '.$menuitem1->icon_menuitem.'"></span><span class="name">'.$menuitem1->name_menuitem.'</span></a></li>';
                } else {
                    $result .= '<hr><div class="toggle-box"><span class="toggle-icon toggle-close"></span><div class="toggle-title">'.$menuitem1->name_menuitem.'</div><div class="toggle-content"';
                    $result .= !$menuitem1->toggle_menuitem ? 'style="display: none;"' : '';
                    $result .= '>';
                    foreach ($menuitem1->sub as $menuitem2) {
                        if ($menuitem2->subcount == 0) {
                            $result .= '<li class="sidebar-item-2 '.$menuitem2->active.'"><a href="'.$menuitem2->url_menuitem.'" ><span class="icon '.$menuitem2->icon_menuitem.'"></span><span class="name">'.$menuitem2->name_menuitem.'</span></a></li>';
                        } else {
                            $result .='<li><div class="fw"><span class="icon '.$menuitem2->icon_menuitem.'"></span><span class="name">'.$menuitem2->name_menuitem.'</span></div><ul class="submenu">';
                            foreach ($menuitem2->sub as $menuitem3) {
                                $result .='<li class="sidebar-item-2 '.$menuitem3->active.'"><a href="'.$menuitem3->url_menuitem.'"><span class="child"> </span><span class="name">'.$menuitem3->name_menuitem.'</span></a></li>';
                            }
                            $result .='</ul></li>';
                        }
                    }
                    $result .= '</div>';
                }
            }
            $result .= '</ul>';
            // dump($obj_menu);
        }

        return $result;
    }
}
