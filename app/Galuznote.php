<?php

namespace App;

use App\Cikl as Cycle;
use App\Galuz as Branch;
use Illuminate\Database\Eloquent\Model;

class Galuznote extends Model
{
    protected $table = 'galuznotes';
    public $primaryKey  = 'id_galuznote';    
    public $timestamps = FALSE;

    public function getBranchNoteCycle()
    {
        $cycle = Cycle::find($this->id_cikl);

        return $cycle->name_cikl;
    }

    public function getBranchNoteBranch()
    {
        $branch = Branch::find($this->id_galuz);

        return $branch;
    }
}
