<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusUser extends Model
{
    protected $table = 'statususers';
    public $primaryKey  = 'id_statususer';
    public $timestamps = FALSE;
}
