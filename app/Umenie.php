<?php

namespace App;

use App\Galuz as Branch;
use Illuminate\Database\Eloquent\Model;

class Umenie extends Model
{
    protected $table = 'umenies';
    public $primaryKey  = 'id_umenie';    
    public $timestamps = FALSE;

    public function getSkillBranch()
    {
    	$branch = Branch::find($this->id_galuz);

    	return $branch;
    }
}
