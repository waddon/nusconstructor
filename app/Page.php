<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //protected $table = 'pages';
    protected $primaryKey = 'id_page';
    public $incrementing = 'id_page';
    public $timestamps = TRUE;
}