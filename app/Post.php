<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    protected $primaryKey = 'id_post';
    public $incrementing = 'id_post';
    public $timestamps = TRUE;


    public static function search_posts($attr=[])
    {
        $string = isset($attr['string']) ? $attr['string'] : '';
        $category = isset($attr['category']) ? $attr['category'] : '[]';
        $limit = isset($attr['limit']) ? $attr['limit'] : 0;
        $offset = isset($attr['offset']) ? $attr['offset'] : 0;

        if ($string != ''){
            $sql = "SELECT * FROM posts WHERE (posts.name_post LIKE '%$string%' OR posts.excerpt_post LIKE '%$string%' OR posts.content_post LIKE '%$string%')";

            if (count($category)>0) {
                $sql .= " AND id_cat IN (" . implode(",", $category) . ")";
            }

            $sql .= " ORDER BY id_post DESC";

            if ($limit>0){
                $sql .= " LIMIT $limit";
            }
            if ($offset>0){
                $sql .= " OFFSET $offset";
            }

            $posts = DB::select( $sql );
            return $posts;
        } else {
            return false;
        }
    }

}