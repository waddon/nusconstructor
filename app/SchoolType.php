<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolType extends Model
{
    protected $table = 'schooltypes';
    public $primaryKey  = 'id_schooltype';
    public $timestamps = FALSE;

    public static function getSchoolTypes()
    {
    	$schooltypes = SchoolType::all()->pluck('name_schooltype', 'id_schooltype')->prepend('--', 0);

    	return $schooltypes;
    }
}
