<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ptype extends Model
{
    protected $table = 'ptypes';
    public $primaryKey  = 'id_ptype';    
    public $timestamps = FALSE;
}
