<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galuz extends Model
{
    protected $table = 'galuzes';
    public $primaryKey  = 'id_galuz';
    public $timestamps = FALSE;
}
