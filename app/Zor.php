<?php

namespace App;

use App\Cikl as Cycle;
use App\Galuz as Branch;
use Illuminate\Database\Eloquent\Model;

class Zor extends Model
{
    protected $table = 'zors';
    public $primaryKey  = 'id_zor';    
    public $timestamps = FALSE;

    public function getExpectedOutcomeBranch()
    {
        $branch = Branch::find($this->id_galuz);

        return $branch;
    }

    public function getExpectedOutcomeCycle()
    {
        $cycle = Cycle::find($this->id_cikl);

        return $cycle->name_cikl;
    }
}
