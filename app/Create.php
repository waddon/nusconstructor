<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Create extends Model
{
    protected $table = 'creates';
    public $primaryKey  = 'id_create';    
    public $timestamps = FALSE;
}
