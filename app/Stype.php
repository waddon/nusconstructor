<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stype extends Model
{
    protected $table = 'stypes';
    public $primaryKey  = 'id_stype';    
    public $timestamps = FALSE;
}
