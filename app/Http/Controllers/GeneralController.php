<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
//use App\Glossary;
use App\Galuz;
use App\Riven;
use App\Cikl;
use App\Zor;
use App\Kor;
use App\Zmist;
use App\SchoolClass;
use App\Page;
use App\Cat;
use App\Post;
use App\School;
use App\SchoolUser;
use App\Note;
use App\Galuznote;

class GeneralController extends MainController
{

    public $laters = ['А', 'Б', 'В', 'Г', 'Ґ', 'Д', 'Е', 'Є', 'Ж', 'З', 'И', 'І', 'Ї', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ю', 'Я'];


    // public function __construct()
    // {
    //     $this->data['topmenu'] = Menu::getMenu(['id_menu'=>1, 'id'=>'loginform-buttons']);
    // }


    public function index()
    {
        $this->data['menu'] = [];
        $this->data['menu'][1] = 'active';
        $this->data['page'] = Page::where( 'id_page', 1 )->first();
        $this->data['news'] = Post::join('cats', 'posts.id_cat', '=', 'cats.id_cat')->where('posts.id_cat', 1)->orderby('id_post', 'desc')->limit(5)->get();
        $this->data['news_title'] = 'Новини проекту';

        return view('page', $this->data);
    }



    public function glossary($later = '')
    {
        $this->data['menu'] = [];
        $this->data['menu'][2] = 'active';
        $later = $later!='' ? mb_strtoupper($later) : $this->laters[0];
        $this->data['later'] = $this->laters[0];
        $this->data['laters'] = $this->laters;
        foreach ($this->laters as $key => $value) {
            if ($later == $value) {$this->data['later'] = $value;}
        }

        $terms = Post::where('id_cat', 6)->where('name_post', 'like', $later . '%')->get();
        $this->data['terms'] = $terms;
        return view('glossary', $this->data);
    }



    public function news(){
        $this->data['menu'] = [];
        $this->data['menu'][3] = 'active';
        $news = Post::join('cats', 'posts.id_cat', '=', 'cats.id_cat')->where('posts.id_cat', 1)->orderby('id_post', 'desc')->limit(10)->get();

        $date_title = '';
        $result = '';
        $iterration = 0;

        $this->data['news'] = $news;
        foreach ($news as $new) {
            if ($date_title != $this->ukr_date(date("l d F Y", strtotime($new->created_at))) ){
                $iterration = $iterration != 3 ? $iterration + 1 : 1;
                $date_title = $this->ukr_date(date("l d F Y", strtotime($new->created_at)));
                $result .= '<div class="news-title color' . $iterration . '"><i class="fa fa-comments" aria-hidden="true"></i>' . $date_title . '</div>';

            }

            $result .= '<div class="news-item color' . $iterration . '" data-date="'. $this->ukr_date(date("l d F Y", strtotime($new->created_at))) .'" data-color="'.$iterration.'" data-id-post="'.$new->id_post.'">'.$new->name_post.'</div>';
        }

        $this->data['result'] = $result;
        return view('news', $this->data);
    }



    public function materials(){
        $this->data['menu'] = [];
        $this->data['menu'][17] = 'active';
        $this->data['toggle'] = 1;
        $materials = Post::join('cats', 'posts.id_cat', '=', 'cats.id_cat')->where('posts.id_cat', 2)->orderby('id_post', 'desc')->limit(10)->get();
        $this->data['materials'] = $materials;
        return view('materials', $this->data);
    }

    public function laws(){
        $this->data['menu'] = [];
        $this->data['menu'][51] = 'active';
        $this->data['toggle'] = 5;
        $this->data['title'] = 'Закони';
        $materials = Post::join('cats', 'posts.id_cat', '=', 'cats.id_cat')->where('posts.id_cat', 3)->orderby('id_post', 'desc')->limit(10)->get();
        $this->data['materials'] = $materials;
        return view('materials', $this->data);
    }

    public function regulations(){
        $this->data['menu'] = [];
        $this->data['menu'][52] = 'active';
        $this->data['toggle'] = 5;
        $this->data['title'] = 'Положення';
        $materials = Post::join('cats', 'posts.id_cat', '=', 'cats.id_cat')->where('posts.id_cat', 4)->orderby('id_post', 'desc')->limit(10)->get();
        $this->data['materials'] = $materials;
        return view('materials', $this->data);
    }
    public function guidelines(){
        $this->data['menu'] = [];
        $this->data['menu'][53] = 'active';
        $this->data['toggle'] = 5;
        $this->data['title'] = 'Методичні рекомендації';
        $materials = Post::join('cats', 'posts.id_cat', '=', 'cats.id_cat')->where('posts.id_cat', 5)->orderby('id_post', 'desc')->limit(10)->get();
        $this->data['materials'] = $materials;
        return view('materials', $this->data);
    }


    public function rivens(){
        $this->data['menu'] = [];
        $this->data['menu'][12] = 'active';
        $this->data['toggle'] = 1;
        $this->data['title'] = 'Рівні та цикли';
        //$this->data['rivens'] = Riven::leftJoin('cikls', 'rivens.id_riven', '=', 'cikls.id_riven')->leftJoin('classes', 'cikls.id_cikl', '=', 'classes.id_cikl')->get();
        $result = '';
        $rivens = Riven::get();
        $result .= '<table class="rivens"><tr><td><div class="level1">Загальноосвітній навчальний заклад</div><table class="rivens"><tr>';
        foreach ($rivens as $riven) {
            $result .= '<td><div class="level2">'.$riven->name_riven.'</div>';
            $cikls = Cikl::where('id_riven',$riven->id_riven)->get();
            if ($cikls) {
                $result .= '<table class="rivens"><tr>';
                foreach ($cikls as $cikl) {
                    $result .= '<td><div class="level3">'.$cikl->name_cikl.'</div>';
                    $classes = SchoolClass::where('id_cikl',$cikl->id_cikl)->get();
                    if ($classes) {
                        $result .= '<table class="rivens"><tr>';
                        foreach ($classes as $class) {
                            $result .= '<td><div class="level4">'.$class->name_class.'</div></td>';
                        }
                        $result .= '</tr></table>';
                    }
                    $result .= '</td>';
                }
                $result .= '</tr></table>';
            }
            $result .= '</td>';
        }
        $result .= '</tr></table></td></tr></table>';
        $this->data['result'] = $result;
        return view('standart.riven', $this->data);
    }



    public function galuzes(){
        $this->data['menu'] = [];
        $this->data['menu'][13] = 'active';
        $this->data['toggle'] = 1;
        $this->data['title'] = 'Галузі';
        $this->data['galuzes'] = Galuz::get();
        return view('standart.galuz', $this->data);
    }



    public function zors(){
        $this->data['menu'] = [];
        $this->data['menu'][14] = 'active';
        $this->data['toggle'] = 1;
        $this->data['title'] = 'Обов`язкові результати навчання';
        $this->data['galuzes'] = Galuz::get();
        $this->data['cikls'] = Cikl::get();
        $this->data['zors'] = Zor::where('zors.id_galuz',1)->where('id_cikl',1)->leftjoin('umenies', 'zors.key_zor', '=', 'umenies.key_zor')->where('umenies.id_galuz',1)->orderBy('umenies.key_zor', 'asc')->get();
        // dump($this->data['zors']);
        return view('standart.zor', $this->data);
    }



    public function get_zors(){
        $id_galuz = isset($_GET['id_galuz']) ? (int)$_GET['id_galuz'] : 1;
        $id_cikl = isset($_GET['id_cikl']) ? (int)$_GET['id_cikl'] : 1;
        $zors = Zor::where('zors.id_galuz', $id_galuz)->where('id_cikl', $id_cikl)->leftjoin('umenies', 'zors.key_zor', '=', 'umenies.key_zor')->where('umenies.id_galuz',$id_galuz)->orderBy('umenies.key_zor', 'asc')->get();
        return json_encode($zors);
    }



    public function note(){
        $this->data['menu'][21] = 'active';
        $this->data['toggle'] = 2;
        $this->data['cikls'] = Cikl::get();
        $this->data['note'] = Note::where('id_cikl',1)->first();
        return view('standart.note', $this->data);
    }

    public function get_note(){
        $id_cikl = isset($_GET['id_cikl']) ? (int)$_GET['id_cikl'] : 1;
        $note = Note::where('id_cikl',$id_cikl)->first();
        if ( !$note ) { $note = new Note; }
        return json_encode($note);
    }


    public function kors(){
        $this->data['menu'] = [];
        $this->data['menu'][22] = 'active';
        $this->data['toggle'] = 2;
        $this->data['title'] = 'Очікувані результати навчання';
        $this->data['galuzes'] = Galuz::get();
        $this->data['cikls'] = Cikl::get();
        $this->data['galuznote'] = Galuznote::where('id_galuz',1)->where('id_cikl',1)->first();
        $result = '';
        $zmists = Zmist::get();
        foreach ($zmists as $zmist) {
            $zors = Zor::select('zors.*')->distinct()->join('kors', 'zors.id_zor', '=', 'kors.id_zor')->where('id_galuz',1)->where('id_cikl',1)->where('kors.id_zmist',$zmist->id_zmist)->get();
            if ($zors->count()>0) {
                $result .= '<tr><td colspan="2" style="text-align:center;">Змістова лінія "' . $zmist->name_zmist . '"</td></tr>';
                foreach ($zors as $zor) {
                    $result .= '<tr>';
                    $result .= '<td>'.$zor->name_zor;
                    $kors = Kor::where('id_zor',$zor->id_zor)->where('id_zmist',$zmist->id_zmist)->get();
                    $result .= '<td>';
                    foreach ($kors as $kor) {
                        $result .= '- '.$kor->name_kor.' <span class="key">['.$kor->key_kor.']</span><br>';
                    }
                    $result .= '</td>';
                    $result .= '</td>';
                    $result .= '</tr>';
                }
                $result .= '<tr><td colspan="2">' . $zmist->text_zmist . '</td></tr>';
            }
        }
        $this->data['result'] = $result;
        return view('standart.kor', $this->data);
    }


    public function get_kors(){
        $res = [];
        $id_galuz = isset($_GET['id_galuz']) ? (int)$_GET['id_galuz'] : 1;
        $id_cikl = isset($_GET['id_cikl']) ? (int)$_GET['id_cikl'] : 1;
        $result = '';
        $zmists = Zmist::get();
        foreach ($zmists as $zmist) {
            $zors = Zor::select('zors.*')->distinct()->join('kors', 'zors.id_zor', '=', 'kors.id_zor')->where('id_galuz',$id_galuz)->where('id_cikl',$id_cikl)->where('kors.id_zmist',$zmist->id_zmist)->get();
            if ($zors->count()>0) {
                $result .= '<tr><td colspan="2" style="text-align:center;">Змістова лінія "' . $zmist->name_zmist . '"</td></tr>';
                foreach ($zors as $zor) {
                    $result .= '<tr>';
                    $result .= '<td>'.$zor->name_zor;
                    $kors = Kor::where('id_zor',$zor->id_zor)->where('id_zmist',$zmist->id_zmist)->get();
                    $result .= '<td>';
                    foreach ($kors as $kor) {
                        $result .= '- '.$kor->name_kor.' <span class="key">['.$kor->key_kor.']</span><br>';
                    }
                    $result .= '</td>';
                    $result .= '</td>';
                    $result .= '</tr>';
                }
                $result .= '<tr><td colspan="2">' . $zmist->text_zmist . '</td></tr>';
            }
        }
        $res['result'] = $result;
        $galuznote = Galuznote::where('id_galuz',$id_galuz)->where('id_cikl',$id_cikl)->first();
        if (!$galuznote) {$galuznote = new Galuznote;}
        $res['galuznote'] = $galuznote;

        return json_encode($res);
    }


    public function all($param1='', $param2='')
    {

        $this->data['title'] = 'Route All';

        if ( $param1 == 'description' ) {$this->data['menu'][11] = 'active';$this->data['toggle'] = 1;}
        if ( $param1 == 'appraisal' )   {$this->data['menu'][16] = 'active';$this->data['toggle'] = 1;}
        if ( $param1 == 'basicplans' )  {$this->data['menu'][15] = 'active';$this->data['toggle'] = 1;}

        if ( $param2=='' ) {
            $page = Page::where( 'slug_page', $param1 )->first();
            if ($page) {
                $this->data['page'] = $page;
                $layout = 'page';
            } else {
                $layout = 'errors.404';
            }
        } else {
            $layout = 'errors.404';
        }

        return view( $layout, $this->data);
    }



    public function pagenotfound()
    {
        return view('errors.404', $this->data);
    }

    public function unauthorized()
    {
        return view('errors.401', $this->data);
    }



}
