<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{

    protected $data = [];

    public function __construct(){
        //$this->data['config'] = ConfigModel::get();
        $this->middleware(function ($request, $next)
        {
            $this->data['user'] = Auth::user(); return $next($request);
        }
        );

    public function index()
    {
        echo "$ projects";
    }


}
