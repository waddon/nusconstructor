<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Page;
use App\Cat;
use App\Post;

class SearchController extends MainController
{
    public $string = '';
    public $ajaxcount = 4;


    public function index(Request $request){

        $posts_count = 0;
        $result = [];
        $result['ajaxcount'] = $this->ajaxcount;
        $this->string = $request->input('s') ? $request->input('s') : '';

        if ($this->string == '') {
            $result['message'] = 'Отсутствует строка поиска';
        } else {
            if (mb_strlen($this->string) < 3) {
                $result['message'] = 'Длина строки меньше 3-х символов';
            } else {
                /* поиск */

                $news = Post::search_posts([ "string" => $this->string , "category" => [1] ]);
                $news_visible = Post::search_posts([ "string" => $this->string , "category" => [1], "limit"=>$this->ajaxcount ]);

                if (count($news_visible) == count($news)){
                    $result['news_more_visible'] = false;
                } else {
                    $result['news_more_visible'] = true;
                }
                $this->data['news'] = $news_visible;
                $posts_count += count($news);



                $materials = Post::where('id_cat', 2)->
                        where(function ($query) {
                        $query->where('name_post', 'like', '%' . $this->string . '%')
                        ->orWhere('excerpt_post', 'like', '%' . $this->string . '%' )
                        ->orWhere('content_post', 'like', '%' . $this->string . '%');
                        })
                    ->orderBy('id_post', 'DESC')
                    ->get();

                $this->data['materials'] = $materials;
                $posts_count += count($materials);

                $laws = Post::whereIn('id_cat', [3, 4, 5])->
                        where(function ($query) {
                        $query->where('name_post', 'like', '%' . $this->string . '%')
                        ->orWhere('excerpt_post', 'like', '%' . $this->string . '%' )
                        ->orWhere('content_post', 'like', '%' . $this->string . '%');
                        })
                    ->orderBy('id_post', 'DESC')
                    ->get();

                $this->data['laws'] = $laws;
                $posts_count += count($laws);


                $glossary = Post::search_posts([ "string" => $this->string , "category" => [6] ]);
                $glossary_visible = Post::search_posts([ "string" => $this->string , "category" => [6], "limit"=>$this->ajaxcount ]);

                if (count($glossary_visible) == count($glossary)){
                    $result['glossary_more_visible'] = false;
                } else {
                    $result['glossary_more_visible'] = true;
                }
                $this->data['glossary'] = $glossary_visible;
                $posts_count += count($glossary);
            }
        }

        $this->data['result'] = (object)$result;
        $this->data['result_count'] = $posts_count;
        return view( 'search', $this->data);
    }



    public function get_posts(Request $request)
    {
        $result = [];
        $string = $request->input('string') ? $request->input('string') : '';
        $ajaxcount = $request->input('ajaxcount') ? (int)$request->input('ajaxcount') : $this->ajaxcount;
        $count = $request->input('count') ? (int)$request->input('count') : $this->ajaxcount;
        $category = $request->input('category') ? (array)$request->input('category') : [];
        //$category = $request->input('category') ? explode("_", $request->input('category')) : [];


        $all_posts = Post::search_posts([ "string" => $string , "category" => $category ]);
        $result['posts'] = Post::search_posts([ "string" => $string , "category" => $category, "limit" => $ajaxcount, "offset" => $count]);
        if (count($all_posts) <= count($result['posts']) + $count) {
            $result['more_visible'] = false;
        } else {
            $result['more_visible'] = true;
        }
        foreach ($result['posts'] as $value) {
            $value->date1 = ukr_date(date("d F Y", strtotime($value->created_at)));
        }
        $result['all_posts'] = count($all_posts);        
        $result['ajaxcount'] = $ajaxcount;
        $result['old_count'] = $count;
        $result['count'] = count($result['posts']) + $count;
        //$result['count'] = count($result['posts']);

        return json_encode($result);
    }

}


