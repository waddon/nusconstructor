<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\School;
use App\User;
use App\SchoolUser;
use App\StatusUser;
use App\SchoolKor;
use App\Galuz;
use App\Cikl;
use App\Package;
use App\Status;
use App\Plan;
use App\Program;

class SchoolController extends MainController
{


    public function index()
    {
        //
    }

    public function schoolusers()
    {

        $currentschool = auth()->user()->id_school;

        $this->data['school'] = School::where('id_school', $currentschool)->first();

        if ($this->data['school']){
            $supervisor = User::where('id', $this->data['school']->supervisor_school)->first();
            if ($supervisor) {
                $this->data['supervisor'] = $supervisor;
            } else {
                $this->data['supervisor'] = new User;
            }

            $this->data['schoolusers'] = SchoolUser::where('schoolusers.id_school', $currentschool)->leftJoin('table_users', 'schoolusers.id_user', '=', 'table_users.id')->leftJoin('statususers', 'schoolusers.id_statususer', '=', 'statususers.id_statususer')->get();        

        } else {
            $this->data['school'] = new School;
            $this->data['supervisor'] = new User;
        }

        $this->data['title'] = 'Користувачі навчального закладу';
        $this->data['activetop'] = 5;
        return view('school.users', $this->data);

    }

    public function schoolusersstatus()
    {

        $id_schooluser = isset($_GET['id_schooluser']) ? (int)$_GET['id_schooluser'] : 1;
        $id_statususer = isset($_GET['id_statususer']) ? (int)$_GET['id_statususer'] : 1;
        
        if (auth()->user()->can('director')) {
            $schoolusers = SchoolUser::where('id_schooluser', $id_schooluser)->leftJoin('schools', 'schoolusers.id_school', '=', 'schools.id_school')->first();
            if ($schoolusers->supervisor_school == auth()->user()->id) {
                $schoolusers->id_statususer = $id_statususer;
                $schoolusers->save();
                $result['director'] = 'director';
            }
        }

        $schooluser = SchoolUser::where('id_schooluser', $id_schooluser)->first();
        $statususer = StatusUser::where('id_statususer', $schooluser->id_statususer)->first();
        $result['id_statususer'] = $statususer->id_statususer;
        $result['name_statususer'] = $statususer->name_statususer;
        return json_encode($result);
    }



    public function kors()
    {
        $this->data['menu'] = [];
        $this->data['menu'][33] = 'active';
        $this->data['toggle'] = 3;
        $this->data['title'] = 'Кори закладу освіти';
        $this->data['galuzes'] = Galuz::get();
        $currentschool = auth()->user()->id_school;
        $this->data['school'] = School::where('id_school', $currentschool)->first();        
        if ($this->data['school']){
            $this->data['schoolkors'] = SchoolKor::join('galuzes', 'schoolkors.id_galuz', '=', 'galuzes.id_galuz')->get();
        } else {
            $this->data['school'] = new School;
            $this->data['schoolkors'] = new SchoolKor;
        }
        return view('school.kors', $this->data);
    }



    public function korssave(Request $request)
    {
        $id_schoolkor   = (int)$request->input('id_schoolkor');
        $schoolkor = SchoolKor::where('id_schoolkor', $id_schoolkor)->first();
        if (!$schoolkor){
            $schoolkor = new SchoolKor;
        }
        $schoolkor->id_school      = auth()->user()->id_school;
        $schoolkor->id_galuz       = (int)$request->input('id_galuz');
        $schoolkor->key_schoolkor  = (string)$request->input('key_schoolkor');
        $schoolkor->name_schoolkor = (string)$request->input('name_schoolkor');
        $schoolkor->save();

        $id_schoolkor = $schoolkor->id_schoolkor;

        return json_encode($id_schoolkor);
    }



    public function korsdelete(Request $request)
    {
        $schoolkor = SchoolKor::where('id_schoolkor', $request->input('id_schoolkor'))->delete();
        $result = $request->input('id_schoolkor');
        return json_encode($result);
    }


    public function package_list()
    {
        $this->data['title'] = 'Пакети документів';
        $this->data['activetop'] = 4;
        $this->data['cikls'] = Cikl::get();
        $this->data['packages'] = Package::select('id_package', 'name_package', 'plancount_package', 'created_at', 'updated_at', 'packages.id_status','name_status')->where('id_school',auth()->user()->id_school)->leftjoin('statuses','packages.id_status','=','statuses.id_status')->get();
        return view('school.packages', $this->data);
    }


    public function package_get(Request $request)
    {
        $package = Package::where('id_package', $request->input('id_package'))->first();
        if (!$package){
            $package = new Package;
        }
        return json_encode($package);
    }


    public function package_save(Request $request)
    {
        $plans = (object)[];
        $new = false;
        $id_package = (int)$request->input('id_package');
        $name_package = $request->input('name_package') ? $request->input('name_package') : 'Новий пакет';
        $plancount_package = $request->input('plancount_package') ? $request->input('plancount_package') : 0;
        $plans_package = $request->input('plans_package') ? $request->input('plans_package') : '{}';
        $vam_package = $request->input('vam_package') ? $request->input('vam_package') : '';
        $critical_package = $request->input('critical_package') ? $request->input('critical_package') : '';
        $package = Package::where('id_package', $id_package)->first();
        if (!$package){
            $package = new Package;
            $new = true;
        } else {
            /* вытаскиваем все имеющиеся планы в пакете */
            $plan_in_package = json_decode($package->plans_package);
            foreach ($plan_in_package as $plan) {
                if ($plan->id_plan != "0"){
                    $field = (int)$plan->id_plan;
                    $plans->{$field} = (object)['old_plan'=>true, 'new_plan'=>false];
                }                   
            }
        }

        /* просматриваем все новые планы пакета */
            $plan_in_package = json_decode($plans_package);
            foreach ($plan_in_package as $plan) {
                if ($plan->id_plan != "0"){
                    $field = (int)$plan->id_plan;
                    if (isset($plans->{$field})){
                        $plans->{$field}->new_plan = true;
                    } else {
                        $plans->{$field} = (object)['old_plan'=>false, 'new_plan'=>true];
                    }
                }                   
            }
        /* изменяем статусы для задействованных планов */
            foreach ($plans as $key => $plan) {
                if ($plan->old_plan != $plan->new_plan) {
                    $current_plan = Plan::where('id_plan', $key )->first();
                    if ($current_plan){
                        if ($plan->old_plan){
                            $current_plan->id_status = $current_plan->id_check + 2;
                        } else {
                            $current_plan->id_status = 4;
                        }
                        $current_plan->save();
                    }
                }
            }


        $package->name_package = $name_package;
        $package->id_school = auth()->user()->id_school;
        $package->plancount_package = $plancount_package;
        $package->plans_package = $plans_package;
        $package->vam_package = $vam_package;
        $package->critical_package = $critical_package;
        $package->save();
        $package->new = $new;
        $package->date1 = ukr_date(date("d F Y", strtotime($package->created_at)));
        $package->name_status = Status::where('id_status', 1 )->first()->name_status;
        $package->hexa = $package->plans_package;
        $package->hexa = $plans;
        return json_encode($package);
    }



    public function package_delete(Request $request)
    {
        # Всем задействованным планам зменить статус
        //$package = Package::where('id_package', $request->input('id_package'))->delete();
        $result = [];
        $package = Package::where('id_package', $request->input('id_package'))->first();
        $plan_in_package = json_decode($package->plans_package);
        $count = 0;
        foreach ($plan_in_package as $plan) {
            if ($plan->id_plan != "0"){$count++;}
        }
        if ($count == 0){
            $result['status'] = true;
            $package->delete();
        } else {
            $result['status'] = false;
            $result['message'] = 'Видалення неможливе. Пакет містить плани.';
        }
        return json_encode($result);
    }



    public function package_approve(Request $request)
    {
        $school = true;
        $package = Package::where('id_package', $request->input('id_package'))->first();
        $result['status'] = true;
        $result['message'] = 'Пакет утвержден';

        if ($package){
            if ($package->vam_package == '') {
                $result['status'] = false;
                $result['message'] = 'Не заполнено поле "Візія і міссія"';
            } else {
                $plan_in_package = json_decode($package->plans_package);
                foreach ($plan_in_package as $plan) {
                    if ($plan->id_status == "2") {$school = false;}
                    if ($plan->id_plan == "0"){                        
                        $result['status'] = false;
                        $result['message'] = 'Не прікріплені плани до всіх циклів навчання';
                    }                   
                }

                if ($result['status']){
                    if ($school) {
                        $result['message'] = 'Узгоджено на рівні навчального закладу';
                        $package->id_status = 5;
                        $package->save();
                    } else {
                        $result['message'] = 'Направлено на узгодження в МОН';
                        $package->id_status = 6;
                        $package->save();
                    }
                    /* Устанавливае статусы всем планам и программам */
                    foreach ($plan_in_package as $plan) {
                        $current_plan = Plan::where('id_plan', $plan->id_plan)->first();
                        if ($current_plan){
                            $programs_in_plan = json_decode($current_plan->content_plan);
                            foreach ($programs_in_plan as $program) {
                                $current_program = Program::where('id_program', $program->program)->first();
                                if ($current_program){
                                    $current_program->id_status = $package->id_status;
                                    $current_program->save();
                                }
                            }
                            $current_plan->id_status = $package->id_status;
                            $current_plan->save();
                        }
                    }

                }

            }
            $result['package'] = $package;
        } else {
            $result['status'] = false;
            $result['message'] = 'Пакет не найден';
        }

        return json_encode($result);
    }

}


