<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use App\Cikl;
use App\SchoolClass;
use App\Galuz;
use App\Plan;
use App\School;
use App\Program;
use App\Subject;
use App\Kor;
use App\Status;

class PlanController extends MainController
{



    public function plans_list($action='')
    {

        $where = '';
        $this->data['cikls'] = Cikl::get();
        $this->data['chartgaluzes'] = Galuz::get();

        if ($action !='basic' && $action != 'typical' && $action != 'school' && $action != 'active' && $action != 'archive') {
            $action='typical';
        }


        if ($action=='basic') {
            $this->data['menu'][411] = 'active';
            $this->data['title'] = 'Базові навчальні плани';
            $this->data['type'] = 1;
            $this->data['toggle'] = 4;
            $where = ' id_ptype=1 ';
        }
        if ($action=='typical') {
            $this->data['menu'][412] = 'active';
            $this->data['title'] = 'Типові навчальні плани';
            $this->data['type'] = 2;
            $this->data['toggle'] = 4;
            $where = ' id_ptype=2 ';
        }
        if ($action=='school') {
            $this->data['menu'][413] = 'active';
            $this->data['title'] = 'Навчальні плани закладу освіти';
            $this->data['type'] = 3;
            $this->data['toggle'] = 4;
            $where = ' id_school='.(auth()->user()->id_school).' AND id_ptype=3 AND t1.id_status IN (1,2,3,4) ';
        }        
        // if ($action=='user') {
        //     $this->data['menu'][414] = 'active';
        //     $this->data['title'] = 'Користувацькі навчальні плани';
        //     $this->data['type'] = 4;
        //     $this->data['toggle'] = 4;
        //     $where = ' id_user=' . Auth::user()->id;
        // }

        if ($action=='active') {
            $this->data['menu'][311] = 'active';
            $this->data['title'] = 'Діючі навчальні плани';
            $this->data['type'] = 5;
            $this->data['toggle'] = 3;
            $where = ' id_school='.(auth()->user()->id_school).' AND id_ptype=3 AND t1.id_status NOT IN (1,2,3,4,9) ';
        }
        if ($action=='archive') {
            $this->data['menu'][321] = 'active';
            $this->data['title'] = 'Архів навчальних планів';
            $this->data['type'] = 5;
            $this->data['toggle'] = 3;
            $where = ' id_school='.(auth()->user()->id_school).' AND id_ptype=3 AND t1.id_status = 9 ';
        }

        /************************************/
        /* разбираем фильтр на составляющие */
        /************************************/

        foreach ($_GET as $key => $filter) {
            $this->data['filter'][$key] = $filter;
        }

        // $sql = "SELECT *, date(created_at) AS date1, date(updated_at) AS date2, '' AS name_cikl, '' AS name_create FROM `plans` WHERE " . $where;

        $sql = "SELECT t1.*, date(t1.created_at) AS date1, date(t1.updated_at) AS date2, t2.name_cikl, t3.name_create, t4.name_status FROM `plans` AS t1 LEFT JOIN `cikls` AS t2 ON t1.id_cikl=t2.id_cikl LEFT JOIN `creates` AS t3 ON t1.id_create=t3.id_create LEFT JOIN `statuses` AS t4 ON t1.id_status=t4.id_status WHERE " . $where;

        // if (isset($_GET['id'])) {$sql .= " AND t1.`id_plan` =  '" . $_GET['id'] . "'";}
        // if (isset($_GET['name'])) {$sql .= " AND t1.`name_plan` like  '%" . $_GET['name'] . "%'";}
        // if (isset($_GET['cikl'])) {$sql .= " AND t1.`id_cikl` =  '" . $_GET['cikl'] . "'";}

        $sql .= " ORDER BY `id_plan` ";

        $plans = DB::select( $sql );

        foreach ($plans as $key => $plan) {
            $subjects_count = 0;
            $programs_count = 0;
            foreach (json_decode($plan->content_plan) as $subject) {
                $subjects_count++;
                if (isset($subject->program) && $subject->program != "0") $programs_count++;
            }
            $plan->subjects_count = $subjects_count;
            $plan->programs_count = $programs_count;
        }

        $this->data['plans'] = $plans;


        return view('constructor.plan-list', $this->data);
    }


    public function plan_view($id_plan=0)
    {
        $this->data['toggle'] = 4;
        $this->data['title'] = 'Перегляд плану';
        $this->data['galuzes'] = Galuz::get();

        if ( $id_plan != 0 ) {
            $info_plan = Plan::where('id_plan', $id_plan)->first();
            $this->data['classes'] = SchoolClass::where('id_cikl', $info_plan->id_cikl)->get();
        } else {
            $info_plan = new Plan();
            $info_plan->content_plan = '{}';
        }
        
        $this->data[ 'info_plan' ] = $info_plan;
        $this->data[ 'current_plan' ] = json_decode( $info_plan->content_plan );

        return view('constructor.plan-view', $this->data);
    }


    /*
     * Конструктор планов
     */

    public function plans_edit($id_plan=0){        
        $this->data['toggle'] = 4;
        $this->data['menu'] = [];
        $this->data['menu'][4] = 'active';
        $this->data['title'] = 'Створити навчальний план';
        $this->data['color'] = 'blue';

        $this->data['galuzes'] = Galuz::get();
        $this->data['subjects'] = Subject::get();

        $this->data['subjects_json'] = json_encode($this->data['subjects']);

        /* Считываем редактируемый план из базы */
        $plan = Plan::where('id_plan', $id_plan)->join('creates', 'plans.id_create', '=', 'creates.id_create')->first();
        if (!$plan) {
            $plan = new Plan;
        }

        $this->data['plan'] = $plan;
        $this->data['content_plan'] = json_decode( $plan->content_plan );
        $this->data['classes'] = SchoolClass::where('id_cikl',$plan->id_cikl)->get();

        return view('constructor.plan-edit', $this->data);
    }



    /*
     * Создаем новый план из базового
     */
    public function new_plan_from_basic()
    {
            $id_plan = isset($_GET['id_plan']) ? (int)$_GET['id_plan'] : 1;
        /* делаем проверку на "директора" и получаем id школы */
            if (auth()->user()->can('director')) {
                $id_school = auth()->user()->id_school;
                $school = School::where('id_school',$id_school)->get();
                if (count($school)==0) $id_school = 0;
            } else {
                $id_school = 0;
            }        
        /* если есть действительный id школы, то продолжаем */
            if ( $id_school>0 ) {
                /* Получаем базовый план */
                $basic = Plan::where('id_plan', $id_plan)->where('id_ptype',1)->first();
                $plan = new Plan;
                $plan->id_parent = $basic->id_plan;   /* Номер базового плана */
                $plan->name_plan = $basic->name_plan; /* Присваиваем название как у базового */
                $plan->id_cikl = $basic->id_cikl;     /* Цикл */
                $plan->id_ptype = 3;                  /* Говорим, что план принаджежит школе*/
                $plan->id_school = $id_school;        /* Номер школы */
                $plan->id_create = 1;                 /* Можно потом редактировать, а не только менать числа */
                $plan->content_plan = '{}';           /* Пустой план */
                $plan->save();
            }
        $result = $id_school;
        return $result;
    }



    /*
     * Создаем новый план из типичного
     */
    public function new_plan_from_typical()
    {
            $id_plan = isset($_GET['id_plan']) ? (int)$_GET['id_plan'] : 1;
        /* делаем проверку на "директора" и получаем id школы */
            if (auth()->user()->can('director')) {
                $id_school = auth()->user()->id_school;
                $school = School::where('id_school',$id_school)->get();
                if (count($school)==0) $id_school = 0;
            } else {
                $id_school = 0;
            }        
        /* если есть действительный id школы, то продолжаем */
            if ( $id_school>0 ) {
                /* Получаем базовый план */
                $typical = Plan::where('id_plan', $id_plan)->where('id_ptype',2)->first();
                $plan = new Plan;
                $plan->id_parent = $typical->id_parent; /* Номер базового плана */
                $plan->name_plan = $typical->name_plan; /* Присваиваем название как у базового */
                $plan->id_cikl = $typical->id_cikl;     /* Цикл */
                $plan->id_ptype = 3;                    /* Говорим, что план принаджежит школе*/
                $plan->id_school = $id_school;          /* Номер школы */
                $plan->id_create = 2;                   /* Можно потом редактировать, а не только менать числа */

                /* создаем копии всех прикрепленных программ */
                $current_plan = json_decode($typical->content_plan);
                foreach ($current_plan as $subject) {
                    if ($subject->program != "0") {
                        $model = Program::where('id_program', $subject->program)->first();
                        $new_program = new Program;
                        $new_program->name_program = $model->name_program;
                        $new_program->id_school = auth()->user()->id_school;
                        // $new_program->id_user = auth()->user()->id;
                        $new_program->id_user = 0;
                        $new_program->id_ptype = 3;
                        $new_program->id_stype = $model->id_stype;
                        $new_program->id_cikl = $model->id_cikl;
                        $new_program->id_create = 2;
                        $new_program->content_program = $model->content_program;
                        // $new_program->users_program = '{"0":{"id_user":"' . (auth()->user()->id) . '", "name_user":"' . (auth()->user()->second_name) . ' ' . (auth()->user()->name). ' ' . (auth()->user()->patronymic) . '"}}';
                        $new_program->users_program = '{}';
                        $new_program->galuzes_program = $model->galuzes_program;
                        $new_program->save();
                        $subject->program = $new_program->id_program;
                    }
                }
                $plan->content_plan = json_encode( $current_plan );
                /*********************************************/

                $plan->save();
            }
        $result = $id_school;
        return $result;
    }



    /*
     * Сохранение плана
     */
    public function plans_save()
    {
        $result = 'Saved';
        $id_plan = isset($_POST['id_plan']) ? (int)$_POST['id_plan'] : 0;
        $name_plan = isset($_POST['name_plan']) ? (string)$_POST['name_plan'] : '';
        $id_status = isset($_POST['id_status']) ? (string)$_POST['id_status'] : 1;
        $content_plan = isset($_POST['content_plan']) ? (string)$_POST['content_plan'] : '{}';
        /* делаем проверку на "директора" и что план принадлежит школе */
            if (auth()->user()->can('director')) {
                $plan = Plan::where('id_plan', $id_plan)->where('id_school',auth()->user()->id_school)->first();
                if ($plan){
                    /* сохраняем для всех программ статус как у плана */
                        $content_plan2 = json_decode($plan->content_plan);
                        foreach ($content_plan2 as $subject) {
                            $program = Program::where('id_program', $subject->program)->first();
                            if ($program){
                                $program->id_status = $id_status;
                                $program->save();
                            }
                        }

                    /* */
                    $plan->name_plan = $name_plan;
                    $plan->id_status = $id_status;
                    $plan->content_plan = $content_plan;
                    $plan->save();
                    $plan->name_status = Status::where('id_status',$plan->id_status)->first()->name_status;
                    $result = $plan;
                }
            }
        return json_encode($result);
    }



    /*
     * Удаление плана
     */
    public function plans_delete()
    {
        $result = 'Deleted';
        $id_plan = isset($_GET['id_plan']) ? (int)$_GET['id_plan'] : 0;
        /* делаем проверку на "директора" и что план принадлежит школе */
            if (auth()->user()->can('director')) {
                $plan = Plan::where('id_plan', $id_plan)->where('id_school',auth()->user()->id_school)->delete();
            }
        return $result;
    }



    /*
     * Проверка плана
     */
    public function plans_check()
    {
        $result = [];
        $check = ['1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0,'6'=>0,'7'=>0,'8'=>0,'9'=>0,'10'=>0];
        $check_b = $check;

        $id_plan = isset($_GET['id_plan']) ? (int)$_GET['id_plan'] : 0;        

        $plan = Plan::where('id_plan', $id_plan)->first();
        if ($plan) {
            $result['content_plan'] = $plan->content_plan;
            $content_plan = json_decode($plan->content_plan);
            foreach ($content_plan as $subject) {
                foreach ($subject->galuzes as $galuz) {
                    foreach ($galuz->classes as $class) {
                        $check[$galuz->galuz] += $class->hours;
                    }
                }
            }
        }

        if (isset($plan->id_parent)) {
            $basic = Plan::where('id_plan', $plan->id_parent)->first();
            if ($basic) {
                $content_plan = json_decode($basic->content_plan);
                foreach ($content_plan as $subject) {
                    foreach ($subject->galuzes as $galuz) {
                        foreach ($galuz->classes as $class) {
                            $check_b[$galuz->galuz] += $class->hours;
                        }
                    }
                }
            }
        }

        $result['output'] = '<table class="check">';
        $result['output'] .= '<thead><tr><td rowspan="2">Галузь</td><td colspan="2">Години на цикл</td><td rowspan="2"></td></tr>';
        $result['output'] .= '<tr><td>План</td><td>Базовий план</td></tr></thead>';
        $galuzes = Galuz::get();
        foreach ($galuzes as $galuz) {
            $result['output'] .= '<tr><td>'.$galuz->name_galuz.'</td><td>'.($check[$galuz->id_galuz]*35).'</td><td>'.$check_b[$galuz->id_galuz].'</td><td>';
            if ( ($check[$galuz->id_galuz]*35) == $check_b[$galuz->id_galuz]) {
                $result['output'] .= '<i class="fa fa-check-square-o" aria-hidden="true"></i>';
            } else {
                $result['output'] .= '<i class="fa fa-square-o" aria-hidden="true"></i>';
            }

            $result['output'] .='</td></tr>';
        }
        $result['output'] .= '</table>';

        $result['check'] = $check;
        $result['check_b'] = $check_b;
        $result['plan_fullinfo'] = $this->plan_fullinfo( $id_plan );
        return json_encode($result);
    }


    public function plan_popup(Request $request)
    {
        $plan = Plan::where('id_plan',(int)$request->input('id_plan'))->first();
        if ($plan) {
            $result['plan'] = $plan;
            $result['galuzes'] = Galuz::get();
            $result['classes'] = SchoolClass::where('id_cikl', $plan->id_cikl)->get();
        } else {
            die();
            $result = 'Access denied';
        }
        return json_encode($result);
    }


    /* Выбираем все планы для пакета */
    public function plans_get_all(Request $request) 
    {
        $plans = Plan::leftjoin('statuses', 'plans.id_status', '=', 'statuses.id_status')->where('id_school',auth()->user()->id_school)->whereIn('plans.id_status',  [2,3] )->where('id_cikl',(int)$request->input('id_cikl'))->get();
        if (!$plans) {die();}
        return json_encode($plans);
    }



    /* Проверка плана и установление статуса */
    public function plans_perevirka(Request $request)
    {
        $id_plan = $request->input('id_plan') ? (int)$request->input('id_plan') : 0;
        $kors_all = $this->plan_fullinfo( $id_plan );
        $no = $request->input('no') ? true : false;
        $id_status_program = $request->input('no') ? 2 : 3;
        $plan = Plan::where('id_plan',$id_plan)->first();
        if ($plan){            
            $result['status'] = true;
            /* Проверяем количество часов и количество коров - общее число не меньше базового и стандартных не меньше 80% */
                foreach ($kors_all as $galuz) {
                    if ($galuz->active){
                        foreach ($galuz->classes as $class) {
                            if ($class->basic_hours != $class->plan_hours){
                                $result['status'] = false;
                            }
                        }
                        if ($galuz->basic_kors_count > $galuz->program_kors_count || ($galuz->basic_kors_count)/5*4 > $galuz->standart_kors_count){
                            $result['status'] = false;
                        }
                    }
                }
            /* Если пройдены все проверки - помечаем план и все программы как проверенные */
                $content_plan = json_decode($plan->content_plan);
                if ($result['status'] || $no) {
                    foreach ($content_plan as $key => $subject) {
                        $program = Program::where('id_program', (int)$subject->program)->first();
                        if ($program){
                            $program->id_status = 4;
                            $program->save();
                        }
                    }
                    $plan->id_status = $id_status_program;
                    $plan->id_check = $id_status_program - 2;
                    $plan->save();
                    $plan->name_status = Status::where('id_status',$plan->id_status)->first()->name_status;
                    $result['plan'] = $plan;
                    $result['status'] = true;
                    $result['no'] = $no;
                }

        } else {
            $result['status'] = false;
        }

        $result['kors_all'] = $kors_all;
        $result['message'] = 'Перевірку пройдено. План достатньо узгодити за вірні навчального закладу.';
        return json_encode($result);
    }



    /* получение развернутых данных по плану в части использования коров */
    public function plan_fullinfo($id_plan = 0)
    {
        if ($id_plan>0){
            $kors_all = (object)[];
            $plan = Plan::where('id_plan',$id_plan)->first();
            if ($plan){

                /* Создаем объект со всеми галузями и классами цикла*/
                    $galuzes = Galuz::all();
                    $classes = SchoolClass::where('id_cikl',$plan->id_cikl)->get();
                    foreach ($galuzes as $galuz) {
                        $count = count(Kor::leftjoin('zors', 'kors.id_zor','=','zors.id_zor')->where('id_cikl',$plan->id_cikl)->where('id_galuz',$galuz->id_galuz)->get());
                        $kors_all->{$galuz->id_galuz} = (object)['active'=>false, 'basic_kors_count'=>$count, 'program_kors_count'=>0, 'standart_kors_count'=>0, 'program_kors'=>(object)[], 'standart_kors'=>(object)[], 'classes'=>(object)[]];
                        foreach ($classes as $class) {
                            $kors_all->{$galuz->id_galuz}->classes->{$class->id_class} = (object)['basic_hours'=>0, 'plan_hours'=>0];
                        }
                    }

                /* Выбираем активные галузи базового плана и количество часов */
                    $basic_plan = Plan::where('id_plan',$plan->id_parent)->first();
                    $basic_content_plan = json_decode($basic_plan->content_plan);
                    foreach ($basic_content_plan as $subject) {
                        foreach ($subject->galuzes as $galuz) {
                            $field = (int)($galuz->galuz);
                            $kors_all->{$field}->active = true;
                            foreach ($galuz->classes as $class) {
                                $value = (int)$class->hours;
                                $kors_all->{$field}->classes->{$class->class}->basic_hours = $value;
                            }
                        }
                    }
                /* Проверка плана - вытаскиваем все коры */
                    $content_plan = json_decode($plan->content_plan);
                    foreach ($content_plan as $key => $subject) {
                        /* Вытаскиваем количество часов */
                            foreach ($subject->galuzes as $galuz){
                                $field = (int)($galuz->galuz);                            
                                foreach ($galuz->classes as $class){
                                    $value = 35*(int)$class->hours;
                                    $kors_all->{$field}->classes->{$class->class}->plan_hours += $value;
                                }
                            }
                        /* Присутствуют все программы */
                        if ($subject->program == "0") {
                            $result['status'] = false;
                        } else {
                            $program = Program::where('id_program', (int)$subject->program)->first();
                            $content_program = json_decode($program->content_program);
                            foreach ($content_program as $line) {
                                foreach ($line->problems as $problem){
                                    foreach ($problem->opises as $opis){
                                        foreach ($opis->kors as $kor){
                                            $kors_all->{$kor->galuz}->program_kors->{$kor->kod} = true;
                                            if ($kor->owner == 1){ $kors_all->{$kor->galuz}->standart_kors->{$kor->kod} =true; }
                                        }
                                    }
                                }
                            }
                        }
                    }
                /* Суммируем все коры в разрезе галузей */
                    foreach ($kors_all as $galuz) {
                        foreach ($galuz->program_kors as $kor) {
                            $galuz->program_kors_count ++;
                        }
                        foreach ($galuz->standart_kors as $kor) {
                            $galuz->standart_kors_count ++;
                        }
                    }

                return $kors_all;
            } else {
                return false;
            }
        } else {
            return false;
        }        
    }


}
