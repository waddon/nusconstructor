<?php

namespace App\Http\Controllers\Auth;

use Mail;
use App\User;
use App\School;
use App\SchoolType;
use App\SchoolUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Spatie\Permission\Models\Role;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:table_users',
            'name' => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'patronymic' => 'max:255',
            'phone' => 'max:12',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function showRegistrationForm()
    {
        $schooltypes = Schooltype::getSchoolTypes();
        $region_schools = School::select('region_school')->distinct()->orderBy('region_school', 'asc')->get()->pluck('region_school', 'region_school')->prepend('--', 0);

        return view('auth.register', compact('schooltypes', 'region_schools'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $password = $data['password'];
        $data['patronymic'] = isset($data['patronymic']) ? $data['patronymic'] : '';

        $user =  User::create([
            'email' => $data['email'],
            'name' => $data['name'],
            'second_name' => $data['second_name'],
            'patronymic' => $data['patronymic'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
        ]);

        $user->assignRole('user');

        if (isset($data['id_school'])) {
            $schooluser = new SchoolUser;
            $schooluser->id_school = $data['id_school'];
            $schooluser->id_user = $user->id;
            $schooluser->id_statususer = 2;
            $schooluser->save();
        }

        // Mail::send('emails.registration', ['user' => $user, 'password' => $password],
        //     function ($m) use ($user) {
        //         $m->from('noreply@nus.org.ua', 'Нова українська школа');
        //         $m->to($user->email, $user->name)->subject('Ваші дані доступу до платформи');
        //     }
        // );

        return $user;
    }
}
