<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\School;
use App\SchoolUser;
use App\User;
use App\SchoolType;
use Hash;

class UserController extends MainController
{


    public function profile()
    {
        $this->data['title'] = 'Профіль користувача';
        $this->data['current_school'] = '';
        $this->data['schools'] = SchoolUser::where('id_user',auth()->user()->id)->where('id_statususer',1)->join('schools', 'schoolusers.id_school', '=', 'schools.id_school')->get();

        $this->data['entry_schools'] = SchoolUser::where('id_user',auth()->user()->id)->where('id_statususer',2)->join('schools', 'schoolusers.id_school', '=', 'schools.id_school')->get();

        $this->data['regions'] = School::select('region_school')->distinct()->orderBy('region_school', 'asc')->get();
        $this->data['schooltypes'] = SchoolType::get();

        if (auth()->user()->id_school != 0){
            $temp = School::where('id_school', auth()->user()->id_school)->first();
            if ($temp) {
                $this->data['current_school'] = $temp->name_school;
            } else {
                $this->data['current_school'] = 'Не визначено-';
            }
        }

        return view('user.profile', $this->data);        
    }



    public function profile_save(Request $request)
    {
        $result = [];
        $result['status'] = '';
        $new_file = false;
        $new_password = false;
        $password = $request->input('password') ? (string)$request->input('password') : '';
        $newpassword = $request->input('newpassword') ? (string)$request->input('newpassword') : '';
        $repeatnewpassword = $request->input('repeatnewpassword') ? (string)$request->input('repeatnewpassword') : '';


        if ($newpassword != '' && $repeatnewpassword != '' && $password != ''){
            if (Hash::check($password, auth()->user()->password) && $newpassword == $repeatnewpassword) {
                $new_password = true;
            }
        }

        /* придумывем случайную директорию из 2-х символов и создаем ее, если ее еще нет */
        $dir = $this->generateRandomString() . '/';
        if (!is_dir( 'uploads/' . $dir )) { mkdir( 'uploads/' . $dir ); }
        /* цикл по всем прикрепленным файлам */
        foreach ($request->file() as $file) {
                $findname = false;
                /* перебираем пока не найдем первое незанятое случайное имя */
                while(!$findname){
                    $filename = str_random(20) . '.' . $file->getClientOriginalExtension();
                    if (!file_exists( '/uploads/' . $dir . '/' . $filename )) {$findname = true;}
                } 
            //$filename = $file->getClientOriginalName();
            $file->move(public_path( 'uploads/'. $dir ), $filename);
            $new_file = true;
            $result['filename'] = '/uploads/' . $dir . '/' . $filename;
        }

        $user = User::where('id', auth()->user()->id)->first();
        $user->name = (string)$request->input('name');
        $user->second_name = (string)$request->input('second_name');
        $user->patronymic = (string)$request->input('patronymic');
        $user->id_school = (int)$request->input('id_school');
        if ($new_file) {$user->thumb = '/uploads/' . $dir . '/' . $filename;}
        if ($request->input('freephoto')) {$user->thumb = '';}
        if ($new_password) {
            $user->password = bcrypt($request->input('newpassword'));
            $result['status'] .= "Пароль змінено. ";
        }
        $user->save();
        $result['status'] .= "Усі зміни збережено.";

        return json_encode($result);
    }



    public function get_schools(Request $request)
    {
        $result = [];
        $region_school = $request->input('region_school') ? (string)$request->input('region_school') : '';
        $schooltype = $request->input('schooltype') ? (int)$request->input('schooltype') : 0;
        if ($region_school != "" && $schooltype != 0){
            $result['list_schools'] = School::where('region_school',$region_school)->where('id_schooltype', $schooltype)->get();
        } else {
            $result['list_schools'] = new School;
        }
        return json_encode($result);
    }



    public function add_entry()
    {
        $result = [];
        $id_school = isset($_POST['id_school']) ? (int)$_POST['id_school'] : 0;
        $schooluser = SchoolUser::where('id_school', $id_school)->where('id_user',auth()->user()->id)->first();
        if ($schooluser) {
            $result['status'] = 'Repeat';
        } else {
            $schooluser = new SchoolUser;
            $schooluser->id_school = $id_school;
            $schooluser->id_user = auth()->user()->id;
            $schooluser->id_statususer = 2;
            $schooluser->save();
            $result['status'] = 'Ok';
            $result['name_school'] = School::where('id_school', $id_school)->first()->name_school;
        }
        return json_encode($result);
    }


    function generateRandomString($length = 2) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
