<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use App\Galuz;
use App\Stype;
use App\Tool;
use App\Program;
use App\SchoolUser;
use App\SchoolKor;
use App\Cikl;
use App\Kor;
use App\Zmist;

class ProgramController extends MainController
{


    public function programs_list($action='')
    {
        $this->data['stypes'] = Stype::get();

        if ($action != 'typical' && $action != 'school' && $action !='user' && $action != 'active' && $action != 'archive') {
            $action='typical';
        }


        /**************************************/
        /* разблокируем все старые блокировки */
        /**************************************/
            DB::update('UPDATE `programs` SET `session_program` = "", `blocked_at` = NULL WHERE `blocked_at` < SUBDATE( NOW() ,INTERVAL 30 MINUTE)' );

        /* 
         * Подразумевается разный выбор при разном $action
         * Программы школы, пользовательские программы, типичные программы
         * Пока берем только пользовательские
         */

        if ($action=='typical') {
            $this->data['menu'][421] = 'active';
            $this->data['title'] = 'Типові навчальні програми';
            $this->data['toggle'] = 4;
            $this->data['type'] = 2;
            $this->data['programs'] = Program::where('id_ptype', 2)->leftjoin('stypes', 'programs.id_stype', '=', 'stypes.id_stype')->leftjoin('cikls', 'programs.id_cikl', '=', 'cikls.id_cikl')->leftjoin('creates', 'programs.id_create', '=', 'creates.id_create')->leftjoin('statuses', 'programs.id_status', '=', 'statuses.id_status')->get();
        }
        if ($action=='school') {
            $this->data['menu'][422] = 'active';
            $this->data['title'] = 'Навчальні програми закладу освіти';
            $this->data['toggle'] = 4;
            $this->data['type'] = 3;
            $this->data['programs'] = Program::where('id_school', auth()->user()->id_school)->where('id_ptype', 3)->leftjoin('cikls', 'programs.id_cikl', '=', 'cikls.id_cikl')->leftjoin('stypes', 'programs.id_stype', '=', 'stypes.id_stype')->leftjoin('creates', 'programs.id_create', '=', 'creates.id_create')->leftjoin('statuses', 'programs.id_status', '=', 'statuses.id_status')->get();
        }

        if ($action=='user') {
            $this->data['menu'][423] = 'active';
            $this->data['title'] = 'Користувацькі навчальні програми';
            $this->data['type'] = 4;
            $this->data['activetop'] = 2;
            $this->data['programs'] = Program::where('id_user', auth()->user()->id)->where('id_ptype', 4)->leftjoin('stypes', 'programs.id_stype', '=', 'stypes.id_stype')->leftjoin('cikls', 'programs.id_cikl', '=', 'cikls.id_cikl')->leftjoin('creates', 'programs.id_create', '=', 'creates.id_create')->leftjoin('statuses', 'programs.id_status', '=', 'statuses.id_status')->get();
        }

        if ($action=='active') {
            $this->data['menu'][312] = 'active';
            $this->data['title'] = 'Діючі навчальні програми закладу освіти';
            $this->data['toggle'] = 3;
            $this->data['type'] = 5;
            $this->data['programs'] = Program::where('id_school', auth()->user()->id_school)->where('id_ptype', 3)->where('programs.id_status','>',4)->leftjoin('cikls', 'programs.id_cikl', '=', 'cikls.id_cikl')->leftjoin('stypes', 'programs.id_stype', '=', 'stypes.id_stype')->leftjoin('creates', 'programs.id_create', '=', 'creates.id_create')->leftjoin('statuses', 'programs.id_status', '=', 'statuses.id_status')->get();
        }

        if ($action=='archive') {
            $this->data['menu'][322] = 'active';
            $this->data['title'] = 'Ахів навчальних програм закладу освіти';
            $this->data['toggle'] = 3;
            $this->data['type'] = 5;
            $this->data['programs'] = Program::where('id_school', auth()->user()->id_school)->where('id_ptype', 3)->where('programs.id_status',9)->leftjoin('cikls', 'programs.id_cikl', '=', 'cikls.id_cikl')->leftjoin('stypes', 'programs.id_stype', '=', 'stypes.id_stype')->leftjoin('creates', 'programs.id_create', '=', 'creates.id_create')->leftjoin('statuses', 'programs.id_status', '=', 'statuses.id_status')->get();
        }

        /************************************/
        /************************************/

        if (Auth::check()) {
            $temp_str = '"id_user":"'.auth()->user()->id.'"';
            foreach ($this->data['programs'] as $key => $value) {
                if ($value->id_status==1 && (strripos($value->users_program, $temp_str) || auth()->user()->can('director'))) {
                    $value->edit = true;
                } else {
                    $value->edit = false;
                }
            }
            $this->data['schoolusers'] = SchoolUser::where('schoolusers.id_school',auth()->user()->id_school)->join('table_users', 'schoolusers.id_user', '=', 'table_users.id')->get();
        } else {
            $this->data['schoolusers'] = new SchoolUser;
            $temp_str ='"hexanull"';
        }
        
        $this->data['cikls'] = Cikl::get();
        $this->data['stypes'] = Stype::get();
        $this->data['galuzes'] = Galuz::get();

        return view('constructor.program-list', $this->data);
    }



    /*
     * Конструктор программ
     */

    public function program_edit($id_program=0)
    {
        $this->data['toggle'] = 4;
        $this->data['menu'] = [];
        $this->data['menu'][6] = 'active';
        $this->data['title'] = 'Створити навчальну програму';
        $this->data['color'] = 'blue';

        $this->data['tools'] = Tool::get();

        $id_program = (int)$id_program;

        if ( $id_program != 0 ) {
            $info_program = Program::where( 'id_program', $id_program )->leftjoin('stypes', 'programs.id_stype', '=', 'stypes.id_stype')->leftjoin('creates', 'programs.id_create', '=', 'creates.id_create')->leftjoin('cikls', 'programs.id_cikl', '=', 'cikls.id_cikl')->first();
            /*****************************************/
            /* здесь проверка на уже редактируемость */
            /*****************************************/
            DB::update('UPDATE `programs` SET `session_program` = "' . session()->getId() . '", `blocked_at` = NOW() WHERE `id_program` = '. $id_program );

        } else {
            $info_program = new Program();
            $info_program->content_program = '{}';
        }


        /* Выбираем только галузи присутствующие в программе */
        $temp = [];
        $curent_galuzes = json_decode( $info_program->galuzes_program );
        foreach ($curent_galuzes as $curent_galuz) {
            $temp[] = $curent_galuz->id_galuz;
        }

        $this->data['galuzes'] = Galuz::get();
        $this->data['kors_in_galuzes'] = Kor::select(DB::raw('id_galuz, count(*) as count_kors'))->leftjoin('zors','kors.id_zor','=','zors.id_zor')->where('zors.id_cikl', $info_program->id_cikl)->groupBy('id_galuz')->get();
        foreach ($this->data['kors_in_galuzes'] as $curent_line) {
            foreach ($this->data['galuzes'] as $curent_galuz) {
                if ($curent_galuz->id_galuz == $curent_line->id_galuz) {$curent_galuz->count_kors = $curent_line->count_kors;}
            }
        }
        
        $this->data[ 'info_program' ] = $info_program;
        $this->data[ 'current_program' ] = json_decode( $info_program->content_program );

        $this->data[ 'kors' ] = Kor::join('zors', 'kors.id_zor', '=', 'zors.id_zor')->where('id_cikl', $info_program->id_cikl)->whereIn('id_galuz', $temp)->get();
        $this->data[ 'schoolkors' ] = SchoolKor::whereIn('id_galuz', $temp)->where('id_school', auth()->user()->id_school)->get();
        if ($info_program->id_stype == 1){
            $this->data[ 'zmists' ] = Zmist::where('id_galuz', $temp[0])->get();
        }

        return view('constructor.program-edit', $this->data);
    }



    public function program_save(Request $request)
    {
        $result = [];
        $id_program = isset($_POST['id_program']) ? (int)$_POST['id_program'] : 0;
        $name_program = isset($_POST['name_program']) ? (string)$_POST['name_program'] : '';
        $hours_program = isset($_POST['hours_program']) ? (float)$_POST['hours_program'] : 0;
        $content_program = isset($_POST['content_program']) ? (string)$_POST['content_program'] : '{}';
        $content_program = str_replace("&#39;", "’", $content_program);

        if ($id_program == 0){
            $result['message'] = 'Нову програму збережено';
            $model = new Program();
            $model->id_user = Auth::user()->id;
            $model->name_program = $name_program;
            $model->content_program = $content_program;
            $model->session_program = session()->getId();
            $model->blocked_at = NOW();
            $model->save();
            $id_program = $model->id_program;
        } else {
            DB::beginTransaction();
            $model = Program::where('id_program', $id_program)->where('session_program', session()->getId())->first();
            if ($model) {
                $result['message'] = 'Програму збережено';
                $model->name_program = $name_program;
                $model->content_program = $content_program;
                $model->hours_program = $hours_program;
                $model->blocked_at = NOW();
                $model->save();
            } else {
                $result['message'] = 'Недостатньо прав на збереження';
            }
            DB::commit();
        }

        $result['id_program'] = $id_program;
        $result['name_program'] = $name_program;
        $result['content_program'] = $content_program;

        return json_encode($result);
    }


    public function program_change_editor()
    {
        $result = [];
        $result['status'] = 0;
        $id_program = isset($_GET['id_program']) ? (int)$_GET['id_program'] : 0;
        if ($id_program != 0) {
            DB::beginTransaction();
            $model = Program::where('id_program', $id_program)->where('session_program', session()->getId())->first();
            if ($model){
                $result['status'] = 1;
                DB::update('UPDATE `programs` SET `blocked_at` = NOW(), `session_program` = "' . (session()->getId()) . '" WHERE `id_program` = '. $id_program );
            } else {
                $result['status'] = 2;
            }
            DB::commit();
        } else {
            $result['status'] = 1;
        }
        return json_encode($result);
    }



    public function program_free()
    {
        $result = [];
        $id_program = isset($_GET['id_program']) ? (int)$_GET['id_program'] : 0;

        DB::beginTransaction();
        $model = Program::where('id_program', $id_program)->where('session_program', session()->getId())->first();
        if ($model) {
            DB::update('UPDATE `programs` SET `session_program` = "", `blocked_at` = NULL WHERE `id_program` = '. $id_program );
        }
        DB::commit();

        return json_encode($result);
    }


    /* Проверка редактируется ли программа */
    public function program_is_open()
    {
        $result = [];
        $result['status'] = 0;
        $id_program = isset($_GET['id_program']) ? (int)$_GET['id_program'] : 0;
        $model = Program::where('id_program', $id_program)->first();
        if ($model){
            if ( $model->session_program == '' || $model->session_program == session()->getId() ){
                $result['status'] = 1;
            } else {
                $result['status'] = 2;
            }
        }
        return json_encode($result);
    }

    /* Просмотр программы */
    public function program_view($id_program=0)
    {
        return 'Перегляд програми - ' . $id_program;
    }


    public function program_save_users(Request $request)
    {
        $id_program = $request->input('id_program') ? (int)$request->input('id_program') : '0';
        $users_program = $request->input('users_program') ? (string)$request->input('users_program') : '{}';
        $model = Program::where('id_program', $id_program)->first();
        if ($model){
            $model->users_program = $users_program;
            $model->save();
        }
        return json_encode('Ok');
    }


    public function program_create_new(Request $request)
    {
        //$new_galuzes = (object)[];
        $res = true;
        $id_cikl = $request->input('id_cikl') ? (int)$request->input('id_cikl') : 1;
        $id_stype = $request->input('id_stype') ? (int)$request->input('id_stype') : 1;
        $id_ptype = $request->input('id_ptype') ? (int)$request->input('id_ptype') : 1;
        $galuzes_program = $request->input('galuzes_program') ? (string)$request->input('galuzes_program') : '{}';

        /* Подсчитываем количество галузей и добавляем/убираем в зависимости от типа предмета */
        $count = 0;
        foreach (json_decode($galuzes_program) as $galuz) {
             $count ++;
        }
        if (($id_stype==1 && $count!=1) || ($id_stype==2 && $count<2)) {$res = false;}
        /* */

        if ($res){
            $model = new Program;
            $model->name_program = "Нова програма";
            $model->id_cikl = $id_cikl;
            $model->id_stype = $id_stype;
            $model->id_ptype = $id_ptype;
            $model->id_school = auth()->user()->id_school;
            $model->id_user = auth()->user()->id;
            $model->content_program = '{}';
            $model->galuzes_program = $galuzes_program;
            if (auth()->user()->can('director')) {
                $users = '{}';
            } else {
                $users = '{"0":{"id_user":"' . auth()->user()->id . '", "name_user":"' . auth()->user()->second_name . ' ' . auth()->user()->name . ' ' . auth()->user()->patronymic . '"}}';
            }
            $model->users_program = $users;
            $model->save();

            $result = Program::where('id_program', $model->id_program)->leftjoin('stypes', 'programs.id_stype', '=', 'stypes.id_stype')->leftjoin('cikls', 'programs.id_cikl', '=', 'cikls.id_cikl')->leftjoin('creates', 'programs.id_create', '=', 'creates.id_create')->leftjoin('statuses','programs.id_status','=','statuses.id_status')->first();
            $result->date1 = ukr_date(date("d F Y", strtotime($result->created_at)));            
            $result->res = $res;
        } else {
            $result = (object)['res'=>$res, 'message'=>'Некоректна кількість галузей для обраного типу предмету'];
        }

        return json_encode($result);
    }

    /* Просмотр программы */
    public function program_delete(Request $request)
    {
        $id_program = $request->input('id_program') ? (int)$request->input('id_program') : '0';
        $model = Program::where('id_program', $id_program)->delete();
        return json_encode('Ok');
    }


    /* Выбор программ для прикрепления к планам */
    public function program_get_all(Request $request)
    {
        $id_cikl = $request->input('id_cikl') ? (int)$request->input('id_cikl') : 1;
        $id_stype = $request->input('id_stype') ? (int)$request->input('id_stype') : 1;
        if (Auth::check()) {
            $programs = Program::where('id_school', auth()->user()->id_school)->where('id_cikl',$id_cikl)->where('id_status',1)->where('id_stype',$id_stype)->where('id_ptype',3)->get();
        } else {
            $programs = new Program;
        }
        return json_encode( $programs );
    }


    public function new_program_from_typical(Request $request)
    {
        $model = Program::where('id_program', $request->input('id_program'))->first();
        $new_program = new Program;
        $new_program->name_program = $model->name_program;
        if ($request->input('destination')=='school'){
            $new_program->id_school = auth()->user()->id_school;
            $new_program->id_user = 0;
            $new_program->id_ptype = 3;
            $new_program->users_program = '{"0":{"id_user":"' . (auth()->user()->id) . '", "name_user":"' . (auth()->user()->second_name) . ' ' . (auth()->user()->name). ' ' . (auth()->user()->patronymic) . '"}}';
        } else {
            $new_program->id_school = 0;
            $new_program->id_user = auth()->user()->id;
            $new_program->id_ptype = 4;
            $new_program->users_program = '{}';
        }
        $new_program->id_stype = $model->id_stype;
        $new_program->id_cikl = $model->id_cikl;
        $new_program->id_create = 1;
        $new_program->hours_program = $model->hours_program;
        $new_program->content_program = $model->content_program;
        $new_program->galuzes_program = $model->galuzes_program;
        $new_program->save();
        return json_encode( 'Ok' );
    }


    public function program_popup(Request $request)
    {
        $program = Program::where('id_program',(int)$request->input('id_program'))->leftjoin('cikls','programs.id_cikl','=','cikls.id_cikl')->first();
        if ($program) {
            $result['program'] = $program;
        //     $result['galuzes'] = Galuz::get();
        //     $result['classes'] = SchoolClass::where('id_cikl', $plan->id_cikl)->get();
        } else {$result = 'Access denied';}
        return json_encode($result);
    }

}
