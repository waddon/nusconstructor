<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Category;
use App\Post;

class PostController extends MainController
{

    public function index()
    {
        //
    }

    public function get_post()
    {
        $id_post = isset($_GET['id_post']) ? (int)$_GET['id_post'] : 1;
        $post = Post::where('id_post', $id_post)->first();
        if (!$post) $post = new Post;
        return json_encode($post);
    }
}