<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\School;
use App\SchoolUser;
use App\Menu;

class MainController extends Controller
{

    public $data = [];

    protected $projects;


    public function __construct()
    {
        $this->data['topmenu'] = Menu::getMenu(['id_menu'=>1, 'id'=>'loginform-buttons']);
        $this->data['togglemenu'] = Menu::getToggleMenu(['id_menu'=>2, 'class'=>'sidebar-menu']);
    }

    public function create_breadcrumbs($bread = [])
    {
        $result = '';
        foreach ($bread as $key => $value) {
            if ($value['permalink']) {
                $result = '<span>' . $this->separator . '</span>' . '<a href="' . $value['permalink'] . '">' . $value['name'] . '</a>' . $result;
            } else {
                $result = '<span>' . $this->separator . '</span>' . $value['name'] . $result;
            }            
        }
        if (strlen($result) > 0) {
            $result = '<a href="' . route('home') . '"><i class="fa fa-home" aria-hidden="true"></i></a>' . $result;
        }

        return $result;
    }



    public function ukr_date($string='')
    {
        $string = str_replace('Sunday', 'Неділля', $string);
        $string = str_replace('Monday', 'Понеділок', $string);
        $string = str_replace('Tuesday', 'Вівторок', $string);
        $string = str_replace('Wednesday', 'Середа', $string);
        $string = str_replace('Thursday', 'Четвер', $string);
        $string = str_replace('Friday', 'П`ятниця', $string);
        $string = str_replace('Sunday', 'Суббота', $string);

        $string = str_replace('January', 'січня', $string);
        $string = str_replace('February', 'лютого', $string);
        $string = str_replace('March', 'березня', $string);
        $string = str_replace('April', 'квітня', $string);
        $string = str_replace('May', 'травня', $string);
        $string = str_replace('June', 'червня', $string);
        $string = str_replace('July', 'липня', $string);
        $string = str_replace('August', 'серпня', $string);
        $string = str_replace('September', 'вересня', $string);
        $string = str_replace('October', 'жовтня', $string);
        $string = str_replace('November', 'листопада', $string);
        $string = str_replace('December', 'грудня', $string);
        return $string;
    }

    public function directorInfo($id=0)
    {
        $school = School::where('supervisor_school', $id);
        $count_new_users = SchoolUser::where('id_school',$school->id_school)->where('id_statususer',2)->count();
        return $count_new_users;
    }
}

