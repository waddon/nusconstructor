<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\School;
use App\SchoolType;
use App\Package;
use App\Plan;
use App\Program;

class ApproveController extends Controller
{

    public $data = [];

    public function __construct()
    {
        # code...
    }

    public function index()
    {

        $this->data['current_school_regions'] = '';

        $this->data['school_regions'] = School::select('region_school')->distinct()->orderBy('region_school', 'asc')->get();

        $this->data['packages'] = (object)[];

        return view('dashboard.approve.index', $this->data);
    }


    public function filter(Request $request)
    {

        $school_region = $request['school_region'] ? $request['school_region'] : '';

        $this->data['current_school_regions'] = $school_region;
        $this->data['school_regions'] = School::select('region_school')->distinct()->orderBy('region_school', 'asc')->get();

        $this->data['packages'] = Package::where('packages.id_status', 6)->leftjoin('schools','packages.id_school','=','schools.id_school')->where('region_school', $school_region)->leftjoin('statuses','packages.id_status','=','statuses.id_status')->get();

        return view('dashboard.approve.index', $this->data);
    }


    public function package_approve(Request $request)
    {
        $result = [];
        $id_package = $request->input('id_package') ? (int)$request->input('id_package') : 0;
        $id_status = $request->input('id_status') ? (int)$request->input('id_status') : 8;
        $critical_package = $request->input('critical_package') ? $request->input('critical_package') : '';
        $package = Package::findOrFail($id_package);
        if ($package){

            $plan_in_package = json_decode($package->plans_package);
            /* Устанавливае статусы всем планам и программам */
            foreach ($plan_in_package as $plan) {
                //$current_plan = Plan::where('id_plan', $plan->id_plan)->first();
                $current_plan = Plan::findOrFail($plan->id_plan);
                if ($current_plan){
                    $programs_in_plan = json_decode($current_plan->content_plan);
                    foreach ($programs_in_plan as $program) {
                        //$current_program = Program::where('id_program', $program->program)->first();
                        $current_program = Program::findOrFail($program->program);
                        if ($current_program){
                            $current_program->id_status = $id_status;
                            $current_program->save();
                        }
                    }
                    $current_plan->id_status = $id_status;
                    $current_plan->save();
                }
            }

            $package->id_status = $id_status;
            $package->approve_user = auth()->user()->id;
            $package->critical_package = $critical_package;
            $package->save();

        }

        $result['package'] = $package;
        return json_encode($result);
    }

}
