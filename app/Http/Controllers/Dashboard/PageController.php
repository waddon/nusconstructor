<?php

namespace App\Http\Controllers\Dashboard;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class PageController extends AdminController
{
    public function index()
    {
        $pages = Page::all();

        return view('dashboard.pages.index', compact('pages'));
    }

    public function create()
    {
        return view('dashboard.pages.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_page' => 'required',
            'slug_page' => 'required',
            'content_page' => 'required'
        ]);

        $page = new Page;
        $page->name_page = $request['name_page'];
        $page->slug_page = $request['slug_page'];
        $page->content_page  = $request['content_page'];

        $page->save();

        return redirect()->route('pages.index')
            ->with('flash_message',
             'Страница '.$page->name_page.' створен!');
    }

    public function edit($page_id)
    {
        $page = Page::findOrFail($page_id);

        return view('dashboard.pages.edit', compact('page'));
    }

    public function update(Request $request, $page_id)
    {
        $page = Page::findOrFail($page_id);
        $this->validate($request, [
            'name_page' => 'required',
            'slug_page' => 'required',
            'content_page' => 'required'
        ]);

        $page->name_page = $request['name_page'];
        $page->slug_page = $request['slug_page'];
        $page->content_page  = $request['content_page'];
        $page->save();

        return redirect()->route('pages.index')
            ->with('flash_message',
             'Страница '.$page->name_page.' оновлен!');
    }

    public function destroy($page_id)
    {
        $page = Page::findOrFail($page_id); 
        $page->delete();

        return redirect()->route('pages.index')
            ->with('flash_message',
             'Страница '.$page->name_page.' вилучений!');
    }
}
