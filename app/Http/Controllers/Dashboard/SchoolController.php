<?php

namespace App\Http\Controllers\Dashboard;

use App\School;
use App\SchoolType;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class SchoolController extends AdminController
{
    public function index()
    {
        $school_regions = School::select('region_school')->distinct()->orderBy('region_school', 'asc')->get();

        return view('dashboard.schools.index', compact('school_regions'));
    }

    public function filter(Request $request)
    {
        $school_region = $request['school_region'];
        $schools = School::where('region_school', $school_region)->get();
        $school_regions = School::select('region_school')->distinct()->orderBy('region_school', 'asc')->get();

        return view('dashboard.schools.index', compact('schools', 'school_regions'));
    }

    public function create()
    {
        $school_regions = School::select('region_school')
                            ->distinct()->orderBy('region_school', 'asc')->get()
                            ->pluck('region_school', 'region_school');
        $school_types = SchoolType::all()->pluck('name_schooltype', 'id_schooltype')->prepend('--', 0);

        return view('dashboard.schools.create', compact('school_regions', 'school_types'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_school' => 'required',
            'region_school' => 'required',
            'adress_school' => 'required'
        ]);

        $school = new School;
        $school->name_school = $request['name_school'];
        $school->region_school  = $request['region_school'];
        $school->adress_school  = $request['adress_school'];
        $school->id_schooltype = $request['id_schooltype'];
        if (isset($request['supervisor_school']) && $request['supervisor_school'] != null) {
            $school->supervisor_school = $request['supervisor_school'];
        } else {
            $school->supervisor_school = 0;
        }
        $school->save();

        return redirect()->route('schools.index')
            ->with('flash_message',
             'Школа '.$school->name_example.' створена!');
    }

    public function edit($school_id)
    {
        $school = School::findOrFail($school_id);
        $school_regions = School::select('region_school')
                            ->distinct()->orderBy('region_school', 'asc')->get()
                            ->pluck('region_school', 'region_school');
        $school_types = SchoolType::all()->pluck('name_schooltype', 'id_schooltype')->prepend('--', 0);

        return view('dashboard.schools.edit', compact('school', 'school_regions', 'school_types'));
    }

    public function update(Request $request, $school_id)
    {
        $school = School::findOrFail($school_id);
        $this->validate($request, [
            'name_school' => 'required',
            'region_school' => 'required',
            'adress_school' => 'required'
        ]);

        $school->name_school = $request['name_school'];
        $school->region_school  = $request['region_school'];
        $school->adress_school  = $request['adress_school'];
        $school->id_schooltype = $request['id_schooltype'];
        if (isset($request['supervisor_school']) && $request['supervisor_school'] != null) {
            $school->supervisor_school = $request['supervisor_school'];
        } else {
            $school->supervisor_school = '';
        }
        $school->save();

        return redirect()->route('schools.index')
            ->with('flash_message',
             'Школа '.$school->name_example.' оновлена!');
    }

    public function destroy($school_id)
    {
        $school = School::findOrFail($school_id); 
        $school->delete();

        return redirect()->route('schools.index')
            ->with('flash_message',
             'Школа '.$school->name_example.' вилучена!');
    }
}
