<?php

namespace App\Http\Controllers\Dashboard;

use App\Stype;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class STypeController extends AdminController
{
    public function index()
    {
        $stypes = Stype::all();

        return view('dashboard.stypes.index', compact('stypes'));
    }

    public function create()
    {
        return view('dashboard.stypes.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_stype' => 'required'
        ]);

        $stype = new Stype;
        $stype->name_stype = $request['name_stype'];
        $stype->save();

        return redirect()->route('stypes.index')
            ->with('flash_message',
             'Цикл '.$stype->name_stype.' створен!');
    }

    public function edit($stype_id)
    {
        $stype = Stype::findOrFail($stype_id);

        return view('dashboard.stypes.edit', compact('stype'));
    }

    public function update(Request $request, $stype_id)
    {
        $stype = Stype::findOrFail($stype_id);
        $this->validate($request, [
            'name_stype' => 'required'
        ]);

        $stype->name_stype = $request['name_stype'];
        $stype->save();

        return redirect()->route('stypes.index')
            ->with('flash_message',
             'Цикл '.$stype->name_stype.' оновлен!');
    }

    public function destroy($stype_id)
    {
        $stype = Stype::findOrFail($stype_id); 
        $stype->delete();

        return redirect()->route('stypes.index')
            ->with('flash_message',
             'Цикл '.$stype->name_stype.' вилучений!');
    }
}
