<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;

class UserController extends Controller
{

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
    //Get all users and pass it to the view
        $users = User::all(); 
        return view('dashboard.users.index')->with('users', $users);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
    //Get all roles and pass it to the view
        $roles = Role::get();
        return view('dashboard.users.create', ['roles'=>$roles]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        if(!Auth::user()->hasRole('admin') && in_array('1', $request['roles'])) {
            return redirect()->back()->with('error_message', 'Вы не можете создавать администраторов')->withInput();
        }

        $this->validate($request, [
            'email' => 'required|string|email|max:255|unique:table_users',
            'name' => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'patronymic' => 'required|string|max:255',
            // 'id_role' => 'required|numeric',
            // 'id_position' => 'required|numeric',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::create($request->only('email', 'name', 'second_name', 'patronymic', /*'id_role', 'id_position',*/ 'phone', 'password'));

        $roles = $request['roles']; //Retrieving the roles field checking if a role was selected
        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();            
                $user->assignRole($role_r); //Assigning role to user
            }
        }        
    //Redirect to the users.index view and display message
        return redirect()->route('users.index')
            ->with('flash_message',
             'Користувач успішно додан.');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function show(Request $request, $id)
    {
        $newuser = User::find($id);
        if ($newuser){
            Auth::login($newuser);
        }
        //return redirect()->route('home')
        return redirect('users')
            ->with('flash_message', 'Вы авторизовались под пользователем ' . auth()->user()->name);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $user = User::findOrFail($id); //Get user with specified id
        $roles = Role::get(); //Get all roles

        return view('dashboard.users.edit', compact('user', 'roles')); //pass user and roles data to view

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->hasRole('admin') && in_array('1', $request['roles'])) {
            return redirect()->back()->with('error_message', 'Вы не можете создавать администраторов')->withInput();
        }
        
        $user = User::findOrFail($id);

        if (!Auth::user()->hasRole('admin') && $user->hasRole('admin')) {
            return redirect()->route('users.index')->with('error_message', 'Вы не можете редактировать администраторов')->withInput();
        }

        //Validate name, email and password fields  
        $this->validate($request, [
            'email'=>'required|email|unique:table_users,email,'.$id,
            'name' => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'patronymic' => 'required|string|max:255',
            // 'id_role' => 'required|numeric',
            // 'id_position' => 'required|numeric',
        ]);
        $input = $request->only(['email', 'name', 'second_name', 'patronymic', 'phone']);
        $roles = $request['roles'];
        $user->fill($input)->save();

        if (isset($roles)) {        
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        } else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect()->route('users.index')
            ->with('flash_message',
             'User successfully edited.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //Find a user with a given id and delete
        $user = User::findOrFail($id);

        if (!Auth::user()->hasRole('admin') && $user->hasRole('admin')) {
            return redirect()->route('users.index')->with('error_message', 'Вы не можете редактировать администраторов');
        }

        $user->delete();

        return redirect()->route('users.index')
            ->with('flash_message',
             'User successfully deleted.');
    }
}