<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Umenie as Skill;
use App\Galuz as Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class SkillController extends AdminController
{
    public function index()
    {
        $skills = Skill::all();

        return view('dashboard.skills.index', compact('skills'));
    }

    public function create()
    {
        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.skills.create', compact('branches'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'skill_name' => 'required',
            'skill_key' => 'required',
            'branch_id' => 'required'
        ]);

        $skill = new Skill;
        $skill->name_umenie = $request['skill_name'];

        if ($request['branch_id'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть галузь');
        }

        $skill->key_zor = $request['skill_key'];
        $skill->id_galuz  = $request['branch_id'];
        $skill->save();

        return redirect()->route('skills.index')
            ->with('flash_message',
             'уміння' . $skill->skill_name . ' створено!');
    }

    public function edit($skill_id)
    {
        $skill = Skill::findOrFail($skill_id);

        if (!Auth::user()->hasAnyPermission($skill->getSkillBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }

        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.skills.edit', compact('skill', 'branches'));
    }

    public function update(Request $request, $skill_id)
    {
        $skill = Skill::findOrFail($skill_id);

        if (!Auth::user()->hasAnyPermission($skill->getSkillBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }

        $this->validate($request, [
            'name_umenie' => 'required',
            'key_zor' => 'required',
            'id_galuz' => 'required'
        ]);

        $skill->name_umenie = $request['name_umenie'];

        if ($request['id_galuz'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть галузь');
        }
        
        $skill->id_galuz = $request['id_galuz'];
        $skill->key_zor = $request['key_zor'];
        $skill->save();

        return redirect()->route('skills.index')
            ->with('flash_message',
             'Цикл '.$skill->name_umenie.' оновлен!');
    }

    public function destroy($skill_id)
    {
        $skill = Skill::findOrFail($skill_id);
        
        if (!Auth::user()->hasAnyPermission($skill->getSkillBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }

        $skill->delete();

        return redirect()->route('skills.index')
            ->with('flash_message',
             'Уміння '.$skill->name_example.' вилучено!');
    }
}
