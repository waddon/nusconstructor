<?php

namespace App\Http\Controllers\Dashboard;

use App\Post;
use App\Cat;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class PostController extends AdminController
{
    public function index()
    {
        $posts = Post::all();

        return view('dashboard.posts.index', compact('posts'));
    }

    public function create()
    {
        $cats = Cat::all()->pluck('name_cat', 'id_cat')->prepend('--', 0);

        return view('dashboard.posts.create', compact('cats'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_post' => 'required',
            'slug_post' => 'required|unique:posts',
            'id_cat' => 'required',
            'content_post' => 'required',
        ]);

        $post = new Post;
        $post->name_post = $request['name_post'];
        $post->slug_post  = $request['slug_post'];
        

        if (isset($request['id_cat']) && $request['id_cat'] == 0) {
            return back()->withInput()->with('error_message', 'Выбирете категорию');
        } else {
            $post->id_cat  = $request['id_cat'];
        }

        $post->content_post  = $request['content_post'];
        $post->excerpt_post  = $request['excerpt_post'];

        if (isset($request['excerpt_post']) && $request['link_post'] !== null) {
            $post->excerpt_post = $request['excerpt_post'];
        } else {
            $post->excerpt_post = '';
        }

        if (isset($request['download_post'])) {
            $post->download_post = $request['download_post'];
        } else {
            $post->download_post = 0;
        }

        if (isset($request['thumb_post']) && $request['link_post'] !== null) {
            $post->thumb_post = $request['thumb_post'];
        } else {
            $post->thumb_post = '';
        }

        if (isset($request['link_post']) && $request['link_post'] !== null) {
            $post->link_post = $request['link_post'];
        } else {
            $post->link_post = '';
        }

        $post->save();

        return redirect()->route('posts.index')
            ->with('flash_message',
             'Запис '.$post->name_post.' створен!');
    }

    public function edit($post_id)
    {
        $post = Post::findOrFail($post_id);
        $cats = Cat::all()->pluck('name_cat', 'id_cat')->prepend('--', 0);

        return view('dashboard.posts.edit', compact('post', 'cats'));
    }

    public function update(Request $request, $post_id)
    {
        $post = Post::findOrFail($post_id);
        $this->validate($request, [
            'name_post' => 'required',
            'slug_post' => 'required|unique:posts,slug_post,'.$post_id.',id_post',
            'id_cat' => 'required',
            // 'content_post' => 'required',
        ]);

        $post->name_post = $request['name_post'];
        $post->slug_post  = $request['slug_post'];
        

        if (isset($request['id_cat']) && $request['id_cat'] == 0) {
            return back()->withInput()->with('error_message', 'Выбирете категорию');
        } else {
            $post->id_cat  = $request['id_cat'];
        }

        if (isset($request['content_post']) && $request['content_post'] !== null) {
            $post->content_post = $request['content_post'];
        } else {
            $post->content_post = '';
        }

        if (isset($request['excerpt_post']) && $request['excerpt_post'] !== null) {
            $post->excerpt_post = $request['excerpt_post'];
        } else {
            $post->excerpt_post = '';
        }

        if (isset($request['download_post'])) {
            $post->download_post = $request['download_post'];
        } else {
            $post->download_post = 0;
        }

        if (isset($request['thumb_post']) && $request['link_post'] !== null) {
            $post->thumb_post = $request['thumb_post'];
        } else {
            $post->thumb_post = '';
        }

        if (isset($request['link_post']) && $request['link_post'] !== null) {
            $post->link_post = $request['link_post'];
        } else {
            $post->link_post = '';
        }

        $post->save();

        return redirect()->route('posts.index')
            ->with('flash_message',
             'Запис '.$post->name_post.' оновлен!');
    }

    public function destroy($post_id)
    {
        $post = Post::findOrFail($post_id); 
        $post->delete();

        return redirect()->route('posts.index')
            ->with('flash_message',
             'Запис '.$post->name_post.' вилучений!');
    }
}
