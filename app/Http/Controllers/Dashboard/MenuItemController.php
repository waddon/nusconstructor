<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\MenuItem;

class MenuItemController extends Controller
{

    public $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $menuitems = (object)[];
        $id_menu = $request->input('id_menu') ? (int)$request->input('id_menu') : 1;
        $this->data['menus'] = Menu::all()->pluck('name_menu', 'id_menu');
        $this->data['menuitems'] = MenuItem::where('id_menu',$id_menu)->pluck('name_menuitem', 'id_menuitem')->prepend('--', 0);

        $treemenuitems = MenuItem::where('id_menu',$id_menu)->where('parent_menuitem',0)->orderBy('sort_menuitem', 'asc', 'id_menuitem', 'asc')->get();
        //$childmenuitems = MenuItem::where('id_menu',$id_menu)->where('parent_menuitem','!=',0)->get();
        foreach ($treemenuitems as $treemenuitem) {
            $menuitems->{$treemenuitem->id_menuitem} = (object)[
                    'id_menuitem'=>$treemenuitem->id_menuitem,
                    'parent_menuitem'=>$treemenuitem->parent_menuitem,
                    'name_menuitem'=>$treemenuitem->name_menuitem,
                    'url_menuitem'=>$treemenuitem->url_menuitem,
                    'paramurl_menuitem'=>$treemenuitem->paramurl_menuitem,
                    'icon_menuitem'=>$treemenuitem->icon_menuitem,
                    'sort_menuitem'=>$treemenuitem->sort_menuitem,
                    'sub'=>(object)[]
                ];

            $childmenuitems = MenuItem::where('id_menu',$id_menu)->where('parent_menuitem', $treemenuitem->id_menuitem)->orderBy('sort_menuitem', 'asc', 'id_menuitem', 'asc')->get();
            foreach ($childmenuitems as $childmenuitem) {
                $menuitems->{$treemenuitem->id_menuitem}->sub->{$childmenuitem->id_menuitem} = (object)[
                    'id_menuitem'=>$childmenuitem->id_menuitem,
                    'parent_menuitem'=>$childmenuitem->parent_menuitem,
                    'name_menuitem'=>$childmenuitem->name_menuitem,
                    'url_menuitem'=>$childmenuitem->url_menuitem,
                    'paramurl_menuitem'=>$childmenuitem->paramurl_menuitem,
                    'icon_menuitem'=>$childmenuitem->icon_menuitem,
                    'sort_menuitem'=>$childmenuitem->sort_menuitem,
                    'sub'=>(object)[]
                ];

                $grandchildmenuitems = MenuItem::where('id_menu',$id_menu)->where('parent_menuitem', $childmenuitem->id_menuitem)->orderBy('sort_menuitem', 'asc', 'id_menuitem', 'asc')->get();
                foreach ($grandchildmenuitems as $grandchildmenuitem) {
                    $menuitems->{$treemenuitem->id_menuitem}->sub->{$childmenuitem->id_menuitem}->sub->{$grandchildmenuitem->id_menuitem} = (object)[
                        'id_menuitem'=>$grandchildmenuitem->id_menuitem,
                        'parent_menuitem'=>$grandchildmenuitem->parent_menuitem,
                        'name_menuitem'=>$grandchildmenuitem->name_menuitem,
                        'url_menuitem'=>$grandchildmenuitem->url_menuitem,
                        'paramurl_menuitem'=>$grandchildmenuitem->paramurl_menuitem,
                        'sort_menuitem'=>$grandchildmenuitem->sort_menuitem,
                        'icon_menuitem'=>$grandchildmenuitem->icon_menuitem,
                    ];
                }

            }
        }
        $this->data['treemenuitems'] = $menuitems;

        return view('dashboard.menuitems.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //Validate name field
        $this->validate($request, [
            'name_menuitem' => 'required',
            'id_menu' => 'required|min:1',
            ]
        );

        if ($request['id_menu'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть меню');
        }


        $menuitem = new MenuItem;
        $menuitem->id_menu  = $request['id_menu'];
        $menuitem->name_menuitem = $request['name_menuitem'] ? $request['name_menuitem'] : '';
        $menuitem->url_menuitem = $request['url_menuitem'] ? $request['url_menuitem'] : '';
        $menuitem->paramurl_menuitem = $request['paramurl_menuitem'] ? $request['paramurl_menuitem'] : '';
        $menuitem->icon_menuitem = $request['icon_menuitem'] ? $request['icon_menuitem'] : '';
        $menuitem->sort_menuitem = MenuItem::where('id_menu', $menuitem->id_menu)->max('sort_menuitem')+1;
        $menuitem->parent_menuitem = $request['parent_menuitem'];

        $menuitem->save();

        return redirect()->route('menuitems.index',['id_menu'=>$menuitem->id_menu])
            ->with('flash_message',
             'Пункт ' . $menuitem->name_menuitem . ' створено!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_menuitem)
    {
        $menus = Menu::all()->pluck('id_menu', 'name_menu')->prepend('--', null);
        $menuitem = MenuItem::findOrFail($id_menuitem);

        return view('dashboard.menuitems.edit', compact('menuitem', 'menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_menuitem)
    {
        $menuitem = MenuItem::findOrFail($id_menuitem);

        if ($request->input('action')){
            if ($request->input('action') == 'up'){
                $presort = MenuItem::where('id_menu', $menuitem->id_menu)->where('sort_menuitem','<', $menuitem->sort_menuitem )->max('sort_menuitem');
            } else {
                $presort = MenuItem::where('id_menu', $menuitem->id_menu)->where('sort_menuitem','>', $menuitem->sort_menuitem )->min('sort_menuitem');
            }
            if ($presort){
                $pre_menuitem = MenuItem::where('id_menu', $menuitem->id_menu)->where('sort_menuitem', $presort )->first();
                $pre_menuitem->sort_menuitem = $menuitem->sort_menuitem;
                $pre_menuitem->save();
                $menuitem->sort_menuitem = $presort;
            }            
        } else {
            $menuitem->name_menuitem = $request['name_menuitem'] ? $request['name_menuitem'] : '';
            $menuitem->url_menuitem = $request['url_menuitem'] ? $request['url_menuitem'] : '';
            $menuitem->paramurl_menuitem = $request['paramurl_menuitem'] ? $request['paramurl_menuitem'] : '';
            $menuitem->icon_menuitem = $request['icon_menuitem'] ? $request['icon_menuitem'] : '';
            $menuitem->parent_menuitem = $request['parent_menuitem'] ? (int)$request['parent_menuitem'] : 0;
        }
        
        $menuitem->save();

        return redirect()->route('menuitems.index',['id_menu'=>$menuitem->id_menu])
            ->with('flash_message',
             'Пункт '.$menuitem->name_menuitem.' оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_menuitem)
    {
        //Find a user with a given id and delete
        $menuitem = MenuItem::findOrFail($id_menuitem); 
        if ($menuitem) {
            /* При удалении сдвигать все дочерние пункты удаляемого элемента на удин уровень вверх */
            MenuItem::where('parent_menuitem',$menuitem->id_menuitem)->update(['parent_menuitem'=>$menuitem->parent_menuitem]);
        }
        $menuitem->delete();

        return redirect()->route('menuitems.index',['id_menu'=>$menuitem->id_menu])
            ->with('flash_message',
             'Пункт '.$menuitem->name_menuitem.' вилучено!');
    }

    /**
     * Chevron-up
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function chevronup($id_menuitem)
    {

        return redirect()->route('menuitems.index',['id_menu'=>$menuitem->id_menu])
            ->with('flash_message',
             'Пункт '.$menuitem->name_menuitem.' пересунуто вгору!');
    }

}
