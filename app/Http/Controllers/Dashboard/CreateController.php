<?php

namespace App\Http\Controllers\Dashboard;

use App\Create;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class CreateController extends AdminController
{
    public function index()
    {
        $creates = Create::all();

        return view('dashboard.creates.index', compact('creates'));
    }

    public function create()
    {
        return view('dashboard.creates.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_create' => 'required'
        ]);

        $create = new Create;
        $create->name_create = $request['name_create'];
        $create->save();

        return redirect()->route('creates.index')
            ->with('flash_message',
             'Роль '.$create->name_create.' створена!');
    }

    public function edit($example_id)
    {
        $create = Create::findOrFail($example_id);
        return view('dashboard.creates.edit', compact('create'));
    }

    public function update(Request $request, $example_id)
    {
        $create = Create::findOrFail($example_id);
        $this->validate($request, [
            'name_create' => 'required'
        ]);

        $create->name_create = $request['name_create'];
        $create->save();

        return redirect()->route('creates.index')
            ->with('flash_message',
             'Роль '.$create->name_create.' оновленa!');
    }

    public function destroy($example_id)
    {
        $create = Create::findOrFail($example_id); 
        $create->delete();

        return redirect()->route('creates.index')
            ->with('flash_message',
             'Роль '.$create->name_create.' вилученa!');
    }
}
