<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Cikl as Cycle;
use App\Galuz as Branch;
use App\Galuznote as BranchNote;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class BranchNoteController extends AdminController
{
    public function index()
    {
        $branch_notes = BranchNote::all();

        return view('dashboard.branchnotes.index', compact('branch_notes'));
    }

    public function create()
    {
        $cycles = Cycle::all()->pluck('name_cikl', 'id_cikl')->prepend('--', 0);
        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.branchnotes.create', compact('cycles', 'branches'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'text_branchnote' => 'required',
        ]);

        $branch_note = new BranchNote;
        $branch_note->text_galuznote = $request['text_branchnote'];

        if ($request['cycle_id'] <= 0) {
            return redirect()->back()
            ->with('error_message',
             'Виберіть цикл')->withInput();
        }

        if ($request['branch_id'] <= 0) {
            return redirect()->back()
            ->with('error_message',
             'Виберіть галузь')->withInput();
        }

        $branch_note->id_cikl  = $request['cycle_id'];
        $branch_note->id_galuz  = $request['branch_id'];
        $branch_note->save();

        return redirect()->route('branchnotes.index')
            ->with('flash_message',
             'Опис'.$branch_note->id_galuznote.' створен!');
    }

    public function edit($branchnote_id)
    {
        $branch_note = BranchNote::findOrFail($branchnote_id);

        if (!Auth::user()->hasAnyPermission($branch_note->getBranchNoteBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }

        $cycles = Cycle::all()->pluck('name_cikl', 'id_cikl')->prepend('--', 0);
        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.branchnotes.edit', compact('branch_note', 'cycles', 'branches'));
    }

    public function update(Request $request, $branchnote_id)
    {
        $branch_note = BranchNote::findOrFail($branchnote_id);

        if (!Auth::user()->hasAnyPermission($branch_note->getBranchNoteBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }

        $this->validate($request, [
            'text_galuznote' => 'required'
        ]);
        $branch_note->text_galuznote = $request['text_galuznote'];
        
        if ($request['id_cikl'] <= 0) {
            return redirect()->back()
            ->with('error_message',
             'Виберіть цикл')->withInput();
        }

        if ($request['id_galuz'] <= 0) {
            return redirect()->back()
            ->with('error_message',
             'Виберіть галузь')->withInput();
        }

        $branch_note->id_cikl = $request['id_cikl'];
        $branch_note->id_galuz = $request['id_galuz'];
        $branch_note->save();

        return redirect()->route('branchnotes.index')
            ->with('flash_message',
             'Опис '.$branch_note->id_galuznote.' оновлен!');
    }

    public function destroy($branchnote_id)
    {
        $branch_note = BranchNote::findOrFail($branchnote_id);

        if (!Auth::user()->hasAnyPermission($branch_note->getBranchNoteBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }

        $branch_note->delete();

        return redirect()->route('branchnotes.index')
            ->with('flash_message',
             'Опис '.$branch_note->id_galuznote.' вилучений!');
    }
}
