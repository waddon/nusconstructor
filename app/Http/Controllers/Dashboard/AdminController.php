<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\User;
use Auth;
use App\Http\Controllers\Controller;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;

class AdminController extends Controller 
{ 
    public function index()
    {
        return view('dashboard.index', [
                        'user' => Auth::user(),
                    ]);
    }

}