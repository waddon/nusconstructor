<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\MenuItem;

class MenuController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::all();

        return view('dashboard.menus.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.menus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name_menu' => 'required',
        ]);

        $menu = new Menu;
        $menu->name_menu = $request['name_menu'];
        $menu->save();

        return redirect()->route('menus.index')
            ->with('flash_message',
             'Меню '.$menu->name_menu.' створено!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_menu)
    {
        $menu = Menu::findOrFail($id_menu);

        return view('dashboard.menus.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_menu)
    {
        $menu = Menu::findOrFail($id_menu);
        $this->validate($request, [
            'name_menu' => 'required',
        ]);
        $menu->name_menu = $request['name_menu'];
        $menu->save();

        return redirect()->route('menus.index')
            ->with('flash_message',
             'Меню '.$menu->name_menu.' оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_menu)
    {
        $menu = Menu::findOrFail($id_menu);
        if ($menu){
            /* удаляем все пункты меню */
            MenuItem::where('id_menu',$menu->id_menu)->delete();
        }
        $menu->delete();

        return redirect()->route('menus.index')
            ->with('flash_message',
             'Меню '.$menu->name_menu.' вилучено!');
    }
}
