<?php

namespace App\Http\Controllers\Dashboard;

use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class StatusController extends AdminController
{
    public function index()
    {
        $statuses = Status::all();

        return view('dashboard.statuses.index', compact('statuses'));
    }

    public function create()
    {
        return view('dashboard.statuses.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_status' => 'required'
        ]);

        $status = new Status;
        $status->name_status = $request['name_status'];

        $status->save();

        return redirect()->route('statuses.index')
            ->with('flash_message',
             'Статус '.$status->name_status.' створен!');
    }

    public function edit($status_id)
    {
        $status = Status::findOrFail($status_id);

        return view('dashboard.statuses.edit', compact('status'));
    }

    public function update(Request $request, $status_id)
    {
        $status = Status::findOrFail($status_id);
        $this->validate($request, [
            'name_status' => 'required'
        ]);

        $status->name_status = $request['name_status'];
        $status->save();

        return redirect()->route('statuses.index')
            ->with('flash_message',
             'Статус '.$status->name_status.' оновлен!');
    }

    public function destroy($status_id)
    {
        $status = Status::findOrFail($status_id); 
        $status->delete();

        return redirect()->route('statuses.index')
            ->with('flash_message',
             'Статус '.$status->name_status.' вилучений!');
    }
}
