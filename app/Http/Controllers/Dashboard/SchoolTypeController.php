<?php

namespace App\Http\Controllers\Dashboard;

use App\SchoolType;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class SchoolTypeController extends AdminController
{
    public function index()
    {
        $schooltypes = SchoolType::all();

        return view('dashboard.schooltypes.index', compact('schooltypes'));
    }

    public function create()
    {
        return view('dashboard.schooltypes.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            
        ]);

        $schooltype = new SchoolType;
        $schooltype->name_schooltype = $request['name_schooltype'];

        $schooltype->save();

        return redirect()->route('schooltypes.index')
            ->with('flash_message',
             'Тип '.$schooltype->name_schooltype.' створен!');
    }

    public function edit($schooltype_id)
    {
        $schooltype = SchoolType::findOrFail($schooltype_id);

        return view('dashboard.schooltypes.edit', compact('schooltype'));
    }

    public function update(Request $request, $schooltype_id)
    {
        $schooltype = SchoolType::findOrFail($schooltype_id);
        $this->validate($request, [
            
        ]);

        $schooltype->name_schooltype = $request['name_schooltype'];
    
        $schooltype->save();

        return redirect()->route('schooltypes.index')
            ->with('flash_message',
             'Тип '.$schooltype->name_schooltype.' оновлен!');
    }

    public function destroy($schooltype_id)
    {
        $schooltype = SchoolType::findOrFail($schooltype_id); 
        $schooltype->delete();

        return redirect()->route('schooltypes.index')
            ->with('flash_message',
             'Тип '.$schooltype->name_schooltype.' вилучений!');
    }
}
