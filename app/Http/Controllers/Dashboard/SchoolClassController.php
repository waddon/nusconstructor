<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Cikl as Cycle;
use App\SchoolClass;

class SchoolClassController extends AdminController
{
    public function index()
    {
        $classes = SchoolClass::all();

        return view('dashboard.classes.index', compact('classes'));
    }

    public function create()
    {
    	$cycles = Cycle::all()->pluck('name_cikl', 'id_cikl')->prepend('--', 0);

        return view('dashboard.classes.create', compact('cycles'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'class_name' => 'required',
            ]
        );

        $class = new SchoolClass;
        $class->name_class = $request['class_name'];

        if ($request['cycle_id'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть цикл');
        }

        $class->id_cikl = $request['cycle_id'];
        $class->save();

        return redirect()->route('classes.index')
            ->with('flash_message',
             'Клас'.$class->name_class.' створен!');
    }

    public function edit($class_id)
    {
    	$class = SchoolClass::find($class_id);
    	$cycles = Cycle::all()->pluck('name_cikl', 'id_cikl')->prepend('--', 0);

    	return view('dashboard.classes.edit', compact('class', 'cycles'));
    }

    public function update(Request $request, $class_id)
    {
    	$class = SchoolClass::find($class_id);
    	$this->validate($request, [
            'name_class' => 'required',
        ]);
    	$class->name_class = $request['name_class'];

    	if ($request['id_cikl'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть цикл');
        }

    	$class->id_cikl = $request['id_cikl'];
    	$class->save();

    	return redirect()->route('classes.index')
    		->with('flash_message',
    		 'Клас '.$class->name_class.' оновлен');
    }

    public function destroy($class_id)
    {
        //Find a user with a given id and delete
        $class = SchoolClass::findOrFail($class_id); 
        $class->delete();

        return redirect()->route('classes.index')
            ->with('flash_message',
             'Цикл '.$class->name_class.' вилучений!');
    }
}