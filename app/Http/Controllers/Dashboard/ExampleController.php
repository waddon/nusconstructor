<?php

namespace App\Http\Controllers\Dashboard;

use App\Example_rus as Example;
use App\Example2_rus as Example2;
use App\Example3_rus as Example3;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class ExampleController extends AdminController
{
    public function index()
    {
        $var_examples = Example::all();

        return view('dashboard.examplesview.index', compact('var_examples'));
    }

    public function create()
    {
        $examples2 = Example2::all()->pluck('text', 'value')->prepend('--', null);
        $examples3 = Example3::all()->pluck('text', 'value')->prepend('--', null);

        return view('dashboard.examplesview.create', compact('examples2', 'examples3'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            
        ]);

        $var_example = new Example;
        $var_example->example_smth = $request['example_smth'];

        // if ($request['option_var'] <= 0) {
        //     return redirect()->back()
        //     ->with('flash_message',
        //      'Виберіть рівень');
        // }

        $var_example->example_smth  = $request['example_smth'];

        // if (isset($request['cycle_label']) && $request['marking_cikl'] !== null) {
        //     $var_example->marking_cikl = $request['cycle_label'];
        // }

        $var_example->save();

        return redirect()->route('examplesview.index')
            ->with('flash_message',
             'Цикл '.$var_example->name_example.' створен!');
    }

    public function edit($example_id)
    {
        $var_example = Example::findOrFail($example_id);
        $examples2 = Example2::all()->pluck('text', 'value')->prepend('--', null);
        $examples3 = Example3::all()->pluck('text', 'value')->prepend('--', null);

        return view('dashboard.examplesview.edit', compact('var_example', 'examples2', 'examples3'));
    }

    public function update(Request $request, $example_id)
    {
        $var_example = Example::findOrFail($example_id);
        $this->validate($request, [
            
        ]);

        $var_example->example_smth = $request['example_smth'];
        
        // if (isset($request['marking_cikl']) && $request['marking_cikl'] !== null) {
        //     $var_example->marking_cikl = $request['marking_cikl'];
        // }

        // if ($request['option_var'] <= 0) {
        //     return redirect()->back()
        //     ->with('flash_message',
        //      'Виберіть рівень');
        // }

        $var_example->example2_id = $request['example2_id'];
        $var_example->save();

        return redirect()->route('examplesview.index')
            ->with('flash_message',
             'Цикл '.$var_example->name_example.' оновлен!');
    }

    public function destroy($example_id)
    {
        $var_example = Example::findOrFail($example_id); 
        $var_example->delete();

        return redirect()->route('examplesview.index')
            ->with('flash_message',
             'Цикл '.$var_example->name_example.' вилучений!');
    }
}
