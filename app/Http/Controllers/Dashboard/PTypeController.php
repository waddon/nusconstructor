<?php

namespace App\Http\Controllers\Dashboard;

use App\Ptype;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class ptypeController extends AdminController
{
    public function index()
    {
        $ptypes = Ptype::all();

        return view('dashboard.ptypes.index', compact('ptypes'));
    }

    public function create()
    {
        return view('dashboard.ptypes.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_ptype' => 'required'
        ]);

        $ptype = new Ptype;
        $ptype->name_ptype = $request['name_ptype'];
        $ptype->save();

        return redirect()->route('ptypes.index')
            ->with('flash_message',
             'Цикл '.$ptype->name_ptype.' створен!');
    }

    public function edit($ptype_id)
    {
        $ptype = Ptype::findOrFail($ptype_id);

        return view('dashboard.ptypes.edit', compact('ptype'));
    }

    public function update(Request $request, $ptype_id)
    {
        $ptype = Ptype::findOrFail($ptype_id);
        $this->validate($request, [
            'name_ptype' => 'required'
        ]);

        $ptype->name_ptype = $request['name_ptype'];
        $ptype->save();

        return redirect()->route('ptypes.index')
            ->with('flash_message',
             'Цикл '.$ptype->name_ptype.' оновлен!');
    }

    public function destroy($ptype_id)
    {
        $ptype = Ptype::findOrFail($ptype_id); 
        $ptype->delete();

        return redirect()->route('ptypes.index')
            ->with('flash_message',
             'Цикл '.$ptype->name_ptype.' вилучений!');
    }
}
