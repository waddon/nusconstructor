<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Cikl as Cycle;
use App\Galuz as Branch;
use Illuminate\Http\Request;
use App\Zor as ExpectedOutcome;
use App\Http\Controllers\Dashboard\AdminController;

class ExpectedOutcomeController extends AdminController
{
    public function index()
    {
        $expected_outcomes = ExpectedOutcome::all();

        return view('dashboard.expected-outcomes.index', compact('expected_outcomes'));
    }

    public function create()
    {
        if (!Auth::user()->can('edit_content')) {
            abort('401');
        }

        $cycles = Cycle::all()->pluck('name_cikl', 'id_cikl')->prepend('--', 0);
        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.expected-outcomes.create', compact('cycles', 'branches'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->can('edit_content')) {
            abort('401');
        }

        $this->validate($request, [
            'expected_outcome_name' => 'required',
            'expected_outcome_key' => 'required',
            'cycle_id' => 'required',
            'branch_id' => 'required'
        ]);

        if ($request['cycle_id'] == 0) {
            return redirect()->back()->with('error_message',
                'Оберить цикл');
        }

        if ($request['branch_id'] == 0) {
            return redirect()->back()->with('error_message',
                'Оберить галузь');
        }

        $expected_outcome = new ExpectedOutcome;
        $expected_outcome->name_zor = $request['expected_outcome_name'];
        $expected_outcome->key_zor = $request['expected_outcome_key'];
        $expected_outcome->id_cikl = $request['cycle_id'];
        $expected_outcome->id_galuz = $request['branch_id'];
        $expected_outcome->save();

        return redirect()->route('expected-outcomes.index')
            ->with('flash_message', 
            'ЗОР '.$expected_outcome->name_zor.' створен!');
    }

    public function edit($expected_outcome_id)
    {
        $expected_outcome = ExpectedOutcome::find($expected_outcome_id);

        if (!Auth::user()->hasAnyPermission('edit_content', $expected_outcome->getExpectedOutcomeBranch()->marking_galuz)){
            abort('401');
        }

        $cycles = Cycle::all()->pluck('name_cikl', 'id_cikl')->prepend('--', 0);
        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.expected-outcomes.edit', compact('expected_outcome', 'cycles', 'branches'));
    }

    public function update(Request $request, $expected_outcome_id)
    {
        $expected_outcome = ExpectedOutcome::find($expected_outcome_id);
        
        if (!Auth::user()->hasAnyPermission('edit_content', $expected_outcome->getExpectedOutcomeBranch()->marking_galuz)){
            abort('401');
        }

        $this->validate($request, [
            'name_zor' => 'required',
            'key_zor' => 'required',
            'id_cikl' => 'required',
            'id_galuz' => 'required',
        ]);

        if ($request['id_cikl'] == 0) {
            return redirect()->back()->with('error_message',
                'Оберить цикл');
        }

        if ($request['id_galuz'] == 0) {
            return redirect()->back()->with('error_message',
                'Оберить галузь');
        }

        $expected_outcome->name_zor = $request['name_zor'];
        $expected_outcome->key_zor = $request['key_zor'];
        $expected_outcome->id_cikl = $request['id_cikl'];
        $expected_outcome->id_galuz = $request['id_galuz'];
        $expected_outcome->save();

        return redirect()->route('expected-outcomes.index')
            ->with('flash_message', 
            'ЗОР '.$expected_outcome->name_zor.' оновлений!');
    }

    public function destroy($expected_outcome_id)
    {
        if (!Auth::user()->can('edit_content')){
            abort('401');
        }

        $expected_outcome = ExpectedOutcome::find($expected_outcome_id);
        $expected_outcome->delete();

        return redirect()->route('expected-outcomes.index')
            ->with('flash_message', 
            'ЗОР '.$expected_outcome->name_zor.' вилучений!');
    }
}
