<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Subject;
use App\Galuz as Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class SubjectController extends AdminController
{
    public function index()
    {
        $subjects = Subject::all();

        return view('dashboard.subjects.index', compact('subjects'));
    }

    public function create()
    {
        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.subjects.create', compact('branches'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_subject' => 'required',
            'branch_id' => 'required'
        ]);

        $subject = new Subject;
        $subject->name_subject = $request['name_subject'];

        if ($request['branch_id'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть галузь');
        }

        $subject->id_galuz  = $request['branch_id'];
        $subject->save();
        return redirect()->route('subjects.index')
            ->with('flash_message',
             'Урок '.$subject->subject_name.' створен!');
    }

    public function edit($subject_id)
    {
        $subject = Subject::findOrFail($subject_id);

        if(!Auth::user()->hasAnyPermission($subject->getSubjectBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }

        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.subjects.edit', compact('subject', 'branches'));
    }

    public function update(Request $request, $subject_id)
    {
    	$subject = Subject::findOrFail($subject_id);

        if(!Auth::user()->hasAnyPermission($subject->getSubjectBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }

        $this->validate($request, [
            'name_subject' => 'required',
            'id_galuz' => 'required'
        ]);

        $subject->name_subject = $request['name_subject'];

        if ($request['id_galuz'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть рівень');
        }

        $subject->id_galuz = $request['id_galuz'];
        $subject->save();

        return redirect()->route('subjects.index')
            ->with('flash_message',
             'Урок '.$subject->subject_name.' оновлен!');
    }

    public function destroy($subject_id)
    {
        $subject = Subject::findOrFail($subject_id);

        if(!Auth::user()->hasAnyPermission($subject->getSubjectBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }
        
        $subject->delete();

        return redirect()->route('subjects.index')
            ->with('flash_message',
             'Урок '.$subject->subject_name.' вилучений!');
    }
}
