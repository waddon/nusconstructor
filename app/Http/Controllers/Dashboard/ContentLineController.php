<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Galuz as Branch;
use Illuminate\Http\Request;
use App\Zmist as ContentLine;
use App\Http\Controllers\Controller;

class ContentLineController extends Controller
{
    public function index()
    {
        $content_lines = ContentLine::all();

        return view('dashboard.content-lines.index', compact('content_lines'));
    }

    public function create()
    {
        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.content-lines.create', compact('branches'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'content_line_name' => 'required',
            'branch_id' => 'required',
            // 'content_line_text' => 'required'
        ]);

        if ($request['branch_id'] == 0) {
            return redirect()->back()->withInput()
            ->with('error_message',
             'Оберіть галузь');
        }

        if ($request['content_line_label'] == null) {
            $request['content_line_label'] = '';
        }

        if ($request['content_line_text'] == null) {
            $request['content_line_text'] = '';
        }

        $content_line = new ContentLine;
        $content_line->name_zmist = $request['content_line_name'];
        $content_line->marking_zmist = $request['content_line_label'];
        $content_line->id_galuz = $request['branch_id'];
        $content_line->text_zmist = $request['content_line_text'];
        $content_line->save();

        return redirect()->route('content-lines.index')
            ->with('flash_message',
             'Змістова лінія '.$content_line->name_zmist.' створена');
    }

    public function edit($content_line_id)
    {
        $content_line = ContentLine::find($content_line_id);

        if(!Auth::user()->hasAnyPermission($content_line->getContentLineBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }

        $branches = Branch::all()->pluck('name_galuz', 'id_galuz')->prepend('--', 0);

        return view('dashboard.content-lines.edit', compact('content_line', 'branches'));
    }

    public function update(Request $request, $content_line_id)
    {
        $content_line = ContentLine::find($content_line_id);

        if(!Auth::user()->hasAnyPermission($content_line->getContentLineBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }
        
        $this->validate($request, [
            'name_zmist' => 'required',
            'id_galuz' => 'required',
            // 'text_zmist' => 'required'
        ]);

        if ($request['marking_zmist'] == null) {
            $request['marking_zmist'] = '';
        }

        if ($request['text_zmist'] == null) {
            $request['text_zmist'] = '';
        }

        $content_line->name_zmist = $request['name_zmist'];
        $content_line->id_galuz = $request['id_galuz'];
        $content_line->text_zmist = $request['text_zmist'];
        $content_line->marking_zmist = $request['marking_zmist'];
        $content_line->save();

        return redirect()->route('content-lines.index')
            ->with('flash_message',
             'Змістова лінія '.$content_line->name_zmist.' оновлена');

    }

    public function destroy(Request $request, $content_line_id)
    {
        $content_line = ContentLine::find($content_line_id);

        if(!Auth::user()->hasAnyPermission($content_line->getContentLineBranch()->marking_galuz, 'edit_content')) {
            abort('401');
        }
        
        $content_line->delete();

        return redirect()->route('content-lines.index')
            ->with('flash_message',
             'Змістова лінія '.$content_line->name_zmist.' видалена');
    }
}
