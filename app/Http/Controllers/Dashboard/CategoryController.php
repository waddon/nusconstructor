<?php

namespace App\Http\Controllers\Dashboard;

use App\Cat as Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class CategoryController extends AdminController
{
    public function index()
    {
        $categories = Category::all();

        return view('dashboard.categories.index')->with('categories', $categories);
    }

    public function show($id)
    {
        return redirect('category.index');
    }

    public function create()
    {
        return view('dashboard.categories.create');
    }

    public function store(Request $request)
    {
    //Validate name field
        $this->validate($request, [
            'name_cat' => 'required|unique:cats,name_cat',
            'slug_cat' => 'required|alpha_dash|unique:cats,slug_cat',
            ]
        );
        
        $category = new Category;
        $category->name_cat = $request['name_cat'];
        $category->slug_cat = $request['slug_cat'];

        $category->save();

        return redirect()->route('categories.index')
            ->with('flash_message',
             'Категорія' . $category->name_cat . ' створена!'); 
    }

    public function edit($id_cat)
    {
        $category = Category::findOrFail($id_cat);

        return view('dashboard.categories.edit', compact('category'));
    }

    public function update(Request $request, $id_cat)
    {
        $category = Category::findOrFail($id_cat);
        $this->validate($request, [
            'name_cat' => 'required|unique:cats,name_cat,'.$category->id_cat.',id_cat',
            'slug_cat' => 'required|unique:cats,slug_cat,'.$category->id_cat.',id_cat',
        ]);

        $name_cat = $request['name_cat'];
        $slug_cat = $request['slug_cat'];
        $category->name_cat = $name_cat;
        $category->slug_cat = $slug_cat;
        $category->save();

        return redirect()->route('categories.index')
            ->with('flash_message',
             'Категорія '. $category->name_cat.' оновлена!');
    }

    public function destroy($id_cat)
    {
        //Find a user with a given id and delete
        $category = Category::findOrFail($id_cat); 
        $category->delete();

        return redirect()->route('categories.index')
            ->with('flash_message',
             'Категорія '. $category->name_cat.' видалена!');
    }
}
