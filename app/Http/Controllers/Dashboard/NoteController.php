<?php

namespace App\Http\Controllers\Dashboard;

use App\Note;
use App\Cikl as Cycle;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class NoteController extends AdminController
{
    public function index()
    {
        $notes = Note::all();

        return view('dashboard.notes.index', compact('notes'));
    }

    public function create()
    {
        $cycles = Cycle::all()->pluck('name_cikl', 'id_cikl')->prepend('--', 0);

        return view('dashboard.notes.create', compact('cycles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'text_note' => 'required',
            'cycle_id' => 'required',
        ]);

        $note = new Note;
        $note->text_note = $request['text_note'];

        if ($request['cycle_id'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть рівень');
        }

        $note->id_cikl  = $request['cycle_id'];
        $note->save();

        return redirect()->route('notes.index')
            ->with('flash_message',
             'Цикл' . $note->id_note . ' створен!');
    }

    public function edit($note_id)
    {
        $note = Note::findOrFail($note_id);
        $cycles = Cycle::all()->pluck('name_cikl', 'id_cikl')->prepend('--', 0);

        return view('dashboard.notes.edit', compact('note', 'cycles'));
    }

    public function update(Request $request, $note_id)
    {
        $note = Note::findOrFail($note_id);
        $this->validate($request, [
            'text_note' => 'required',
            'id_cikl' => 'required',
        ]);

        $note->text_note = $request['text_note'];
        
        if ($request['id_cikl'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть рівень');
        }

        $note->id_cikl = $request['id_cikl'];
        $note->save();

        return redirect()->route('notes.index')
            ->with('flash_message',
             'Цикл '.$note->id_note.' оновлен!');
    }

    public function destroy($note_id)
    {
        $note = Note::findOrFail($note_id); 
        $note->delete();

        return redirect()->route('notes.index')
            ->with('flash_message',
             'Цикл '.$note->id_note.' вилучений!');
    }
}
