<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Riven as Level;

class LevelController extends AdminController
{
    public function index()
    {
        $levels = Level::all();

        return view('dashboard.levels.index')->with('levels', $levels);
    }

    public function show($id)
    {
        return redirect('levels.index');
    }

    public function create()
    {
        return view('dashboard.levels.create');
    }

    public function store(Request $request)
    {
    //Validate name field
        $this->validate($request, [
            'name_riven' => 'required',
            ]
        );
        // dd($request['name_riven']);
        $level_name = $request['name_riven'];
        $level = new Level;
        $level->name_riven = $level_name;

        $level->save();

        return redirect()->route('levels.index')
            ->with('flash_message',
             'Рівень' . $level->name_riven . ' створен!'); 
    }

    public function edit($id_level)
    {
        $level = Level::findOrFail($id_level);

        return view('dashboard.levels.edit', compact('level'));
    }

    public function update(Request $request, $level_id)
    {
        $level = Level::findOrFail($level_id);
        $this->validate($request, [
            'name_riven' => 'required',
        ]);

        $level->name_riven = $request['name_riven'];
        $level->save();

        return redirect()->route('levels.index')
            ->with('flash_message',
             'Рівень '. $level->name_riven.' оновлен!');
    }

    public function destroy($id_level)
    {
        //Find a user with a given id and delete
        $level = Level::findOrFail($id_level); 
        $level->delete();

        return redirect()->route('levels.index')
            ->with('flash_message',
             'Рівень '. $level->name_riven.' вилучений!');
    }
}
