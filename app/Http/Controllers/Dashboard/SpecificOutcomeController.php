<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Illuminate\Http\Request;
use App\Zmist as ContentLine;
use App\Zor as ExpectedOutcome;
use App\Kor as SpecificOutcome;
use App\Http\Controllers\Dashboard\AdminController;

class SpecificOutcomeController extends AdminController
{
    public function index()
    {
        //$specific_outcomes = SpecificOutcome::all();
        $specific_outcomes = SpecificOutcome::paginate(10);
        return view('dashboard.specific-outcomes.index',
            compact('specific_outcomes')); 
    }

    public function create()
    {
        $expected_outcomes = ExpectedOutcome::all()->pluck('name_zor', 'id_zor')->prepend('--', 0);
        $content_lines = ContentLine::all()->pluck('name_zmist', 'id_zmist')->prepend('--', 0);

        return view('dashboard.specific-outcomes.create',
            compact('expected_outcomes', 'content_lines'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'specific_outcome_name' => 'required',
            'specific_outcome_key' => 'required',
            'expected_outcome_id' => 'required',
            'content_line_id' => 'required',
        ]);

        $specific_outcome = new SpecificOutcome;
        $specific_outcome->name_kor = $request['specific_outcome_name'];
        $specific_outcome->key_kor = $request['specific_outcome_key'];
        $specific_outcome->id_zor = $request['expected_outcome_id'];
        $specific_outcome->id_zmist = $request['content_line_id'];
        $specific_outcome->save();

        return redirect()->route('specific-outcomes.index')
            ->with('flash_message',
             'КОР '.$specific_outcome->name_kor.' створен!');
    }

    public function edit($specific_outcome_id)
    {
        $specific_outcome = SpecificOutcome::find($specific_outcome_id);
        // if (!Auth::user()->hasAnyPermission($specific_outcome->getBranchByContentLine()->marking_galuz, 'edit_content')) {
        //     abort('401');
        // }

        $expected_outcomes = ExpectedOutcome::all()->pluck('name_zor', 'id_zor')->prepend('--', 0);
        $content_lines = ContentLine::all()->pluck('name_zmist', 'id_zmist')->prepend('--', 0);

        return view('dashboard.specific-outcomes.edit', 
            compact('specific_outcome', 'expected_outcomes', 'content_lines'));
    }

    public function update(Request $request, $specific_outcome_id)
    {
        $specific_outcome = SpecificOutcome::find($specific_outcome_id);

        // if (!Auth::user()->hasAnyPermission($specific_outcome->getBranchByContentLine()->marking_galuz, 'edit_content')) {
        //     abort('401');
        // }

        $this->validate($request, [
            'name_kor' => 'required',
            'key_kor' => 'required',
            'id_zor' => 'required',
            'id_zmist' => 'required',
        ]);

        $specific_outcome->name_kor = $request['name_kor'];
        $specific_outcome->key_kor = $request['key_kor'];
        $specific_outcome->id_zor = $request['id_zor'];
        $specific_outcome->id_zmist = $request['id_zmist'];
        $specific_outcome->save();

        return redirect()->route('specific-outcomes.index')
            ->with('flash_message',
             'КОР '.$specific_outcome->name_kor.' оновлен!');
    }

    public function destroy($specific_outcome_id)
    {
        $specific_outcome = SpecificOutcome::find($specific_outcome_id);

        // if (!Auth::user()->hasAnyPermission($specific_outcome->getBranchByContentLine()->marking_galuz, 'edit_content')) {
        //     abort('401');
        // }
        
        $specific_outcome->delete();

        return redirect()->route('specific-outcomes.index')
            ->with('flash_message',
             'КОР '.$specific_outcome->name_kor.' видален!');
    }
}
