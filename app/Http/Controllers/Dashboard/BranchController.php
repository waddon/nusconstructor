<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Spatie\Permission\Models\Permission;
use App\Galuz as Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class BranchController extends AdminController
{
    public function index()
    {
        $branches = Branch::all();

        return view('dashboard.branches.index', compact('branches'));
    }

    public function create()
    {
        if (!Auth::user()->can('edit_branches')) {
            abort('401');
        }
        return view('dashboard.branches.create');
    }

    public function store(Request $request)
    {
        if (!Auth::user()->can('edit_content')){
            abort('401');
        }

        $this->validate($request, [
            'branch_name' => 'required|unique:galuzes,name_galuz',
            'branch_label' => 'required|unique:galuzes,marking_galuz',
            'branch_color' => 'required|unique:galuzes,color_galuz',
        ]);

        $permission = new Permission;
        $permission->name = $request['branch_label'];
        $permission->save();

        $branch = new Branch;
        $branch->name_galuz = $request['branch_name'];
        $branch->marking_galuz = $request['branch_label'];
        $branch->text_galuz = $request['branch_text'];
        if ($branch->text_galuz == null) {
            $branch->text_galuz = '';
        }
        $branch->color_galuz = $request['branch_color'];
        
        $branch->save();

        return redirect()->route('branches.index')
        ->with('flash_message',
             'Галузь '.$branch->name_galuz.' створена!');
    }

    public function edit($branch_id)
    {
        $branch = Branch::find($branch_id);

        if (!Auth::user()->hasAnyPermission('edit_content', $branch->marking_galuz)){
            abort('401');
        }

        return view('dashboard.branches.edit', compact('branch'));
    }

    public function update(Request $request, $branch_id)
    {
        $branch = Branch::find($branch_id);

        if (!Auth::user()->hasAnyPermission('edit_content', $branch->marking_galuz)){
            abort('401');
        }

        $this->validate($request, [
            'name_galuz' => 'required|unique:galuzes,name_galuz,'.$branch->id_galuz.',id_galuz',
            'marking_galuz' => 'required|unique:galuzes,marking_galuz,'.$branch->id_galuz.',id_galuz',
            'color_galuz' => 'required|unique:galuzes,color_galuz,'.$branch->id_galuz.',id_galuz',
        ]);

        $permission = Permission::where('name', $branch->marking_galuz)->first();
        $permission->name = $request['marking_galuz'];
        $permission->save();

        $branch->name_galuz = $request['name_galuz'];
        if (Auth::user()->can('edit_content')) {
            $branch->marking_galuz = $request['marking_galuz'];
        }
        $branch->text_galuz = $request['text_galuz'];

        if ($branch->text_galuz == null) {
            $branch->text_galuz = '';
        }

        $branch->color_galuz = $request['color_galuz'];
        $branch->save();

        return redirect()->route('branches.index')
        ->with('flash_message',
             'Галузь '.$branch->name_galuz.' оновлена!');
    }

    public function destroy($branch_id)
    {
        if (!Auth::user()->hasAnyPermission('edit_content', 'admin')) {
            abort('401');
        }

        $branch = Branch::find($branch_id);
        $branch->delete();

        $permission = Permission::where('name', $branch->marking_galuz)->first();
        $permission->delete();

        return redirect()->route('branches.index')
            ->with('flash_message',
             'Галузь '.$branch->name_galuz.' видалена!');
    }
}
