<?php

namespace App\Http\Controllers\Dashboard;

use App\Tool;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\AdminController;

class ToolController extends AdminController
{
    public function index()
    {
        $tools = Tool::all();

        return view('dashboard.tools.index', compact('tools'));
    }

    public function create()
    {
        return view('dashboard.tools.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_tool' => 'required',
        ]);

        $tool = new Tool;
        $tool->name_tool = $request['name_tool'];
        $tool->save();

        return redirect()->route('tools.index')
            ->with('flash_message',
             'Iнструмент '.$tool->name_tool.' створен!');
    }

    public function edit($id_tool)
    {
        $tool = Tool::findOrFail($id_tool);

        return view('dashboard.tools.edit', compact('tool'));
    }

    public function update(Request $request, $id_tool)
    {
        $tool = Tool::findOrFail($id_tool);
        $this->validate($request, [
            'name_tool' => 'required',
        ]);
        $tool->name_tool = $request['name_tool'];
        $tool->save();

        return redirect()->route('tools.index')
            ->with('flash_message',
             'Iнструмент '.$tool->name_tool.' оновлен!');
    }

    public function destroy($id_tool)
    {
        $tool = Tool::findOrFail($id_tool); 
        $tool->delete();

        return redirect()->route('tools.index')
            ->with('flash_message',
             'Iнструмент '.$tool->name_tool.' вилучений!');
    }
}
