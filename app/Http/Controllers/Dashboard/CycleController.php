<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cikl as Cycle;
use App\Riven as Level;

class CycleController extends AdminController
{
    public function index()
    {
        $cycles = Cycle::all();

        return view('dashboard.cycles.index', compact('cycles'));
    }

    public function show($id)
    {
        return redirect('cycles.index');
    }

    public function create()
    {
        $levels = Level::all()->pluck('name_riven', 'id_riven')->prepend('--', 0);

        return view('dashboard.cycles.create', compact('levels'));
    }

    public function store(Request $request)
    {
    //Validate name field
        $this->validate($request, [
            'cycle_name' => 'required',
            'level_id' => 'required|min:1',
            ]
        );

        $cycle = new Cycle;
        $cycle->name_cikl = $request['cycle_name'];

        if ($request['level_id'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть рівень');
        }

        $cycle->id_riven  = $request['level_id'];

        if (isset($request['cycle_label']) && $request['marking_cikl'] !== null) {
            $cycle->marking_cikl = $request['cycle_label'];
        }

        $cycle->save();

        return redirect()->route('cycles.index')
            ->with('flash_message',
             'Цикл' . $cycle->name_riven . ' створен!');
    }

    public function edit($cycle_id)
    {
        $cycle = Cycle::findOrFail($cycle_id);
        $levels = Level::all()->pluck('name_riven', 'id_riven')->prepend('--', 0);

        return view('dashboard.cycles.edit', compact('cycle', 'levels'));
    }

    public function update(Request $request, $cycle_id)
    {
        $cycle = Cycle::findOrFail($cycle_id);
        $this->validate($request, [
            'name_cikl' => 'required',
        ]);

        $cycle->name_cikl = $request['name_cikl'];
        
        if (isset($request['marking_cikl']) && $request['marking_cikl'] !== null) {
            $cycle->marking_cikl = $request['marking_cikl'];
        }

        if ($request['id_riven'] <= 0) {
            return redirect()->back()
            ->with('flash_message',
             'Виберіть рівень');
        }

        $cycle->id_riven = $request['id_riven'];
        $cycle->save();

        return redirect()->route('cycles.index')
            ->with('flash_message',
             'Цикл '.$cycle->name_cikl.' оновлен!');
    }

    public function destroy($cycle_id)
    {
        //Find a user with a given id and delete
        $cycle = Cycle::findOrFail($cycle_id); 
        $cycle->delete();

        return redirect()->route('cycles.index')
            ->with('flash_message',
             'Цикл '.$cycle->name_cikl.' вилучений!');
    }
}
