<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Table_Page;
use App\Div_Cat;
use App\Table_Post;

class AjaxController extends MainController
{

    public function get_posts()
    {
        $result=[];
        $result['data'] = '';
        $result['display'] = true;

        $pager = isset($_GET['pager']) ? (int)$_GET['pager'] : 1;
        $cat = isset($_GET['cat']) ? (int)$_GET['cat'] : 1;

        $s = isset($_GET['s']) ? $_GET['s'] : '';

        $post_count = Table_Post::where('id_cat',$cat)->get()->count();
        if ( $this->select_count * ($pager+1) > $post_count) {$result['display'] = false;}

        $posts = Table_Post::where('id_cat',$cat)
            ->skip( $this->select_count * $pager )
            ->take( $this->select_count )
            ->orderBy('id_post', 'DESC')
            ->get();

        foreach ($posts as $key => $post) {
            $this->data['post'] = $post;
            $this->data['cat'] = Div_Cat::where( 'id_cat', $cat )->first();
            $view = view('template-part.news-item', $this->data);
            $result['data'] .= $view;
        }

        $result['pager'] = $pager + 1;
        $result['post_count'] = $post_count;

        return json_encode($result);
    }

}
