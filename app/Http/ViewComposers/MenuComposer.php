<?php

namespace App\Http\ViewComposers;

use App\Menu;
use Illuminate\View\View;

class MenuComposer
{
    public function compose(View $view)
    {
        $view->with('topmenu', Menu::getMenu(['id_menu'=>1, 'id'=>'loginform-buttons']));
        $view->with('togglemenu', Menu::getToggleMenu(['id_menu'=>2, 'class'=>'sidebar-menu']));
        // $this->data['topmenu'] = Menu::getMenu(['id_menu'=>1, 'id'=>'loginform-buttons']);
        // $this->data['togglemenu'] = Menu::getToggleMenu(['id_menu'=>2, 'class'=>'sidebar-menu']);
    }
}