<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Program;

class ProgramEditMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /*
         * программа должна принадлежать школе или пользователю
         */

        $access = false;
        if (Auth::check()) {
            if ( $request->route('id_program') != null ) {
                $program = Program::where('id_program', $request->route('id_program'))->first();
                if($program) {
                    if ($program->id_ptype == 4 && $program->id_user == auth()->user()->id){
                        $access = true;
                    }
                    if ($program->id_ptype == 3) {
                        if (auth()->user()->can('director') && $program->id_school == auth()->user()->id_school){
                            $access = true;
                        }
                        $schoolusers = json_decode($program->users_program);
                        foreach ($schoolusers as $schooluser) {
                            if ($schooluser->id_user == auth()->user()->id) {
                                $access = true;
                            }
                        }
                    }
                }
            }
        }

        /* если нет доступа то редирект */
        if ( !$access ) {
            return redirect()->route('unauthorized');
        } else {
            return $next($request);
        }

    }
}
