<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Plan;

class PlanEditMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $access = false;
        if (Auth::check()) {            
            if ( $request->route('id_plan') != null ) {
                $plan = Plan::where('id_plan', $request->route('id_plan'))->first();
                if($plan) {
                    if ($plan->id_ptype == 3 && auth()->user()->can('director') && $plan->id_school == auth()->user()->id_school){
                        $access = true;
                    }
                }
            }
        }

        /* если нет доступа то редирект */
        if ( !$access ) {
            return redirect()->route('unauthorized');
        } else {
            return $next($request);
        }
        
    }
}
