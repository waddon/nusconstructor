<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Table_Document;

class DocumentsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $document = Table_Document::where('id_document', $request->route('id_doc'))
            ->where('id_user', Auth::user()->id)->first();

        if ( !$document && $request->route('id_doc')!='new' ) {
            return redirect()->route('unauthorized');
        }

        return $next($request);
    }
}
