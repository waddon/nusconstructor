<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Program;

class ProgramMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /*
         * программа должна быть либо типичной, либо этого пользователя, либо группы школы, в кторой есть этот пользователь
         * если параметра нет - значит создается новая программа
         */
        if ( $request->route('id_program') != null ) {

            $access = false;
            $program = Program::where('id_program', $request->route('id_program'))->first();

            if($program) {
                if ($program->id_ptype == 1 || $program->id_ptype == 2){
                    $access = true;
                }
                if (Auth::check() && $program->id_school == Auth::user()->id_school && $program->id_ptype == 3) {
                     $access = true;
                }
                if (Auth::check() && $program->id_user == Auth::user()->id && $program->id_ptype == 4) {
                     $access = true;
                }
            }
            /* если нет доступа то редирект */
            if ( !$access ) {
                return redirect()->route('unauthorized');
            }
        }

        return $next($request);
    }
}
