<?php

namespace App;

use App\Cikl as Cycle;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'notes';
    public $primaryKey  = 'id_note';    
    public $timestamps = FALSE;

    public function getNoteCycle()
    {
    	$cycle = Cycle::find($this->id_cikl);

    	return $cycle->name_cikl;
    }
}
