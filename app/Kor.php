<?php

namespace App;

use App\Zmist as ContentLine;
use App\Zor as ExpectedOutcome;
use App\Galuz as Branch;
use Illuminate\Database\Eloquent\Model;

class Kor extends Model
{
    protected $table = 'kors';
    public $primaryKey  = 'id_kor';    
    public $timestamps = FALSE;

    public function getSpecificOutcomeContentLine()
    {
        $content_line = ContentLine::find($this->id_zmist);
        // dump($content_line);
        if ($content_line) {
            return $content_line->name_zmist;
        }

        return null;
    }

    public function getSpecificOutcomeExpectedOutcome()
    {
        $expected_outcome = ExpectedOutcome::find($this->id_zor);
        $name_zor = "";

        if ($expected_outcome) {
            $name_zor = $expected_outcome->name_zor;
        }

        return $name_zor;
        
    }

    public function getBranchByContentLine()
    {
        $content_line = ContentLine::find($this->id_zmist);
        // dump($content_line);
        if ($content_line) {
            $branch = Branch::find($content_line->id_galuz);

            return $branch->marking_galuz;
        }

        return null;
    }
}
