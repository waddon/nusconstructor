<?php

return [

    'title'           => 'Результати пошуку',
    'error_short'     => 'Довжина менше 3 символов',
    'error_null'      => 'Параметра пошуку не знайдено',
    'result_count'    => 'За вашим запитом знайдено записів у кількості:',

    'title_news'      => 'Новини:',
    'title_materials' => 'Матеріали:',
    'title_reference' => 'Довідники:',

    'button_load_more' => 'Більше матеріалів',
    'button_loading'   => 'Завантаження...',
];
