<?php

return [

    'title'           => 'Кабінет користувача',
    'title-data'      => 'Реєстріційні дані',
    'title-documents' => 'Документи',
    'title-plans'     => 'Навчальні плани',
    'title-programs'  => 'Навчальні програми',

    'hint-view'       => 'Перегляд',
    'hint-edit'       => 'Редагування',
    'hint-clone'      => 'Клонування',
    'hint-archive'    => 'Передача до архіву',
    'hint-delete'     => 'Видалення',

];
