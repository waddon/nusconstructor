@extends('common.layout-new')

    @section('content')
        <style>
            .appended_post{
                display: none;
            }
        </style>

        <div class="content-box-title blue">Результати пошуку</div>
        <div class="content-box">
            <p class="result-count text-center">За вашим запитом знайдено записів у кількості: {{ $result_count or '0' }}</p>
            <p class="result-count text-center">{{ $result->message or '' }}</p>
            
            @if(isset($news))
                <section class="search-section search-section-color1">
                    <div class="search-section-title">Новини</div>
                    <div class="search-section-content">
                        @foreach($news as $new)
                            <div class="search-news-wrap search-news-item" data-date="{{ ukr_date(date("d F Y", strtotime($new->created_at))) }}" data-color="1" data-id-post="{{$new->id_post}}">
                                <div class="search-news-title">{{$new->name_post}} ({{$new->id_post}})<span class="search-news-meta" style="">{{ ukr_date(date("d F Y", strtotime($new->created_at))) }}</span><div class="clearfix"></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="search-section-buttons">
                        @if ($result->news_more_visible)
                            <span class="button-more" data-ajaxcount="{{$result->ajaxcount}}" data-count="{{$result->ajaxcount}}" data-string="{{$_GET['s'] or ''}}" data-category="1">Більше</span>
                        @endif
                    </div>
                </section>
            @endif

            @if(isset($materials))
                <section class="search-section search-section-color3">
                    <div class="search-section-title">Матеріали</div>
                    <div class="search-section-content">
                        @foreach($materials as $material)
                            <div class="material-wrap">
                                <div class="material-title">
                                    @if ($material->thumb_post != '')
                                        <img src="{{ $material->thumb_post }}" alt="">
                                    @endif
                                    {{ $material->name_post }}
                                        <span class="material-meta" style="float: right;">{{ ukr_date(date("d F Y", strtotime($material->created_at))) }}</span>
                                    @if ($material->download_post)
                                        <a class="download" href="{{$material->link_post}}" download="">Завантажити</a>
                                    @endif
                                    <div class="clearfix"></div>
                                </div>
                                <div class="material-excerpt" style="display: none;">
                                    {{ $material->excerpt_post }}
                                    @if (!$material->download_post)
                                        @if ($material->link_post == '')
                                            <span class="material-link material-item" data-id-post="{{ $material->id_post }}" data-date="{{ ukr_date(date("d F Y", strtotime($material->created_at))) }}">Читати далі</span>
                                        @else
                                            <a class="material-link" href="{{$material->link_post}}" target="_blank">Перейти за посиланням</a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>
            @endif

            @if(isset($laws))
                <section class="search-section search-section-color2">
                    <div class="search-section-title">Закони, положення та методичні рекомендації</div>
                    <div class="search-section-content">
                        @foreach($laws as $law)
                            <div class="search-news-wrap search-news-item" data-date="{{ ukr_date(date("d F Y", strtotime($law->created_at))) }}" data-color="1" data-id-post="{{$law->id_post}}">
                                <div class="search-news-title">{{$law->name_post}}<span class="search-news-meta" style="">{{ ukr_date(date("d F Y", strtotime($law->created_at))) }}</span><div class="clearfix"></div>
                                </div>
                            </div>                            
                        @endforeach
                    </div>
                </section>
            @endif

            @if(isset($glossary))
                <section class="search-section search-section-color4">
                    <div class="search-section-title">Глосарій</div>
                    <div class="search-section-content">
                        @foreach($glossary as $glossaryitem)
                            <div class="search-news-wrap search-glossary-item" data-date="{{ ukr_date(date("d F Y", strtotime($glossaryitem->created_at))) }}" data-title="{{$glossaryitem->name_post}}" data-color="1" data-id-post="{{$glossaryitem->id_post}}">
                                <div class="search-news-title">{{$glossaryitem->name_post}}<span class="search-news-meta" style="">{{ ukr_date(date("d F Y", strtotime($glossaryitem->created_at))) }}</span><div class="clearfix"></div>
                                </div>
                            </div>                            
                        @endforeach
                    </div>
                    <div class="search-section-buttons">
                        @if ($result->glossary_more_visible)
                            <span class="button-more" data-ajaxcount="{{$result->ajaxcount}}" data-count="{{$result->ajaxcount}}" data-string="{{$_GET['s'] or ''}}" data-category="6">Більше</span>
                        @endif
                    </div>
                </section>
            @endif
        </div>

    @endsection

    @section('dialogs')
        <style>
            .ui-dialog-titlebar span{
                color: #2f2f2f;
            }
            .ui-dialog-titlebar:before{
                content: "\f086";
                font-family: FontAwesome;
                position: absolute;
                color: #fff;
                border-radius: 50%;
                display: inline-block;
                height: 36px;
                width: 36px;
                line-height: 36px;
                text-align: center;
            }
        </style>

        <div id="dialog-news-one" title="Title">
            <div class="dialog-content"></div>
        </div>

        <div id="dialog-material-one" title="Title">
            <div class="dialog-content"></div>
        </div>
        <div id="dialog-glossary-one" title="Title">
            <div class="dialog-content"></div>
        </div>


    @endsection


@section('scripts')
<script>
jQuery(document).ready(function(){
        /* нажатие на новость */

        $( ".search-news-item" ).click(function(){ search_news_item($(this)); });

        function search_news_item(sender){
            $(".ui-dialog-title").html( sender.attr("data-date") );
            $(".ui-dialog-titlebar").removeClass("color1").removeClass("color2").removeClass("color3").removeClass("color4").addClass( "color1" );
            /* Получаем данные поста */
            $.get('{{ route("get_post") }}', {id_post: sender.attr("data-id-post")} ,function(result){
                result = JSON.parse(result);
                $(".dialog-content").empty();
                $(".dialog-content").append('<h2>'+result.name_post+'</h2>');
                $(".dialog-content").append(result.content_post);
                $( "#dialog-news-one" ).dialog( "open" );
            });
        }

        $( ".search-glossary-item" ).click(function(){ search_glossary_item($(this)); });

        function search_glossary_item(sender){
            $(".ui-dialog-title").html( sender.attr("data-title") );
            $(".ui-dialog-titlebar").removeClass("color1").removeClass("color2").removeClass("color3").removeClass("color4").addClass( "color4" );
            /* Получаем данные поста */
            $.get('{{ route("get_post") }}', {id_post: sender.attr("data-id-post")} ,function(result){
                result = JSON.parse(result);
                $(".dialog-content").empty();
                $(".dialog-content").append(result.content_post);
                $( "#dialog-glossary-one" ).dialog( "open" );
            });
        }



        /* Модальное окно с новостью */
        $( function() {
            $( "#dialog-news-one" ).dialog({
                resizable: false,
                width: "80%",
                modal: true,
                autoOpen: false,
                show: {
                    effect: "fade",
                    duration: 100
                },
                hide: {
                    effect: "fade",
                    duration: 100
                },
            });
        });


            $( ".material-item" ).click(function(){
                $(".ui-dialog-title").html( $(this).attr("data-date") );
                $(".ui-dialog-titlebar").removeClass("color1").removeClass("color2").removeClass("color3").removeClass("color4").addClass( "color3" );
                /* Получаем данные поста */
                $.get('{{ route("get_post") }}', {id_post: $(this).attr("data-id-post")} ,function(result){
                    result = JSON.parse(result);
                    $(".dialog-content").empty();
                    $(".dialog-content").append('<h2>'+result.name_post+'</h2>');
                    $(".dialog-content").append(result.content_post);
                    /* Открываем диалоговое окно*/
                    $( "#dialog-material-one" ).dialog( "open" );
                });
            });
            /* Модальное окно с новостью */
            $( function() {
                $( "#dialog-material-one" ).dialog({
                    resizable: false,
                    width: "80%",
                    modal: true,
                    autoOpen: false,
                    show: {
                        effect: "fade",
                        duration: 100
                    },
                    hide: {
                        effect: "fade",
                        duration: 100
                    },
                });
            });



            $('.material-title').click(function(){
                $(this).siblings('.material-excerpt').slideToggle( "fast" );
            });


            /* Модальное окно с новостью */
            $( function() {
                $( "#dialog-glossary-one" ).dialog({
                    resizable: false,
                    width: "80%",
                    modal: true,
                    autoOpen: false,
                    show: {
                        effect: "fade",
                        duration: 100
                    },
                    hide: {
                        effect: "fade",
                        duration: 100
                    },
                });
            });


        $(".button-more").click(function(){
            var data = new FormData();
            data.append( 'string', $(this).attr("data-string") );
            data.append( 'ajaxcount', $(this).attr("data-ajaxcount") );
            data.append( 'count', $(this).attr("data-count") );
            data.append( 'category', $(this).attr("data-category") );
            hexathis = $(this);

                    $.ajax ({
                        url: '{{ route("get_posts") }}',
                        type: "POST",
                        dataType: 'json',
                        data: data,
                        processData: false,
                        contentType : false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (result) {
                            if (!result.more_visible) { hexathis.hide(); }
                            hexathis.attr("data-ajaxcount", result.ajaxcount);
                            hexathis.attr("data-count", result.count);
                            jQuery.each(result.posts, function( index ){
                                if (hexathis.attr("data-category")=="1"){
                                    $('<div class="search-news-wrap search-news-item appended_post" date-date="'+this.date1+'" data-color="1" data-id-post="'+this.id_post+'"><div class="search-news-title">'+this.name_post+'<span class="search-news-meta" style="">'+this.date1+'</span></div><div class="clearfix"></div>').appendTo(hexathis.parent().siblings(".search-section-content")).show('fast');
                                    $( ".search-news-item" ).click(function(){search_news_item($(this));});
                                } else if(hexathis.attr("data-category")=="6"){
                                    $('<div class="search-news-wrap search-glossary-item appended_post" date-date="'+this.date1+'" data-title="'+this.name_post+'" data-color="1" data-id-post="'+this.id_post+'"><div class="search-news-title">'+this.name_post+'<span class="search-news-meta" style="">'+this.date1+'</span></div><div class="clearfix"></div>').appendTo(hexathis.parent().siblings(".search-section-content")).show('fast');
                                    $( ".search-glossary-item" ).click(function(){ search_glossary_item($(this)); });
                                }
                            });
                            console.log(result);
                        },
                        error: function (result) {
                            $().toastmessage('showToast', {
                                text     : 'Error',
                                type     : 'error',
                            });
                        }
                    });

        });

});
</script>
@endsection
