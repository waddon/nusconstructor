@extends('common.layout-new')


    @section('content')
        <div class="content-box-title">{{ $title or 'Матеріали' }}</div>
        <div class="content-box">
            @foreach ($materials as $material)
                <div class="material-wrap">
                    <div class="material-title">
                        @if ($material->thumb_post != '')
                            <img src="{{ $material->thumb_post }}" alt="">
                        @endif
                        {{ $material->name_post }}
                            <span class="material-meta" style="float: right;">{{ ukr_date(date("d F Y", strtotime($material->created_at))) }}</span>
                        @if ($material->download_post)
                            <a class="download" href="{{$material->link_post}}" download="">Завантажити</a>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <div class="material-excerpt" style="display: none;">
                        {{ $material->excerpt_post }}
                        @if (!$material->download_post)
                            @if ($material->link_post == '')
                                <span class="material-link material-item" data-id-post="{{ $material->id_post }}" data-date="{{ ukr_date(date("l d F Y", strtotime($material->created_at))) }}">Читати далі</span>
                            @else
                                <a class="material-link" href="{{$material->link_post}}" target="_blank">Перейти за посиланням</a>
                            @endif
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    @endsection


    @section('dialogs')
        <div id="dialog-material-one" title="Title">
            <div class="dialog-content"></div>
        </div>
    @endsection


    @section('scripts')
        <script>
            /* нажатие на новость */
            $( ".material-item" ).click(function(){
                $(".ui-dialog-title").html( $(this).attr("data-date") );
                /* Получаем данные поста */
                $.get('{{ route("get_post") }}', {id_post: $(this).attr("data-id-post")} ,function(result){
                    result = JSON.parse(result);
                    $(".dialog-content").empty();
                    $(".dialog-content").append('<h2>'+result.name_post+'</h2>');
                    $(".dialog-content").append(result.content_post);
                    /* Открываем диалоговое окно*/
                    $( "#dialog-material-one" ).dialog( "open" );
                });
            });
            /* Модальное окно с новостью */
            $( function() {
                $( "#dialog-material-one" ).dialog({
                    resizable: false,
                    width: "80%",
                    modal: true,
                    autoOpen: false,
                    show: {
                        effect: "fade",
                        duration: 100
                    },
                    hide: {
                        effect: "fade",
                        duration: 100
                    },
                });
            });



            $('.material-title').click(function(){
                $(this).siblings('.material-excerpt').slideToggle( "fast" );
            });

        </script>
    @endsection
