@section('popup')
<div id="popup-plan" title="Перегляд навчального плану">
    <div class="dialog-content">        
    </div>
</div>
<div id="popup-program" title="Перегляд навчальної програми">
    <div class="dialog-content">        
    </div>
</div>

<script src="/assets/lib/pdfmake/pdfmake.min.js"></script>
<script src="/assets/lib/pdfmake/vfs_fonts.js"></script>

<script>
        var docInfo = {
            info: {
                title: 'Тестовый документ PDF',
                author: 'Нова Українська Школа',
                subject: 'Навчальний план',
                keywords: 'ключевые слова'
            },
            pageSize: 'A4',
            pageOrientation: 'portrait',
            pageMargins: [50,50,50,50],
            header: [
                {
                    table: {
                        widths: ['*'],
                        body:[
                            [
                                {
                                    image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABLCAYAAADK+7ojAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA+BpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJDb3JlbERSQVcgMjAxNyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1MkRDODA2NUE0N0MxMUU4QjFCRTg3MEUxQ0ZEODE5QyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1MkRDODA2NkE0N0MxMUU4QjFCRTg3MEUxQ0ZEODE5QyI+IDxkYzpjcmVhdG9yPiA8cmRmOlNlcT4gPHJkZjpsaT5BY2VyPC9yZGY6bGk+IDwvcmRmOlNlcT4gPC9kYzpjcmVhdG9yPiA8ZGM6dGl0bGU+IDxyZGY6QWx0PiA8cmRmOmxpIHhtbDpsYW5nPSJ4LWRlZmF1bHQiPkxvZ28uY2RyPC9yZGY6bGk+IDwvcmRmOkFsdD4gPC9kYzp0aXRsZT4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTJEQzgwNjNBNDdDMTFFOEIxQkU4NzBFMUNGRDgxOUMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NTJEQzgwNjRBNDdDMTFFOEIxQkU4NzBFMUNGRDgxOUMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6ui6RRAAAagElEQVR42uydDXwUxfnHf7N7l+TyciG5JPe2h9DyIrEGa5AXCyKICe+gaJVWUNRiLVaqwL++4XupVWvFoohVrIKVKiIYIiRAeAmVFwkSNLwEK7F3gYS8X5K7JHe785/ZTRQBFVtrk3O+n8+ytze7M7uZuR/PM/vMDKGUQtD1ueNp15NtQcw0RUXOM6lhgBDggssk202ZvpCoZQERgtW1yTmh2E78C4E7h4UC/hYgOoKerZU3ULb9Ya1pWMYwUplt9R0RNf79xiT+BF0bTUOISAjY3ARyBRCbGDnP1hZggsVaqMmMQlWFR9S2QAhWF0eSYKYaYqvLaaCRWVjNzREkWO0WVpg5g7IMi6htgRCsLk6oDf6bBvhCb050oqmOuYRxkfNsYeYTMusRcVZ4WoPwwyrq+/uO6MOKEHJrFYVZWyZWneHIaZ3sP1T2PGOTfD5RwwIhWAKBoEshiT+BQCDoKog+rAjh7XLFTKT2+mRG82SXL3jqOWsqFCs13iqaJzl8/i9L5z3dbB++wn167BM/h7+ZlL4kj3cqjXRCYNZUBM+UR8c5PI+J9jPcx3HFwu7BrD8KRWiy8/RnORke2sHveUKqr0a0BOESCjq5UHFRGD7OSRtrmcls0t+oYfpDUs/u/UjjhDRfzXq/p/eutdrQN/6gLk2wETTWUPQdKAWnPSgl8mvXsfSi9Vrmit+rr8cnEbQ0ASluYOPbx8nJovDao9q+fVs0JTmNoPoERfY0+eUJt0pPZsV7D/Aydr6jjVj5pLokIYUg0AC4ehGse/3YZ3m8w/N4WCv5aLtm75bK8qiiGDNdfmHsTGkhzyOnSrF9WkKTlz2olfJmyTcTe5Zp7Fk85xrPciYB3bmWznL/gKw5dwgxjbJ494tWISwsQSdFNsP6UpHi379NQ32TEQbAK/WnAemoOQZ92MeamFjY6yux9MB+DdHsjFZmgskysfBrO9IbqvB6yb6OdMBRZuS/mgnCZGZJsbySPymmyuEPNViY+RRkapLxiTQjJh4r2GkHYuJgr63AkpLiz/Oor5S+IKrmaCR8/AG1Hyj+PI8LPpFmsjze4nlEsTJamlFatFXjRqK+cTPr6pOe5dTnT3KQwR9uURe8PJ8ueL7IHJuvedKz4rwHRMuITEQfVheHGu5XW7KDIDmBICWZwMYsIJmpFnfJ+DlqGMEoC3SRsDkJuL9lTTGu/Sw9BoiTWTrLJzGWIMlOPsuf73leCclAFLvW5mL5s31sArs2BH9HHtG8DBhlJEQTdLO332O7Ec/yCFtt0AWN5yF15BFGoD09xO87hVloKbYzP8upqCoCSl+CygqK+yeGApTlsSHgyRAtQwiWQNAJFVuP9mcCCnxURHFXdri0uQFHNgaFaAnBEnQl+n+fRIvbg4pCsG+nhnvHhAJN9TiS3+xJF81ACJagc7uIugv2m8vL93uSyKAtYc/Q7jEkk7t44bN8v8LcL0jMZNnY4sl0u8iwbfBk8byiY9Gpo1L543X3EBwopnj4ynCAecAmIVqRheh0jzDM0cZwlmtuc1XYXNje0owpcYkE/9xHYbUSfbqWr8MSD7DrsPwhdQ/v21JDABerYx8DiVbSeR+eu4ds52GWVjGztP7vsnDxA6tMsRslT4Z4eygES9AZKzTK+OGufla1twFTuAnNf8RxrKZT2Q85fBazSsXEAcEmJliPqfq1pD2P1CSCxGScVR7/UzOL3bC73T28b1wo8NHeCrK2WrGPT/FVihYiBEvQyVxCTpr7dEvo64Smw53UJ85jSudwfTEPbp2Fu8A0erRdtHr0ICj5gOLc8xz0sY3mWNE6hGAJOhlcaLiVUeWj+vQs/O0Z73eKZ5ZXspPoYnQqshmW1iAQYL/0tsozd2xyHUy2Ed1dVLvA8GqJCVag0ficPkQKQcRHC8ESdD64BcQtoeHXSLDaSGVbC+yWON7/RHFwJ0XUGWaVqq9EUY8fkT4zHzSV8vRT+7m4CHI3cdMyio+LKRK6/VcfYQDb9vxHYsXutyUAnKihmDZXxswn5PFD4Q2K1iEES9DJCLVCD7Rcv+I42QNPVm0zfK440m/1G+rKnevCSIk53VW8wuUNrpGVyosvlbOZ8ZR3mgAA2clA7wM7wouK3uOC9fUd76pq3MfmsGeoUyGxW6kn5E4mFi6I4ZOstngmfoqJpBe0ecLubsQ+Jtu35jHqwL87YoyLVbCZWZi1FFNny7j5CTm76hgthEu0DSFYgs7nEhJjW7jJnd53ACkdnegr29zmsfLJ/eSv0JlJdp//L7uVnWd6iyjJ2Hzjhb78xlrnopizDIThVl3AD6x+Ri00mQ03MiYWqPYCCXFGIQmxBId3Uyx7RV3SUE151Pv9fypz3cWFjrtw4W/oenZYVlysps2T8cvH5ezKcrr5TAOwBUKwBJ0IqqFE04x50PnMCF9lsWwIejK2rdBGPPvr8NPWFKLHYJ1C2++Ig89uqg/dORviEgE/c8mWzNG+8KaR94MlphgCZktlvt8GioJVYb0hMqPs4Sg+PMjZ7t5+A8Hi+Xe4gdcxN/AWJlYVPrr5SkWIlRAsQUQhy4hlYvR0XbPxhu0MgqV/z/uxzFHGUJivQ2t3CVP4m8b2t3Z6RLr0eac9FyQ+ljA2nnyWrk9t8w3dQW5ZcVXiYsXdwF8wN/AEcwOFWAnBEnRCF5D/wHmgJ4+dks36KjN6iAKP9NbPkWDmIhFk51n4edxkCn7eud6hD5Lh/p1RsDi8f4xvMKwhhNv0vPUyWJlW/R5YbqHj384Qio63nOpXLELBn621vTudd7D/8gnhBgrBEnRe14/qwhPFrJ82HnLABYtbNuyH3L9jfnc+4wL77hELwXxuJVn80N8Wdlgy7enzLDKe4OlfJlhfsMrq9SDV/XyiP64qAT+OpnXH9IuGSq/GJxMjvOI/RH+BwPK2JJAvXYSC33u1j+LS8ZIuVsINjPD/oMUEfpHB4u2KwtwwLwyrqeesS3xlp56zaIvSm7lPpdxdM1uQcsvgL06I15GOs+imUsO0T5yVlN14yorMfCI/TaXfTgjBWS5C8ac8d+Yd2eVFq48rlq+bnVQgLCxBJ0DpiyCzjDzcI/uy/4J+0B9BJlYOdp6ZWS+hM6ZrcJxNkKUsS+YxSd7T8hht9X7nqzP/4AJyZJVPMTPLSoiVsLAEAoFAWFiCbxE+YZ1sQqymIWL6b3iHOrf2RkZ5d4oaFgjBiiDy/6pe1RbEfH22hghBH4RNsLBsmFJ044WiI10gXMIuD1/N5sS/EJgzPBRoCAAxEfRsPIKCv2x8LNc07EdDSeVoq++IqHFhYQm6MJqxzmCAL0JBKoE4awQJFo8Vk/UwjUJNNaL2BUKwBF3ZRJZgZkZyVGMt2vzNRpBlxAhWmxEAzwdSS5JoqwIhWF3fwgojcFOmL/T8+U4kVxvTG0cKoRbDwoqKxnnhEETIgkD0YQkEgq6DWDUnguDBk5H2PJH2TAJhYQkEAmFhCQQCgRAsgUAgEIIlEAiEYAkEAoEQLIFAIBCCJRAIhGAJBALB/xIxNCdCeKdSsSbZyeBh8Obz45wqxZaUSjKHth93wBc25ePz+AIUDdV0/0S7z9+RVtDmGSzJMPF52tneEvDTo2O6fXGq5XdOKLbkNJJZX02Lx6f4KsVfXiAES/CNWFPhsR7aTQeX7NQe+micUvzDPgQfFtLBh/Zod+3LUo46epMySwySQ63w57+izvrnh/TaVBc5sPzJY+dtbPFkjorxFvF8Vi9UZzY1YEZzAxAdCwy4XOofylBsE9OMud9zaz1K2UewLs9R8y4eIw1YN9BjGdPNWyZqQPCdwSPdxda1t21Uybr2104KpNIn1rpoA/XQqbON43nPuuguqtC3yxU68FIH+y6NWmDX96mpdrpwozt9B7t+0WZ3b5l9Z2Jp5/az04RoO4012+njOa4R6xuU3rycrey8ERMcer4ZmQ76bp3SQ/z9xfZdbqIPKwIIh+CPT+KfCLyHKDZ+qKHZz4+IvvgpM5bw2qMqdm9RcdN8GbkNZjy43ISqKoo/3xouCQRRYUlAWRRzEwddTvDnLWZMmiUhEKIINKCAyDBzS2xXjhba9a6GtBQJJUUadudqI7iLKWpA8F0hBCtSfHszwEcJL56rYkpGCJte1RBvJjBHA/Xs+91rNcSZJFwxW0aUFdmjfy5lDxom4eMjFCMt3v18ua5UhYC5lrj+ghBef0rFoOESfny5BH8dPRIVA1v+y1qBLBP89lUT4mMJtr2pLSVE/O0FQrAE3xA+/zlfjXnCL2Tc+2cTLmSWUotqfM+nyOrej6A5TPHhNg3JgH0QvPmfMGvMZiXIOeGx8TxqKyl+eAFB+bEKcvvTpoXFuzXer4Vz3GToJez84gJjoPzbz6gItwIfbKI4spcWMddQETUgEIIlOCtkEyzcBdRAMfxagvm3yXytPoQ0TV++nhtBP7+fWVYSwT1XhjH3qvCrjlQHrarSMPU+Cc40ZD5XqCjNbdBdylkPu3I/KtRmtwQp6isRshGk//RXruqqBor0nxAkWIEBownqmjTkLtYCcd1Ib1ELAiFYgrOiNYBK5s694OkloaUJ2fvb6MhYK1Yo7DghGVPLG9Gn14Wk/4NvmCb3YpZW3luavqT97D+aXxj/S7nPiRpazFzK4JDLJMQnEaz8szb2wA6K8dfJWPXisajSaqys8lJb7wyCnQXHyWN/N02+4yU58fyBMtj3GE68m0UtCL4LxHxY3yM2BDwZLU20fEJ7mAKPu/qqNf/4WofNDbRsksOI1VpzXLFMOmkp+HcqFOtEx+dxXAKBECzBt84aJjSTviWh2dTqyeQuZ1sr/OZoWC+LNmK6BALhEgq+Fb4tscqp8thWPaXOfekudc/axWrpC3PUXP79N5nWmFtt/41nFFMrC8ESdHLeOenH//axLxeC1ew87s59WXpeo6f3unqlx9eVF2WBvbyUXntkL4XvMHBkr2bXG5X89SMouLupiydzMdfVKcraGo/9mz7v2mpFv+alotPF6UrFF/qqv4FACJbgf+XiMQEaMcFJ52WHA/y4CJ6s7Svp71NdDvp/z7kXbA17ho6c5KQZA5y0IOQZPJmJxK2Dww2XjHXSjQ2eHs//Q7Ff9BMnPfc8J7WnOei948Klpe/Dut7v6d3hQvJ9RqaTnv9jp96HsKZSsWbHew/c+ozc55x0gvKPKe57wxTF+8lemKsFLhhonPdunUfJ/qmL/ugC45iPcdzM7iHnWe03qckOGk0cdP5E1XtkD1W2MhF78l330O59HfTm+a4C3sd2sshyYeqb7qBX3+qsPgjPtMI36OxElsc/VmkF79Z7dIHled/3snu2vYeD5j6v5W5q8WSKFhJBiHD/rr9tDilDb77fmceHzNxwt3PHXqqMTUs1ht/w9I1BJdOZlkZTk+z6ca9+RtrjOW66myr08TUu/dieYqeXjjOG76SlGeeuOua2FFIla+4i1wKeP9/mPON6gn/H09fWuJU+/fg1xvn8+15McPgQn62qMuIQ9czgIsPzfHGP28yHAd3+pGsJP3baWXnjjbRx1znpfnYvj/zdpZcx5lonfY96sjYElIx1DUrv1ZVu2/PvufW0idc76Uu73fp13XsY5a4+7rby/aZWZfCQkcbwIZvVSMupcttFO4mMTQx+jgAuNXm3t8zx3Pbealqau0QbrIaRe6KKYtYCecVOZm1dFuPNt/d0oq6Com8/Jz16iOLZzWacfylBZR3VQxw4I6ZKePYZE66fFsLa5SoeXO6eOeJn0pH6GuTlPKehX3/DIF+7WJt76VTpybdbFbNsgj++G2CNMfJQNYQczNap/BT4631qQUwCQVsLhdNOwBd8Zafkj5xnz3MqBMe8FWQ7u7/d67W8xBSCqqDuTqJbrIS9GzVMcbblnfMjgqvvlJE5igyplGii0y41fHqAYs5lYfTNIFi4zdyTu6+THL4y/gJgV47mLt5CMfhSGUVs/4sHXHnTH5L/yMsVLUW4hIJOQrwVPac/LKG2lmLZEyrOY+Iy/lfy3dVVVH9rF5sAeD+lKD2kIS4aukhpGjexjett3Qh2rtVw+RAuVhrO6SHhwevKX4iSYP5wq4bDBzRMvUvGNXNlHD6o4aNCrSLVTdqohoZT78XCygowkXr59yoW3xPGiX8BCTbm1h1TLIvfU+xBVmb6hZLuutbX0OKRoyWccx5BsI1b/EBrGOBCNXaGhPc3Ujx6TZhH4RfNHOTzW1k+xe9rqG+kSGBiSAmOhlpRy8slBOacxdpKwlr19b+T4enFXMS/0ay6ExBvLoVgCToTNRV05/mXSLhohKSr0OU3SM/GJ6LP+FQj5qqlGUiMJ5g2T0aYCcLjM8JoZD/zJCZUunAx2A8fbcwGGnudhLtfMzl4p3aY94mtM1TtuduZAN2h6p83v0YRZh+lqNMHE/pZiYlMTJ5ab8JLe8zowcSoxgdMdvmC3fsALgfBtndUbMnV8sKtqPjTXSrylmpIiDUsrGAbRfpggnsWmDDoMoI6Jk7HP0Uyf/NXVwFk/kTCVbNk7PmHhiW/UZFiJ4PzGz29D++mvsO7qD4caQETucoy4OjHGkq2a0N535ZoJUKwBJ2A/GZP+geb6JSfKSHs3Kzhx0NkZN0gPdkxed+qcsVcVqKhronitsfl7NmLTfd8ckTDA5PDfMD0SObWDaip19CTWTWv7DFn//YV00jlXDB3j1i2rdTy3nqRWWwZEmb8XtY3/jn/rTC2rNDgYBYPj3b3c4tqr2I2M4us4ihFLROZ8y4mfTIzpclN9UxQ/RQvvq9YklNI/7l/NQ0wmYC548OY1iOMV/+g4uBOim7sZsJtxjO99piG801teG+ThhETJJx7IbHzN38najVwF3TBItMtQ0ZJWP1yGC8/oM5xJCD9jcdVb12zhsm3S7j+ARk3PCKBa/HSu9XVrUHUiJbS9RF9WBFAa4BWMlfpYN+BBImpBPlvHCcdb804V7p9oZxZrp3N9eg9kIlYzdVKqbfUdO3xozRjoOTdPDCbid50J/pdKC0sZy7khFRvDR8hvY7l0VyHey65Ulow5Vap/7BRkj7IOVVB7dqXsUMN4Tf+BqwZfZO0pLocw2Zc6Avl1noOZN8orak9hqFZCb4jayo8lZdfT/JrjsnDbr5Ij5LPD47wDP5jgcmz8inNW1cNDBkvL8u6ntzmq4I5yU6Sp9wi72FWkrWpgaLPAGnZqGnSLa1Balt6UDFPvEHmA7mXVbbQovc2HCdZP3PSgB89M+Bd072fq2iS3ZR+7d2SQ3GQwU0aQi0BzK84ihGjrd4joqV0fUSke4TAY44SmfXCPzfW0gOnBofycANmScV2DMXhLhITnMCoWO9+frxN9Qxt8qNsbJLX94V8mXWW7CCDGmrowY6ZR3NOKDarjfSrOU53casnr0lJj4ohySNM3u08Pa9R6W2OIbaRZqOsfJZuPimdw2d4iI4l9nAIQZMZlpMj5Pm9USY2zP43c4dzhPnz4UNb2H0GG+HjM53ye7Oxe2tsoGXjkn2+dQ1Kj9h4ogyXPy+HhzWwJh4aZTGeUyAESyAQCEQfluDs4R3SvOO545i/6s+t/XyeqpPTTj3mC0vk+T2nTRGz3q/0Lmj1ZObWKKdFoXPrhkfEc0vpXVZWXqPnjFPMbGSWnYg4FwjBEnyBmmOwFG/TPhOWQ7uo7eO9NJl/fmmPoqctbR/Cwju/+TGPHM+r9/Tgrt62VdrYZza503n6WiZQXID2b6XKpr9pez49APOG9qE0HVT5YNm/TSs9vJt6D++iRz/k+e89fYjMrvVazytcviDv+Be1JPiPEdGzkRHpPuMeZyGP/H4i1zV0vd/dOyXJTn/wQzvdTpWsWY85X+Vps59yLjpIlWkzH3Lm8uMlO9x0XY2iWGLSqEsxosJ5pDjfj7zCqUeSd2w33esqXOc3FqMoYOX9/E5nSUeaWV+8Io3eudD5dMf1W1RlxPR5zmI9av1nDsqvEXUlNrEIhUCfUpTVpbEYBAUTLpRqKjriq/JY2rT2tFns31fZ8Vh+ePyfFPdPCXt53NK9y009eYd2qBU12T910oK3w8i6WsaDL5rm9U0neOl34aH/WKVl8Whyfm24Dbo1Nn2ejMt/LoPHa7HyZne4mMMl7+aNr2oZ/OZ25VCU7ae7zmZAtUAgXMJIt5I1hOX2ABUiYQgTpD7RFiAqxghk70jjQZlcw8xRQBz7/NxsFe9v0XDfKyYMGS4drT1OCz8toaVb3tSQ3l/Gb5fLmHqTVHOopEKPDt26QlvE8t7DPhaGQ0ae0x+RkT3DaEYS22kUO/hCq79a4F5ZUUlx6yMm/SZWL9La4hJJT1FbAiFYAnS87J0zpnyn00rSuUhwAXEzCyfBdkqls+9bmQXWGjQaQOEqDVXNFPE86l01RI6vhsMFjh2G2404PY3v45kGNdUaBdZVUT1wtIMoJoZ11TSv8E1tSrREEBVHkZgGbFuh4RLi3cxnaxC1JRCC9X32CAlMHWMCb7zXVbhqmWYPtQF8BecVb6nYv9lI5ILDK5yncRW6fbGMG+bL2MDOeftPKqzx4GP6eg4aLaFkn4rnfq2icBMtGjjcqevhsKuk+y0xyN6+Xhvy/iaKtCSCVAdBsNEom7uWKUzR9rG0kn0a4qzA8gc0NNUB9UGKXy1wreyWSsR0LwIhWN9vxdLdPn0Yzt8WaEMfnR5ewq0jPn7wnqvCKHjN6KxkwvYs9+I0De/yc50/JLj5YTn7nF4S/jJfxdY1WnZCN/QpXHecDBsn483FKm4ZFSp5f5uGGXeZuOu34WJ48x+/Xt3BB1lPvkNak2LC1JYAXQbD9VzINPGqDX/VdP28c6k8rtpfQfg+it3AhlfolCofFQtWCP79pi4CRyODv+xWEuoqaDkXqg63T7d6VL1fS7eubE5iv2mAL8jPrWHnprgI3D3Qa0yK78T85e4Z3ZJxaM7Y8h3v1iluQoj8QYH2UX0V0L0vwbmDCPw11MYj2x/5u/sqWcbS9IuJnQ9obs/vorsmlhfwMhe85R4XG09e73MR+hETiaEqbfnkAxxk7uON868pXylqS/Dv8v8CDACY5kVLb8Rt5AAAAABJRU5ErkJggg==',
                                    width: 150,
                                    alignment: 'center',
                                    margin: [0,0,0,0],
                                    fillColor: '#a5d81b',
                                    color: '#ffffff',
                                    border: [false, false, false, false]
                                }
                            ]
                        ]
                    }
                }
            ],
            footer: function(currentPage, pageCount) {
                return {
                    text: currentPage.toString() + ' из ' + pageCount,
                    alignment: 'right',
                    margin: [30,30,30,30],
                }
            },
            content: [
            ],
            styles: {
                h1: {
                    fontSize:30,
                    alignment: 'center',
                    margin: [30,30,30,30],
                    fillColor: '#eeeeee',
                },
                thead:{
                    fillColor: '#f3f3f3',
                    margin: [10,10,10,10],
                },
                tbody:{
                    margin: [10,10,10,10],
                }
            }
        }


        function get_plan(id_plan = 0){
            var data = new FormData();
            data.append( 'id_plan', id_plan );

            $.ajax ({
                url: '{{ route("user-plans-popup") }}',
                type: "POST",
                dataType: 'json',
                data: data,
                processData: false,
                contentType : false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (result) {
                    var temp='<h2 class="text-center">'+result.plan.name_plan+'</h2>';

                    docInfo.content = [];
                    docInfo.content[docInfo.content.length] = {text:result.plan.name_plan, style: 'h1'};                    

                    var table = {
                        widths: ['*'],
                        headerRows: 2,
                        body: [
                            [{rowSpan: 2, text: 'Навчальні предмети\n(Галузі)', alignment: 'center', style: 'thead'}, {colSpan: result.classes.length+1, text:'Кількість годин', alignment: 'center', style: 'thead'}, {}, {}],
                        ]
                    }

                    temp += '<table class="popup"><thead><tr><td rowspan="2">Навчальні предмети</td><td colspan="'+(result.classes.length+1)+'">Кількість годин</td><td rowspan="2">Програма</td></tr><tr>';
                    var line = [{}];
                    for (var key in result.classes) {
                        temp += '<td>'+result.classes[key].name_class+'</td>';
                        table.widths[table.widths.length] = 70;
                        line[line.length] = {text: result.classes[key].name_class, alignment: 'center', style: 'thead'};
                    }
                    table.widths[table.widths.length] = 70;
                    line[line.length] = {text:'Разом', alignment: 'center', style: 'thead'};
                    table.body[table.body.length] = line;

                    temp += '<td>Разом:</td></tr></thead><tbody>';
                    // console.log(result.galuzes);
                    var plan = JSON.parse(result.plan.content_plan);
                    for (var key in plan) {
                        var line=[];
                        var counter = {};
                        counter['all'] = 0;
                        temp += '<tr><td>'+plan[key].name_subject+'<br>';
                        //line[line.length] = {text: plan[key].name_subject, style: 'tbody'};
                        separator = '<span class="popup-plan-galuz">(';
                        temp_line = plan[key].name_subject + '\n(';
                        separator_line = '';
                        for (var key2 in plan[key].galuzes) {
                            for (var key3 in result.galuzes) {
                                if (plan[key].galuzes[key2].galuz == result.galuzes[key3].id_galuz){
                                    temp += separator + result.galuzes[ key3 ].name_galuz;
                                    separator = '<br>';
                                    temp_line += separator_line + result.galuzes[ key3 ].name_galuz;
                                    separator_line = ', ';
                                }

                            }
                            for (var key3 in plan[key].galuzes[key2].classes){
                                if (counter[plan[key].galuzes[key2].classes[key3].class] == undefined) {counter[plan[key].galuzes[key2].classes[key3].class]=0;}
                                counter[plan[key].galuzes[key2].classes[key3].class] += parseInt(plan[key].galuzes[key2].classes[key3].hours);
                                counter["all"] += parseInt(plan[key].galuzes[key2].classes[key3].hours);
                            }
                        }
                        temp_line += ')';
                        // console.log(temp_line);
                        line[line.length] = {text: temp_line, style: 'tbody'};
                        temp += ')</span></td>';
                        for (var key2 in result.classes) {
                            temp += '<td class="text-center">'+counter[result.classes[key2].id_class]+'</td>';
                            line[line.length]={text: counter[result.classes[key2].id_class], alignment: 'center', style: 'tbody'};
                        }
                        temp += '<td class="text-center">'+counter["all"]+'</td><td class="text-center">';
                        line[line.length]={text:counter["all"], alignment: 'center', style: 'tbody'};
                        if (plan[key].program != "0" && plan[key].program != "") {temp += '<span class="popup-program table-button" data-id-program="'+plan[key].program+'" data-toggle="tooltip" title="Переглянути"><i class="fa fa-eye" aria-hidden="true"></i></span>';}

                        

                        temp += '</td></tr>';
                        table.body[table.body.length] = line;
                    }
                    temp += '</tbody></table>';
                    $("#popup-plan .dialog-content").html( temp );
                    $(".popup-program").click(function(){
                        get_program($(this).attr("data-id-program"));
                    });
                    //$(".ui-dialog-titlebar").removeClass( "color1, color2, color3, color4" ).addClass( "color2" );


                    docInfo.content[docInfo.content.length] = {table}

                    $( "#popup-plan" ).dialog( "open" );
                },
                error: function (result) {
                    $().toastmessage('showToast', {
                        text     : 'Error',
                        type     : 'error',
                    });
                }
            });
        };


        function get_program(id_program = 0){
            var data = new FormData();
            data.append( 'id_program', id_program );
            $.ajax ({
                url: '{{ route("user-programs-popup") }}',
                type: "POST",
                dataType: 'json',
                data: data,
                processData: false,
                contentType : false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (result) {
                    var program = JSON.parse(result.program.content_program);
                    var galuzes_program = JSON.parse(result.program.galuzes_program);
                    var temp = '', separator = '';
                    for (var key in galuzes_program) {
                        temp += separator + galuzes_program[key].name_galuz;
                        separator = '\n';
                    }

                    docInfo.content = [];
                    docInfo.content[docInfo.content.length] = {text:result.program.name_program, style: 'h1'};
                    var temp_line = {
                        columns: [{text: 'Цикл: ' + result.program.name_cikl, margin: [0,5,0,5]}, {text: 'Кількість годин: ' + result.program.hours_program, margin: [0,5,0,5]}, {text: 'Галузі: ' + temp, margin: [0,5,0,5]}]
                    }
                    docInfo.content[docInfo.content.length] = temp_line;
                    var table_program = {
                        table: {
                            widths: [10, '*'],
                            body:[[{text:'', border: [false, false, false, false]},{text:'', border: [false, false, false, false]}]]
                        }
                    }

                    var temp='<h2 class="text-center">'+result.program.name_program+'</h2>';

                    for (var key in program) {

                        var table_program_content = [
                            {text: 'Назва змістової лінії або тема:\n' + program[key].name_line}, {text: 'Короткий опис:\n' + program[key].info_line}
                        ];
                        var table_problem = {
                            table: {
                                widths: [10, '*'],
                                body:[[{text:'', border: [false, false, false, false]},{text:'', border: [false, false, false, false]}]]
                            }
                        }


                        temp += '<div class="line">';
                        temp += '<p class="title-item">';
                        if (result.program.id_stype==1) {temp += 'Змістова лінія';} else {temp += 'Тема';}
                        temp += '</p>';
                        temp += '<div class="content-item">';
                        temp += '<div class="border-box">'+program[key].name_line+'</div><div class="border-box">'+program[key].info_line+'</div>';
                        temp += '<div class="problems">';

                        for (var key2 in program[key].problems) {
                            var table_problem_content = [
                                {text: 'Проблемне питання:\n' + program[key].problems[key2].name_problem}
                            ];
                            var table_opis = {
                                table: {
                                    widths: [10, '*'],
                                    body:[[{text:'', border: [false, false, false, false]},{text:'', border: [false, false, false, false]}]]
                                }
                            }


                            temp += '<div class="problem">';
                            temp += '<p class="title-item">Проблемне питання</p>';
                            temp += '<div class="content-item">';
                            temp += '<div class="border-box">'+program[key].problems[key2].name_problem+'</div>';
                            temp += '<div class="opises">';
                            for (var key3 in program[key].problems[key2].opises) {
                                var table_opis_content = [
                                    {text: 'Опис діяльності:\n' + program[key].problems[key2].opises[key3].name_opis}
                                ];
                                var table_kors = {
                                    table: {
                                        widths: [70, '*', 100],
                                        body:[[{text:'', border: [false, false, false, false]},{text:'', border: [false, false, false, false]},{text:'', border: [false, false, false, false]}],[{text:'Код', alignment: 'center',fillColor: '#273135',color:'#ffffff'}, {text:'Очікувані результати навчання', alignment: 'center',fillColor: '#273135',color:'#ffffff'},{text:'Інструмент оцінювання', alignment: 'center',fillColor: '#273135',color:'#ffffff'}]]
                                    }
                                }
                                temp += '<div class="opis">';
                                temp += '<p class="title-item">Опис діяльності</p>';
                                temp += '<div class="content-item">';
                                temp += '<div class="border-box">'+program[key].problems[key2].opises[key3].name_opis+'</div>';
                                temp += '<table class="popup-kors">';
                                temp += '<thead><tr><td>Код</td><td>Очікуваний результат</td><td>Інструмент</td></tr></thead>';
                                for (var key4 in program[key].problems[key2].opises[key3].kors) {
                                    temp += '<tr><td>'+program[key].problems[key2].opises[key3].kors[key4].kod+'</td><td>'+program[key].problems[key2].opises[key3].kors[key4].name+'</td><td>'+program[key].problems[key2].opises[key3].kors[key4].tool+'</td></tr>';
                                    table_kors.table.body[table_kors.table.body.length] = [
                                            { text: program[key].problems[key2].opises[key3].kors[key4].kod, alignment: 'center',fillColor: '#eeeeee'},
                                            { text: program[key].problems[key2].opises[key3].kors[key4].name,fillColor: '#eeeeee'},
                                            { text: program[key].problems[key2].opises[key3].kors[key4].tool, alignment: 'center',fillColor: '#eeeeee'},
                                        ];
                                }
                                temp += '</table>';
                                temp += '</div>';
                                temp += '<button class="minimize-item" value="-"><i class="fa fa-minus" aria-hidden="true"></i></button></div>';
                                table_opis_content[table_opis_content.length] = table_kors;
                                table_opis.table.body[table_opis.table.body.length] = [{text:'',fillColor: '#f97254'},table_opis_content];
                            }
                            temp += '</div>';
                            temp += '</div>';
                            temp += '<button class="minimize-item" value="-"><i class="fa fa-minus" aria-hidden="true"></i></button></div>';
                            table_problem_content[table_problem_content.length] = table_opis;
                            table_problem.table.body[table_problem.table.body.length] = [{text:'',fillColor: '#2eb398'},table_problem_content];
                        }
                        temp += '</div>';
                        temp += '</div>';
                        temp += '<button class="minimize-item" value="-"><i class="fa fa-minus" aria-hidden="true"></i></button></div>';
                        table_program_content[table_program_content.length] = table_problem;
                        table_program.table.body[table_program.table.body.length] = [{text:'',fillColor: '#447aba'},table_program_content];
                    }

                    docInfo.content[docInfo.content.length] = table_program;

                    $("#popup-program .dialog-content").html( temp );
                    /**/
                        $( '.minimize-item' ).unbind( "click" );
                        $( '.minimize-item' ).bind('click', function(){
                            $( this ).siblings('.content-item').slideToggle( "fast" );
                            if ($( this ).val() == '-'){
                                $( this ).val('+');
                                $( this ).html('<i class="fa fa-plus" aria-hidden="true"></i>');
                            } else {
                                $( this ).val('-');
                                $( this ).html('<i class="fa fa-minus" aria-hidden="true"></i>');
                            }
                        });
                    /**/
                    //$('[aria-describedby="popup-program"] .ui-dialog-titlebar').removeClass( "color1, color2, color3, color4" ).addClass( "color3" );
                    $( "#popup-program" ).dialog( "open" );
                },
                error: function (result) {
                    $().toastmessage('showToast', {
                        text     : 'Error',
                        type     : 'error',
                    });
                }
            });
        }




    jQuery(document).ready(function(){

        $(".popup-plan").click(function(){
            get_plan($(this).attr("data-id-plan"));
        });

        $(".popup-program").click(function(){
            get_program($(this).attr("data-id-program"));
        });




        $( "#popup-plan" ).dialog({
            resizable: false,
            height: "auto",
            width: "60%",
            modal: true,
            autoOpen: false,
            buttons: [
                {
                    text: "Відкрити в PDF",
                    class: "btn-notice",
                    click: function() {
                        pdfMake.createPdf( docInfo ).open();
                        //$( this ).dialog( "close" );
                    }
                },
                {
                    text: "Закрити",
                    class: "btn-cancel",
                    click: function() {
                        $( this ).dialog( "close" );
                    }
                }
            ]
        });

        $( "#popup-program" ).dialog({
            resizable: false,
            height: "auto",
            width: "80%",
            modal: true,
            autoOpen: false,
            buttons: [
                {
                    text: "Відкрити в PDF",
                    class: "btn-notice",
                    click: function() {
                        pdfMake.createPdf( docInfo ).open();
                        // $( this ).dialog( "close" );
                    }
                },
                {
                    text: "Закрити",
                    class: "btn-cancel",
                    click: function() {
                        $( this ).dialog( "close" );
                    }
                }
            ]
        });

    });
</script>
@endsection
