<div class="content-item col-lg-{{ $item_width }} col-md-6">
    <img class="content-item-image" src="{{ $post->thumb_post }}" alt="">
    <a href="{{ route('all', [$cat->slug_cat, $post->slug_post] ) }}"><div class="content-item-title">{{$post->name_post}}</div></a>
</div>