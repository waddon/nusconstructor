@extends('common.layout-new')

    @section('content')

        <div class="content-box-title {{ $color or '' }}">{{ $title or 'Title' }}</div>
        <div class="content-box profile">
                <div class="row">
                    <div class="col-lg-4">
                        <img id="user-photo" src="@if(auth()->user()->thumb != '') {{ auth()->user()->thumb }} @else {{ '/assets/images/user-photo.png' }} @endif"
                            style="width:100%;">
                        <div class="fw" style="align-items: center;justify-content: center;">
                            <button type="button" class="add-photo" id="selectfile">Оберіть файл</button>
                            <input type="file" class="add-photo" name="photo" id="photo" accept="image/*" style="display:none;">
                            <span class="delete-photo">Видалити фото</span>
                            <input type="hidden" name="freephoto" id="freephoto" value="0">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <label>Email</label><input type="email" id="email" value="{{ auth()->user()->email }}" readonly>
                        <label>Призвищє</label><input type="text" name="second_name" id="second_name" value="{{ auth()->user()->second_name }}">
                        <label>Ім`я</label><input type="text" name="name" id="name" value="{{ auth()->user()->name }}">
                        <label>Побатькові</label><input type="text" name="patronymic" id="patronymic" value="{{ auth()->user()->patronymic }}">
                        
                    </div>

                    <div class="col-lg-6">
                        <h2>Навчальні заклади</h2>
                        @can('director')
                            <label>Поточна школа</label>
                            <input type="hidden" id="school" value="{{ auth()->user()->id_school }}" readonly>
                            <input type="text" id="temp" value="{{ $current_school }}" readonly>
                        @else
                            <label>Поточна школа</label>
                                <select id="school">
                                    @if (auth()->user()->id_school==0) <option></option>@endif
                                    @foreach ($schools as $school)
                                        <option value="{{ $school->id_school }}" @if($school->id_school == auth()->user()->id_school) {{'selected'}} @endif>{{ $school->name_school }}</option>
                                    @endforeach
                                </select>
                        <div class="fw" style="align-items: center;">
                            <label style="flex: 1 1 1px;">Перелік нерозглянутих заяв</label>
                            <span class="add-school">Додати заклад</span>
                        </div>

                            <div class="entry">
                                    @foreach ($entry_schools as $school)
                                        <p>{{ $school->name_school }}</p>
                                    @endforeach
                            </div>
                        @endcan

                    </div>

                    <div class="col-lg-6">
                        <h2>Змінити пароль</h2>
                        <label>Старий пароль</label><input type="password" id="password" value="">
                        <label>Новий пароль</label><input type="password" id="newpassword" value="">
                        <label>Підтвердіть новий пароль</label><input type="password" id="repeatnewpassword" value="">
                    </div>

                </div>

            <div style="text-align:center;"><button class="save btn-ok">Зберегти зміни</button></div>
        </div>        

    @endsection



    @section('dialogs')
        <div id="dialog-add-school" title="Додання навчального закладу">
                <div class="select-zors">
                    <span>Тип</span>
                    <select name="schooltype" id="schooltype">
                        <option value="0">Оберіть тип</option>
                        @foreach ($schooltypes as $schooltype)
                            <option value="{{ $schooltype->id_schooltype }}">{{ $schooltype->name_schooltype }}</option>
                        @endforeach
                    </select>
                    <span>Регіон</span>
                    <select name="region" id="region">
                        <option>Оберіть регіон</option>
                        @foreach ($regions as $region)
                            <option>{{ $region->region_school }}</option>
                        @endforeach
                    </select>
                    <span>Фільтр</span>
                    <!--input type="text" id="filter"-->
                    <input type="text" id="search">
                </div>

            <div class="dialog-content">
                <table class="list-schools">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Назва</td>
                            <td>Адреса</td>
                            <td>Дія</td>
                        </tr>
                    </thead>
                    <tbody class="body-list-school">
                        
                    </tbody>
                </table>
            </div>
        </div>
    @endsection



    @section('scripts')
        <script>
            jQuery(document).ready(function($){

                                            /* Фильтр */
                                            var $rows = $('.body-list-school tr');
                                            $('#search').keyup(function() {
                                                var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
                                                $rows.show().filter(function() {
                                                    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                                                    return !~text.indexOf(val);
                                                }).hide();
                                            });


                var parent = $(".body-list-school");
                var items = {};
                var files;


                /* прикрепляем фото */
                $('input[type=file]').change(function(){
                    files = this.files;
                    readURL(this);
                    $("#freephoto").val(0);                    
                });


                $("#selectfile").click(function(){
                    $("#photo").click();
                });

                /* удаляем фото */
                $(".delete-photo").click(function(){
                    $("#user-photo").attr("src", "/assets/images/user-photo.png");
                    $("#freephoto").val(1);                    
                });



                /* ajax сохранение */
                $(".save").click(function(){
                        var data = new FormData();
                            if (files) {
                                $.each( files, function( key, value ){
                                    data.append( key, value );
                                });
                            }
                            data.append( 'name', $("#name").val() );
                            data.append( 'second_name', $("#second_name").val() );
                            data.append( 'patronymic', $("#patronymic").val() );
                            data.append( 'id_school', $("#school").val() );
                            data.append( 'password', $("#password").val() );
                            data.append( 'newpassword', $("#newpassword").val() );
                            data.append( 'repeatnewpassword', $("#repeatnewpassword").val() );                            
                            if ($("#freephoto").val()==1) {data.append( 'freephoto', $("#freephoto").val());}

                        $.ajax ({
                            url: '{{ route("user-profile-save") }}',
                            type: "POST",
                            dataType: 'json',
                            data: data,
                            processData: false,
                            contentType : false,
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            success: function (result) {
                                $().toastmessage('showToast', {
                                    text     : result['status'],
                                    type     : 'success',
                                });
                                $("img.user-face").attr('src', $('#user-photo').attr('src'));
                            },
                            error: function (result) {
                                $().toastmessage('showToast', {
                                    text     : 'Error saved',
                                    type     : 'error',
                                });
                            }
                        });
                });



                /* отображаем загруженную картинку */
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#user-photo').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }



                /* нажатие на кнопку добавления школы */
                $( ".add-school" ).click(function(){
                    $( "#dialog-add-school" ).dialog( "open" );
                });



                /* модальное окно со списком школ */
                $( function() {
                    $( "#dialog-add-school" ).dialog({
                        resizable: false,
                        width: "80%",
                        modal: true,
                        autoOpen: false,
                        show: {
                            effect: "fade",
                            duration: 100
                        },
                        hide: {
                            effect: "fade",
                            duration: 100
                        },
                    });
                });



                /* выбираем регион */
                $("#region, #schooltype").change(function(){
                    $("#filter").val("");
                    $(".body-list-school").empty();

                    $.get('{{ route("user-profile-get-schools") }}', {region_school: $("#region").val(), schooltype: $("#schooltype").val()} ,function(result){
                        result = JSON.parse(result);
                        $.each(result.list_schools, function(index, value) {
                            $(".body-list-school").append('<tr><td class="id_school">'+value.id_school+'</td><td>'+value.name_school+'</td><td>'+value.adress_school+'</td><td><span class="school-button table-button"><i class="fa fa-plus" aria-hidden="true"></i></span></td></tr>');
                            school = {};
                            school['id_school'] = value.id_school;
                            school['name_school'] = value.name_school;
                            school['adress_school'] = value.adress_school;
                            items[index] = school;
                        });
                        $rows = $('.body-list-school tr');
                        /* нажатие кнопки напротив школы */
                        $(".school-button").click(function(){
                            add_school_entry($(this).parent().siblings(".id_school").text());
                        });
                    });
                });



                /* фильтруем записи */
                // $("#filter").keyup(function(){
                //     let filteredObj = objectFilter(items, $("#filter").val().toLowerCase());
                // });

                // function objectFilter(obj, search) {
                //     parent.empty();
                //     filtered = {};
                //     $.each(obj, function(index, value) {
                //         if (value.name_school.toLowerCase().indexOf( search )>=0 || value.adress_school.toLowerCase().indexOf( search )>=0){
                //             school = {};
                //             school['id_school'] = value.id_school;
                //             school['name_school'] = value.name_school;
                //             school['adress_school'] = value.adress_school;
                //             filtered[index] = school;
                //             parent.append('<tr><td class="id_school">'+value.id_school+'</td><td>'+value.name_school+'</td><td>'+value.adress_school+'</td><td><button class="school-button">+</button></td></tr>');
                //         }
                //     });
                    /* нажатие кнопки напротив школы */
                //     $(".school-button").click(function(){
                //         add_school_entry($(this).parent().siblings(".id_school").text());
                //     });
                //     return filtered;
                // }



                /* Обработка нажатия кнопки возле школы */
                function add_school_entry(id_school=''){

                    message = '';
                    // message = id_school;
                    type='success';

                    $(".ui-dialog-titlebar-close").click();

                    param = {id_school: id_school};

                        $.ajax({
                            type: 'POST',
                            url: '{{ route("user-profile-add-entry") }}',
                            dataType: 'json',
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            data: (param),
                            success: function (result) {
                                if (result['status'] == 'Ok' ) {
                                        message = 'Заяву на прийняття до лав закладу освіти направлено до керівництва';
                                        type='success';
                                        $(".entry").append("<p>"+result['name_school']+"</p>");
                                    }
                                if (result['status'] == 'Repeat' ) {message = 'Раніше подана заява знаходится на розгляді або доступ вже отримано';type='notice'}
                                $().toastmessage('showToast', {
                                    text     : message,
                                    type     : type,
                                });
                            },
                            error: function (result) {
                                $().toastmessage('showToast', {
                                    text     : 'Error',
                                    type     : 'error',
                                });
                            }
                        });
                }

            });
        </script>
    @endsection
