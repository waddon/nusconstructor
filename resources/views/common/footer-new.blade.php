@section('popup')
@show

    <div id="footer">
        <div class="container-fluid">
            <img src="http://polishproject.nus.org.ua/wp-content/uploads/2016/09/polska.pomoc_.png">
            Проект співфінансується у рамках Програми польської співпраці з розвитку МЗС Республіки Польща у 2018 р.
            {{--© Нова Українська школа, 2017--}}
        </div>
    </div>


@section('scripts')
@show

<script>
    $('.toggle-title').click(function(){
        $(this).siblings('.toggle-content').slideToggle( "fast" );
        if ( $(this).siblings('.toggle-icon').hasClass("toggle-open")) {
            $(this).siblings('.toggle-icon').removeClass( "toggle-open" );
            $(this).siblings('.toggle-icon').addClass( "toggle-close" );
        } else {
            $(this).siblings('.toggle-icon').removeClass( "toggle-close" );
            $(this).siblings('.toggle-icon').addClass( "toggle-open" );
        }
    });
</script>

</body>
</html>

