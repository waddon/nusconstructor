

@include('common.header-new')

@section('top')
@show

    <div id="body">
        <div class="container-fluid">            

        @section('content')
        @show

        </div>
    </div>

@section('bottom')
@show

<div style="display: none;">
@section('dialogs')
@show
</div>

@include('common.footer-new')