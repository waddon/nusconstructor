<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="/assets/images/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/lib/jQueryUI/jquery-ui.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/lib/toastmessage/css/jquery.toastmessage.css" />
    <link rel="stylesheet" href="/assets/lib/perfect-scrollbar/perfect-scrollbar.css">

    <link rel="stylesheet" href="/assets/css/typography.css">
    <link rel="stylesheet" href="/assets/css/login.css">
    <link rel="stylesheet" href="/assets/css/dialog.css">
    <link rel="stylesheet" href="/assets/css/glossary.css">
    <link rel="stylesheet" href="/assets/css/sidebar.css">
    <link rel="stylesheet" href="/assets/css/buttons.css">
    <link rel="stylesheet" href="/assets/css/forms.css">
    <link rel="stylesheet" href="/assets/css/plans.css">
    <link rel="stylesheet" href="/assets/css/news.css">
    <link rel="stylesheet" href="/assets/css/materials.css">
    <link rel="stylesheet" href="/assets/css/rivens.css">
    <link rel="stylesheet" href="/assets/css/zors.css">
    <link rel="stylesheet" href="/assets/css/galuzes.css">
    <link rel="stylesheet" href="/assets/css/iframe.css">
    <link rel="stylesheet" href="/assets/css/school.css">
    <link rel="stylesheet" href="/assets/css/programs.css">
    <link rel="stylesheet" href="/assets/css/popup.css">
    <link rel="stylesheet" href="/assets/css/profile.css">
    <link rel="stylesheet" href="/assets/css/tables.css">
    <link rel="stylesheet" href="/assets/css/search.css">
    <link rel="stylesheet" href="/assets/css/style.css">

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" ></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

    <script type="text/javascript" src="/assets/lib/chart/Chart.min.js"></script>

    <script type="text/javascript" src="/assets/lib/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'.tinymce', height : "400" });</script>
    <script type="text/javascript" src="/assets/lib/toastmessage/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="/assets/lib/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="/assets/js/script.js"></script>


<style>

@if ( isset( $galuzes ) )
    @foreach ($galuzes as $galuz)
        @if ($galuz->color_galuz != '')
            .color{{ $galuz->id_galuz }}:after
            {
                border: 7px solid transparent; border-left: 7px solid {{ $galuz->color_galuz }}; border-top: 7px solid {{ $galuz->color_galuz }};
            }
            .background{{$galuz->id_galuz}}
            {
                background: {{$galuz->color_galuz}};    
            }
        @endif
    @endforeach
@endif

#loginform-buttons li {
    width: 25%;
}
</style>
</head>
<body>

    <div id="editor"></div>
    <div id="sidebar">
        <div id="loginform" style="position: relative;">
            <div id="loginform-face">
                @auth
                    <div class="user-short-info">
                        <img class="user-face" src="@if(auth()->user()->thumb != '') {{ auth()->user()->thumb }} @else {{ '/assets/images/user-photo.png' }} @endif" alt="">
                        <div class="user-name"><div>{{ Auth::user()->name . ' ' . Auth::user()->second_name }}</div></div>
                        <a href="{{ route('user-profile') }}"><img src="/assets/images/settings.png" alt=""></a>
                    </div>
                @else
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-login">
                            <input type="email" placeholder="Поштова скрінька" style="margin-bottom: 5px;" id="email" name="email" value="{{ old('email') }}" required><br>
                            <input type="password" placeholder="Пароль" id="password" name="password" required>
                        </div>
                        <button type="submit"></button>
                    </form>
                @endauth
            </div>
            {{-- {{ $variable }} --}}
            {!! $topmenu !!}
            @can('director')
                <span class="notification">
                    {{ Auth::user()->hasNewNotifications() }}
                </span>
            @endcan


        </div>
        <div id="scroll-box">
            @auth
                @if (Auth::user()->hasAnyPermission('dashboard'))
                    <ul class="sidebar-menu">
                        <li class="sidebar-item-2"><a href="{{route('dashboard')}}"><span class="icon"><i class="fa fa-cogs" aria-hidden="true"></i></span><span class="name">Адміністрування</span></a></li>
                    </ul>
                @endif
            @endauth
                {!! $togglemenu !!}
        </div>
    </div>

    <div id="navbar" class="fw">

        <img src="/assets/images/logo-top-new.png" alt="">
        @include('common.form-search-new')        
        <div id="navbar-right-button" class="fw">            
            @auth
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><img src="/assets/images/logout.png" alt=""></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            @else
                <a href="{{ route('register') }}"><img src="/assets/images/register.png" alt=""></a>
            @endauth
        </div>

    </div>

