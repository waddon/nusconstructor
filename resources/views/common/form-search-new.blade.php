<div class="form-search fw">
	<form method="get" action="{{ route('search') }}" class="fw">
        <input type="text" placeholder="Пошук..." name="s" value="{{ $_GET['s'] or '' }}">
        <button type="submit" class="pull-right search-submit" /><i class="fa fa-search"></i></button>        
	</form>
</div>
