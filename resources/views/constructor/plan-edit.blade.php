@extends('common.layout-new')

    @section('content')

        <div class="content-box-title {{ $color or '' }}">{{ $title or 'Title' }}</div>
        <div class="content-box">
            {{ csrf_field() }}
            <div style="float: right;">{{ $plan->name_create }}</div>
            <p>Назва плану</p>
            <input type="text" id="name_plan" data-id-plan="{{ $plan->id_plan }}" value="{{ $plan->name_plan }}">

            <table id="plan">
                <tr>
                    <td class="col1 text-center">Тип предмету</td>
                    <td class="col2 text-center">Назва</td>
                    <td class="col3 text-center">
                        <table class="plan-galuz">
                            <tr>
                                <td class="col5 text-center">Галузь</td>
                                @foreach ($classes as $class)
                                    <td>{{ $class->name_class }}</td>
                                @endforeach
                                <td class="col4"></td>
                            </tr>
                        </table>
                    </td>
                    <td class="col4 text-center">Дія</td>
                </tr>
                @foreach ($content_plan as $subject)
                    <tr class="subject" data-program="{{$subject->program}}" data-name-program="{{$subject->name_program or ''}}" data-stype="{{$subject->type_subject}}">
                        <td class="col1">@if ($subject->type_subject == 1) {{ 'Предмет' }} @else {{ 'Інтегрований предмет' }} @endif</td>
                        <td class="col2">
                            @if ($subject->type_subject == 1)
                                @foreach ($subject->galuzes as $galuz)
                                    <?php $gal = $galuz->galuz; ?>
                                @endforeach
                                <select>
                                    @foreach ($subjects as $one_subject)
                                        @if ($one_subject['id_galuz'] == $gal) <option @if($one_subject['name_subject']==$subject->name_subject) {{'selected'}} @endif>{{ $one_subject['name_subject']}}</option>@endif
                                    @endforeach
                                </select>
                            @else
                                <input type="text" value="{{ $subject->name_subject }}">
                            @endif
                        </td>
                        <td class="col3">
                            <table class="plan-galuz">
                                <tbody>
                                    @foreach ($subject->galuzes as $galuz)
                                        <tr class="galuz" data-galuz="{{$galuz->galuz}}">
                                            <td class="col5">@foreach ($galuzes as $one_galuz) @if($one_galuz->id_galuz==$galuz->galuz) {{$one_galuz->name_galuz}} @endif @endforeach</td>
                                            @foreach ($galuz->classes as $class)
                                                <td class="class col6" data-class="{{ $class->class }}"><input type="number" value="{{$class->hours}}"></td>
                                            @endforeach
                                            <td class="col4">
                                                @if($subject->type_subject == 2)
                                                    @if ($plan->id_create==1)<button class="remove-item">X</button>@endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if($subject->type_subject == 2)
                                @if ($plan->id_create==1)<button class="add-galuz btn-add"><img class="add-items" src="/assets/images/add.png" alt=""> Додати галузь</button>@endif
                            @endif
                        </td>
                        <td class="col4">@if ($plan->id_create==1)<button class="remove-item">X</button>@endif</td>
                    </tr>
                @endforeach
            </table>

            @if ($plan->id_create==1)
                <button class="btn-add" id="add-subject"><img class="add-items" src="/assets/images/add.png" alt=""> Додати предмет</button>
                <button class="btn-add" id="add-kurs"><img class="add-items" src="/assets/images/add.png" alt=""> Додати інтегрований предмет</button>
            @endif
            <div class="text-right" style="margin-top: 30px;">
                <button class="btn-ok" id="save-plan">Зберегти план</button>
                <a href="{{ route('user-plans-list', 'school') }}"><button class="btn-warning">До списку планів</button></a>
            </div>
        </div>

    
    <div id="dialog-add-subject" class="dialog-window" title="Додати предмет">
        <p>Оберіть галузь</p>
        <select id="name-galuz">
            @foreach ($galuzes as $galuz)
                <option value="{{ $galuz->id_galuz }}">{{ $galuz->name_galuz }}</option>
            @endforeach
        </select>
    </div>

    <div id="dialog-add-galuz" class="dialog-window" title="Додати галузь">
        <p>Оберіть галузь</p>
        <select id="name-galuz2">
            @foreach ($galuzes as $galuz)
                <option value="{{ $galuz->id_galuz }}">{{ $galuz->name_galuz }}</option>
            @endforeach
        </select>
    </div>


    <script>

    var place;

    var subjects = $.parseJSON(("{{ $subjects_json }}").replace(/&quot;/g,'"'));

        /* Добавление галузи */
            $('.add-galuz').click(function(){
                place = $(this).siblings('.plan-galuz');
                $( "#dialog-add-galuz" ).dialog( "open" );
            });


        /* Добавление предмета */
            $('#add-subject').click(function(){
                place = $(this).siblings('#plan');
                $( "#dialog-add-subject" ).dialog( "open" );
            });


        /* Добавление курса */
            $('#add-kurs').click(function(){
                $(this).siblings('#plan').append('<tr class="subject" data-program="0" data-name-program="" data-stype="2" data-id-subject="new"><td class="col1">Інтегрований предмет</td><td class="col2"><input type="text"></td><td class="col3"><table class="plan-galuz"></table><button class="add-galuz btn-add"><img class="add-items" src="/assets/images/add.png" alt=""> Додати галузь</button></td><td class="col4"><button class="remove-item">X</button></td></tr>');
                /* переопределяем функцию добавления галузи */
                    $('.add-galuz').click(function(){
                        place = $(this).siblings('.plan-galuz');
                        $( "#dialog-add-galuz" ).dialog( "open" );
                    });
                /* переопределяем функцию удаления объекта */
                    $('.remove-item').click(function(){
                        $(this).parent().parent().remove();
                    });
            });


        /* Удаление объекта */
            $('.remove-item').click(function(){
                $(this).parent().parent().remove();
            });


        // Диалоговое окно добавления предмета
            $( "#dialog-add-subject" ).dialog({
                autoOpen: false,
                width: 400,
                modal: true,            
                buttons: [
                    {
                        text: "Додати",
                        class: "btn-ok",
                        click: function() {
                            var temp = '';
                            temp = '<tr class="subject" data-program="0" data-name-program="" data-stype="1" data-id-subject="new"><td class="col1">Предмет</td><td class="col2"><select>';
                            subjects.forEach(function(item, i) {
                                if (item.id_galuz == $( "#name-galuz" ).val()){
                                    temp += '<option value="' + item.id + '">' + item.name_subject + '</option>';
                                }

                            });
                            temp += '</select></td><td class="col3"><table class="plan-galuz"><tr class="galuz" data-galuz="'+$( "#name-galuz" ).val()+'"><td class="col5">'+$("#name-galuz option:selected").text()+'</td>';

                                @foreach ($classes as $class)
                                    temp += '<td class="class col6" data-class="{{$class->id_class}}"><input type="number"></td>';
                                @endforeach

                            temp += '<td class="col4"></td></tr></table></td><td class="col4"><button class="remove-item">X</button></td></tr>';

                            place.append(temp);



                            /* переопределяем функцию удаления объекта */
                                $('.remove-item').click(function(){
                                    $(this).parent().parent().remove();
                                });
                            $( this ).dialog( "close" );                        
                        }
                    },
                    {
                        text: "Скасувати",
                        class: "btn-cancel",
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });


        // Диалоговое окно добавления галузи
            $( "#dialog-add-galuz" ).dialog({
                autoOpen: false,
                width: 400,
                modal: true,            
                buttons: [
                    {
                        text: "Додати",
                        class: "btn-ok",
                        click: function() {
                            temp = '<tr class="galuz" data-galuz="'+$( "#name-galuz2" ).val()+'"><td class="col5">'+$("#name-galuz2 option:selected").text()+'</td>';

                                @foreach ($classes as $class)
                                    temp += '<td class="class col6" data-class="{{$class->id_class}}"><input type="number"></td>';
                                @endforeach

                            temp += '<td class="col4"><button class="remove-item">X</button></td></tr>';

                            place.append(temp);
                            /* переопределяем функцию удаления объекта */
                                $('.remove-item').click(function(){
                                    $(this).parent().parent().remove();
                                });
                            $( this ).dialog( "close" );                        
                        }
                    },
                    {
                        text: "Скасувати",
                        class: "btn-cancel",
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });


        // Сохранение плана
            $( "#save-plan").click(function(){

                var plan = new Object();

                /* Проставим нули там где пусто */
                $(".class input").each(function( index ) {if($(this).val()=='') $(this).val(0);});

                $("#plan .subject").each(function( index ) {
                    var subject = new Object();
                    subject.type_subject = $( this ).attr('data-stype');
                    if (subject.type_subject == 1) {
                        subject.name_subject = $( this ).children('.col2').children('select').children('option:selected').text();
                    } else {
                        subject.name_subject = $( this ).children('.col2').children('input').val();
                    }
                    subject.program = $( this ).attr('data-program');
                    subject.name_program = $( this ).attr('data-name-program');

                    var galuzes = new Object();
                    current_line = $( this ).children('.col3').children('.plan-galuz').children('tbody').children('.galuz');
                    current_line.each(function( index2 ){
                        var galuz = new Object();
                        galuz.galuz = $( this ).attr('data-galuz');

                        var classes = new Object();
                        current_galuz = $( this ).children('.class');
                        current_galuz.each(function( index3 ){
                            var myclass = new Object();
                            myclass.class = $( this ).attr('data-class');
                            myclass.hours = $( this ).children('input').val();
                            classes[ index3 ] = myclass;
                        });
                        galuz.classes = classes;
                        galuzes[ index2 ] = galuz;
                    });
                    subject.galuzes = galuzes;
                    plan[ index ] = subject;
                });

                /* ajax сохранение */

                    param = {id_plan: $("#name_plan").attr('data-id-plan'),
                            name_plan: $("#name_plan").val(),
                            content_plan: JSON.stringify(plan).replace(/\'/g,"&#39;")};

                        $.ajax({
                            type: 'POST',
                            url: '{{ route("user-plans-save") }}',
                            dataType: 'json',
                            headers: { 'X-CSRF-TOKEN': $('[name="_token"]').val() },
                            data: (param),
                            success: function (result) {
                                $().toastmessage('showToast', {
                                    text     : result,
                                    type     : 'success',
                                });
                            },
                            error: function (result) {
                                $().toastmessage('showToast', {
                                    text     : 'Error saved',
                                    type     : 'error',
                                });
                            }
                        });

            });

    </script>

    @endsection



