
@extends('common.layout-new')

    @section('content')

        <div class="content-box-title {{ $color or '' }}">{{ $title or 'Title' }}</div>
        <div class="content-box program">

            <div class="text-right" style="display: none;">Збереження - <span id="timer"></span>  Редагування - <span id="editor"></span>  Змінено - <span id="edited"></span></div>

            <div class="program-info">
                <label>Назва програми</label><input type="text" id="info-program" data-id="{{ $info_program->id_program }}" value="{{ $info_program->name_program or 'Нова програма' }}">
                <div class="row">
                    <div class="col-lg-6">
                        <label>Тип предмету</label><input type="text"  readonly value="{{ $info_program->name_stype }}">
                        <label>Тип створення</label><input type="text" readonly value="{{ $info_program->name_create }}">
                        <label>Цикл</label><input type="text" readonly value="{{ $info_program->name_cikl }}">
                    </div>
                    <div class="col-lg-6">
                        <label>Кількість годин на тиждень</label><input type="text" id="hours-program" value="{{ $info_program->hours_program }}">
                        <label>Галузі</label>
                        <div class="entry">
                            @foreach (json_decode($info_program->galuzes_program) as $galuz_program)
                                <span class="galuz-current-program" data-id-galuz="{{ $galuz_program->id_galuz }}">{{ $galuz_program->name_galuz }}</span><br>
                            @endforeach
                        </div>
                    </div>
                </div>
                
            </div>

            <div id="program" class="program">
                @foreach ($current_program as $line)
                    <div class="line">
                        <p class="title-item">@if($info_program->id_stype==1) Змістова лінія @else Тема @endif</p>
                        <div class="content-item">
                            @if($info_program->id_stype==1)
                                <select class="name_line" style="width: 100%;">
                                    @foreach ($zmists as $zmist)
                                        <option @if($line->name_line==$zmist->name_zmist) selected @endif>{{ $zmist->name_zmist}}</option>
                                    @endforeach
                                </select>
                            @else
                                <input class="name_line" type="text" value="{{ $line->name_line }}">
                            @endif
                            <p>Опис</p>
                            <textarea class="info_line">{{ $line->info_line }}</textarea>
                            <div class="problems">
                                @foreach ($line->problems as $problem)
                                    <div class="problem">
                                        <p class="title-item">Проблемне питання</p>
                                        <div class="content-item">
                                            <textarea class="name_problem">{{ $problem->name_problem }}</textarea>
                                            <div class="opises">
                                                @foreach ($problem->opises as $opis)
                                                    <div class="opis">
                                                        <p class="title-item">Опис діяльності</p>
                                                        <div class="content-item">
                                                            <textarea class="name_opis">{{ $opis->name_opis }}</textarea>
                                                            <table class="kors">
                                                                <tr>
                                                                    <td>Код</td>
                                                                    <td>Очікуваний результат</td>
                                                                    <td>Інструмент</td>
                                                                    <td></td>
                                                                </tr>
                                                                @foreach ($opis->kors as $kor)
                                                                    <tr class="kor">
                                                                        <td class="kod-kor color{{ $kor->galuz }}" data-galuz="{{ $kor->galuz }}" data-owner="{{ $kor->owner or '1' }}">{{ $kor->kod }}</td>
                                                                        <td class="name-kor">{{ $kor->name }}</td>
                                                                        <td class="tool-kor">
                                                                            <select>
                                                                                @foreach ($tools as $tool)
                                                                                    <option @if ($kor->tool == $tool->name_tool) {{ 'selected' }} @endif>{{ $tool->name_tool }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </td>
                                                                        <td><button class="remove-item">X</button></td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                            <button class="add-kor btn-add"><img class="add-items" src="/assets/images/add.png" alt=""> кор</button>
                                                            @if ($info_program->id_create==1)
                                                            <button class="remove-item"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                            @endif
                                                        </div>
                                                        <button class="minimize-item" value="-"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    </div>
                                                @endforeach
                                            </div>
                                            @if ($info_program->id_create==1)
                                            <button class="add-opis btn-add"><img class="add-items" src="/assets/images/add.png" alt=""> опис діяльності</button>
                                            <button class="remove-item"><i class="fa fa-times" aria-hidden="true"></i></button>
                                            @endif
                                        </div>
                                        <button class="minimize-item" value="-"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                    </div>
                                @endforeach
                            </div>
                            @if ($info_program->id_create==1)
                            <button class="add-problem btn-add"><img class="add-items" src="/assets/images/add.png" alt=""> Проблемне питання</button>
                            <button class="remove-item"><i class="fa fa-times" aria-hidden="true"></i></button>
                            @endif
                        </div>
                        <button class="minimize-item" value="-"><i class="fa fa-minus" aria-hidden="true"></i></button>
                    </div>
                @endforeach
            </div>
            @if ($info_program->id_create==1)
                <button id="add-line" class="btn-add"><img class="add-items" src="/assets/images/add.png" alt=""> @if($info_program->id_stype==1) Змістову лінію @else Тему @endif</button>
            @endif
            <div class="text-right" style="margin-top: 30px;margin-bottom: 30px;">
                <button class="btn-ok" id="save-program">Зберегти програму</button>
                <a href="{{ route('user-programs-list', 'school') }}"><button class="btn-warning">До списку програм</button></a>
            </div>

            <!-- Statictics -->
            <div class="card">
                <div class="card-header toggle-header">Статистика використання очікуваних результатів навчання</div>
                <div class="card-block toggle-content" style="display: none;">
                    
                    <div style="font-size: 14px;">
                        <h4 class="text-center">Кількість використаних корів</h4>
                        <div class="row">
                            <div class="col-lg-4">
                                <div style="width: 100%;">
                                    <canvas id="canvas-doughnut" width="300" height="250"></canvas>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div style="width: 100%;">
                                    <canvas id="canvas-bar" width="300" height="250"></canvas>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                @foreach ($galuzes as $galuz)
                                    <p style="margin-bottom: 0;">
                                        <span style="display:inline-block;height: 13px;width: 13px;background: {{$galuz->color_galuz}};"></span>
                                        <span class="count-kors galuz{{$galuz->id_galuz}}" data-count-kors="{{$galuz->count_kors or '0'}}" style="color: {{$galuz->color_galuz}};">{{$galuz->name_galuz}}</span>
                                    </p>
                                @endforeach
                            </div>
                        </div>
                        <h6 class="text-center">Відповідно до кожної галузі</h6>
                        <div class="fw">
                            <div class="left-buttons">                        
                                @foreach ($galuzes as $key => $galuz)
                                    @if ($key < count($galuzes)/2)
                                    <span class="frequency-galuz background{{$galuz->id_galuz}}" data-id-galuz="{{$galuz->id_galuz}}">{{$galuz->name_galuz}}</span>
                                    @endif
                                @endforeach
                            </div>
                            <div class="central-list">
                                <table class="frequency">
                                    <thead>
                                        <tr>
                                            <td>Код кору</td>
                                            <td>Назва кору</td>
                                            <td>Кількість використань</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($kors as $key => $kor)
                                            <tr class="frequency-kor" data-frequency-galuz="{{$kor->id_galuz}}" data-id-kor="{{$kor->id_kor}}" data-frequency-kor="{{$kor->key_kor}}">
                                                <td>{{$kor->key_kor}}</td>
                                                <td>{{$kor->name_kor}}</td>
                                                <td class="frequency-count">0</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="right-buttons">
                                @foreach ($galuzes as $key => $galuz)
                                    @if ($key >= count($galuzes)/2)
                                    <span class="frequency-galuz background{{$galuz->id_galuz}}" data-id-galuz="{{$galuz->id_galuz}}">{{$galuz->name_galuz}}</span>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        
    @endsection




@section('dialogs')
    <style>.dialog-content{max-height: 400px;overflow-y: scroll;overflow-x: hidden;}</style>
    <div id="select-kors" class="dialog-window" title="Обрати очікувані результати навчання">
            <select name="dialog-galuzes" id="dialog-galuzes" class="dialog-galuzes" style="margin-bottom: 5px;">
                <option value="0">Всі галузі</option>
                @foreach ($galuzes as $galuz)
                    <option value="{{$galuz->id_galuz}}">{{$galuz->name_galuz}}</option>
                @endforeach
            </select>
        <div class="dialog-content" style="padding:15px;">
            <table class="change-kors">
                <tr>
                    <td>КОД</td>
                    <td>НАЗВА</td>
                    <td>КІЛЬКІСТЬ</td>
                    <td>Дія</td>
                </tr>
                @foreach ($kors as $kor)
                    <tr class="frequency-kor2" data-frequency-galuz2="{{$kor->id_galuz}}" data-frequency-kor2="{{$kor->key_kor}}">
                        <td class="kod kod-kor color{{ $kor->id_galuz }}" data-galuz="{{ $kor->id_galuz }}" data-owner="1">{{ $kor->key_kor }}</td>
                        <td class="name">{{ $kor->name_kor }}</td>
                        <td class="frequency-count">0</td>
                        <td><input class="kor-checkBox" type="checkbox"></td>
                    </tr>
                @endforeach
                @foreach ($schoolkors as $schoolkor)
                    <tr class="frequency-kor2" data-frequency-galuz2="{{$schoolkor->id_galuz}}" data-frequency-kor2="{{$schoolkor->key_schoolkor}}">
                        <td class="kod kod-kor color{{ $schoolkor->id_galuz }}" data-galuz="{{ $schoolkor->id_galuz }}" data-owner="2">{{ $schoolkor->key_schoolkor }}</td>
                        <td class="name">{{ $schoolkor->name_schoolkor }}</td>
                        <td class="frequency-count">0</td>
                        <td><input class="kor-checkBox" type="checkbox"></td>
                    </tr>                    
                @endforeach
            </table>
        </div>
    </div>

    <div id="dialog-close" title="перехоплення редагування">
        <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
        Редагування перехоплено іншим користувачем. Йому буде відкрито останній збережений варіант документу. Вас буде повернено на сторінку переліку документів.</p>
    </div>

@endsection

@section('scripts')
    <script>
        jQuery(document).ready(function(){
            change_editor();

            @if (isset($zmists))
            //var zmists = $.parseJSON(('{{ $zmists }}').replace(/&quot;/g,'"'));
            var zmists = ('{{ $zmists }}').replace(/&quot;/g,'"');
            @endif

            /* Определение переменных */
            var mytime = 300; // Как часто авто запись
            var editor = 20; // Как часто опрос редактора
            make_save = true; // Можно ли сохранять (флаг, не происходит ли сохранение в данный момент)
            make_editor = true; // Можно ли проверять редактора (флаг, не происходит ли проверка в данный момент)

            var place;
            var new_line = '<div class="line"><p class="title-item">@if($info_program->id_stype==1) Змістова лінія @else Тема @endif</p><div class="content-item">';
            @if($info_program->id_stype==1)
                new_line += '<select class="name_line" style="width: 100%;">';
                @foreach ($zmists as $zmist)
                    new_line += '<option>{{ $zmist->name_zmist}}</option>';
                @endforeach
                new_line += '</select>';
            @else
                new_line += '<input class="name_line" type="text">';
            @endif

            new_line += '<p>Опис</p><textarea class="info_line"></textarea><div class="problems"></div><button class="add-problem btn-add"><img class="add-items" src="/assets/images/add.png" alt=""> Проблемне питання</button><button class="remove-item"><i class="fa fa-times" aria-hidden="true"></i></button></div><button class="minimize-item" value="-"><i class="fa fa-minus" aria-hidden="true"></i></button></div>';
            var new_problem = '<div class="problem"><p class="title-item">Проблемне питання</p><div class="content-item"><textarea class="name_problem"></textarea><div class="opises"></div><button class="add-opis btn-add"><img class="add-items" src="/assets/images/add.png" alt=""> опис діяльності</button><button class="remove-item"><i class="fa fa-times" aria-hidden="true"></i></button></div><button class="minimize-item" value="-"><i class="fa fa-minus" aria-hidden="true"></i></button></div>';
            var new_opis = '<div class="opis"><p class="title-item">Опис діяльності</p><div class="content-item"><textarea class="name_opis"></textarea><table class="kors"><tr><td>Код</td><td>Очікуваний результат</td><td>Інструмент</td><td></td></tr></table><button class="add-kor btn-add"><img class="add-items" src="/assets/images/add.png" alt=""> кор</button><button class="remove-item"><i class="fa fa-times" aria-hidden="true"></i></button></div><button class="minimize-item" value="-"><i class="fa fa-minus" aria-hidden="true"></i></button></div>';
            var new_kor = '<tr class="kor"><td class="kod-kor">2 МОВ 1.8-2</td><td class="name-kor">Уживає форму кличного відмінка іменника та ввічливі слова</td><td class="tool-kor"><select><option>Спостереження</option><option>Завдання</option></select></td><td><button class="remove-item">X</button></td></tr>';

            var new_kor_part1 = '<tr class="kor"><td class="kod-kor color';
            var new_kor_part1_1 = '" data-galuz="';
            var new_kor_part1_2 = '" data-owner="';
            var new_kor_part1_3 = '">';
            var new_kor_part2 = '</td><td class="name-kor">';
            var new_kor_part3 = '</td><td class="tool-kor"><select>@foreach ($tools as $tool)<option>{{ $tool->name_tool }}</option>@endforeach</select></td><td><button class="remove-item">X</button></td></tr>';
                    
            /* Минимизируем все после загрузки */
            // minimize_all();

            /* добавление змістової линии */
            $('#add-line').click(function(){
                $('#program').append( new_line );
                $( '.add-problem' ).unbind( "click" );
                $( '.add-problem' ).bind('click', function(){
                    place = $(this).siblings('.problems');
                    place.append( new_problem );
                    $( '.add-opis' ).unbind( "click" );
                    $( '.add-opis' ).bind('click', function(){
                        place = $(this).siblings('.opises');
                        place.append( new_opis );
                        $( '.add-kor' ).unbind( "click" );
                        $( '.add-kor' ).bind('click', function(){
                            place = $(this).siblings('.kors');
                            $( "[type=checkbox]" ).attr('checked',false);
                            $( "#select-kors" ).dialog( "open" );
                        });
                        remove_item_rebind();
                    });
                    remove_item_rebind();
                });
                remove_item_rebind();
            });



            /* Добавление проблемы */
            $( '.add-problem' ).bind('click', function(){
                place = $(this).siblings('.problems');
                place.append( new_problem );
                $( '.add-opis' ).unbind( "click" );
                $( '.add-opis' ).bind('click', function(){
                    place = $(this).siblings('.opises');
                    place.append( new_opis );
                    $( '.add-kor' ).unbind( "click" );
                    $( '.add-kor' ).bind('click', function(){
                        place = $(this).siblings('.kors');
                        $( "[type=checkbox]" ).attr('checked',false);
                        $( "#select-kors" ).dialog( "open" );
                    });
                    remove_item_rebind();
                });
                remove_item_rebind();
            });



            /* Добавление описания действия */    
            $( '.add-opis' ).bind('click', function(){
                place = $(this).siblings('.opises');
                place.append( new_opis );
                $( '.add-kor' ).unbind( "click" );
                $( '.add-kor' ).bind('click', function(){
                    place = $(this).siblings('.kors');
                    $( "[type=checkbox]" ).attr('checked',false);
                    $( "#select-kors" ).dialog( "open" );
                });
                remove_item_rebind();
            });



            /* добавление коров */
            $( '.add-kor' ).bind('click', function(){
                place = $(this).siblings('.kors');
                $( "[type=checkbox]" ).attr('checked',false);
                $( "#select-kors" ).dialog( "open" );
            });



            /* Сохранение программы */
            $('#save-program').click(function(){
                var program = new Object();
                $("#program .line").each(function( index ) {
                    var line = new Object();
                    line.name_line = $( this ).children('.content-item').children('.name_line').val();
                    line.info_line = $( this ).children('.content-item').children('.info_line').val();
                    var problems = new Object();
                    current_line = $( this ).children('.content-item').children('.problems').children('.problem');
                    current_line.each(function( index2 ){
                            var problem = new Object();
                            problem.name_problem = $( this ).children('.content-item').children('.name_problem').val();
                            var opises = new Object();
                            current_opis = $( this ).children('.content-item').children('.opises').children('.opis');
                            current_opis.each(function( index3 ){
                                    var opis = new Object();
                                    opis.name_opis = $( this ).children('.content-item').children('.name_opis').val();
                                    var kors = new Object();
                                    current_kor = $( this ).children('.content-item').children('.kors').children('tbody').children('.kor');
                                    current_kor.each(function( index4 ){
                                        var kor = new Object();
                                        kor.galuz = $( this ).children('.kod-kor').data('galuz');
                                        kor.owner = $( this ).children('.kod-kor').data('owner');
                                        kor.kod = $( this ).children('.kod-kor').text();
                                        kor.name = $( this ).children('.name-kor').text();
                                        kor.tool = $( this ).children('.tool-kor').children('select').val();
                                        kors[ index4 ] = kor;
                                    });
                                    opis.kors = kors;
                                    opises[ index3 ] = opis;
                            });
                            problem.opises = opises;
                            problems[ index2 ] = problem;
                    });
                    line.problems = problems;
                    program[ index ] = line;
                });

                /* отправка ajax-запроса на сохранение */

                    param = {id_program: $("#info-program").attr('data-id'),
                            name_program: $("#info-program").val(),
                            hours_program: $("#hours-program").val(),
                            content_program: JSON.stringify(program).replace(/\'/g,"&#39;")};

                    if (changed == 'yes') {
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('user-program-save') }}',
                            dataType: 'json',
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            data: (param),
                            success: function (result) {
                                // console.log(result);
                                // console.log(result.id_program);
                                $().toastmessage('showToast', {
                                    text     : result.message,
                                    type     : 'success',
                                });
                                $("#info-program").attr('data-id', result.id_program );
                                changed = 'no';
                                $('#edited').text(changed);
                                make_save = true;
                                mytime = 300;
                                //change = 10;
                            },
                            error: function (result) {
                                $().toastmessage('showToast', {
                                    text     : 'Error saved',
                                    type     : 'error',
                                });
                            }
                        });
                    } else {
                        $().toastmessage('showToast', {
                            text     : 'Изменений нет',
                            type     : 'success',
                        });
                        make_save = true;
                        mytime = 300;
                        //change = 10;
                    }

            });



            /* Переопределение кнопки удаления */
            function remove_item_rebind(){
                $( '.remove-item' ).unbind( "click" );
                $( '.remove-item' ).bind('click', function(){
                    $(this).parent().parent().remove();
                    edited();
                    chart_update();
                });            
                $( '.minimize-item' ).unbind( "click" );
                $( '.minimize-item' ).bind('click', function(){
                    $( this ).siblings('.content-item').slideToggle( "fast" );
                    if ($( this ).val() == '-'){
                        $( this ).val('+');
                        $( this ).html('<i class="fa fa-plus" aria-hidden="true"></i>');
                    } else {
                        $( this ).val('-');
                        $( this ).html('<i class="fa fa-minus" aria-hidden="true"></i>');
                    }
                });
                edited();
                chart_update();
            }



            /* Минимизация всего */
            function minimize_all(){
                $('.content-item').toggle();
                $( '.minimize-item' ).val('+');
                $( '.minimize-item' ).html('<i class="fa fa-plus" aria-hidden="true"></i>');
            }


        
            /* Модальное окно выбора коров */
            $( "#select-kors" ).dialog({
                autoOpen: false,
                width: '80%',
                modal: true,            
                buttons: [
                    {
                        text: "Додати",
                        class: "btn-ok",
                        click: function() {
                            $(".kor-checkBox:checked").each(function( index ) {
                                place.append( new_kor_part1 + $(this).parent().siblings('.kod').data('galuz') + new_kor_part1_1 + $(this).parent().siblings('.kod').data('galuz') + new_kor_part1_2 + $(this).parent().siblings('.kod').data('owner') + new_kor_part1_3 +
                                        $(this).parent().siblings('.kod').text() +
                                        new_kor_part2 +
                                        $(this).parent().siblings('.name').text() +
                                        new_kor_part3 );
                                remove_item_rebind();
                            });
                            // chart_update();
                            $( this ).dialog( "close" );
                        }
                    },
                    {
                        text: "Скасувати",
                        class: "btn-cancel",
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });



            /* Проверка режима редактирования */
            function change_editor(){
                $.get('{{ route("user-program-change-editor") }}', {id_program: $("#info-program").attr('data-id')} ,function(result){
                    result = JSON.parse(result);
                    if (result.status==1){
                        console.log('Ok');
                        editor = 20;
                        make_editor = true;
                    } else {
                        changed='no';
                        clearInterval(timerId);
                        $( "#dialog-close" ).dialog( "open" );
                    }
                });
            }



            /* освобождение документа от отметки о редактировании */
            function program_free(){
                $.get('{{ route("user-program-free") }}', {id_program: $("#info-program").attr('data-id')} ,function(result){
                });
            }



            /* Проверка внесенных изменений и вызов модального окна подтверждения выхода */
            $('input[type="text"], select:not(#dialog-galuzes), textarea').change(function() {
                edited();
            });
             
            $(window).on('beforeunload', function() {
                if (changed == 'yes'){
                    return ("Были внесены изменения, которые НЕ БЫЛИ сохранены");
                }
                program_free();
            });



            /* Таймер */
            var timerId = setInterval(function() {
                mytime -= 1;
                editor -= 1;
                $('#timer').text( mytime );
                $('#editor').text( editor );
                
                if (mytime <1 && make_save){
                    make_save = false;
                    $('#save-program').click();
                }

                if (editor <1 && make_editor){
                    make_editor = false;
                    change_editor();
                }

            }, 1000);



            /* Закрытие при перехвате */
            $( function() {
                $( "#dialog-close" ).dialog({
                    resizable: false,
                    height: "auto",
                    width: 500,
                    modal: true,
                    autoOpen: false,
                    buttons: [
                        {
                            text: "До переліку програм",
                            class: "btn-cancel",
                            click: function() {
                                $( this ).dialog( "close" );
                            }
                        }
                    ],
                    close: function() {
                        $(location).attr('href','{{ route('user-programs-list', 'school') }}');

                    }            
                });
            });



            /* Отметка о внесении изменений */
            function edited(){
                changed='yes';
                $('#edited').text(changed);
            }




            {{-- Диаграммы --}}

            window.chartColors = {
                @foreach ($galuzes as $key=>$galuz)
                color{{$key}} : '{{$galuz->color_galuz}}',
                @endforeach
            };
            var color = Chart.helpers.color;

            var doughnutChartData = {
            datasets: [{
                data: [1,2,3],
                backgroundColor: [
                    window.chartColors.color0,
                    window.chartColors.color1,
                    window.chartColors.color2,
                    window.chartColors.color3,
                    window.chartColors.color4,
                    window.chartColors.color5,
                    window.chartColors.color6,
                    window.chartColors.color7,
                    window.chartColors.color8,
                    window.chartColors.color9
                ],
                label: 'Dataset 0'
            }],
            labels: [
                @foreach ($galuzes as $key=>$galuz)
                    "{{$galuz->name_galuz}}",
                @endforeach
                ]
            };

            var barChartData = {
                datasets: [
                    @foreach ($galuzes as $key=>$galuz)
                    {
                        label: '{{$galuz->name_galuz}}',
                        backgroundColor: color(window.chartColors.color{{$key}}).alpha(0.9).rgbString(),
                        borderColor: color(window.chartColors.color{{$key}}).rgbString(),
                        borderWidth: 1,
                        data: [ {{$key}} ]
                    },
                    @endforeach
                ]

            };

            var ctx1 = document.getElementById("canvas-doughnut").getContext("2d");
            window.myBar1 = new Chart(ctx1, {
                type: 'doughnut',
                data: doughnutChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Відповідно до використаних КОРів (кількість)'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });


            var ctx2 = document.getElementById("canvas-bar").getContext("2d");
            window.myBar2 = new Chart(ctx2, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Відповідно до існуючих КОРів (%)'
                    }
                }
            });


            function chart_update(){
                var obj = {};
                var kor_count = [];
                var per_count = [];
                @foreach ($galuzes as $key=>$galuz)
                    kor_count[{{$key}}] = 0;
                    per_count[{{$key}}] = 0;
                @endforeach

                $(".frequency-kor, .frequency-kor2").children(".frequency-count").text("0");
                $(".kor").each(function( index ){
                    if ($( this ).children('.kod-kor').data('owner')==1){
                        var str = $( this ).children('.kod-kor').text();
                        var temp_int = parseInt($('[data-frequency-kor="'+str+'"]').children(".frequency-count").text());
                        $('[data-frequency-kor="'+str+'"]').children(".frequency-count").text( temp_int+1 );
                        $('[data-frequency-kor2="'+str+'"]').children(".frequency-count").text( temp_int+1 );
                        obj[ str ] = $( this ).children('.kod-kor').data('galuz');
                    } else {
                        var str = $( this ).children('.kod-kor').text();
                        var temp_int = parseInt($('[data-frequency-kor2="'+str+'"]').children(".frequency-count").text());
                        $('[data-frequency-kor2="'+str+'"]').children(".frequency-count").text( temp_int+1 );
                    }
                });
                jQuery.each(obj, function( index ){
                    kor_count[parseInt(this)-1] ++;
                });

                jQuery.each(kor_count, function(i) {
                    var temp_int = parseInt($(".galuz"+(i+1)).attr("data-count-kors"));
                    if (temp_int>0) {per_count[i] = 100 / temp_int * kor_count[i] ;}
                });

                jQuery.each(barChartData.datasets, function(i) {
                    this.data = [ per_count[i] ];
                });


                doughnutChartData.datasets[0].data = kor_count;
                window.myBar1.update();
                window.myBar2.update();
            }


            $(".frequency-galuz").click(function(){
                $(".frequency-galuz").removeClass("background-all");
                $(this).addClass("background-all");
                $(".frequency-kor").hide();
                $('[data-frequency-galuz="'+$(this).attr("data-id-galuz")+'"]').show();
            });

            $(".kor-checkBox").change(function(){
                if ($(this).prop("checked")){
                    $(this).parent().siblings(".frequency-count").text(parseInt($(this).parent().siblings(".frequency-count").text())+1);
                } else {
                    $(this).parent().siblings(".frequency-count").text(parseInt($(this).parent().siblings(".frequency-count").text())-1);
                }
            });

            $("#dialog-galuzes").change(function(){
                if ($(this).val() == 0) {
                    $(".frequency-kor2").show();
                } else {
                    $(".frequency-kor2").hide();
                    $('[data-frequency-galuz2="'+$(this).val()+'"]').show();
                }
            });

            remove_item_rebind();
            var changed='no';$('#edited').text(changed);
        });


    </script>
@endsection

