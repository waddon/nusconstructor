@extends('common.layout-new')

    @section('content')

        <div class="content-box-title {{ $color or '' }}">{{ $title or 'Plan List' }}</div>
        <div class="content-box">
            <table class="list">
                <thead>
                    <tr>
                        <td class="sort">#</td>
                        <td class="sort">Назва</td>
                        <td class="sort">Цикл</td>
                        <td class="sort">Предметів</td>
                        <td class="sort">Програм</td>
                        <td class="sort">Створено</td>
                        <td class="sort">Змінено</td>
                        @if ( $type > 1)                            
                            <td class="sort">Базовий план</td>
                        @endif
                        @if ( $type > 2)
                            <td class="sort">Створення</td>
                            <td class="sort">Статус</td>
                        @endif
                    <td class="action-buttons">Дії</td>
                    </tr>
                </thead>

                <tbody id="plan-list">
                    @foreach ($plans as $plan)
                        <tr id="plan{{ $plan->id_plan }}">
                            <td class="text-center">{{ $plan->id_plan }}</td>
                            <td class="name-plan">{{ $plan->name_plan }}</td>
                            <td class="text-center">{{ $plan->name_cikl }}</td>
                            <td class="text-center">{{ $plan->subjects_count }}</td>
                            <td class="programs-count text-center">{{ $plan->programs_count }}</td>
                            <td>{{ $plan->date1 }}</td>
                            <td>{{ $plan->date2 }}</td>
                            @if ( $type > 1)
                                <td class="text-center">{{ $plan->id_parent }}</td>
                            @endif
                            @if ( $type > 2)
                                <td class="text-center">{{ $plan->name_create }}</td>
                                <td class="name-status text-center" data-id-status="{{$plan->id_status}}">{{ $plan->name_status }}</td>
                            @endif
                            <td class="action-buttons text-center">
                                <span class="popup-plan table-button" data-id-plan="{{$plan->id_plan}}" data-toggle="tooltip" title="Переглянути"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                @can('director')                                
                                    @if ( $type == 1)
                                        <span class="from-basic table-button" data-id-plan="{{ $plan->id_plan }}" data-toggle="tooltip" title="Створити пустий план на основі базового"><i class="fa fa-files-o" aria-hidden="true"></i></span>
                                    @elseif ( $type == 2 )
                                        <span class="from-typical table-button" data-id-plan="{{ $plan->id_plan }}" data-toggle="tooltip" title="Створити копію типового з усіма програмами"><i class="fa fa-files-o" aria-hidden="true"></i></span>
                                    @elseif ( $type == 3 )
                                        <span class="check table-button" data-id-plan="{{ $plan->id_plan }}" data-id-cikl="{{ $plan->id_cikl }}" data-toggle="tooltip" title="Детальний перегляд" @if($plan->id_status>3)style="display:none;"@endif><i class="fa fa-search" aria-hidden="true"></i></span>
                                        <span class="edit table-button" data-id-plan="{{ $plan->id_plan }}" data-toggle="tooltip" title="Редагувати" @if($plan->id_status!=1)style="display:none;"@endif><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                        <span class="delete table-button" data-id-plan="{{ $plan->id_plan }}" data-toggle="tooltip" title="Видалити" @if($plan->id_status!=1)style="display:none;"@endif><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <span class="perevirka table-button" data-id-plan="{{ $plan->id_plan }}" data-toggle="tooltip" title="Перевірка та підготовка до узгодження" @if($plan->id_status!=1)style="display:none;"@endif><i class="fa fa-check" aria-hidden="true"></i></span>
                                        <span class="perevirka table-button" data-id-plan="{{ $plan->id_plan }}" data-no="true" data-toggle="tooltip" title="Підготовка до узгодження без перевірки" @if($plan->id_status!=1)style="display:none;"@endif><i class="fa fa-bolt" aria-hidden="true"></i></span>
                                    @endif
                                @endcan
                            </td>
                        </tr>
                    @endforeach

                </tbody>

            </table>
        </div>

    @endsection


    @section('dialogs')
        <div id="dialog-plan-one" title="Детальна інформація стосовно навчального плану">
            <div class="dialog-content">
                
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">Прикріплені програми</a></li>
                        <li><a href="#tabs-2">Навантаження</a></li>
                        <li><a href="#tabs-3">Використання КОРів</a></li>
                    </ul>
                    <div id="tabs-1"></div>
                    <div id="tabs-2"></div>
                    <div id="tabs-3">
                        <table class="list">
                            <thead>
                                <tr><td rowspan="2">Галузь</td><td colspan="4">Очікувані результати навчання</td></tr>
                                <tr><td>Всього стандартних</td><td>Всього у програмі</td><td>Стандартних у програмі</td><td>Навчального закладу</td></tr>
                            </thead>
                            <tbody>
                                @foreach ($chartgaluzes as $chartgaluz)
                                    <tr class="chartgaluz{{$chartgaluz->id_galuz}}"><td>{{$chartgaluz->name_galuz}}</td><td class="text-center data-count-standart" data-count="standart"></td><td class="text-center" data-count="program"></td><td class="text-center" data-count="intersection"></td><td class="text-center" data-count="school"></td></tr>
                                @endforeach
                            </tbody>
                        </table>
                        <h2>Стандартні</h2>
                        <div class="row">
                            <div class="col-lg-4">
                                <div style="width: 100%;">
                                    <canvas id="canvas-doughnut" width="300" height="250"></canvas>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div style="width: 100%;">
                                    <canvas id="canvas-bar" width="300" height="250"></canvas>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                @foreach ($chartgaluzes as $chartgaluz)
                                    <p style="margin-bottom: 0;">
                                        <span style="display:inline-block;height: 13px;width: 13px;background: {{$chartgaluz->color_galuz}};"></span>
                                        <span class="count-kors galuz{{$chartgaluz->id_galuz}}" data-count-kors="{{$chartgaluz->count_kors or '0'}}" style="color: {{$chartgaluz->color_galuz}};">{{$chartgaluz->name_galuz}}</span>
                                    </p>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div id="add-program-to-plan" title="Прикріпити програму">
            <div class="dialog-content">
            </div>
        </div>

    @endsection



    @section('scripts')
        <script>
            jQuery(document).ready(function($){

                var hexathis, current_plan = {};

                @can('director')
                        $(".from-basic").click(function(){
                            $.get('{{ route("user-plans-new-from-basic") }}', {id_plan: $(this).attr("data-id-plan")} ,function(result){
                                $().toastmessage('showToast', {
                                    text     : 'Пустий план створено',
                                    type     : 'success',
                                });
                            });
                        });

                        $(".from-typical").click(function(){
                            $.get('{{ route("user-plans-new-from-typical") }}', {id_plan: $(this).attr("data-id-plan")} ,function(result){
                                $().toastmessage('showToast', {
                                    text     : 'Копію плану та пов`язаних програм створено',
                                    type     : 'success',
                                });
                            });
                        });

                        $(".edit").click(function(){
                            $(location).attr('href', '{{route("user-plans-edit")}}/'+$(this).attr("data-id-plan"));
                        });

                        $(".delete").click(function(){
                            hexathis=$(this);
                            $.get('{{ route("user-plans-delete") }}', {id_plan: $(this).attr("data-id-plan")} ,function(){
                                $().toastmessage('showToast', {
                                    text     : 'План видалено',
                                    type     : 'success',
                                });
                                hexathis.parent().parent().remove();
                            });
                        });

                @endcan



                /* нажатие на проверку плана */
                $( ".check" ).click(function(){
                    $(".ui-dialog-titlebar").removeClass( "color4" ).addClass( "color4" );
                    temp_int = parseInt($(this).attr("data-id-plan"));
                    temp_int2 = parseInt($(this).attr("data-id-cikl"));
                    temp_str = $(this).parent().siblings(".name-plan").text();
                    // if ($(this).parent().siblings(".name-status").attr("data-id-status") != "1"){
                    //     $(".btn-ok").hide();
                    // } else {
                    //     $(".btn-ok").show();
                    // }

                    $.get('{{ route("user-plans-check") }}', {id_plan: $(this).attr("data-id-plan")} ,function(result){
                        result = JSON.parse(result);
                        console.log(result);

                        result.content_plan = JSON.parse(result.content_plan);
                        current_plan.id_plan = temp_int;
                        current_plan.name_plan = temp_str;
                        current_plan.content_plan = result.content_plan;

                        $( "#tabs-1" ).empty();
                        temp = '<table class="check"><thead><tr><td>#</td><td>Назва</td><td>Програма</td><td>Дія</td></tr></thead><tbody>';
                        for (key in result.content_plan) {
                            temp +='<tr class="list-of-subjects subject'+key+'"><td>' + key + '</td><td class="name-subject">' + result.content_plan[key].name_subject + '</td><td class="name-program" data-id-program="'+result.content_plan[key].program+'">'+result.content_plan[key].name_program+'</td><td><span class="add-program-to-subject table-button" data-id-subject="'+key+'" data-id-cikl="'+temp_int2+'" data-id-stype="'+result.content_plan[key].type_subject+'"><i class="fa fa-plus" aria-hidden="true"></i></span> <span class="remove-program-from-subject table-button"><i class="fa fa-minus" aria-hidden="true"></i></span></td></tr>';

                            // $( "#tabs-1" ).append();
                        }
                        temp += '</tbody></table>';
                        $( "#tabs-1" ).append( temp );
                        /* Переопределяем функцию нажатия на кнопки */
                        $(".add-program-to-subject").click(function(){ hexathis = $(this); change_program(); });
                        $(".remove-program-from-subject").click(function(){ $(this).parent().siblings(".name-program").attr("data-id-program", "0"); $(this).parent().siblings(".name-program").empty();});

                        /* Открываем диалоговое окно*/
                        $( "#tabs-2" ).empty();
                        $( "#tabs-2" ).append( result.output );
                        /* Меняем данные диаграмм */

                        chart_update(result.plan_fullinfo);
                        $( "#dialog-plan-one" ).dialog( "open" );
                    });
                });


                /* Открытие окна с выбором подходящих программ */
                function change_program() {
                    //console.log(hexathis.attr("data-id-subject"));
                    //console.log("Stype - "+hexathis.attr("data-id-stype"))
                    $.get('{{ route("user-program-get-all") }}', {id_cikl: hexathis.attr("data-id-cikl"), id_stype: hexathis.attr("data-id-stype")} ,function(result){
                        result = JSON.parse(result);
                        $( "#add-program-to-plan .dialog-content" ).empty();
                        temp = '<table class="list"><thead><tr><td>Назва</td><td>Дія</td></tr></thead><tbody><form>';
                        for (key in result) {
                            temp += '<tr><td class="name-program">' + result[key].name_program +'</td><td><input id="radio'+key+'" type="radio" name="question1" value="'+result[key].id_program+'"';
                            if (parseInt(result[key].id_program) == parseInt(hexathis.parent().siblings(".name-program").attr("data-id-program"))){ temp +=" checked"; }
                            //console.log(result[key].id_program + ' - ' + hexathis.parent().siblings(".name-program").attr("data-id-program"));
                            temp += '></td></tr>';
                        }
                        temp += '</form></tbody></table>';
                        $( "#add-program-to-plan .dialog-content" ).append(temp);
                        $( "#add-program-to-plan" ).dialog( "open" );
                    });
                }


                /* Проверка плана на все правила и установление соответствующего статуса*/
                $(".perevirka").click(function(){
                    var data = new FormData();
                    data.append( 'id_plan', $( this ).attr( "data-id-plan" ) );
                    if ($( this ).attr( "data-no" )) {
                        data.append( 'no', $( this ).attr( "data-no" ) );
                    }
                    $.ajax ({
                        url: '{{ route("user-plans-perevirka") }}',
                        type: "POST",
                        dataType: 'json',
                        data: data,
                        processData: false,
                        contentType : false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (result) {
                            if (result.status) {
                                // $("#plan"+result.plan.id_plan+" .edit").remove();
                                // $("#plan"+result.plan.id_plan+" .delete").remove();
                                // $("#plan"+result.plan.id_plan+" .perevirka").remove();
                                $("#plan"+result.plan.id_plan+" .edit").hide();
                                $("#plan"+result.plan.id_plan+" .delete").hide();
                                $("#plan"+result.plan.id_plan+" .perevirka").hide();
                                $("#plan"+result.plan.id_plan).children(".name-status").text(result.plan.name_status);
                                $("#plan"+result.plan.id_plan).children(".name-status").attr("data-id-status",result.plan.id_status);
                                $().toastmessage('showToast', {
                                    text     : result.message,
                                    type     : 'success',
                                });
                            } else {
                                $().toastmessage('showToast', {
                                    text     : 'Перевірку не пройдено',
                                    type     : 'warning',
                                });
                            }
                            console.log(result);
                        },
                        error: function (result) {
                            $().toastmessage('showToast', {
                                text     : 'Error',
                                type     : 'error',
                            });
                        }
                    });
                });

                /* Модальное окна и прочие функции */
                $( function() {
                    // $( "#tabs" ).tabs();


                    /* Окно с табами */
                    $( "#dialog-plan-one" ).dialog({
                        resizable: false,
                        width: "80%",
                        modal: true,
                        autoOpen: false,
                        show: {
                            effect: "fade",
                            duration: 100
                        },
                        hide: {
                            effect: "fade",
                            duration: 100
                        },
                        buttons: [
                            {
                                text: "Зберегти",
                                class: "btn-ok",
                                click: function() {
                                    temp = 0;
                                    for (key in current_plan.content_plan) {
                                        current_plan.content_plan[key].program = $(".subject"+key).children(".name-program").attr("data-id-program");
                                        current_plan.content_plan[key].name_program = $(".subject"+key).children(".name-program").text();
                                        if ($(".subject"+key).children(".name-program").attr("data-id-program")!="0") {temp++;}
                                    }
                                    $( "#plan"+current_plan.id_plan ).children(".programs-count").text(temp); /* отображаем в таблице число программ */
                                    /* запись в базу */
                                        param = {id_plan: current_plan.id_plan,
                                                name_plan: current_plan.name_plan,
                                                content_plan: JSON.stringify(current_plan.content_plan).replace(/\'/g,"&#39;")};

                                        $.ajax({
                                            type: 'POST',
                                            url: '{{ route("user-plans-save") }}',
                                            dataType: 'json',
                                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                            data: (param),
                                            success: function (result) {
                                                $("#plan"+result.id_plan).children(".name-status").text(result.name_status);
                                                // $(".edit").click(function(){
                                                //     $(location).attr('href', '{{route("user-plans-edit")}}/'+result.id_plan);
                                                // });
                                                $("#plan"+result.id_plan+" .edit").show();
                                                $("#plan"+result.id_plan+" .delete").show();
                                                $("#plan"+result.id_plan+" .perevirka").show();


                                                $().toastmessage('showToast', {
                                                    text     : 'Сохранено',
                                                    type     : 'success',
                                                });
                                            },
                                            error: function (result) {
                                                $().toastmessage('showToast', {
                                                    text     : 'Error saved',
                                                    type     : 'error',
                                                });
                                            }
                                        });
                                    /*****************/

                                    $( this ).dialog( "close" );
                                }
                            },
                            {
                                text: "Скасувати",
                                class: "btn-cancel",
                                click: function() {
                                    $( this ).dialog( "close" );
                                }
                            }
                        ]
                    });


                    /* Окно со списком программ */
                    $( "#add-program-to-plan" ).dialog({
                        resizable: false,
                        width: "60%",
                        modal: true,
                        autoOpen: false,
                        show: {
                            effect: "fade",
                            duration: 100
                        },
                        hide: {
                            effect: "fade",
                            duration: 100
                        },
                        buttons: [
                            {
                                text: "Додати",
                                class: "btn-ok",
                                click: function() {
                                    hexathis.parent().siblings(".name-program").text($('input[name=question1]:checked').parent().siblings(".name-program").text());
                                    hexathis.parent().siblings(".name-program").attr("data-id-program", $('input[name=question1]:checked').val()) ;
                                    $( this ).dialog( "close" );
                                }
                            },
                            {
                                text: "Скасувати",
                                class: "btn-cancel",
                                click: function() {
                                    $( this ).dialog( "close" );
                                }
                            }
                        ]

                    });
                });





                {{-- Диаграммы --}}

                window.chartColors = {
                    @foreach ($chartgaluzes as $key=>$chartgaluz)
                    color{{$key}} : '{{$chartgaluz->color_galuz}}',
                    @endforeach
                };
                var color = Chart.helpers.color;

                var doughnutChartData = {
                datasets: [{
                    data: [1,1,1,1,1,1,1,1,1,1],
                    backgroundColor: [
                        window.chartColors.color0,
                        window.chartColors.color1,
                        window.chartColors.color2,
                        window.chartColors.color3,
                        window.chartColors.color4,
                        window.chartColors.color5,
                        window.chartColors.color6,
                        window.chartColors.color7,
                        window.chartColors.color8,
                        window.chartColors.color9
                    ],
                    label: 'Dataset 0'
                }],
                labels: [
                    @foreach ($chartgaluzes as $key=>$chartgaluz)
                        "{{$chartgaluz->name_galuz}}",
                    @endforeach
                    ]
                };

                var barChartData = {
                    datasets: [
                        @foreach ($chartgaluzes as $key=>$chartgaluz)
                        {
                            label: '{{$chartgaluz->name_galuz}}',
                            backgroundColor: color(window.chartColors.color{{$key}}).alpha(0.9).rgbString(),
                            borderColor: color(window.chartColors.color{{$key}}).rgbString(),
                            borderWidth: 1,
                            data: [ {{$key}} ]
                        },
                        @endforeach
                    ]

                };

                var ctx1 = document.getElementById("canvas-doughnut").getContext("2d");
                window.myBar1 = new Chart(ctx1, {
                    type: 'doughnut',
                    data: doughnutChartData,
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                            display: false,
                        },
                        title: {
                            display: true,
                            text: 'Відповідно до використаних КОРів (кількість)'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        }
                    }
                });


                var ctx2 = document.getElementById("canvas-bar").getContext("2d");
                window.myBar2 = new Chart(ctx2, {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                            display: false,
                        },
                        title: {
                            display: true,
                            text: 'Відповідно до існуючих КОРів (%)'
                        }
                    }
                });




                function chart_update(obj){
                    var kor_count = [];
                    var per_count = [];
                    @foreach ($chartgaluzes as $key=>$chartgaluz)
                        kor_count[{{$key}}] = 0;
                        per_count[{{$key}}] = 0;
                    @endforeach

                    jQuery.each(obj, function( index ){
                        $(".chartgaluz"+index).children('[data-count="standart"]').text(this.basic_kors_count);
                        $(".chartgaluz"+index).children('[data-count="program"]').text(this.program_kors_count);
                        $(".chartgaluz"+index).children('[data-count="intersection"]').text(this.standart_kors_count);
                        $(".chartgaluz"+index).children('[data-count="school"]').text(this.program_kors_count - this.standart_kors_count);
                        if (this.active){
                            kor_count[ index - 1 ]= this.standart_kors_count;
                            if (this.basic_kors_count>0) {
                                per_count[ index-1 ] = this.standart_kors_count / this.basic_kors_count * 100;
                            } else {
                                per_count[ index-1 ] = 0;
                            }
                        }
                    });

                    jQuery.each(barChartData.datasets, function( index ) {
                        this.data = [ per_count[ index ] ];
                    });

                    doughnutChartData.datasets[0].data = kor_count;
                    window.myBar1.update();
                    window.myBar2.update();
                }



            });

        </script>
    @endsection

@include('template-part.popup-plan')
