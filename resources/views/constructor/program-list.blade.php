@extends('common.layout-new')

    @section('content')

        <div class="content-box-title {{ $color or '' }}">{{ $title or 'Title' }}</div>
        <div class="content-box">

            @if( $type == 4 )
                <div class="top-button-line">
                    <img class="add-items" src="/assets/images/add.png" alt=""><span class="create-program">Створити програму</span>
                </div>
            @endif
            @if( $type == 3 )
                @if ( auth()->user()->id_school != 0 )
                <div class="top-button-line">
                    <img class="add-items" src="/assets/images/add.png" alt=""><span class="create-program">Створити програму</span>
                </div>
                @else
                    <p>Відсутня належність до навчальних закладів</p>
                @endif
            @endif

            <table class="list">
                <thead>
                    <tr>
                        <td class="sort">#</td>
                        <td class="sort">Назва</td>
                        <td class="sort">Тип</td>
                        <td class="sort">Цикл</td>
                        <td class="sort">Галузі</td>
                        <td class="sort">Створено</td>
                        <td class="sort">Змінено</td>
                        <td class="sort">Редагування</td>
                        <td class="sort">Годин</td>
                        <td class="sort">Створення</td>
                        <td class="sort">Користувачі</td>
                        <td class="sort">Статус</td>
                        <td class="action-buttons">Дії</td>
                    </tr>
                </thead>
                <tbody id="programs-list">
                    @foreach ($programs as $program)
                        <tr>
                            <td class="id_program">{{ $program->id_program }}</td>
                            <td>{{ $program->name_program }}</td>
                            <td>{{ $program->name_stype }}</td>
                            <td>{{ $program->name_cikl }}</td>
                            <td>
                                @foreach (json_decode($program->galuzes_program) as $galuz_program)
                                    <span class="galuz-current-program" data-id-galuz="{{ $galuz_program->id_galuz }}">{{ $galuz_program->name_galuz }}</span><br>
                                @endforeach
                            </td>
                            <td>{{ ukr_date(date("d F Y", strtotime($program->created_at))) }}</td>
                            <td>{{ ukr_date(date("d F Y", strtotime($program->updated_at))) }}</td>
                            <td>@if($program->blocked_at == NULL ) {{ 'Доступний' }} @else {{ 'Заблокований' }} @endif</td>
                            <!--td>{{ $program->id_plan }}</td-->
                            <td>{{ $program->hours_program }}</td>
                            <td>{{ $program->name_create }}</td>
                            <td class="users-current-program">
                                @foreach (json_decode($program->users_program) as $user_program)
                                    <span class="user-current-program" data-id-user="{{ $user_program->id_user }}">{{ $user_program->name_user }}</span><br>
                                @endforeach
                            </td>
                            <td class="text-center">{{ $program->name_status }}</td>
                            <td class="buttons-current-program text-center">
                                <span class="popup-program table-button" data-id-program="{{$program->id_program}}" data-toggle="tooltip" title="Переглянути"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                @if( $type == 2 )
                                    @auth
                                        <span class="copy-program table-button" data-id-program="{{$program->id_program}}" data-destination="user" data-toggle="tooltip" title="Скопіювати до особистих планів"><i class="fa fa-folder-open" aria-hidden="true"></i></span>
                                        @if(auth()->user()->id_school != 0)
                                            <span class="copy-program table-button" data-id-program="{{$program->id_program}}" data-destination="school" data-toggle="tooltip" title="Скопіювати до планів закладу освіти"><i class="fa fa-files-o" aria-hidden="true"></i></span>
                                        @endif
                                    @endauth
                                @elseif( $type == 3 )
                                    {{-- Кнопка редактирования --}}
                                    @if($program->edit)
                                        <span class="edit-program table-button" data-id="{{$program->id_program}}" data-edit="{{ route('user-program-edit', $program->id_program) }}" data-view="{{ route('user-program-view', $program->id_program) }}" data-toggle="tooltip" title="Редагувати"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                        <span class="delete-program table-button" data-id="{{$program->id_program}}" data-toggle="tooltip" title="Видалити"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <span class="copy-program table-button" data-id-program="{{$program->id_program}}" data-destination="user" data-toggle="tooltip" title="Скопіювати до особистих планів"><i class="fa fa-folder-open" aria-hidden="true"></i></span>
                                        {{-- Кнопка добавления пользователей --}}
                                        @can('director')
                                            <span class="edit-users table-button" data-toggle="tooltip" title="Змінити перелік користувачів"><i class="fa fa-users" aria-hidden="true"></i></span>
                                        @endcan
                                    @endif
                                @elseif( $type == 4 )
                                        <span class="edit-program table-button" data-id="{{$program->id_program}}" data-edit="{{ route('user-program-edit', $program->id_program) }}" data-view="{{ route('user-program-view', $program->id_program) }}" data-toggle="tooltip" title="Редагувати"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                        <span class="delete-program table-button" data-id="{{$program->id_program}}" data-toggle="tooltip" title="Видалити"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        @if(auth()->user()->id_school != 0)
                                            <span class="copy-program table-button" data-id-program="{{$program->id_program}}" data-destination="school" data-toggle="tooltip" title="Скопіювати до планів закладу освіти"><i class="fa fa-files-o" aria-hidden="true"></i></span>
                                        @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

    @endsection



@auth
    @section('dialogs')
        <div id="dialog-open" title="Редагування документу">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
            Документ позначено як той, що редагується. Повторне відкриття позбавить попереднього користувача прав на редагування. Ви, також, можете відкрити документ у режимі перегляду. Ви впевнені, що бажаєте почати редагування цього документу?</p>
        </div>

        <div id="dialog-create-program" title="Створення програми">
            <div class="dialog-content" style="padding:15px;">
            <div class="select-zors">
                <span>Цикл</span>
                <select name="id_cikl" id="id_cikl">
                    @foreach ($cikls as $cikl)
                        <option value="{{ $cikl->id_cikl }}">{{ $cikl->name_cikl }}</option>
                    @endforeach
                </select>
                <span>Тип</span>
                <select name="id_stype" id="id_stype">
                    @foreach ($stypes as $stype)
                        <option value="{{ $stype->id_stype }}">{{ $stype->name_stype }}</option>
                    @endforeach
                </select>
            </div>
                <table class="galuzes">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Назва галузі</td>
                            <td>Дія</td>
                        </tr>
                    </thead>
                    <tbody class="list-of-galuzes">
                        @foreach ($galuzes as $galuz)
                            <tr>
                                <td>{{ $galuz->id_galuz }}</td>
                                <td>{{ $galuz->name_galuz }}</td>
                                <td><input class="galuz-new-program" type="checkbox" data-id-galuz="{{$galuz->id_galuz}}" data-name-galuz="{{$galuz->name_galuz}}"></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>


        @can('director')
            <div id="dialog-add-users" title="Перелік користувачів, які мають можливість редагування" data-id-program="0">
                <div class="dialog-content" style="padding:15px;">
                    <table class="program-users">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>ПІБ</td>
                                <td>Дозвіл</td>
                            </tr>
                        </thead>
                        <tbody class="program-users-body">
                            @foreach ($schoolusers as $schooluser)
                                <tr>
                                    <td>{{ $schooluser->id_user }}</td>
                                    <td>{{ $schooluser->second_name . ' ' . $schooluser->name . ' ' . $schooluser->patronymic }}</td>
                                    <td><input id="user{{ $schooluser->id_user }}" class="list-users" data-id-user="{{ $schooluser->id_user }} " type="checkbox"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endcan

    @endsection
@endauth


    @section('scripts')
        <script>

        jQuery(document).ready(function(){

            @auth
            var users = {}, hexathis, galuzes = {};

            @foreach ($schoolusers as $schooluser)
                var user = {};
                user.id_user = {{$schooluser->id_user}};
                user.name_user = "{{ $schooluser->second_name . ' ' . $schooluser->name . ' ' . $schooluser->patronymic }}";
                users[{{$schooluser->id_user}}] = user;
            @endforeach



            {{-- Копирование типичной программы в программы школы --}}
            $( ".copy-program" ).click(function(){
                var data = new FormData();
                data.append( 'id_program', $( this ).attr( "data-id-program" ) );
                data.append( 'destination', $( this ).attr( "data-destination" ) );
                $.ajax ({
                    url: '{{ route("user-programs-new-from-typical") }}',
                    type: "POST",
                    dataType: 'json',
                    data: data,
                    processData: false,
                    contentType : false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (result) {
                        $().toastmessage('showToast', {
                            text     : 'Створено',
                            type     : 'success',
                        });
                    },
                    error: function (result) {
                        $().toastmessage('showToast', {
                            text     : 'Error',
                            type     : 'error',
                        });
                    }
                });
            });



            /* Кнопка редактирования документа */
            $('.edit-program').click(function(){
                route_edit = $(this).attr('data-edit');
                route_view = $(this).attr('data-view');

                $.get('{{ route('user-program-is-open') }}', {id_program: $(this).attr('data-id')} ,function(result){
                    result = JSON.parse(result);
                        if (result.status==1){
                            $(location).attr('href',route_edit);
                        }
                        if (result.status==2){
                            $( "#dialog-open" ).dialog( "open" );
                        }
                });
            });

            /* Модальное окно открытия документа */
            $( "#dialog-open" ).dialog({
                resizable: false,
                height: "auto",
                width: 500,
                modal: true,
                autoOpen: false,
                buttons: [
                    {
                        text: "Редагувати",
                        class: "btn-notice",
                        click: function() {
                            $(location).attr('href',route_edit);
                        }
                    },
                    {
                        text: "Переглянути",
                        class: "btn-ok",
                        click: function() {
                            $(location).attr('href',route_view);
                        }
                    },
                    {
                        text: "Скасувати",
                        class: "btn-cancel",
                        click: function() {
                            $( this ).dialog( "close" );
                            $().toastmessage('showToast', {
                                text     : 'Редагування документу скасовано',
                                type     : 'warning',
                            });
                        }
                    }
                ],
            });


            /* Кнопка редактирования списка пользователей */
            $('.edit-users').click(function(){
                $( "#dialog-add-users" ).attr( "data-id-program", parseInt($(this).parent().siblings(".id_program").text()));
                hexathis = $(this);
                $(".program-users-body").empty();
                for (key in users) {
                    users[key].check_user = 0;
                    $(".program-users-body").append("<tr><td class='id_user'>"+users[key].id_user+"</td><td class='name_user'>"+users[key].name_user+"</td><td><input id='user"+users[key].id_user+"' class='list-users' type='checkbox'></td></tr>");
                }
                $( this ).parent().siblings( ".users-current-program" ).children( ".user-current-program" ).each(function( index ) {
                    $( "#user" + $( this ).attr( "data-id-user" ) ).attr('checked', 'checked');
                });
                $( "#dialog-add-users" ).dialog( "open" );
            });



            /* Модальное окно списка пользователей */
            $( "#dialog-add-users" ).dialog({
                resizable: false,
                height: "auto",
                width: 500,
                modal: true,
                autoOpen: false,
                        buttons: [
                            {
                                text: "Змінити",
                                class: "btn-ok",
                                click: function() {
                                    temp = "";
                                    temp_users = {};
                                    $(".program-users-body .list-users").each(function( index ) {
                                        if ($(this).prop("checked")){
                                            temp += "<span class='user-current-program' data-id-user='" + $(this).parent().siblings(".id_user").text() + "'>"+$(this).parent().siblings(".name_user").text()+"</span><br>";
                                            var temp_user = {};
                                            temp_user['id_user'] = $(this).parent().siblings(".id_user").text();
                                            temp_user['name_user'] = $(this).parent().siblings(".name_user").text();
                                            temp_users[ index ] = temp_user;
                                        }                            
                                    });
                                    var data = new FormData();
                                    data.append( 'id_program', $( "#dialog-add-users" ).attr( "data-id-program" ) );
                                    data.append( 'users_program',  JSON.stringify(temp_users).replace(/\'/g,"&#39;") );
                                    console.log(hexathis.parent().siblings("id_program").text());
                                    $.ajax ({
                                        url: '{{ route("user-program-save-users") }}',
                                        type: "POST",
                                        dataType: 'json',
                                        data: data,
                                        processData: false,
                                        contentType : false,
                                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                        success: function (result) {
                                            hexathis.parent().siblings(".users-current-program").html( temp );
                                            $().toastmessage('showToast', {
                                                text     : 'Змінено',
                                                type     : 'success',
                                            });
                                        },
                                        error: function (result) {
                                            $().toastmessage('showToast', {
                                                text     : 'Error',
                                                type     : 'error',
                                            });
                                        }
                                    });
                                    $( this ).dialog( "close" );
                                }
                            },
                            {
                                text: "Скасувати",
                                class: "btn-cancel",
                                click: function() {
                                    $( this ).dialog( "close" );
                                    $().toastmessage('showToast', {
                                        text     : 'Скасовано',
                                        type     : 'warning',
                                    });
                                }
                            }
                        ]

            });



            /* удаление программы */
            $('.delete-program').click(function(){
                        var data = new FormData();
                        data.append( 'id_program', $(this).attr("data-id") );
                        hexathis = $(this);

                        $.ajax ({
                            url: '{{ route("user-program-delete") }}',
                            type: "POST",
                            dataType: 'json',
                            data: data,
                            processData: false,
                            contentType : false,
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            success: function (result) {
                                hexathis.parent().parent().remove();
                                $().toastmessage('showToast', {
                                    text     : 'Видалено',
                                    type     : 'success',
                                });
                            },
                            error: function (result) {
                                $().toastmessage('showToast', {
                                    text     : 'Error',
                                    type     : 'error',
                                });
                            }
                        });
            });



            /* Нажатие кнопки создания программы */
            $('.create-program').click(function(){
                $( "#dialog-create-program" ).dialog( "open" );
            });

            /* Модальное окно создания программы */
            $( "#dialog-create-program" ).dialog({
                resizable: false,
                height: "auto",
                width: 500,
                modal: true,
                autoOpen: false,

                        buttons: [
                            {
                                text: "Створити",
                                class: "btn-ok",
                                click: function() {
                                    var galuzes = {};
                                    $( ".galuz-new-program" ).each(function( item ) {
                                        if ($(this).prop("checked")){
                                            var galuz = {};
                                            galuz.id_galuz = $(this).attr("data-id-galuz");
                                            galuz.name_galuz = $(this).attr("data-name-galuz");
                                            galuzes[item] = galuz;
                                        }                            
                                    });

                                    var data = new FormData();
                                    data.append( 'id_cikl', $( "#id_cikl" ).val() );
                                    data.append( 'id_stype', $( "#id_stype" ).val() );
                                    data.append( 'id_ptype', {{ $type }} );
                                    data.append( 'galuzes_program', JSON.stringify(galuzes).replace(/\'/g,"&#39;"));
                                    hexathis = $(this);

                                    $.ajax ({
                                        url: '{{ route("user-program-create-new") }}',
                                        type: "POST",
                                        dataType: 'json',
                                        data: data,
                                        processData: false,
                                        contentType : false,
                                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                        success: function (result) {
                                            console.log(result);
                                            if (result.res){
                                                $().toastmessage('showToast', {
                                                    text     : 'Створено',
                                                    type     : 'success',
                                                });
                                                /* добавляем программы у список */
                                                temp = "<tr><td class='id_program'>"+result.id_program+"</td><td>"+result.name_program+"</td><td>"+result.name_stype+"</td><td>"+result.name_cikl+"</td><td>";

                                                for (var key in galuzes) {
                                                    temp += '<span class="galuz-current-program" data-id-galuz="'+galuzes[key].id_galuz+'">'+galuzes[key].name_galuz+'</span><br>';
                                                }

                                                temp += "</td><td>"+result.date1+"</td><td>"+result.date1+"</td><td>Доступний</td><td>0</td><td>"+result.name_create+"</td><td class='users-current-program'><span class='user-current-program' data-id-user='{{auth()->user()->id}}'>{{auth()->user()->second_name.' '.auth()->user()->name.' '.auth()->user()->patronymic}}</span></td><td>"+result.name_status+"</td><td class='text-center'>";

                                                temp += '<span class="popup-program table-button" data-id-program="'+result.id_program+'" data-toggle="tooltip" title="Переглянути"><i class="fa fa-eye" aria-hidden="true"></i></span>';


                                                temp += '<span class="edit-program table-button" data-id="'+result.id_program+'" data-edit="{{ route("user-program-edit") }}/'+result.id_program+'" data-view="{{ route("user-program-view") }}/'+result.id_program+'" data-toggle="tooltip" title="Редагувати"><i class="fa fa-pencil" aria-hidden="true"></i></span>';

                                                //temp += '<span class="edit-program action-button table-button edit-button" data-id="'+result.id_program+'" data-edit="{{ route("user-program-edit") }}/'+result.id_program+'" data-view="{{ route("user-program-view") }}/'+result.id_program+'"></span>';

                                                temp += '</td></tr>';

                                                $("#programs-list").append(temp);

                                                /* перепределяем функцию редактирования */
                                                $('.edit-program').click(function(){
                                                    route_edit = $(this).attr('data-edit');
                                                    route_view = $(this).attr('data-view');

                                                    $.get('{{ route('user-program-is-open') }}', {id_program: $(this).attr('data-id')} ,function(result){
                                                        result = JSON.parse(result);
                                                            if (result.status==1){
                                                                $(location).attr('href',route_edit);
                                                            }
                                                            if (result.status==2){
                                                                $( "#dialog-open" ).dialog( "open" );
                                                            }
                                                    });
                                                });
                                                hexathis.dialog( "close" );
                                            } else {
                                                $().toastmessage('showToast', {
                                                    text     : result.message,
                                                    type     : 'error',
                                                });
                                            }
                                        },
                                        error: function (result) {
                                            $().toastmessage('showToast', {
                                                text     : 'Error',
                                                type     : 'error',
                                            });
                                        }
                                    });
                                    // $( this ).dialog( "close" );
                                }
                            },
                            {
                                text: "Скасувати",
                                class: "btn-cancel",
                                click: function() {
                                    $( this ).dialog( "close" );
                                    $().toastmessage('showToast', {
                                        text     : 'Скасовано',
                                        type     : 'warning',
                                    });
                                }
                            }
                        ]

            });

            @endauth

        });
        </script>
    @endsection

@include('template-part.popup-plan')
