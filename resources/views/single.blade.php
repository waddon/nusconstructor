@extends('common.layout')

    @section('content')
        <h1>{{ $title }}</h1>
        {!! $content !!}
    @endsection

