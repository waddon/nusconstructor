@extends('common.layout-new')

    @section('content')
        {!! $page->content_page or '' !!}

        @if ( isset($news) )
            <div class=" content-box-title white">{{ $news_title or '' }}</div>
            <div class="content-box">
                <table class="news">
                    <thead id="header-news">
                        <tr>
                            <td>#</td>
                            <td>Дата</td>
                            <td>Новина</td>
                        </tr>
                    </thead>
                    <tbody id="content-news">
                    @foreach ($news as $new)
                        <tr>
                            <td>{{ $new->id_post }}</td>
                            <td>{{ date("d.m.Y", strtotime($new->created_at)) }}</td>
                            <td class="news-item" data-date="{{ ukr_date(date("l d F Y", strtotime($new->created_at))) }}" data-color="1" data-id-post="{{$new->id_post}}">{{ $new->name_post }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    @endsection


@if ( isset($news) )
    @section('dialogs')
        <style>
            .ui-dialog-titlebar:before{
                content: "\f086";
                font-family: FontAwesome;
                position: absolute;
                color: #fff;
                border-radius: 50%;
                display: inline-block;
                height: 36px;
                width: 36px;
                line-height: 36px;
                text-align: center;
            }
        </style>

        <div id="dialog-news-one" title="Title">
            <div class="dialog-content"></div>
        </div>
    @endsection
@endif

@section('scripts')
<script>
jQuery(document).ready(function(){
    @if ( isset($news) )
        /* нажатие на новость */
        $( ".news-item" ).click(function(){
            $(".ui-dialog-title").html( $(this).attr("data-date") );
            var color = "color" + $(this).attr("data-color");
            $(".ui-dialog-titlebar").removeClass( "color1" ).removeClass( "color2" ).removeClass( "color3" );
            $("#dialog-news-one").removeClass( "color1" ).removeClass( "color2" ).removeClass( "color3" );
            $(".ui-dialog-titlebar").addClass( color );                
            $("#dialog-news-one").addClass( color );                
            /* Получаем данные поста */
            $.get('{{ route("get_post") }}', {id_post: $(this).attr("data-id-post")} ,function(result){
                result = JSON.parse(result);
                $(".dialog-content").empty();
                $(".dialog-content").append('<h2>'+result.name_post+'</h2>');
                $(".dialog-content").append(result.content_post);
                /* Открываем диалоговое окно*/
                $( "#dialog-news-one" ).dialog( "open" );
            });
        });
        /* Модальное окно с новостью */
        $( function() {
            $( "#dialog-news-one" ).dialog({
                resizable: false,
                width: "80%",
                modal: true,
                autoOpen: false,
                show: {
                    effect: "fade",
                    duration: 100
                },
                hide: {
                    effect: "fade",
                    duration: 100
                },
            });
        });
    @endif


});
</script>
@endsection
