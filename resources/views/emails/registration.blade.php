<h3 style="margin:1em 0;color:#494949;font-family:Helvetica, Arial;font-size:18px;line-height:150%;text-align:left;font-weight:normal">Ви успішно зареєстровані</h3>
<p style="margin:1em 0;color:#494949;font-family:Helvetica, Arial;font-size:14px;line-height:150%;text-align:left;font-weight:normal">Після підтвердження вашого облікового запису модератором, ви отримаєте повідомленняю. Ваші дані для авторизації на <a href="{{ route('login') }}">платформі</a>:</p>

<p style="margin:1em 0;color:#494949;font-family:Helvetica, Arial;font-size:14px;line-height:150%;text-align:left;font-weight:normal">
    <b>Ваш логін: </b> {{ $user->email }}<br>
    <b>Ваш пароль: </b> {{ $password }}
</p>