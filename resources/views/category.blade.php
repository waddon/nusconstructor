@extends('common.layout')

    @section('content')
        <h1>{{ $title }}</h1>

            <section style="margin-bottom:30px;">
                <div class="row"></div>

                <a href="#" class="green-btn" id='load-more' data-cat='{{$cat->id_cat}}' data-pager='0'>{{ trans('search.button_load_more') }}</a>
            </section>

            <script>
                $(document).ready(function(){

                    /* Первоначальный запуск */
                    get_posts('#load-more');

                    /* Нажатие кнопки "Загрузить далее" */
                    $('#load-more').click(function(e){
                        e.preventDefault();
                        $(this).text('{{ trans('search.button_loading') }}');
                        get_posts('#'+$(this).attr('id'));
                    });

                    /**/
                    function get_posts(sender=''){
                        $.get('ajaxroute/posts', {cat: $(sender).attr('data-cat'), pager: $(sender).attr('data-pager')}, function(result){
                            result = JSON.parse(result);
                            console.log(result);
                            $(sender).siblings().append(result.data);
                            $(sender).attr('data-pager', result.pager);
                            if (result.display){
                                $(sender).text('{{ trans('search.button_load_more') }}');
                            } else {
                                $(sender).remove();
                            }

                        });
                    }

                });
                
            </script>

    @endsection

