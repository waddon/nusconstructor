@extends('layouts.app-new')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Реєстрація</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        <div class="col-md-6">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Поштова скринька*</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Iм'я*</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('second_name') ? ' has-error' : '' }}">
                                <label for="second_name" class="col-md-4 control-label">Призвiще*</label>
                                <div class="col-md-6">
                                    <input id="second_name" type="text" class="form-control" name="second_name" value="{{ old('second_name') }}" required>

                                    @if ($errors->has('second_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('second_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('patronymic') ? ' has-error' : '' }}">
                                <label for="patronymic" class="col-md-4 control-label">По-батьковi</label>
                                <div class="col-md-6">
                                    <input id="patronymic" type="text" class="form-control" name="patronymic" value="{{ old('patronymic') }}">

                                    @if ($errors->has('patronymic'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('patronymic') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            {{-- <div class="form-group{{ $errors->has('id_role') ? ' has-error' : '' }}">
                                <label for="role" class="col-md-4 control-label">Роль</label>
                                <div class="col-md-6">
                                    <input id="role" type="text" class="form-control" name="id_role" value="{{ old('id_role') }}" required>

                                    @if ($errors->has('id_position'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('id_position') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div> --}}

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="position" class="col-md-4 control-label">Телефон</label>
                                <div class="col-md-6">
                                    <input id="position" type="text" class="form-control" name="phone" value="{{ old('phone') }}">

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Пароль*</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Пiдтвердiть пароль*</label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Зареєструватися
                                    </button>
                                </div>
                            </div>
                            Поля позначені * обов'язкові для заповнення
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="schooltype" class="col-md-4 control-label">Тип школи</label>
                                <div class="col-md-6">
                                    <select name="school_type" id="schooltype" class="form-control">
                                        @foreach($schooltypes as $id_scholtype => $name_schooltype)
                                            <option value="{{ $id_scholtype }}">{{ $name_schooltype }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="region" class="col-md-4 control-label">Регион школи</label>
                                <div class="col-md-6">
                                    <select name="region_school" id="region" class="form-control">
                                        @foreach($region_schools as $region_school)
                                            <option value="{{ $region_school }}">{{ $region_school }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" id="search" class="form-control" placeholder="Пошук">
                            </div>
                            <table class="table">
                                <thead>
                                    <td>Школа</td>
                                    <td>Адреса</td>
                                    <td></td>
                                </thead>
                                <tbody class="body-list-school"></tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    var $rows;      
    $('#search').keyup(function() {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
        
        $rows.show().filter(function() {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });

    $(document).ready(function(){
        $("#region, #schooltype").change(function(){
            /* Очищаем содержимое места отображени школ */
            $(".body-list-school").empty();
            /* Формируем передаваемые параметры */
            var data = new FormData();
            data.append( 'schooltype', $("#schooltype").val() );
            data.append( 'region', $("#region").val() );
            /* Отправляем аякс запрос */
            // console.log($("#schooltype").val());
            // console.log($("#region").val());
            $.ajax ({
                url: '{{ route("user-profile-get-schools") }}',
                type: "GET",
                dataType: 'json',
                data: {region_school: $("#region").val(), schooltype: $("#schooltype").val()},
                // processData: false,
                // contentType : false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (result) {
                    /* действия при успешном завершении */
                    /* отрисовываем полученный результат */
                    $.each(result.list_schools, function(index, value) {
                        $(".body-list-school").append('<tr><td>'+value.name_school+'</td><td>'+value.adress_school+'</td><td><input type="radio" name="id_school" value="'+value.id_school+'"></td></tr>');
                    });
                    $rows = $('table tbody tr');
                },
                error: function () {
                    /* действия при аварийном завершении */
                    alert('Error');
                }
            });
        });

        var $rows = $('table tr');
        console.log($rows);
        $('#search').keyup(function() {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        });

    });
</script>

@endsection