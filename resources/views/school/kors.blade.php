@extends('common.layout-new')

    @section('content')

        <div class="content-box-title">{{ $title or '' }}</div>
        <div class="content-box">

            @if (auth()->user()->id_school != 0)

                @can('director')
                    <div class="top-button-line">
                        <img class="add-items" src="/assets/images/add.png" alt=""><span class="add-schoolkor">Додати новий очікуваний результат освіти</span>
                    </div>
                @endcan

                <table class="school-kors">
                    <thead>
                        <td>Код</td>
                        <td>Назва</td>
                        <td>Галузь</td>
                        <td>Дії</td>
                    </thead>
                    <tbody class="school-kors-body">
                        @foreach ($schoolkors as $schoolkor)
                            <tr data-id-schoolkor="{{ $schoolkor->id_schoolkor }}" data-id-galuz="{{ $schoolkor->id_galuz }}">
                                <td class="key_schoolkor">{{ $schoolkor->key_schoolkor }}</td>
                                <td class="name_schoolkor">{{ $schoolkor->name_schoolkor }}</td>
                                <td class="name_galuz color-td color{{ $schoolkor->id_galuz or '' }}">{{ $schoolkor->name_galuz }}</td>
                                <td>
                                    @can('director')
                                        <span class="table-button edit-button" data-toggle="tooltip" title="Редагувати"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                        <span class="table-button delete-button" data-toggle="tooltip" title="Видалити"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach                    
                    </tbody>
                </table>
            @else
                <p>Належніть до навчального закладу відсутня</p>
            @endif
        </div>

    @endsection


{{-- Далее блоки, отображаемые только директору школы --}}

@can('director')

    @section('dialogs')
        <div id="dialog-add-schoolkor" title="Додання очікуваного результату навчання">
            <div class="dialog-content" style="padding:15px;">                
                <label>Код</label>
                <input type="hidden" name="id_schoolkor" id="id_schoolkor">
                <input type="text" name="key_schoolkor" id="key_schoolkor">
                <label>Галузь</label>
                <select name="id_galuz" id="id_galuz">
                    @foreach ($galuzes as $galuz)
                        <option value="{{ $galuz->id_galuz }}">{{ $galuz->name_galuz }}</option>
                    @endforeach
                </select>
                <label for="">Назва</label>
                <textarea name="name_schoolkor" id="name_schoolkor" cols="30" rows="5"></textarea>
            </div>
        </div>
    @endsection




    @section('scripts')
        <script>
            jQuery(document).ready(function(){

                var hexathis;

                /* нажатие на кнопку добавления кора школы */
                $( ".add-schoolkor" ).click(function(){
                    hexathis = '';
                    $( "#id_schoolkor" ).val( "0" );
                    $( "#key_schoolkor" ).val( null );
                    $( "#id_galuz" ).val( 1 );
                    $( "#name_schoolkor" ).val( null );
                    $( "#dialog-add-schoolkor" ).dialog( "open" );
                });


                /* нажатие на кнопку редактирования кора школы */
                $( ".edit-button" ).click(function(){
                    hexathis = $( this );
                    $( "#id_schoolkor" ).val( $( this ).parent().parent().attr("data-id-schoolkor") );
                    $( "#key_schoolkor" ).val( $( this ).parent().siblings(".key_schoolkor").text() );
                    $( "#id_galuz" ).val( $( this ).parent().parent().attr("data-id-galuz") );
                    $( "#name_schoolkor" ).val( $( this ).parent().siblings(".name_schoolkor").text() );
                    $( "#dialog-add-schoolkor" ).dialog( "open" );
                });


                /* нажатие на кнопку удаления кора школы */
                $( ".delete-button" ).click(function(){
                    schoolkor_delete( $( this ) );
                });

                /* удаление кора */
                function schoolkor_delete(hexathis) {
                    var data = new FormData();
                    data.append( 'id_schoolkor', hexathis.parent().parent().attr("data-id-schoolkor") );
                    //hexathis = $( this );

                        $.ajax ({
                            url: '{{ route("school-kors-delete") }}',
                            type: "POST",
                            dataType: 'json',
                            data: data,
                            processData: false,
                            contentType : false,
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            success: function (result) {
                                hexathis.parent().parent().remove();
                                $().toastmessage('showToast', {
                                    text     : 'Видалено',
                                    type     : 'warning',
                                });
                            },
                            error: function (result) {
                                $().toastmessage('showToast', {
                                    text     : 'Error',
                                    type     : 'error',
                                });
                            }
                        });
                }



                /* модальное окно редактирования кора школы */
                $( function() {
                    $( "#dialog-add-schoolkor" ).dialog({
                        resizable: false,
                        width: "60%",
                        modal: true,
                        autoOpen: false,
                        show: {
                            effect: "fade",
                            duration: 100
                        },
                        hide: {
                            effect: "fade",
                            duration: 100
                        },
                        buttons: {
                            "Зберегти": function() {
                                var data = new FormData();
                                data.append( 'id_schoolkor', $( "#id_schoolkor" ).val() );
                                data.append( 'key_schoolkor', $( "#key_schoolkor" ).val().replace(/\'/g,"&#39;") );
                                data.append( 'id_galuz', $( "#id_galuz" ).val() );
                                data.append( 'name_schoolkor', $( "#name_schoolkor" ).val().replace(/\'/g,"&#39;") );

                                $.ajax ({
                                    url: '{{ route("school-kors-save") }}',
                                    type: "POST",
                                    dataType: 'json',
                                    data: data,
                                    processData: false,
                                    contentType : false,
                                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                    success: function (result) {
                                        if (hexathis != ''){
                                            hexathis.parent().siblings('.key_schoolkor').text( $( "#key_schoolkor" ).val() );
                                            hexathis.parent().siblings('.name_schoolkor').text( $( "#name_schoolkor" ).val() );
                                            hexathis.parent().siblings('.name_galuz').text( $( "#id_galuz option:selected" ).text() );
                                            hexathis.parent().siblings('.name_galuz').removeClass('color'+hexathis.parent().parent().attr("data-id-galuz")).addClass('color'+$('#id_galuz').val());
                                            hexathis.parent().parent().attr("data-id-galuz", $('#id_galuz').val());
                                        } else {
                                            $('.school-kors-body').append('<tr data-id-schoolkor="' + result + '" data-id-galuz="' + $('#id_galuz').val() + '"><td class="key_schoolkor">' + $( "#key_schoolkor" ).val() + '</td><td class="name_schoolkor">' + $( "#name_schoolkor" ).val() + '</td><td class="name_galuz color-td color' + $('#id_galuz').val() + '">' + $( "#id_galuz option:selected" ).text() + '</td><td><span class="table-button edit-button"></span> <span class="table-button delete-button"></span></td></tr>');
                                                $( ".edit-button" ).unbind( 'click' );
                                                $( ".edit-button" ).click(function(){
                                                    hexathis = $( this );
                                                    $( "#id_schoolkor" ).val( $( this ).parent().parent().attr("data-id-schoolkor") );
                                                    $( "#key_schoolkor" ).val( $( this ).parent().siblings(".key_schoolkor").text() );
                                                    $( "#id_galuz" ).val( $( this ).parent().parent().attr("data-id-galuz") );
                                                    $( "#name_schoolkor" ).val( $( this ).parent().siblings(".name_schoolkor").text() );
                                                    $( "#dialog-add-schoolkor" ).dialog( "open" );
                                                });
                                                $( ".delete-button" ).unbind( 'click' );
                                                $( ".delete-button" ).click(function(){schoolkor_delete( $( this ) );});

                                        }
                                        $().toastmessage('showToast', {
                                            text     : 'Збережено',
                                            type     : 'success',
                                        });
                                    },
                                    error: function (result) {
                                        $().toastmessage('showToast', {
                                            text     : 'Error',
                                            type     : 'error',
                                        });
                                    }
                                });

                                $( this ).dialog( "close" );
                            },
                            "Скасувати": function() {
                                $( this ).dialog( "close" );
                                $().toastmessage('showToast', {
                                    text     : 'Скасовано',
                                    type     : 'warning',
                                });
                            }
                        }            
                    });
                });

            });
        </script>
    @endsection

@endcan
