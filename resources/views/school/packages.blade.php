@extends('common.layout-new')

    @section('content')

        <div class="content-box-title">{{ $title or '' }}</div>
        <div class="content-box">

            @if (Auth::user()->hasAnyPermission('director', 'edit_content') && auth()->user()->id_school != 0)
                <div class="top-button-line">
                    <img class="add-items" src="/assets/images/add.png" alt=""><span class="add-package">Створити новий пакет документів</span>
                </div>

                <table class="school-package list">
                    <thead>
                        <tr>
                            <td>Код</td>
                            <td>Назва</td>
                            <td>Планів</td>
                            <td>Створено</td>
                            <td>Змінено</td>
                            <td>Статус</td>
                            <td class="action-buttons">Дії</td>
                        </tr>
                    </thead>
                    <tbody class="school-package-body">
                        @foreach ($packages as $package)
                            <tr data-id-package="{{$package->id_package}}">
                                <td>{{$package->id_package}}</td>
                                <td class="name-package">{{$package->name_package}}</td>
                                <td class="plancount-package">{{$package->plancount_package}}</td>
                                <td>{{ukr_date(date("d F Y", strtotime($package->created_at)))}}</td>
                                <td>{{ukr_date(date("d F Y", strtotime($package->updated_at)))}}</td>
                                <td class="name-status">{{$package->name_status}}</td>
                                <td class="text-center">
                                    <span class="edit-package table-button" data-id-package="{{$package->id_package}}" data-toggle="tooltip" title="Переглянути"><i class="fa fa-search" aria-hidden="true"></i></span>
                                    @if($package->id_status == 1 || $package->id_status == 8)
                                    <span class="delete-package table-button" data-id-package="{{$package->id_package}}" data-toggle="tooltip" title="Видалити"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    <span class="approve-package table-button" data-id-package="{{$package->id_package}}" data-toggle="tooltip" title="Узгодити"><i class="fa fa-certificate" aria-hidden="true"></i></span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            @else
                <p>Належніть до навчального закладу відсутня або не є директором</p>
            @endif

        </div>

    @endsection


@if (Auth::user()->hasAnyPermission('director', 'edit_content'))
@section('dialogs')
    <div id="dialog-package" title="Пакет документів">
        <div class="dialog-content" style="margin-bottom: 5px;">
            <label for="">Назва</label>
            <input type="text" id="name_package" style="width: 100%; margin-bottom: 10px;">
        </div>

            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Візія і міссія</a></li>
                    <li><a href="#tabs-2">Прикріплені плани</a></li>
                    <li><a href="#tabs-3">Зауваження</a></li>
                </ul>
                <div id="tabs-1">
                    <textarea class="tinymce" name="" id="vam" rows="30"></textarea>
                </div>
                <div id="tabs-2">
                </div>
                <div id="tabs-3">
                    <div class="border-box" id="critical"></div>
                </div>
            </div>
        <!--/div-->
    </div>
    <div id="add-plan-to-package" title="Прикріпити план">
        <div class="dialog-content">
        </div>
    </div>        
@endsection


@section('scripts')
<script>
    jQuery(document).ready(function(){

        var hexathis, current_package = {};

                /* Новый пакет */
                $( ".add-package" ).click(function(){
                    $( "#name_package" ).val("");
                    tinyMCE.activeEditor.setContent("");
                    var temp = '<table class="check"><thead><tr><td>Навчальний цикл</td><td>Назва плану</td><td>Статус плану</td><td class="action-buttons">Дії</td></tr></thead><tbody>';
                    @foreach ($cikls as $cikl)
                        temp += '<tr class="list-of-plans"><td class="name-cikl" data-id-cikl="{{$cikl->id_cikl}}">{{$cikl->name_cikl}}</td><td class="name-plan" data-id-plan="0"></td><td class="name-status" data-id-status="0"></td><td><span class="add-plan-to-package table-button" data-id-cikl="{{$cikl->id_cikl}}" data-toggle="tooltip" title="Додати план"><i class="fa fa-plus" aria-hidden="true"></i></span> <span class="popup-plan table-button" data-id-plan="0" data-toggle="tooltip" title="Переглянути план"><i class="fa fa-eye" aria-hidden="true"></i></span> <span class="remove-plan-from-package table-button" data-toggle="tooltip" title="Видалити план"><i class="fa fa-minus" aria-hidden="true"></i></span></td></tr>';
                    @endforeach
                    temp += '</tbody></table>';
                    $( "#tabs-2" ).html( temp );
                    //$('[data-toggle="tooltip"]').tooltip();
                    /* Переопределяем функцию нажатия на кнопки */
                    $(".add-plan-to-package").click(function(){ hexathis = $(this); change_plan(); });
                    $(".popup-plan").click(function(){
                        get_plan($(this).attr("data-id-plan"));
                    });
                    $(".remove-plan-from-package").click(function(){ $(this).parent().siblings(".name-plan").attr("data-id-plan", "0"); $(this).siblings(".popup-plan").attr("data-id-plan", "0"); $(this).parent().siblings(".name-plan").empty();});
                    /* Открываем диалоговое окно*/
                    current_package.id_package = 0;
                    $( "#dialog-package" ).dialog( "open" );
                });


                /* Редактируем пакет */
                $( ".edit-package" ).click(function(){edit_package($(this));});
                function edit_package(sender){
                    temp_int = parseInt(sender.attr("data-id-package"));
                    var data = new FormData();
                    data.append( 'id_package', sender.attr("data-id-package") );
                    $.ajax ({
                        url: '{{ route("school-package-get") }}',
                        type: "POST",
                        dataType: 'json',
                        data: data,
                        processData: false,
                        contentType : false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (result) {
                            console.log(result);
                            tinyMCE.activeEditor.setContent("");
                            tinyMCE.execCommand('mceInsertContent', false, result.vam_package);
                            $( "#name_package" ).val( result.name_package );
                            $( "#critical" ).append( result.critical_package );
                            plans_package = JSON.parse(result.plans_package);
                            console.log(plans_package);
                            var temp = '<table class="check"><thead><tr><td>Навчальний цикл</td><td>Назва плану</td><td>Статус плану</td><td class="action-buttons">Дії</td></tr></thead><tbody>';
                            for (key in plans_package) {
                                temp += '<tr class="list-of-plans"><td class="name-cikl" data-id-cikl="'+plans_package[key].id_cikl+'">'+plans_package[key].name_cikl+'</td><td class="name-plan" data-id-plan="'+plans_package[key].id_plan+'">'+plans_package[key].name_plan+'</td><td class="name-status" data-id-status="'+plans_package[key].id_status+'">'+plans_package[key].name_status+'</td><td><span class="add-plan-to-package table-button" data-id-cikl="'+plans_package[key].id_cikl+'" data-toggle="tooltip" title="Додати план"><i class="fa fa-plus" aria-hidden="true"></i></span> <span class="popup-plan table-button" data-id-plan="'+plans_package[key].id_plan+'" data-toggle="tooltip" title="Переглянути план"><i class="fa fa-eye" aria-hidden="true"></i></span> <span class="remove-plan-from-package table-button" data-toggle="tooltip" title="Видалити план"><i class="fa fa-minus" aria-hidden="true"></i></span></td></tr>';
                            }
                            temp += '</tbody></table>';
                            $( "#tabs-2" ).html( temp );
                            $(".add-plan-to-package").click(function(){ hexathis = $(this); change_plan(); });
                            $(".popup-plan").click(function(){
                                get_plan($(this).attr("data-id-plan"));
                            });
                            $(".remove-plan-from-package").click(function(){
                                $(this).siblings(".popup-plan").attr("data-id-plan", "0");
                                $(this).parent().siblings(".name-plan").attr("data-id-plan", "0");
                                $(this).parent().siblings(".name-plan").empty();
                                $(this).parent().siblings(".name-status").attr("data-id-status", "0");
                                $(this).parent().siblings(".name-status").empty();
                            });
                            current_package.id_package = temp_int;
                            $( "#dialog-package" ).dialog( "open" );
                        },
                        error: function (result) {
                            $().toastmessage('showToast', {
                                text     : 'Error',
                                type     : 'error',
                            });
                        }
                    });
                }

                /* Удаляем пакет */
                $( ".delete-package" ).click(function(){delete_package($(this))});
                function delete_package(sender){
                    hexathis = sender;
                    var data = new FormData();
                    data.append( 'id_package', sender.attr("data-id-package") );
                    $.ajax ({
                        url: '{{ route("school-package-delete") }}',
                        type: "POST",
                        dataType: 'json',
                        data: data,
                        processData: false,
                        contentType : false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (result) {
                            if (result.status) {
                                hexathis.parent().parent().remove();
                                $().toastmessage('showToast', {
                                    text     : 'Видалено',
                                    type     : 'notice',
                                });
                            } else {
                                $().toastmessage('showToast', {
                                    text     : result.message,
                                    type     : 'warning',
                                });
                            }
                        },
                        error: function (result) {
                            $().toastmessage('showToast', {
                                text     : 'Error',
                                type     : 'error',
                            });
                        }
                    });
                }



                /* Получение всех подходящих планов и открытие окна выбора */
                function change_plan() {
                        console.log(hexathis.attr("class"));
                    $.get('{{ route("user-plans-get-all") }}', {id_cikl: hexathis.attr("data-id-cikl")} ,function(result){
                        result = JSON.parse(result);
                        console.log(result);
                        $( "#add-plan-to-package .dialog-content" ).empty();
                        temp = '<table class="list"><thead><tr><td>Назва</td><td>Статус</td><td>Дія</td></tr></thead><tbody><form>';
                        for (key in result) {
                            temp += '<tr><td class="name-plan">' + result[key].name_plan +'</td><td class="name-status" data-id-status="'+result[key].id_status+'">'+result[key].name_status+'</td><td><input id="radio'+key+'" type="radio" name="question1" value="'+result[key].id_plan+'"';
                            if (parseInt(result[key].id_plan) == parseInt(hexathis.parent().siblings(".name-plan").attr("data-id-plan"))){ temp +=" checked"; }
                            temp += '></td></tr>';
                        }
                        temp += '</form></tbody></table>';
                        $( "#add-plan-to-package .dialog-content" ).append(temp);

                        $( "#add-plan-to-package" ).dialog( "open" );
                    });
                }


                $(".approve-package").click(function(){
                    approve_package($(this).attr("data-id-package"));
                });

                function approve_package(id_package='') {
                    var data = new FormData();
                    data.append( 'id_package', id_package );
                    $.ajax ({
                        url: '{{ route("school-package-approve") }}',
                        type: "POST",
                        dataType: 'json',
                        data: data,
                        processData: false,
                        contentType : false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (result) {
                            console.log(result);
                            if (result.status) {type='success';} else {type='warning';}
                            $().toastmessage('showToast', {
                                text     : result.message,
                                type     : type,
                            });
                        },
                        error: function (result) {
                            $().toastmessage('showToast', {
                                text     : 'Error',
                                type     : 'error',
                            });
                        }
                    });
                }




                $( function() {
                    $( "#dialog-package" ).dialog({
                        resizable: false,
                        width: "80%",
                        modal: true,
                        autoOpen: false,
                        show: {
                            effect: "fade",
                            duration: 100
                        },
                        hide: {
                            effect: "fade",
                            duration: 100
                        },

                        buttons: [
                            {
                                text: "Зберегти",
                                class: "btn-ok",
                                click: function() {
                                var plans = {}, plancount = 0;
                                $( ".list-of-plans" ).each(function( item ) {
                                        var plan = {};
                                        plan.id_cikl = $(this).children(".name-cikl").attr("data-id-cikl");
                                        plan.name_cikl = $(this).children(".name-cikl").text();
                                        plan.id_plan = $(this).children(".name-plan").attr("data-id-plan");
                                        if (plan.id_plan != "0") {plancount++;}
                                        plan.name_plan = $(this).children(".name-plan").text();
                                        plan.id_status = $(this).children(".name-status").attr("data-id-status");
                                        plan.name_status = $(this).children(".name-status").text();
                                        plans[item] = plan;
                                });

                                var data = new FormData();
                                data.append( 'id_package', current_package.id_package );
                                data.append( 'name_package', $("#name_package").val() );
                                data.append( 'plancount_package', plancount );
                                data.append( 'plans_package', JSON.stringify(plans).replace(/\'/g,"&#39;"));
                                data.append( 'vam_package', tinyMCE.activeEditor.getContent() );

                                $.ajax ({
                                    url: '{{ route("school-package-save") }}',
                                    type: "POST",
                                    dataType: 'json',
                                    data: data,
                                    processData: false,
                                    contentType : false,
                                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                    success: function (result) {
                                        if (result.new){
                                            var temp = '<tr data-id-package="'+result.id_package+'"><td>'+result.id_package+'</td><td class="name-package">'+result.name_package+'</td><td class="plancount-package">'+result.plancount_package+'</td><td>'+result.date1+'</td><td>'+result.date1+'</td><td class="name-status">'+result.name_status+'</td><td class="text-center"><span class="edit-package table-button" data-id-package="'+result.id_package+'" data-toggle="tooltip" title="Переглянути"><i class="fa fa-search" aria-hidden="true"></i></span> <span class="delete-package table-button" data-id-package="'+result.id_package+'" data-toggle="tooltip" title="Видалити"><i class="fa fa-times" aria-hidden="true"></i></span> <span class="approve-package table-button" data-id-package="'+result.id_package+'" data-toggle="tooltip" title="Узгодити"><i class="fa fa-certificate" aria-hidden="true"></i></span></td></tr>';

                                            $(".school-package-body").append(temp);
                                            //$('[data-toggle="tooltip"]').tooltip();
                                            $( ".edit-package" ).click(function(){edit_package($(this));});
                                            $( ".delete-package" ).click(function(){delete_package($(this))});
                                            $(".approve-package").click(function(){approve_package($(this).attr("data-id-package"));});

                                        } else {
                                            $('[data-id-package="'+result.id_package+'"]').children(".name-package").text(result.name_package);
                                            $('[data-id-package="'+result.id_package+'"]').children(".plancount-package").text(result.plancount_package);
                                            $('[data-id-package="'+result.id_package+'"]').children(".name-status").text(result.name_status);
                                        }
                                        $().toastmessage('showToast', {
                                            text     : 'Збережено',
                                            type     : 'success',
                                        });
                                        console.log(result.hexa);
                                    },
                                    error: function (result) {
                                        $().toastmessage('showToast', {
                                            text     : 'Error',
                                            type     : 'error',
                                        });
                                    }
                                });
                                $( this ).dialog( "close" );
                                }
                            },
                            {
                                text: "Скасувати",
                                class: "btn-cancel",
                                click: function() {
                                    $( this ).dialog( "close" );
                                    $().toastmessage('showToast', {
                                        text     : 'Скасовано',
                                        type     : 'warning',
                                    });
                                }
                            }
                        ]

                    });

                    /* Окно со списком планов */
                    $( "#add-plan-to-package" ).dialog({
                        resizable: false,
                        width: "60%",
                        modal: true,
                        autoOpen: false,
                        show: {
                            effect: "fade",
                            duration: 100
                        },
                        hide: {
                            effect: "fade",
                            duration: 100
                        },
                        buttons: [
                            {
                                text: "Додати",
                                class: "btn-ok",
                                click: function() {
                                    var data = new FormData();
                                    data.append( 'id_package', $(this).attr("data-id-package") );
                                    hexathis.parent().siblings(".name-plan").text($('input[name=question1]:checked').parent().siblings(".name-plan").text());
                                    hexathis.parent().siblings(".name-status").text($('input[name=question1]:checked').parent().siblings(".name-status").text());
                                    hexathis.parent().siblings(".name-status").attr("data-id-status", $('input[name=question1]:checked').parent().siblings(".name-status").attr("data-id-status"));

                                    hexathis.parent().siblings(".name-plan").attr("data-id-plan", $('input[name=question1]:checked').val());
                                    hexathis.siblings(".popup-plan").attr("data-id-plan", $('input[name=question1]:checked').val());
                                    $( this ).dialog( "close" );
                                }
                            },
                            {
                                text: "Скасувати",
                                class: "btn-cancel",
                                click: function() {
                                    $( this ).dialog( "close" );
                                }
                            }
                        ]

                    });


                });






    });
</script>
@endsection

@endif

@include('template-part.popup-plan')
