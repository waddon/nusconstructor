@extends('common.layout-new')

    @section('content')

        <div class="content-box-title">{{ $title or '' }}</div>
        <div class="content-box">

            @if (auth()->user()->id_school != 0)
                <div class="school-title">
                    Навчальний заклад: {{ $school->name_school }}<br>
                    Адреса: {{ $school->adress_school }}
                </div>
                
                <table class="school-users">
                    <thead>
                        <tr>
                            <td>Фото</td>
                            <td>ПІБ</td>
                            <td>Статус</td>
                            <td>Дії</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img class="user-face-in-list" src="@if($supervisor->thumb != '') {{ $supervisor->thumb }} @else {{ '/assets/images/user-photo.png' }} @endif" alt=""></td>
                            <td>{{ $supervisor->second_name . ' ' . $supervisor->name . ' ' . $supervisor->patronymic }}</td>
                            <td>Голова групи</td>
                            <td></td>
                        </tr>

                        @if(isset($schoolusers))
                        @foreach ($schoolusers as $schooluser)
                            <tr>
                                <td><img class="user-face-in-list" src="@if($schooluser->thumb != '') {{ $schooluser->thumb }} @else {{ '/assets/images/user-photo.png' }} @endif" alt=""></td>
                                <td>{{ $schooluser->second_name . ' ' . $schooluser->name . ' ' . $schooluser->patronymic }}</td>
                                <td class="name_statususer">{{ $schooluser->name_statususer }}</td>
                                <td class="action_user" id="user-{{$schooluser->id_user}}" data-id-schooluser="{{$schooluser->id_schooluser}}">
                                    @can('director')
                                        @if($schooluser->id_statususer==1)
                                            <span class="disabled action-button table-button" data-action="3" data-toggle="tooltip" title="Видалити з групи"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        @elseif($schooluser->id_statususer==2)
                                            <span class="enabled action-button table-button" data-action="1" data-toggle="tooltip" title="Прийняти до групи"><i class="fa fa-check" aria-hidden="true"></i></span>
                                            <span class="disabled action-button table-button" data-action="3" data-toggle="tooltip" title="Видалити з групи"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        @endif
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        @endif

                    </tbody>

                </table>
            @else
                <p>Належніть до навчального закладу відсутня</p>
            @endif
        </div>

    @endsection


{{-- Далее блоки, отображаемые только директору школы --}}

@can('director')

    @section('scripts')
        <script>
            jQuery(document).ready(function(){
                var on_change = false;

                $( ".action-button" ).click(function(){
                    if (!on_change) {
                        on_change = true;
                        hexathis = $(this);
                        $.get('{{ route("school-users-status") }}', {id_schooluser: $(this).parent().attr("data-id-schooluser"), id_statususer: $( this ).attr("data-action")} ,function(result){
                            result = JSON.parse(result);
                            hexathis.parent().siblings(".name_statususer").text(result.name_statususer);
                            if (result.id_statususer == 1) {
                                hexathis.css("display", "none");
                            } else if(result.id_statususer == 3) {
                                hexathis.parent().empty();
                            }
                            console.log(result);
                            on_change = false;
                        });
                    }

                });

            });
        </script>
    @endsection

@endcan
