@extends('common.layout-new')

    @section('content')

        <div class="content-box-title">Глосарій</div>
        <div class="content-box">
            <ul class="laters text-center">
                @foreach ($laters as $value)
                    <li><a href="{{ route('glossary', $value) }}">{{ $value }}</a></li>
                @endforeach                
            </ul>
            <h2 class="text-center">{{ $later or '' }}</h2>
            
            @foreach ($terms as $term)
                <div class="term" style="white-space: nowrap;overflow: hidden;">
                    {{ $term->name_post }}<br>{{ $term->excerpt_post }}<br>
                    <span class="glossary-item" data-title="{{ $term->name_post }}" data-id-post="{{ $term->id_post }}">Більше</span>
                </div>
            @endforeach                

        </div>

    @endsection

    @section('dialogs')
        <div id="dialog-glossary-one" title="Title">
            <div class="dialog-content"></div>
        </div>
    @endsection


    @section('scripts')
        <script>


            /* нажатие на новость */
            $( ".glossary-item" ).click(function(){
                $(".ui-dialog-title").html( $(this).attr("data-title") );
                $(".ui-dialog-titlebar").removeClass( "color4" ).addClass( "color4" );

                // var color = "color" + $(this).attr("data-color");
                // $(".ui-dialog-titlebar").removeClass( "color1" ).removeClass( "color2" ).removeClass( "color3" );
                // $("#dialog-glossary-one").removeClass( "color1" ).removeClass( "color2" ).removeClass( "color3" );
                // $("#dialog-glossary-one").addClass( color );                
                /* Получаем данные поста */
                $.get('{{ route("get_post") }}', {id_post: $(this).attr("data-id-post")} ,function(result){
                    result = JSON.parse(result);
                    $(".dialog-content").empty();
                    $(".dialog-content").append(result.content_post);
                    /* Открываем диалоговое окно*/
                    $( "#dialog-glossary-one" ).dialog( "open" );
                });
            });
            /* Модальное окно с новостью */
            $( function() {
                $( "#dialog-glossary-one" ).dialog({
                    resizable: false,
                    width: "80%",
                    modal: true,
                    autoOpen: false,
                    show: {
                        effect: "fade",
                        duration: 100
                    },
                    hide: {
                        effect: "fade",
                        duration: 100
                    },
                });
            });
        </script>
    @endsection
