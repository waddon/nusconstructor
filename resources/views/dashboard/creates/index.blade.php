@extends('layouts.admin')

@section('title')
    <title>Роли создателей</title>
@endsection

@section('content')

    <h1>Управління ролями создателей</h1>

    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('creates.create') }}" class="btn btn-success">Додати роль создателя</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Назва</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($creates as $create)
            <tr>
                <td>{{ $create->name_create }}</td>
                <td>
                    <a href="{{ route('creates.edit', $create->id_create) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['creates.destroy', $create->id_create] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection