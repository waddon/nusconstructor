@extends('layouts.admin')

@section('title')
    <title>Додати роль</title>
@endsection

@section('content')


<h1>Додати роль</h1>

<div class='col-lg-12'>

    {{ Form::open(['route' => 'creates.index']) }}

        <div class="form-group">
            {{ Form::label('name_create', 'Назва') }}
            {{ Form::text('name_create', null, ['class' => 'form-control' . ($errors->has('name_create') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_create'))
                @foreach ($errors->get('name_create') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection