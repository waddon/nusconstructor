@extends('layouts.admin')

@section('title')
    <title>Користувачі</title>
@endsection

@section('content')


    <h1><i class="fa fa-users"></i> Управління користувачами</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('users.create') }}" class="btn btn-success">Додати користувача</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Ім'я</th>
                <th>Email</th>
                <th>Дата/час додавання</th>
                <th>Ролі користувача</th>
                <th>Операції</th>
            </tr>
        </thead>
        
        <tbody>

            @foreach ($users as $user)
                @if(Auth::user()->hasRole('main_moderator') && strpos($user->roles()->pluck('name')->implode(' '), 'admin') !== false)  
                @else
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                        <td>{{ $user->roles()->pluck('name')->implode(' ') }}</td>{{-- Retrieve array of roles associated to a user and convert to string --}}
                        <td>
                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>
                            <a href="{{ route('users.show', $user->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Увійти</a>

                            {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['users.destroy', $user->id] ]) !!}
                            {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

@endsection