@extends('layouts.admin')

@section('title')
    <title>Додати користувача</title>
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h1><i class='fa fa-user-plus'></i> Додати користувача</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::open(['route' => 'users.index']) }}

         <div class="form-group">
            {{ Form::label('name', 'Ім\'я') }}
            {{ Form::text('name', null, ['class' => 'form-control' . ($errors->has('name') ? ' form-control-danger' : '')]) }}

            @if($errors->has('name'))
                @foreach ($errors->get('name') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('second_name', 'Прізвище') }}
            {{ Form::text('second_name', null, ['class' => 'form-control' . ($errors->has('second_name') ? ' form-control-danger' : '')]) }}

            @if($errors->has('second_name'))
                @foreach ($errors->get('second_name') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('patronymic', 'По-батьковi') }}
            {{ Form::text('patronymic', null, ['class' => 'form-control' . ($errors->has('patronymic') ? ' form-control-danger' : '')]) }}

            @if($errors->has('patronymic'))
                @foreach ($errors->get('patronymic') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{-- <div class="form-group {{ $errors->has('id_position') ? 'has-danger' : '' }}">
            {{ Form::label('id_position', 'Посада') }}
            {{ Form::text('id_position', null, ['class' => 'form-control']) }}

            @if($errors->has('id_position'))
                @foreach ($errors->get('id_position') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group {{ $errors->has('id_position') ? 'has-danger' : '' }}">
            {{ Form::label('id_role', 'Роль') }}
            {{ Form::text('id_role', null, ['class' => 'form-control']) }}

            @if($errors->has('id_role'))
                @foreach ($errors->get('id_role') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div> --}}

        <div class="form-group">
            {{ Form::label('phone', 'Телефон') }}
            {{ Form::text('phone', null, ['class' => 'form-control' . ($errors->has('phone') ? ' form-control-danger' : '')]) }}

            @if($errors->has('phone'))
                @foreach ($errors->get('phone') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, ['class' => 'form-control' . ($errors->has('email') ? ' form-control-danger' : '')]) }}

            @if($errors->has('email'))
                @foreach ($errors->get('email') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <h5><b>Додати роль</b></h5>

        <div class='form-group'>
            @foreach ($roles as $role)
                {{ Form::checkbox('roles[]',  $role->id ) }}
                {{ Form::label($role->name, $role->name) }}<br>
            @endforeach
        </div>
        
        <div class="form-group">
            {{ Form::label('password', 'Пароль') }}
            {{ Form::password('password', ['class' => 'form-control' . ($errors->has('password') ? ' form-control-danger' : '')]) }}
        </div>

        <div class="form-group">
            {{ Form::label('password_confirmation', 'Підтвердження паролю') }}
            {{ Form::password('password_confirmation', ['class' => 'form-control' . ($errors->has('password') ? ' form-control-danger' : '')]) }}
            
            @if($errors->has('password'))
                @foreach ($errors->get('password') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection