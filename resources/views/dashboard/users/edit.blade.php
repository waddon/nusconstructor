@extends('layouts.admin')

@section('title')
    <title>Редагування користувача</title>
@endsection

@section('content')

<h1><i class='fa fa-user-plus'></i> Редагування {{$user->name}}</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT']) }}
        <div class="form-group">
            {{ Form::label('name', 'Ім\'я') }}
            {{ Form::text('name', null, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : '')]) }}

            @if($errors->has('name'))
                @foreach ($errors->get('name') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('second_name', 'Прізвище') }}
            {{ Form::text('second_name', null, ['class' => 'form-control' . ($errors->has('second_name') ? ' is-invalid' : '')]) }}

            @if($errors->has('second_name'))
                @foreach ($errors->get('second_name') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('patronymic', 'По-батьковi') }}
            {{ Form::text('patronymic', null, ['class' => 'form-control' . ($errors->has('patronymic') ? ' is-invalid' : '')]) }}

            @if($errors->has('patronymic'))
                @foreach ($errors->get('patronymic') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('phone', 'Телефон') }}
            {{ Form::text('phone', null, ['class' => 'form-control' . ($errors->has('phone') ? ' is-invalid' : '')]) }}

            @if($errors->has('phone'))
                @foreach ($errors->get('phone') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : '')]) }}

            @if($errors->has('email'))
                @foreach ($errors->get('email') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <h5><b>Додати роль</b></h5>

        <div class='form-group'>
            @foreach ($roles as $role)
                {{ Form::checkbox('roles[]',  $role->id, $user->roles) }}
                {{ Form::label($role->name, $role->name) }}
                <br>
            @endforeach
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection