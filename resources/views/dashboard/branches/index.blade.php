@extends('layouts.admin')

@section('title')
    <title>Галузі</title>
@endsection

@section('content')

    <h1>Галузі</h1>
        @can('edit_content')
            <div class="col-lg-3">
                <div class="form-group">
                    <a href="{{ route('branches.create') }}" class="btn btn-success">Додати галузь</a>
                </div>
            </div>
        @endcan
    <table class="table">
        <thead>
            <tr>
                <th>Назва галузі</th>
                <th>Маркування</th>
                <th>Колір</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($branches as $branch)
                @if(Auth::user()->hasAnyPermission([$branch->marking_galuz, 'edit_branches']))
                    <tr>
                        <td>{{ $branch->name_galuz }}</td>
                        <td>{{ $branch->marking_galuz }}</td>
                        <td style="background-color: {{ $branch->color_galuz }}"></td>
                        <td>
                            <a href="{{ route('branches.edit', $branch->id_galuz) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>
                            {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['branches.destroy', $branch->id_galuz] ]) !!}
                                {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

@endsection