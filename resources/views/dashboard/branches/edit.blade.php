@extends('layouts.admin')

@section('title')
    <title>Редагувати категорію</title>
@endsection

@section('content')

<div class='col-lg-12'>

    <h1>Редагувати {{$branch->galuz_name}}</h1>
    
    {{ Form::model($branch, ['route' => ['branches.update', $branch->id_galuz], 'method' => 'PUT']) }}

    <div class="form-group">
        {{ Form::label('name_galuz', 'Назва галузі') }}
        {{ Form::text('name_galuz', null, ['class' => 'form-control' . ($errors->has('name_galuz') ? ' is-invalid' : '')]) }}

        @if($errors->has('name_galuz'))
            @foreach ($errors->get('name_galuz') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('marking_galuz', 'Маркування') }}
        {{ Form::text('marking_galuz', null, ['class' => 'form-control' . ($errors->has('marking_galuz') ? ' is-invalid' : ''), Auth::user()->can('edit_content') ? '' : 'readonly']) }}

        @if($errors->has('marking_galuz'))
            @foreach ($errors->get('marking_galuz') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('text_galuz', 'Текст') }}
        {{ Form::textarea('text_galuz', null, ['class' => 'form-control tinymce-val' . ($errors->has('text_galuz') ? ' is-invalid' : '')]) }}

        @if($errors->has('text_galuz'))
            @foreach ($errors->get('text_galuz') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('color_galuz', 'Текст') }}
        {{ Form::color('color_galuz', null, ['class' => 'form-control admin-color' . ($errors->has('color_galuz') ? ' is-invalid' : '')]) }}

        @if($errors->has('color_galuz'))
            @foreach ($errors->get('text_galuz') as $message)
                <div class="invalid-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    {{ Form::submit('Редагувати', ['class' => 'btn btn-success']) }}

    {{ Form::close() }}

</div>

@endsection