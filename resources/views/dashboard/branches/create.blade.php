@extends('layouts.admin')

@section('title')
    <title>Додати галузь</title>
@endsection

@section('content')

<div class='col-lg-12'>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <h1>Додати галузь</h1>

    {{ Form::open(['route' => 'branches.index']) }}

        <div class="form-group">
            {{ Form::label('branch_name', 'Назва галузі') }}
            {{ Form::text('branch_name', null, ['class' => 'form-control' . ($errors->has('branch_name') ? ' is-invalid' : '')]) }}

            @if($errors->has('branch_name'))
                @foreach ($errors->get('branch_name') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('branch_label', 'Маркування') }}
            {{ Form::text('branch_label', null, ['class' => 'form-control' . ($errors->has('branch_label') ? ' is-invalid' : '')]) }}

            @if($errors->has('branch_label'))
                @foreach ($errors->get('branch_label') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('branch_text', 'Опис') }}
            {{ Form::textarea('branch_text', null, ['class' => 'form-control tinymce-val' . ($errors->has('branch_text') ? ' is-invalid' : '')]) }}

            @if($errors->has('branch_text'))
                @foreach ($errors->get('branch_text') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('branch_color', 'Колір') }}
            {{ Form::color('branch_color', null, ['class' => 'form-control admin-color' . ($errors->has('branch_color') ? ' is-invalid' : '')]) }}

            @if($errors->has('branch_color'))
                @foreach ($errors->get('branch_color') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати галузь', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection