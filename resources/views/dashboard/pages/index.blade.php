@extends('layouts.admin')

@section('title')
    <title>Страницы</title>
@endsection

@section('content')

    <h1>Страницы</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('pages.create') }}" class="btn btn-success">Додати страницу</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Страницы</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($pages as $page)
            <tr>
                <td>{{ $page->id_page }}</td>
                <td>{{ $page->name_page }}</td>
                <td>
                    <a href="{{ route('pages.edit', $page->id_page) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['pages.destroy', $page->id_page] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection