@extends('layouts.admin')

@section('title')
    <title>Редагування страниці</title>
@endsection

@section('content')

<h1>Редагування {{$page->name_tool}}</h1>

<div class='col-lg-12'>

    {{ Form::model($page, ['route' => ['pages.update', $page->id_page], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_page', 'Назва інструменту') }}
            {{ Form::text('name_page', null, ['class' => 'form-control' . ($errors->has('name_page') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_page'))
                @foreach ($errors->get('name_page') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('slug_page', 'Відносний URL') }}
            {{ Form::text('slug_page', null, ['class' => 'form-control' . ($errors->has('slug_page') ? ' is-invalid' : '')]) }}

            @if($errors->has('slug_page'))
                @foreach ($errors->get('slug_page') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('content_page', 'Контент страниці') }}
            {{ Form::textarea('content_page', null, ['class' => 'form-control tinymce-val' . ($errors->has('content_page') ? ' is-invalid' : '')]) }}

            @if($errors->has('content_page'))
                @foreach ($errors->get('content_page') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection