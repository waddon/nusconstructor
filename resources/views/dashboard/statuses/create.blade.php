@extends('layouts.admin')

@section('title')
    <title>Додати статус</title>
@endsection

@section('content')

<h1>Додати статус</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::open(['route' => 'statuses.index']) }}

        <div class="form-group">
            {{ Form::label('name_status', 'Назва статусу') }}
            {{ Form::text('name_status', null, ['class' => 'form-control' . ($errors->has('name_status') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_status'))
                @foreach ($errors->get('name_status') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection