@extends('layouts.admin')

@section('title')
    <title>Статуси</title>
@endsection

@section('content')

    <h1>Управління статусами</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('statuses.create') }}" class="btn btn-success">Додати статус</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Назва статусу</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($statuses as $status)
            <tr>
                <td>{{ $status->id_status }}</td>
                <td>{{ $status->name_status }}</td>
                <td>
                    <a href="{{ route('statuses.edit', $status->id_status) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['statuses.destroy', $status->id_status] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection