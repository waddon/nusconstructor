@extends('layouts.admin')

@section('title')
    <title>Редагування статусу</title>
@endsection

@section('content')

<h1>Редагування {{$status->name_status}}</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::model($status, ['route' => ['statuses.update', $status->id_status], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_status', 'Назва статусу') }}
            {{ Form::text('name_status', null, ['class' => 'form-control' . ($errors->has('name_status') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_status'))
                @foreach ($errors->get('name_status') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection