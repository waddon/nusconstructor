@extends('layouts.admin')

@section('title')
    <title>ЗОРи</title>
@endsection

@section('content')

    <h1>ЗОРи</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('expected-outcomes.create') }}" class="btn btn-success">Додати ЗОР</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>ЗОР</th>
                <th>Галузь</th>
                <th>Цикл</th>
                <th>Код вміння</th>
                <th>Операції</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($expected_outcomes as $expected_outcome)
            	@if(Auth::user()->hasAnyPermission([$expected_outcome->getExpectedOutcomeBranch()->marking_galuz, 'edit_content']))
	                <tr>
	                    <td>{{ $expected_outcome->name_zor }}</td>
	                    <td style="min-width: 170px;">{{ $expected_outcome->getExpectedOutcomeBranch()->name_galuz }}</td>
	                    <td style="min-width: 90px;">{{ $expected_outcome->getExpectedOutcomeCycle() }}</td>
	                    <td style="min-width: 120px; text-align: center;">{{ $expected_outcome->key_zor }}</td>
	                    <td style="min-width: 233px;">
	                        <a href="{{ route('expected-outcomes.edit', $expected_outcome->id_zor) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>
	                        {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['expected-outcomes.destroy', $expected_outcome->id_zor] ]) !!}
	                            {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
	                        {!! Form::close() !!}
	                    </td>
	                </tr>
	            @endif
            @endforeach
        </tbody>
    </table>

@endsection