@extends('layouts.admin')

@section('title')
    <title>Додати ЗОР</title>
@endsection

@section('content')

<div class='col-lg-12'>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <h1>Додати ЗОР</h1>

    {{ Form::open(['route' => 'expected-outcomes.index']) }}

        <div class="form-group">
            {{ Form::label('expected_outcome_name', 'Назва') }}
            {{ Form::text('expected_outcome_name', null, ['class' => 'form-control' . ($errors->has('expected_outcome_name') ? ' is-invalid' : '')]) }}

            @if($errors->has('expected_outcome_name'))
                @foreach ($errors->get('expected_outcome_name') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('expected_outcome_key', 'Код вміння') }}
            {{ Form::text('expected_outcome_key', null, ['class' => 'form-control' . ($errors->has('expected_outcome_key') ? ' is-invalid' : '')]) }}

            @if($errors->has('expected_outcome_key'))
                @foreach ($errors->get('expected_outcome_key') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('cycle_id', 'Цикл', ['class' => 'form-control-label']) }}
            {{ Form::select('cycle_id', $cycles, 0, ['class' => 'form-control' . ($errors->has('cycle_id') ? 'is-invalid' : '')]) }}

            @if($errors->has('cycle_id'))
                @foreach ($errors->get('cycle_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('branch_id', 'Галузь', ['class' => 'form-control-label']) }}
            {{ Form::select('branch_id', $branches, 0, ['class' => 'form-control' . ($errors->has('branch_id') ? 'is-invalid' : '')]) }}

            @if($errors->has('branch_id'))
                @foreach ($errors->get('branch_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection