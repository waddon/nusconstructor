@extends('layouts.admin')

@section('title')
    <title>Редагувати ЗОР</title>
@endsection

@section('content')

<div class='col-lg-12'>

    <h1>Редагувати {{$expected_outcome->name_zor}}</h1>


    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    {{ Form::model($expected_outcome, ['route' => ['expected-outcomes.update', $expected_outcome->id_zor], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_zor', 'Назва категорії') }}
            {{ Form::text('name_zor', null, ['class' => 'form-control' . ($errors->has('name_zor') ? ' form-control-danger' : '')]) }}

            @if($errors->has('name_zor'))
                @foreach ($errors->get('name_zor') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('key_zor', 'Постійне посилання') }}
            {{ Form::text('key_zor', null, ['class' => 'form-control' . ($errors->has('key_zor') ? ' form-control-danger' : '')]) }}

            @if($errors->has('key_zor'))
                @foreach ($errors->get('key_zor') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_cikl', 'Цикл', ['class' => 'form-control-label']) }}
            {{ Form::select('id_cikl', $cycles, null, ['class' => 'form-control' . ($errors->has('id_cikl') ? 'is-invalid' : '')]) }}

            @if($errors->has('id_cikl'))
                @foreach ($errors->get('id_cikl') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_galuz', 'Галузь', ['class' => 'form-control-label']) }}
            {{ Form::select('id_galuz', $branches, null, ['class' => 'form-control' . ($errors->has('id_galuz') ? 'is-invalid' : '')]) }}

            @if($errors->has('id_galuz'))
                @foreach ($errors->get('id_galuz') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', ['class' => 'btn btn-success']) }}

    {{ Form::close() }}

</div>

@endsection