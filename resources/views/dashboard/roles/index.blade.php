@extends('layouts.admin')

@section('title')
    <title>Ролі</title>
@endsection

@section('content')

    <h1>Ролі</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('roles.create') }}" class="btn btn-success">Додати роль</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Роль</th>
                <th>Дозволи</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($roles as $role)
            <tr>
                <td>{{ $role->name }}</td>
                <td>
                    @foreach($role->permissions()->pluck('name') as $permission)
                        {{ $permission . " " }}
                    @endforeach
                </td>
                <td>
                    <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>
                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection