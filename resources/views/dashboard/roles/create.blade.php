@extends('layouts.admin')

@section('title')
    <title>Додати роль</title>
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class='col-lg-12'>

    <h1><i class='fa fa-key'></i> Додати роль</h1>

    {{ Form::open(array('url' => 'dashboard/roles')) }}

    <div class="form-group">
        {{ Form::label('name', 'Назва ролі') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    <h5><b>Призначити дозволи</b></h5>

    <div class='form-group'>
        @foreach ($permissions as $permission)
            {{ Form::checkbox('permissions[]',  $permission->id ) }}
            {{ Form::label($permission->name, $permission->name) }}<br>
        @endforeach
    </div>

    {{ Form::submit('Додати роль', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection