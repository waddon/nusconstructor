@extends('layouts.admin')

@section('title')
    <title>Меню</title>
@endsection

@section('content')

<h1>Управління меню</h1>

<table class="table">
    <thead>
        <tr>
            <th>Назва</th>
            <th>Операція</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($menus as $menu)
        <tr>
            <td>{{ $menu->name_menu }}</td>
            <td>
                <a href="{{ route('menus.edit', $menu->id_menu) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['menus.destroy', $menu->id_menu] ]) !!}
                    {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<a href="{{ route('menus.create') }}" class="btn btn-success">Додати меню</a>

@endsection