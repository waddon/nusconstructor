@extends('layouts.admin')

@section('title')
    <title>Редагування меню</title>
@endsection

@section('content')

<h1>Редагування {{$menu->name_menu}}</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::model($menu, ['route' => ['menus.update', $menu->id_menu], 'method' => 'PUT']) }}

     <div class="form-group">
        {{ Form::label('name_menu', 'Назва') }}
        {{ Form::text('name_menu', null, ['class' => 'form-control' . ($errors->has('name_menu') ? ' is-invalid' : '')]) }}

        @if($errors->has('name_menu'))
            @foreach ($errors->get('name_menu') as $message)
                <div class="form-control-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection