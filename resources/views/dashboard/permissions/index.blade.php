@extends('layouts.admin')

@section('title')
    <title>Дозволи</title>
@endsection

@section('content')

    <h1>Доступні права доступу</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('permissions.create') }}" class="btn btn-success">Додати дозвіл</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Дозволи</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($permissions as $permission)
            <tr>
                <td>{{ $permission->name }}</td> 
                <td>
                    <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>
                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                    {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection