@extends('layouts.admin')

@section('title')
    <title>Редагувати дозвіл</title>
@endsection

@section('content')

<div class='col-lg-12'>

    <h1><i class='fa fa-key'></i> Редагувати {{$permission->name}}</h1>
    
    {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with permission data --}}

    <div class="form-group">
        {{ Form::label('name', 'Назва дозволу') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Редагувати', array('class' => 'btn btn-success')) }}

    {{ Form::close() }}

</div>

@endsection