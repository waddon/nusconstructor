@extends('layouts.admin')

@section('title')
    <title>Додати дозвіл</title>
@endsection

@section('content')

<div class='col-lg-12'>

    <h1>Додайте дозвіл</h1>
    <br>

    {{ Form::open(array('route' => 'permissions.index')) }}

    <div class="form-group">
        {{ Form::label('name', 'Ім\'я') }}
        {{ Form::text('name', '', array('class' => 'form-control')) }}
    </div>
    @if(!$roles->isEmpty())
        <h4>Призначити дозвіл на роль</h4>

        @foreach ($roles as $role) 
            {{ Form::checkbox('roles[]',  $role->id ) }}
            {{ Form::label($role->name, ucfirst($role->name)) }}<br>

        @endforeach
    @endif
    <br>
    {{ Form::submit('Додати', array('class' => 'btn btn-success')) }}

    {{ Form::close() }}

</div>

@endsection