@extends('layouts.admin')

@section('title')
    <title>Редагувати категорію</title>
@endsection

@section('content')

<div class='col-lg-12'>

    <h1><i class='fa fa-key'></i> Редагувати {{$category->name_cat}}</h1>


    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    {{ Form::model($category, array('route' => array('categories.update', $category->id_cat), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('name_cat', 'Назва категорії') }}
        {{ Form::text('name_cat', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('slug_cat', 'Постійне посилання') }}
        {{ Form::text('slug_cat', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Редагувати', array('class' => 'btn btn-success')) }}

    {{ Form::close() }}

</div>

@endsection