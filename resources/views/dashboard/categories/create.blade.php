@extends('layouts.admin')

@section('title')
    <title>Додати категорію</title>
@endsection

@section('content')

<div class='col-lg-12'>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <h1><i class='fa fa-key'></i> Додати категорію</h1>

    {{ Form::open(array('url' => 'dashboard/categories')) }}

    <div class="form-group">
        {{ Form::label('name_cat', 'Назва категорії') }}
        {{ Form::text('name_cat', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('slug_cat', 'Постійне посилання') }}
        {{ Form::text('slug_cat', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Додати категорію', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection