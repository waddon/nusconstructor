@extends('layouts.admin')

@section('title')
    <title>Категорії</title>
@endsection

@section('content')

    <h1>Категорії</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('categories.create') }}" class="btn btn-success">Додати категорію</a>
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Категорія</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($categories as $category)
            <tr>
                <td>{{ $category->name_cat }}</td>
                <td>
                    <a href="{{ route('categories.edit', $category->id_cat) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>
                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['categories.destroy', $category->id_cat] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection