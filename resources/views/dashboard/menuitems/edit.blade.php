@extends('layouts.admin')

@section('title')
    <title>Редагування пункта меню</title>
@endsection

@section('content')

<h1>Редагування {{$menuitem->name_menuitem}}</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::model($menuitem, ['route' => ['menuitems.update', $menuitem->id_menuitem], 'method' => 'PUT']) }}

    <div class="form-group {{ $errors->has('id_riven') ? 'has-danger' : '' }}">
        {{ Form::label('id_menu', 'Меню', ['class' => 'form-control-label']) }}
        {{ Form::select('id_menu', $menus, null, ['class' => 'form-control' . ($errors->has('id_menu') ? ' form-control-danger' : '')]) }}

        @if($errors->has('id_menu'))
            @foreach ($errors->get('id_menu') as $message)
                <div class="form-control-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('name_menuitem', 'Назва') }}
        {{ Form::text('name_menuitem', null, ['class' => 'form-control' . ($errors->has('name_menuitem') ? ' is-invalid' : '')]) }}

        @if($errors->has('name_menuitem'))
            @foreach ($errors->get('name_menuitem') as $message)
                <div class="form-control-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>


    {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

</div>

@endsection