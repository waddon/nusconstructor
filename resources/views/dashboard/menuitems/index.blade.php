@extends('layouts.admin')

@section('title')
    <title>Пункти Меню</title>
@endsection

@section('content')
<style>
    .card{
        /*padding: 15px;*/
        margin-bottom: 30px;
    }
    .card-block{
        padding: 15px;
    }
    .card-header{
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: -o-flex;
        display: flex;
    }
    .menuitem{
        margin-bottom: 5px;
        background-color: rgba(0,0,0,.03);
        border: 1px solid rgba(0,0,0,.125);
    }
    div.name-menuitem{
        cursor: pointer;
        display: block;
        flex: 1 1 1px;
    }
    div.chevron-buttons{
        cursor: pointer;
        font-size: 8px;
        display: block;
        flex: 0 0 10px;
    }
    input.chevron{
        border: none;
        cursor: pointer;
        font-size: 7px;
        font-weight: bolder;
        color: transparent;
    }

    input.chevron-up{
        background: url(/assets/images/chevron-up.png);
        background-position: center;
        background-repeat: no-repeat;
    }

    input.chevron-down{
        background: url(/assets/images/chevron-down.png);
        background-position: center;
        background-repeat: no-repeat;
    }
</style>

<h1>Управління пунктами меню</h1>

<div class="row">
    <div class='col-lg-5'>

        {{ Form::open(array('route' => 'menuitems.index')) }}
        <!--form action=""-->

        <div class="menus card">
            <div class="card-header">Оберіть меню</div>
            <div class="card-block">
                    <div class="form-group">
                        {{ Form::select('id_menu', $menus, null, ['id'=>'selector', 'class' => 'form-control' . ($errors->has('id_menu') ? ' form-control-danger' : '')]) }}
                    </div>
            </div>
        </div>

        <div class="menuitem-add card">
            <div class="card-header">Додати новий пункт</div>
            <div class="card-block">
                    <div class="form-group">
                        {{ Form::label('name_menuitem', 'Назва') }}
                        {{ Form::text('name_menuitem', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('parent_menuitem', 'Батьківський пункт') }}
                        {{ Form::select('parent_menuitem', $menuitems, null, ['class' => 'form-control' . ($errors->has('parent_menuitem') ? ' form-control-danger' : '')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('url_menuitem', 'Назва посилання (route)') }}
                        {{ Form::text('url_menuitem', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('paramurl_menuitem', 'Параметр посилання') }}
                        {{ Form::text('paramurl_menuitem', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('icon_menuitem', 'Клас зображення') }}
                        {{ Form::text('icon_menuitem', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="text-right">
                        {{ Form::submit('Додати', array('class' => 'btn btn-primary')) }}
                    </div>
            </div>
        </div>

    {{ Form::close() }}

    </div>


    <div class="col-lg-7">
        @if($treemenuitems)
            <div class="card">
                <div class="card-header">Пункти меню</div>
                <div class="card-block">
                    @foreach($treemenuitems as $menuitem)
                        <div class="card menuitem">
                            <div class="card-header menuitem-title">
                                <div class="name-menuitem">{{$menuitem->name_menuitem}} - ( {{$menuitem->sort_menuitem}} )</div>
                                <div class="chevron-buttons">
                                    {{ Form::model($menuitem, ['route' => ['menuitems.update', $menuitem->id_menuitem, 'action=up'], 'method' => 'PUT']) }}
                                        {!! Form::submit('&#8743;', ['class' => 'chevron chevron-up']) !!}
                                    {!! Form::close() !!}
                                    {{ Form::model($menuitem, ['route' => ['menuitems.update', $menuitem->id_menuitem, 'action=down'], 'method' => 'PUT']) }}
                                        {!! Form::submit('&#8744;', ['class' => 'chevron chevron-down']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="card-block menuitem-content" style="display: none;">

                                {{ Form::model($menuitem, ['route' => ['menuitems.update', $menuitem->id_menuitem], 'method' => 'PUT']) }}
                                <div class="form-group">
                                    {{ Form::label('name_menuitem', 'Назва') }}
                                    {{ Form::text('name_menuitem', $menuitem->name_menuitem, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('parent_menuitem', 'Батьківський пункт') }}
                                    {{ Form::select('parent_menuitem', $menuitems, $menuitem->parent_menuitem, ['class' => 'form-control' . ($errors->has('parent_menuitem') ? ' form-control-danger' : '')]) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('url_menuitem', 'Назва посилання (route)') }}
                                    {{ Form::text('url_menuitem', $menuitem->url_menuitem, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('paramurl_menuitem', 'Параметр посилання') }}
                                    {{ Form::text('paramurl_menuitem', $menuitem->paramurl_menuitem, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('icon_menuitem', 'Клас зображення') }}
                                    {{ Form::text('icon_menuitem', $menuitem->icon_menuitem, array('class' => 'form-control')) }}
                                </div>
                                <div class="text-right">
                                    {{ Form::submit('Зберегти', array('class' => 'btn btn-primary', 'style' => 'float:left;')) }}
                                </div>
                                {!! Form::close() !!}

                                {!! Form::open(['style' => 'display: inline-block;float:right;', 'method' => 'DELETE', 'route' => ['menuitems.destroy', $menuitem->id_menuitem] ]) !!}
                                    {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </div>
                        </div>
                        <div style="margin-left: 30px;">                            
                            @if($menuitem->sub)
                            @foreach($menuitem->sub as $childmenuitem)
                                <div class="card menuitem">
                                    <div class="card-header menuitem-title">
                                        <div class="name-menuitem">{{$childmenuitem->name_menuitem}} - ( {{$childmenuitem->sort_menuitem}} )</div>
                                        <div class="chevron-buttons">
                                            {{ Form::model($childmenuitem, ['route' => ['menuitems.update', $childmenuitem->id_menuitem, 'action=up'], 'method' => 'PUT']) }}
                                                {!! Form::submit('&#8743;', ['class' => 'chevron chevron-up']) !!}
                                            {!! Form::close() !!}
                                            {{ Form::model($childmenuitem, ['route' => ['menuitems.update', $childmenuitem->id_menuitem, 'action=down'], 'method' => 'PUT']) }}
                                                {!! Form::submit('&#8744;', ['class' => 'chevron chevron-down']) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                    <div class="card-block menuitem-content" style="display: none;">
                                        {{ Form::model($childmenuitem, ['route' => ['menuitems.update', $childmenuitem->id_menuitem], 'method' => 'PUT']) }}
                                        <div class="form-group">
                                            {{ Form::label('name_menuitem', 'Назва') }}
                                            {{ Form::text('name_menuitem', $childmenuitem->name_menuitem, array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('parent_menuitem', 'Батьківський пункт') }}
                                            {{ Form::select('parent_menuitem', $menuitems, $childmenuitem->parent_menuitem, ['class' => 'form-control' . ($errors->has('parent_menuitem') ? ' form-control-danger' : '')]) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('url_menuitem', 'Назва посилання (route)') }}
                                            {{ Form::text('url_menuitem', $childmenuitem->url_menuitem, array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('paramurl_menuitem', 'Параметр посилання') }}
                                            {{ Form::text('paramurl_menuitem', $childmenuitem->paramurl_menuitem, array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('icon_menuitem', 'Клас зображення') }}
                                            {{ Form::text('icon_menuitem', $childmenuitem->icon_menuitem, array('class' => 'form-control')) }}
                                        </div>
                                        <div class="text-right">
                                            {{ Form::submit('Зберегти', array('class' => 'btn btn-primary', 'style' => 'float:left;')) }}
                                        </div>
                                        {!! Form::close() !!}

                                        {!! Form::open(['style' => 'display: inline-block;float:right;', 'method' => 'DELETE', 'route' => ['menuitems.destroy', $childmenuitem->id_menuitem] ]) !!}
                                            {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <div style="margin-left: 30px;">
                                    @if($childmenuitem->sub)
                                    @foreach($childmenuitem->sub as $grandchildmenuitem)
                                        <div class="card menuitem">
                                            <div class="card-header menuitem-title">
                                                <div class="name-menuitem">{{$grandchildmenuitem->name_menuitem}} - ( {{$grandchildmenuitem->sort_menuitem}} )</div>
                                                <div class="chevron-buttons">
                                                    {{ Form::model($grandchildmenuitem, ['route' => ['menuitems.update', $grandchildmenuitem->id_menuitem, 'action=up'], 'method' => 'PUT']) }}
                                                        {!! Form::submit('&#8743;', ['class' => 'chevron chevron-up']) !!}
                                                    {!! Form::close() !!}
                                                    {{ Form::model($grandchildmenuitem, ['route' => ['menuitems.update', $grandchildmenuitem->id_menuitem, 'action=down'], 'method' => 'PUT']) }}
                                                        {!! Form::submit('&#8744;', ['class' => 'chevron chevron-down']) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                            <div class="card-block menuitem-content" style="display: none;">
                                                {{ Form::model($grandchildmenuitem, ['route' => ['menuitems.update', $grandchildmenuitem->id_menuitem], 'method' => 'PUT']) }}
                                                <div class="form-group">
                                                    {{ Form::label('name_menuitem', 'Назва') }}
                                                    {{ Form::text('name_menuitem', $grandchildmenuitem->name_menuitem, array('class' => 'form-control')) }}
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('parent_menuitem', 'Батьківський пункт') }}
                                                    {{ Form::select('parent_menuitem', $menuitems, $grandchildmenuitem->parent_menuitem, ['class' => 'form-control' . ($errors->has('parent_menuitem') ? ' form-control-danger' : '')]) }}
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('url_menuitem', 'Назва посилання (route)') }}
                                                    {{ Form::text('url_menuitem', $grandchildmenuitem->url_menuitem, array('class' => 'form-control')) }}
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('paramurl_menuitem', 'Параметр посилання') }}
                                                    {{ Form::text('paramurl_menuitem', $grandchildmenuitem->paramurl_menuitem, array('class' => 'form-control')) }}
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('icon_menuitem', 'Клас зображення') }}
                                                    {{ Form::text('icon_menuitem', $grandchildmenuitem->icon_menuitem, array('class' => 'form-control')) }}
                                                </div>
                                                <div class="text-right">
                                                    {{ Form::submit('Зберегти', array('class' => 'btn btn-primary', 'style' => 'float:left;')) }}
                                                </div>
                                                {!! Form::close() !!}

                                                {!! Form::open(['style' => 'display: inline-block;float:right;', 'method' => 'DELETE', 'route' => ['menuitems.destroy', $grandchildmenuitem->id_menuitem] ]) !!}
                                                    {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    @endforeach
                                    @endif
                                </div>
                            @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>

</div>


@endsection
