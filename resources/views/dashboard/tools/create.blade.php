@extends('layouts.admin')

@section('title')
    <title>Додати інструмент</title>
@endsection

@section('content')


<h1>Додати інструмент</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::open(['route' => 'tools.index']) }}

        <div class="form-group">
            {{ Form::label('name_tool', 'Назва інструменту') }}
            {{ Form::text('name_tool', null, ['class' => 'form-control' . ($errors->has('name_tool') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_tool'))
                @foreach ($errors->get('name_tool') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection