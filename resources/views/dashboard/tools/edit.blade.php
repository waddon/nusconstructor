@extends('layouts.admin')

@section('title')
    <title>Редагування інструменту</title>
@endsection

@section('content')

<h1>Редагування {{$tool->name_tool}}</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::model($tool, ['route' => ['tools.update', $tool->id_tool], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_tool', 'Назва інструменту') }}
            {{ Form::text('name_tool', null, ['class' => 'form-control' . ($errors->has('name_tool') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_tool'))
                @foreach ($errors->get('name_tool') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection