@extends('layouts.admin')

@section('title')
    <title>Інструменти оцінювання</title>
@endsection

@section('content')

    <h1>Управління Інструментами оцінювання</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('tools.create') }}" class="btn btn-success">Додати інструмент</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Інструмент оцінювання</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($tools as $tool)
            <tr>
                <td>{{ $tool->id_tool }}</td>
                <td>{{ $tool->name_tool }}</td>
                <td>
                    <a href="{{ route('tools.edit', $tool->id_tool) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['tools.destroy', $tool->id_tool] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection