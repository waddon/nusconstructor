@extends('layouts.admin')

@section('title')
    <title>Редагування запису</title>
@endsection

@section('content')

<h1>Редагування {{$post->name_post}}</h1>

<div class='col-lg-12'>

    {{ Form::model($post, ['route' => ['posts.update', $post->id_post], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_post', 'Назва запису') }}
            {{ Form::text('name_post', null, ['class' => 'form-control' . ($errors->has('name_post') ? ' is-invalid' : '')]) }}

            {{-- <input type="text" name="name_post"> --}}

            @if($errors->has('name_post'))
                @foreach ($errors->get('name_post') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('slug_post', 'Вiдносний URL') }}
            {{ Form::text('slug_post', null, ['class' => 'form-control' . ($errors->has('slug_post') ? ' is-invalid' : '')]) }}

            @if($errors->has('slug_post'))
                @foreach ($errors->get('slug_post') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_cat', 'Категория', ['class' => 'form-control-label']) }}
            {{ Form::select('id_cat', $cats, null, ['class' => 'form-control' . ($errors->has('id_cat') ? ' form-control-danger' : '')]) }}

            @if($errors->has('id_cat'))
                @foreach ($errors->get('id_cat') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('excerpt_post', 'Уривок') }}
            {{ Form::textarea('excerpt_post', null, ['class' => 'form-control' . ($errors->has('excerpt_post') ? ' is-invalid' : '')]) }}

            @if($errors->has('excerpt_post'))
                @foreach ($errors->get('excerpt_post') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('content_post', 'Контент') }}
            {{ Form::textarea('content_post', null, ['class' => 'form-control tinymce-val' . ($errors->has('content_post') ? ' is-invalid' : '')]) }}

            @if($errors->has('content_post'))
                @foreach ($errors->get('content_post') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>
        
        <div class="form-group">
            <div class="form-check">
                 <label class="form-check-label">
                    {{-- <input class="form-check-input" type="checkbox" value="1" name="download_post"> --}}
                    {{ Form::checkbox('download_post', 1, $post->download_post ? : false, ['class' => 'form-check-input' . ($errors->has('content_post') ? ' is-invalid' : '')]) }}
                    Завантажувальний запис
                </label>
            </div>
        </div>

        <div class="form-group">
            <label for="file_or_url">Файл або URL</label>
            <div class="input-group">
                <input type="text" id="file_or_url" class="form-control" name="link_post">
                <span class="input-group-btn">
                    <button data-toggle="modal" href="javascript:;" data-target="#files" class="btn btn-primary" type="button">Обрати файл</button>
                </span>
            </div>
        </div>
        
        Тип запису

        <div class="form-group">
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="thumb_post" value="assets/images/material-text.png"> Текст
                    {{-- {{ Form::radio('thumb_post', 1, old($post->), ['class' => 'form-check-input' . ($errors->has('content_post') ? ' is-invalid' : '')]) }} --}}
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="thumb_post" value="assets/images/material-doc.png"> Документ
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="thumb_post" value="assets/images/material-link.png"> Посилання
                </label>
            </div>
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection