@extends('layouts.admin')

@section('title')
    <title>Записами</title>
@endsection

@section('content')

    <h1>Управління записами</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('posts.create') }}" class="btn btn-success">Додати запис</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Назва запису</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($posts as $post)
            <tr>
                <td>{{ $post->id_post }}</td>
                <td>{{ $post->name_post }}</td>
                <td>
                    <a href="{{ route('posts.edit', $post->id_post) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['posts.destroy', $post->id_post] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection