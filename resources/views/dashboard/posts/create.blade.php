@extends('layouts.admin')

@section('title')
    <title>Додати запис</title>
@endsection

@section('content')

<h1>Додати запис</h1>

<div class='col-lg-12'>

    {{ Form::open(['route' => 'posts.index', 'files' => true]) }}
            
        {{-- <div class="form-group">
            <div class="input-group">
                <input id="field_id" type="text" class="form-control" name="thumbs_post">
                <span class="input-group-btn">
                    <button data-toggle="modal" href="javascript:;" data-target="#images" class="btn btn-primary" type="button">Обрати зображення</button>
                </span>
            </div>
        </div> --}}

        <div class="form-group">
            {{ Form::label('name_post', 'Назва запису') }}
            {{ Form::text('name_post', null, ['class' => 'form-control' . ($errors->has('name_post') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_post'))
                @foreach ($errors->get('name_post') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('slug_post', 'Вiдносний URL') }}
            {{ Form::text('slug_post', null, ['class' => 'form-control' . ($errors->has('slug_post') ? ' is-invalid' : '')]) }}

            @if($errors->has('slug_post'))
                @foreach ($errors->get('slug_post') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_cat', 'Категория', ['class' => 'form-control-label']) }}
            {{ Form::select('id_cat', $cats, null, ['class' => 'form-control' . ($errors->has('id_cat') ? ' form-control-danger' : '')]) }}

            @if($errors->has('id_cat'))
                @foreach ($errors->get('id_cat') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('excerpt_post', 'Вiдривок') }}
            {{ Form::textarea('excerpt_post', null, ['class' => 'form-control' . ($errors->has('excerpt_post') ? ' is-invalid' : '')]) }}

            @if($errors->has('excerpt_post'))
                @foreach ($errors->get('excerpt_post') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('content_post', 'Контент') }}
            {{ Form::textarea('content_post', null, ['class' => 'form-control tinymce-val' . ($errors->has('content_post') ? ' is-invalid' : '')]) }}

            @if($errors->has('content_post'))
                @foreach ($errors->get('content_post') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>
        
        <div class="form-group">
            <div class="form-check">
                 <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" value="1" name="download_post">
                    Завантажувальний запис
                </label>
            </div>
        </div>

        <div class="form-group">
            <label for="file_or_url">Файл або URL</label>
            <div class="input-group">
                <input id="file_or_url" type="text" class="form-control" name="link_post">
                <span class="input-group-btn">
                    <button data-toggle="modal" href="javascript:;" data-target="#files" class="btn btn-primary" type="button">Обрати файл</button>
                </span>
            </div>
        </div>
        
        Тип запису

        <div class="form-group">
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="thumb_post" value="assets/images/material-text.png"> Текст
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="thumb_post" value="assets/images/material-doc.png"> Документ
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="thumb_post" value="assets/images/material-link.png"> Посилання
                </label>
            </div>
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

    {{-- Modal window for images --}}

    {{-- <div id="images" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <iframe width="800" height="400" src="/filemanager/dialog.php?type=1&field_id=field_id&relative_url=1&akey=s2yrxre2XExpkm4fcDtGLdaFVjWXScnaYtnGULh2efuE5u9DtXggmmjjQVKbrcY3" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
            </div>
        </div>
    </div> --}}

    {{-- End Modal window for images --}}

    {{-- Modal window for files --}}

    <div id="files" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <iframe width="800" height="400" src="/filemanager/dialog.php?type=2&field_id=file_or_url&relative_url=1&akey=s2yrxre2XExpkm4fcDtGLdaFVjWXScnaYtnGULh2efuE5u9DtXggmmjjQVKbrcY3" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
            </div>
        </div>
    </div>

    {{-- End Modal window for files --}}

</div>

@endsection