@extends('layouts.admin')

@section('title')
    <title>Додати КОР</title>
@endsection

@section('content')

<div class='col-lg-4 col-lg-offset-4'>
    
    <h1>Додати КОР</h1>

    {{ Form::open(['route' => 'specific-outcomes.index']) }}

        <div class="form-group">
            {{ Form::label('specific_outcome_name', 'Назва') }}
            {{ Form::text('specific_outcome_name', null, ['class' => 'form-control' . ($errors->has('specific_outcome_name') ? ' is-invalid' : '')]) }}

            @if($errors->has('specific_outcome_name'))
                @foreach ($errors->get('specific_outcome_name') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('specific_outcome_key', 'Код КОРа') }}
            {{ Form::text('specific_outcome_key', null, ['class' => 'form-control' . ($errors->has('specific_outcome_key') ? ' is-invalid' : '')]) }}

            @if($errors->has('specific_outcome_key'))
                @foreach ($errors->get('specific_outcome_key') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('expected_outcome_id', 'ЗОР', ['class' => 'form-control-label']) }}
            {{ Form::select('expected_outcome_id', $expected_outcomes, 0, ['class' => 'form-control' . ($errors->has('expected_outcome_id') ? 'is-invalid' : '')]) }}

            @if($errors->has('expected_outcome_id'))
                @foreach ($errors->get('expected_outcome_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('content_line_id', 'Змістова лінія', ['class' => 'form-control-label']) }}
            {{ Form::select('content_line_id', $content_lines, 0, ['class' => 'form-control' . ($errors->has('content_line_id') ? 'is-invalid' : '')]) }}

            @if($errors->has('content_line_id'))
                @foreach ($errors->get('content_line_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection