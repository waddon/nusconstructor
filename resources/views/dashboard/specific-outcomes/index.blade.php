@extends('layouts.admin')

@section('title')
    <title>КОРи</title>
@endsection

@section('content')

    <h1>КОРи</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('specific-outcomes.create') }}" class="btn btn-success">Додати КОР</a>
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>КОР</th>
                <th>Зор</th>
                <th>Змістова лінія</th>
                <th>Код</th>
                <th>Операції</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($specific_outcomes as $specific_outcome)
                {{-- {{ dump($specific_outcome->getBranchByContentLine()) }} --}}
                @if(Auth::user()->hasAnyPermission($specific_outcome->getBranchByContentLine() ? $specific_outcome->getBranchByContentLine() : 'dashboard', 'edit_content'))
                    <tr>
                        <td>{{ $specific_outcome->name_kor }}</td>
                        <td>{{ $specific_outcome->getSpecificOutcomeExpectedOutcome() }}</td>
                        <td>{{ $specific_outcome->getSpecificOutcomeContentLine() }}</td>
                        <td>{{ $specific_outcome->key_kor }}</td>
                        <td>
                            <a href="{{ route('specific-outcomes.edit', $specific_outcome->id_kor) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>
                            {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['specific-outcomes.destroy', $specific_outcome->id_kor] ]) !!}
                                {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    {{ $specific_outcomes->links() }}

@endsection