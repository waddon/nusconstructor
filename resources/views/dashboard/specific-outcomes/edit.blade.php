@extends('layouts.admin')

@section('title')
    <title>Редагувати ЗОР</title>
@endsection

@section('content')

<h1>Редагувати "{{$specific_outcome->name_kor}}"</h1>

<div class='col-lg-4 col-lg-offset-4'>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    {{ Form::model($specific_outcome, ['route' => ['specific-outcomes.update', $specific_outcome->id_kor], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_kor', 'Назва категорії') }}
            {{ Form::text('name_kor', null, ['class' => 'form-control' . ($errors->has('name_kor') ? ' form-control-danger' : '')]) }}

            @if($errors->has('name_kor'))
                @foreach ($errors->get('name_kor') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('key_kor', 'Код') }}
            {{ Form::text('key_kor', null, ['class' => 'form-control' . ($errors->has('key_kor') ? ' form-control-danger' : '')]) }}

            @if($errors->has('key_kor'))
                @foreach ($errors->get('key_kor') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_zor', 'ЗОР', ['class' => 'form-control-label']) }}
            {{ Form::select('id_zor', $expected_outcomes, null, ['class' => 'form-control' . ($errors->has('id_zor') ? 'is-invalid' : '')]) }}

            @if($errors->has('id_zor'))
                @foreach ($errors->get('id_zor') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_zmist', 'Змістова лінія', ['class' => 'form-control-label']) }}
            {{ Form::select('id_zmist', $content_lines, null, ['class' => 'form-control' . ($errors->has('id_zmist') ? 'is-invalid' : '')]) }}

            @if($errors->has('id_zmist'))
                @foreach ($errors->get('id_zmist') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', ['class' => 'btn btn-success']) }}

    {{ Form::close() }}

</div>

@endsection