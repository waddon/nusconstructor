@extends('layouts.admin')

@section('title')
    <title>Редагування рівня</title>
@endsection

@section('content')

<h1>Редагування {{$level->name_riven}}</h1>

<div class='col-lg-12'>

    {{ Form::model($level, ['route' => ['levels.update', $level->id_riven], 'method' => 'PUT']) }}

     <div class="form-group">
        {{ Form::label('name_riven', 'Назва') }}
        {{ Form::text('name_riven', null, ['class' => 'form-control' . ($errors->has('name_riven') ? ' is-invalid' : '')]) }}

        @if($errors->has('name_riven'))
            @foreach ($errors->get('name_riven') as $message)
                <div class="form-control-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection