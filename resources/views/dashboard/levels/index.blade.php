@extends('layouts.admin')

@section('title')
    <title>Рівні</title>
@endsection

@section('content')

    <h1>Управління рівнями</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('levels.create') }}" class="btn btn-success">Додати рівень</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Назва</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($levels as $level)
            <tr>
                <td>{{ $level->name_riven }}</td>
                <td>
                    <a href="{{ route('levels.edit', $level->id_riven) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['levels.destroy', $level->id_riven] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection