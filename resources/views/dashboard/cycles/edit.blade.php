@extends('layouts.admin')

@section('title')
    <title>Редагування рівня</title>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h1>Редагування {{$cycle->name_cikl}}</h1>

<div class='col-lg-12'>

    {{ Form::model($cycle, ['route' => ['cycles.update', $cycle->id_cikl], 'method' => 'PUT']) }}

    <div class="form-group">
        {{ Form::label('name_cikl', 'Назва') }}
        {{ Form::text('name_cikl', null, ['class' => 'form-control' . ($errors->has('name_cikl') ? ' is-invalid' : '')]) }}

        @if($errors->has('name_cikl'))
            @foreach ($errors->get('name_cikl') as $message)
                <div class="form-control-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    <div class="form-group {{ $errors->has('marking_cikl') ? 'has-danger' : '' }}">
            {{ Form::label('marking_cikl', 'Маркування') }}
            {{ Form::text('marking_cikl', null, ['class' => 'form-control' . ($errors->has('marking_cikl') ? ' is-invalid' : '')]) }}

            @if($errors->has('marking_cikl'))
                @foreach ($errors->get('marking_cikl') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group {{ $errors->has('id_riven') ? 'has-danger' : '' }}">
            {{ Form::label('id_riven', 'Рівень', ['class' => 'form-control-label']) }}
            {{ Form::select('id_riven', $levels, null, ['class' => 'form-control' . ($errors->has('id_riven') ? ' form-control-danger' : '')]) }}

            @if($errors->has('id_riven'))
                @foreach ($errors->get('id_riven') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection