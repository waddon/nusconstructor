@extends('layouts.admin')

@section('title')
    <title>Цикли</title>
@endsection

@section('content')

    <h1>Управління циклами</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('cycles.create') }}" class="btn btn-success">Додати цикл</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Назва</th>
                <th>Маркування</th>
                <th>Рівень</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($cycles as $cycle)
            <tr>
                <td>{{ $cycle->name_cikl }}</td>
                <td>{{ $cycle->marking_cikl }}</td>
                <td>{{ $cycle->getCycleLevel() }}</td>
                <td>
                    <a href="{{ route('cycles.edit', $cycle->id_cikl) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['cycles.destroy', $cycle->id_cikl] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection