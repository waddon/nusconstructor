@extends('layouts.admin')

@section('title')
    <title>Додати цикл</title>
@endsection

@section('content')


<h1>Додати цикл</h1>

<div class='col-lg-12'>

    {{ Form::open(array('route' => 'cycles.index')) }}

        <div class="form-group">
            {{ Form::label('cycle_name', 'Назва') }}
            {{ Form::text('cycle_name', null, array('class' => 'form-control')) }}

            @if($errors->has('cycle_name'))
                @foreach ($errors->get('cycle_name') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('cycle_label', 'Маркування') }}
            {{ Form::text('cycle_label', null, array('class' => 'form-control')) }}

            @if($errors->has('cycle_label'))
                @foreach ($errors->get('cycle_label') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('level_id', 'Рівень', ['class' => 'form-control-label']) }}
            {{ Form::select('level_id', $levels, 0, ['class' => 'form-control' . ($errors->has('level_id') ? ' form-control-danger' : '')]) }}

            @if($errors->has('level_id'))
                @foreach ($errors->get('level_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection