@extends('layouts.admin')

@section('title')
    <title>Редагування назви предмету</title>
@endsection

@section('content')

<h1>Редагування {{$subject->id_subject}}</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::model($subject, ['route' => ['subjects.update', $subject->id_subject], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_subject', 'Назву предмету') }}
            {{ Form::text('name_subject', null, ['class' => 'form-control' . ($errors->has('name_subject') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_subject'))
                @foreach ($errors->get('v') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_galuz', 'Цикл', ['class' => 'form-control-label']) }}
            {{ Form::select('id_galuz', $branches, null, ['class' => 'form-control' . ($errors->has('id_galuz') ? ' form-control-danger' : '')]) }}

            @if($errors->has('id_galuz'))
                @foreach ($errors->get('id_galuz') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection