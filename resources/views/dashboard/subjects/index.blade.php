@extends('layouts.admin')

@section('title')
    <title>Назви предметів</title>
@endsection

@section('content')

    <h1>Управління назвами предметів</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('subjects.create') }}" class="btn btn-success">Додати назву предмету</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Назва предмету</th>
                <th>Галузь</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($subjects as $subject)
                @if(Auth::user()->hasAnyPermission($subject->getSubjectBranch()->marking_galuz, 'edit_content'))
                    <tr>
                        <td>{{ $subject->id_subject }}</td>
                        <td>{{ $subject->name_subject }}</td>
                        <td>{{ $subject->getSubjectBranch()->name_galuz }}</td>
                        <td>
                            <a href="{{ route('subjects.edit', $subject->id_subject) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                            {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['subjects.destroy', $subject->id_subject] ]) !!}
                                {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

@endsection