@extends('layouts.admin')

@section('title')
    <title>Додати назву предмету</title>
@endsection

@section('content')


<h1>Додати назву предмету</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::open(['route' => 'subjects.index']) }}

        <div class="form-group">
            {{ Form::label('name_subject', 'Назву предмету') }}
            {{ Form::text('name_subject', null, ['class' => 'form-control' . ($errors->has('name_subject') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_subject'))
                @foreach ($errors->get('name_subject') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('branch_id', 'Галузь', ['class' => 'form-control-label']) }}
            {{ Form::select('branch_id', $branches, null, ['class' => 'form-control' . ($errors->has('branch_id') ? ' is-invalid' : '')]) }}

            @if($errors->has('branch_id'))
                @foreach ($errors->get('branch_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection