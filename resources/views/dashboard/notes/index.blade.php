@extends('layouts.admin')

@section('title')
    <title>Описи освітніх програм циклів</title>
@endsection

@section('content')

    <h1>Управління описами освітніх програм циклу</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('notes.create') }}" class="btn btn-success">Додати опис</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Цикл</th>
                <th>Текст</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($notes as $note)
            <tr>
                <td>{{ $note->id_note }}</td>
                <td>{{ $note->getNoteCycle() }}</td>
                <td>{!! str_limit($note->text_note, 120) !!}</td>
                <td>
                    <a href="{{ route('notes.edit', $note->id_note) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['notes.destroy', $note->id_note] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection