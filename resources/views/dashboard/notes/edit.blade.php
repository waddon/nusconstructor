@extends('layouts.admin')

@section('title')
    <title>Редагування рівня</title>
@endsection

@section('content')

<h1>Редагування {{$note->id_note}}</h1>

<div class='col-lg-12'>

    {{ Form::model($note, ['route' => ['notes.update', $note->id_note], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('text_note', 'Назва') }}
            {{ Form::textarea('text_note', null, ['class' => 'form-control tinymce-val' . ($errors->has('text_note') ? ' is-invalid' : '')]) }}

            @if($errors->has('text_note'))
                @foreach ($errors->get('text_note') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_cikl', 'Цикл', ['class' => 'form-control-label']) }}
            {{ Form::select('id_cikl', $cycles, null, ['class' => 'form-control' . ($errors->has('id_cikl') ? ' form-control-danger' : '')]) }}

            @if($errors->has('id_cikl'))
                @foreach ($errors->get('id_cikl') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection