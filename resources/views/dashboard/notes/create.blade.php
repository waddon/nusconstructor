@extends('layouts.admin')

@section('title')
    <title>Додати опис</title>
@endsection

@section('content')


<h1>Додати опис</h1>

<div class='col-lg-12'>

    {{ Form::open(['route' => 'notes.index']) }}

        <div class="form-group">
            {{ Form::label('text_note', 'Назва') }}
            {{ Form::textarea('text_note', null, ['class' => 'form-control tinymce-val' . ($errors->has('text_note') ? ' is-invalid' : '')]) }}

            @if($errors->has('text_note'))
                @foreach ($errors->get('text_note') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('cycle_id', 'Цикл', ['class' => 'form-control-label']) }}
            {{ Form::select('cycle_id', $cycles, null, ['class' => 'form-control' . ($errors->has('cycle_id') ? ' form-control-danger' : '')]) }}

            @if($errors->has('cycle_id'))
                @foreach ($errors->get('cycle_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection