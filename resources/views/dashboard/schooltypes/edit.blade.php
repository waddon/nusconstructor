@extends('layouts.admin')

@section('title')
    <title>Редагування типа школи</title>
@endsection

@section('content')

<h1>Редагування {{$schooltype->name_schooltype}}</h1>

<div class='col-lg-12'>

    {{ Form::model($schooltype, ['route' => ['schooltypes.update', $schooltype->id_schooltype], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_schooltype', 'Назва типу школ') }}
            {{ Form::text('name_schooltype', null, ['class' => 'form-control' . ($errors->has('name_schooltype') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_schooltype'))
                @foreach ($errors->get('name_schooltype') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection