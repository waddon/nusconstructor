@extends('layouts.admin')

@section('title')
    <title>Типи школ</title>
@endsection

@section('content')

    <h1>Управління типами школ</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('schooltypes.create') }}" class="btn btn-success">Додати Тип</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Назва типу школи</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($schooltypes as $schooltype)
            <tr>
                <td>{{ $schooltype->id_schooltype }}</td>
                <td>{{ $schooltype->name_schooltype }}</td>
                <td>
                    <a href="{{ route('schooltypes.edit', $schooltype->id_schooltype) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['schooltypes.destroy', $schooltype->id_schooltype] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection