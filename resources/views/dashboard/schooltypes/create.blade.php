@extends('layouts.admin')

@section('title')
    <title>Додати тип школ</title>
@endsection

@section('content')

<h1>Додати тип школ</h1>

<div class='col-lg-12'>

    {{ Form::open(['route' => 'schooltypes.index']) }}

        <div class="form-group">
            {{ Form::label('name_schooltype', 'Назва типа школ') }}
            {{ Form::text('name_schooltype', null, ['class' => 'form-control' . ($errors->has('name_schooltype') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_schooltype'))
                @foreach ($errors->get('name_schooltype') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection