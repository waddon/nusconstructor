@extends('layouts.admin')

@section('title')
    <title>Редагування школи</title>
@endsection

@section('content')

<h1>Редагування {{$school->name_school}}</h1>

<div class='col-lg-12'>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{ Form::model($school, ['route' => ['schools.update', $school->id_school], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_school', 'Назва школи') }}
            {{ Form::text('name_school', null, ['class' => 'form-control' . ($errors->has('name_school') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_school'))
                @foreach ($errors->get('name_school') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('region_school', 'Тип школи', ['class' => 'form-control-label']) }}
            {{ Form::select('region_school', $school_regions, null, ['class' => 'form-control' . ($errors->has('region_school') ? ' is-invalid' : '')]) }}

            @if($errors->has('region_school'))
                @foreach ($errors->get('region_school') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_schooltype', 'Тип школи', ['class' => 'form-control-label']) }}
            {{ Form::select('id_schooltype', $school_types, null, ['class' => 'form-control' . ($errors->has('id_schooltype') ? ' is-invalid' : '')]) }}

            @if($errors->has('id_schooltype'))
                @foreach ($errors->get('id_schooltype') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('supervisor_school', 'ID Директора') }}
            {{ Form::text('supervisor_school', null, ['class' => 'form-control' . ($errors->has('supervisor_school') ? ' is-invalid' : '')]) }}

            @if($errors->has('supervisor_school'))
                @foreach ($errors->get('supervisor_school') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('adress_school', 'Адреса') }}
            {{ Form::text('adress_school', null, ['class' => 'form-control' . ($errors->has('adress_school') ? ' is-invalid' : '')]) }}

            @if($errors->has('adress_school'))
                @foreach ($errors->get('adress_school') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection