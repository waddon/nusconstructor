@extends('layouts.admin')

@section('title')
    <title>Школи</title>
@endsection

@section('content')

    <h1>Управління школами {{ isset($schools) ? $schools[0]->region_school : false}}</h1>

    <div class="col-lg-3">
        {{ Form::open(['route' => 'schools.filter', 'method' => 'post']) }}

            <div class="form-group">
                <select name="school_region" class="form-control">
                    @foreach( $school_regions as $school_region )
                        <option value="{{ $school_region->region_school }}">{{ $school_region->region_school }}</option>
                    @endforeach
                </select>
            </div>

            {{ Form::submit('Фильтр', ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 1rem;']) }}

        {{ Form::close() }}
    </div>

    <div class="col-lg-3">
        <div class="form-group">
            <input type="text" id="search" class="form-control" placeholder="Пошук">
        </div>
        <div class="form-group">
            <a href="{{ route('schools.create') }}" class="btn btn-success">Додати школу</a>
        </div>
    </div>

    @if (isset($schools))
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th style="width: 250px">Назва Школи</th>
                    <th>Адреса</th>
                    <th>Область</th>
                    <th>Операція</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($schools as $school)
                <tr>
                    <td>{{ $school->id_school }}</td>
                    <td>{{ $school->name_school }}</td>
                    <td>{{ $school->adress_school }}</td>
                    <td>{{ $school->region_school }}</td>
                    <td>
                        <a href="{{ route('schools.edit', $school->id_school) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                        {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['schools.destroy', $school->id_school] ]) !!}
                            {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    <script type="text/javascript">
        
    </script>

@endsection