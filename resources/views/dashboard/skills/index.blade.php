@extends('layouts.admin')

@section('title')
    <title>Уміння</title>
@endsection

@section('content')

    <h1>Управління уміннями</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('skills.create') }}" class="btn btn-success">Додати уміння</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Назва</th>
                <th>Код уміння</th>
                <th>Галузь</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($skills as $skill)
                @if(Auth::user()->hasAnyPermission($skill->getSkillBranch()->marking_galuz, 'edit_content'))
                    <tr>
                        <td>{{ $skill->name_umenie }}</td>
                        <td>{{ $skill->key_zor }}</td>
                        <td>{{ $skill->getSkillBranch()->name_galuz }}</td>
                        <td>
                            <a href="{{ route('skills.edit', $skill->id_umenie) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                            {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['skills.destroy', $skill->id_umenie] ]) !!}
                                {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

@endsection