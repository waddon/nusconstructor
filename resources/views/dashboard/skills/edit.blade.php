@extends('layouts.admin')

@section('title')
    <title>Редагування рівня</title>
@endsection

@section('content')

<h1>Редагування {{$skill->name_umenie}}</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::model($skill, ['route' => ['skills.update', $skill->id_umenie], 'method' => 'PUT']) }}

    <div class="form-group">
            {{ Form::label('name_umenie', 'Назва') }}
            {{ Form::text('name_umenie', null, ['class' => 'form-control' . ($errors->has('name_umenie') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_umenie'))
                @foreach ($errors->get('name_umenie') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('key_zor', 'Код') }}
            {{ Form::text('key_zor', null, ['class' => 'form-control' . ($errors->has('key_zor') ? ' is-invalid' : '')]) }}

            @if($errors->has('key_zor'))
                @foreach ($errors->get('key_zor') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_galuz', 'Галузь', ['class' => 'form-control-label']) }}
            {{ Form::select('id_galuz', $branches, null, ['class' => 'form-control' . ($errors->has('id_galuz') ? ' form-control-danger' : '')]) }}

            @if($errors->has('id_galuz'))
                @foreach ($errors->get('id_galuz') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection