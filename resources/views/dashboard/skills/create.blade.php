@extends('layouts.admin')

@section('title')
    <title>Додати уміння</title>
@endsection

@section('content')


<h1>Додати уміння</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::open(['route' => 'skills.index']) }}

        <div class="form-group">
            {{ Form::label('skill_name', 'Назва') }}
            {{ Form::text('skill_name', null, ['class' => 'form-control' . ($errors->has('skill_name') ? ' is-invalid' : '')]) }}

            @if($errors->has('skill_name'))
                @foreach ($errors->get('skill_name') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('skill_key', 'Код') }}
            {{ Form::text('skill_key', null, ['class' => 'form-control' . ($errors->has('skill_key') ? ' is-invalid' : '')]) }}

            @if($errors->has('skill_key'))
                @foreach ($errors->get('skill_key') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group {{ $errors->has('branch_id') ? 'has-danger' : '' }}">
            {{ Form::label('branch_id', 'Галузь', ['class' => 'form-control-label']) }}
            {{ Form::select('branch_id', $branches, null, ['class' => 'form-control' . ($errors->has('branch_id') ? ' form-control-danger' : '')]) }}

            @if($errors->has('branch_id'))
                @foreach ($errors->get('branch_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection