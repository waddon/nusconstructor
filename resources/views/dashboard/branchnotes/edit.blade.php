@extends('layouts.admin')

@section('title')
    <title>Редагування рівня</title>
@endsection

@section('content')

<h1>Редагування {{$branch_note->id_galuznote}}</h1>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class='col-lg-12'>

    {{ Form::model($branch_note, ['route' => ['branchnotes.update', $branch_note->id_galuznote], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('text_galuznote', 'Текст') }}
            {{ Form::textarea('text_galuznote', null, ['class' => 'form-control tinymce-val' . ($errors->has('text_galuznote') ? ' is-invalid' : '')]) }}

            @if($errors->has('text_galuznote'))
                @foreach ($errors->get('text_galuznote') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_galuz', 'Галузь', ['class' => 'form-control-label']) }}
            {{ Form::select('id_galuz', $branches, null, ['class' => 'form-control' . ($errors->has('id_galuz') ? ' form-control-danger' : '')]) }}

            @if($errors->has('id_galuz'))
                @foreach ($errors->get('id_galuz') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_cikl', 'Цикл', ['class' => 'form-control-label']) }}
            {{ Form::select('id_cikl', $cycles, null, ['class' => 'form-control' . ($errors->has('id_cikl') ? ' form-control-danger' : '')]) }}

            @if($errors->has('id_cikl'))
                @foreach ($errors->get('id_cikl') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection