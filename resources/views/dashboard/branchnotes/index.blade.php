@extends('layouts.admin')

@section('title')
    <title>Описи галузей</title>
@endsection

@section('content')

<h1>Управління описами галузей</h1>

<div class="col-lg-3">
    <div class="form-group">
        <a href="{{ route('branchnotes.create') }}" class="btn btn-success">Додати опис</a>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Цикл</th>
            <th>Галузь</th>
            <th>Текст</th>
            <th>Операція</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($branch_notes as $branch_note)
            @if(Auth::user()->hasAnyPermission($branch_note->getBranchNoteBranch()->marking_galuz, 'edit_content'))
                <tr>
                    <td>{{ $branch_note->id_galuznote }}</td>
                    <td>{{ $branch_note->getBranchNoteCycle() }}</td>
                    <td>{{ $branch_note->getBranchNoteBranch()->name_galuz }}</td>
                    <td>{!! str_limit($branch_note->text_galuznote, 120) !!}</td>
                    <td>
                        <a href="{{ route('branchnotes.edit', $branch_note->id_galuznote) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                        {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['branchnotes.destroy', $branch_note->id_galuznote] ]) !!}
                            {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endif
        @endforeach
    </tbody>
</table>

@endsection