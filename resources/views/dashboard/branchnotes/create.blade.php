@extends('layouts.admin')

@section('title')
    <title>Додати опис галузі</title>
@endsection

@section('content')

<h1>Додати опис галузі</h1>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class='col-lg-12'>

    {{ Form::open(['route' => 'branchnotes.index']) }}

        <div class="form-group">
            {{ Form::label('text_branchnote', 'Опис') }}
            {{ Form::textarea('text_branchnote', null, ['class' => 'form-control tinymce-val' . ($errors->has('text_branchnote') ? ' is-invalid' : '')]) }}

            @if($errors->has('text_branchnote'))
                @foreach ($errors->get('text_branchnote') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('branch_id', 'Галузь', ['class' => 'form-control-label']) }}
            {{ Form::select('branch_id', $branches, null, ['class' => 'form-control' . ($errors->has('branch_id') ? ' form-control-danger' : '')]) }}

            @if($errors->has('branch_id'))
                @foreach ($errors->get('branch_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('cycle_id', 'Цикл', ['class' => 'form-control-label']) }}
            {{ Form::select('cycle_id', $cycles, null, ['class' => 'form-control' . ($errors->has('cycle_id') ? ' form-control-danger' : '')]) }}

            @if($errors->has('cycle_id'))
                @foreach ($errors->get('cycle_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection