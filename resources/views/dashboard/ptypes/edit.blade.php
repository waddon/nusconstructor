@extends('layouts.admin')

@section('title')
    <title>Редагування типа плану\програми</title>
@endsection

@section('content')

<h1>Редагування {{$ptype->name_ptype}}</h1>

<div class='col-lg-12'>

    {{ Form::model($ptype, ['route' => ['ptypes.update', $ptype->id_ptype], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_ptype', 'Назва типу плану\програми') }}
            {{ Form::text('name_ptype', null, ['class' => 'form-control' . ($errors->has('name_ptype') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_ptype'))
                @foreach ($errors->get('name_ptype') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection