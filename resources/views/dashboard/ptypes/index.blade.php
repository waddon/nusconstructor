@extends('layouts.admin')

@section('title')
    <title>Типи планів\програм</title>
@endsection

@section('content')

    <h1>Управління типами планів\програм</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('ptypes.create') }}" class="btn btn-success">Додати інструмент</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Назва типу предмету</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($ptypes as $ptype)
            <tr>
                <td>{{ $ptype->id_ptype }}</td>
                <td>{{ $ptype->name_ptype }}</td>
                <td>
                    <a href="{{ route('ptypes.edit', $ptype->id_ptype) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['ptypes.destroy', $ptype->id_ptype] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection