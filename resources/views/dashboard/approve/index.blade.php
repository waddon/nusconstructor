@extends('layouts.admin')

@section('title')
    <title>Узгодження пакетів документів</title>
@endsection

@section('content')
    <style>
        .tinymce-val{
            width: 100%;
        }
        .form-group select, .form-group input[type="text"]{
            width: 100%;
        }
        .list{
            width: 100%;
        }
        .list thead{
            text-align: center;
        }
        .list td{
            border: 1px solid #f5f5f5;
        }
        .table-button{
            cursor: pointer;
        }
        table.check{
            width: 100%;
        }

        table.check td{
            border: 1px solid #eaeaea;
            padding: 5px 15px;
        }

        table.check thead td, table.check td:nth-child(2n), table.check td:nth-child(3){
            text-align: center;
        }

        table.check > thead  {
            background: #f3f3f3;
        }

        table.check > tbody > tr:nth-child(2n) {
            background: #fafafa;
        }
    </style>

    <h1>Пакети документів</h1>

    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                {{ Form::open(['route' => 'approve.filter', 'method' => 'post']) }}
                    <div class="form-group">
                        <select name="school_region" class="form-control">
                            @foreach( $school_regions as $school_region )
                                @if (Auth::user()->hasAnyPermission($school_region->region_school, 'edit_content'))
                                    <option value="{{ $school_region->region_school }}" @if($school_region->region_school==$current_school_regions){{'selected'}}@endif >{{ $school_region->region_school }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    {{ Form::submit('Фильтр', ['class' => 'btn btn-primary', 'style' => 'margin-bottom: 1rem;']) }}
                {{ Form::close() }}
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="text" id="search" class="form-control">
                </div>
            </div>
        </div>
    </div>

        <table class="school-package list">
            <thead>
                <tr>
                    <td>Код</td>
                    <td>Навчальний заклад</td>
                    <td>Назва</td>
                    <td>Планів</td>
                    <td>Створено</td>
                    <td>Змінено</td>
                    <!--td>Статус</td-->
                    <td class="action-buttons">Дії</td>
                </tr>
            </thead>
            <tbody class="school-package-body">
                @foreach ($packages as $package)
                    <tr data-id-package="{{$package->id_package}}">
                        <td>{{$package->id_package}}</td>
                        <td class="name-school">{{$package->name_school}}</td>
                        <td class="name-package">{{$package->name_package}}</td>
                        <td class="plancount-package">{{$package->plancount_package}}</td>
                        <td>{{ukr_date(date("d F Y", strtotime($package->created_at)))}}</td>
                        <td>{{ukr_date(date("d F Y", strtotime($package->updated_at)))}}</td>
                        <!--td class="name-status">{{$package->name_status}}</td-->
                        <td class="text-center">
                            <span class="btn btn-info edit-package table-button" data-id-package="{{$package->id_package}}" data-toggle="tooltip" title="Переглянути"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>


@section('dialogs')
    <div id="dialog-package" title="Пакет документів">
        <div class="dialog-content" style="margin-bottom: 5px;">
            <label for="">Назва</label>            
            <input type="text" id="name_package" readonly style="width: 100%; margin-bottom: 10px;">
        </div>

            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Візія і міссія</a></li>
                    <li><a href="#tabs-2">Прикріплені плани</a></li>
                    <li><a href="#tabs-3">Зауваження</a></li>
                </ul>
                <div id="tabs-1">
                    <!--textarea class="tinymce-val" name="" id="vam" rows="30"></textarea-->
                    <div class="vam"></div>
                </div>
                <div id="tabs-2">
                </div>
                <div id="tabs-3">
                    <textarea class="tinymce-val" name="" id="vam" rows="30"></textarea>
                    <!--div class="border-box" id="critical">Зауваження</div-->
                </div>
            </div>
        <!--/div-->
    </div>
    <div id="add-plan-to-package" title="Прикріпити план">
        <div class="dialog-content">
        </div>
    </div>        
@endsection

@include('template-part.popup-plan')

@endsection
