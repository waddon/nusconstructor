@extends('layouts.admin')

@section('title')
    <title>Додати тип предмету</title>
@endsection

@section('content')

<h1>Додати тип предмету</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::open(['route' => 'stypes.index']) }}

        <div class="form-group">
            {{ Form::label('name_stype', 'Назва типа предмету') }}
            {{ Form::text('name_stype', null, ['class' => 'form-control' . ($errors->has('name_stype') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_stype'))
                @foreach ($errors->get('name_stype') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', ['class' => 'btn btn-primary']) }}

    {{ Form::close() }}

</div>

@endsection