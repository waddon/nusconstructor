@extends('layouts.admin')

@section('title')
    <title>Типи предметів</title>
@endsection

@section('content')

    <h1>Управління типами предметів</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('stypes.create') }}" class="btn btn-success">Додати тип предмету</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Назва типу предмету</th>
                <th>Операція</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($stypes as $stype)
            <tr>
                <td>{{ $stype->id_stype }}</td>
                <td>{{ $stype->name_stype }}</td>
                <td>
                    <a href="{{ route('stypes.edit', $stype->id_stype) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>
                    
                    {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['stypes.destroy', $stype->id_stype] ]) !!}
                        {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection