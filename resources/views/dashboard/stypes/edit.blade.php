@extends('layouts.admin')

@section('title')
    <title>Редагування типа предмету</title>
@endsection

@section('content')

<h1>Редагування {{$stype->name_stype}}</h1>

<div class='col-lg-4 col-lg-offset-4'>

    {{ Form::model($stype, ['route' => ['stypes.update', $stype->id_stype], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_stype', 'Назва типу предмету') }}
            {{ Form::text('name_stype', null, ['class' => 'form-control' . ($errors->has('name_stype') ? ' is-invalid' : '')]) }}

            @if($errors->has('name_stype'))
                @foreach ($errors->get('name_stype') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection