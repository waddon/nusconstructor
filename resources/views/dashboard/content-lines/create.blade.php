@extends('layouts.admin')

@section('title')
    <title>Додати змістову лінію</title>
@endsection

@section('content')

<div class='col-lg-12'>

    <h1>Додати змістову лінію</h1>

    {{ Form::open(['route' => 'content-lines.index']) }}

        <div class="form-group">
            {{ Form::label('content_line_name', 'Назва') }}
            {{ Form::text('content_line_name', null, ['class' => 'form-control' . ($errors->has('content_line_name') ? ' is-invalid' : '')]) }}

            @if($errors->has('content_line_name'))
                @foreach ($errors->get('content_line_name') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('content_line_label', 'Код вміння') }}
            {{ Form::text('content_line_label', null, ['class' => 'form-control' . ($errors->has('content_line_label') ? ' is-invalid' : '')]) }}

            @if($errors->has('content_line_label'))
                @foreach ($errors->get('content_line_label') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('branch_id', 'Галузь', ['class' => 'form-control-label']) }}
            {{ Form::select('branch_id', $branches, 0, ['class' => 'form-control' . ($errors->has('branch_id') ? ' is-invalid' : '')]) }}

            @if($errors->has('branch_id'))
                @foreach ($errors->get('branch_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('content_line_text', 'Опис') }}
            {{ Form::textarea('content_line_text', null, ['class' => 'form-control tinymce-val' . ($errors->has('content_line_text') ? ' is-invalid' : '')]) }}

            @if($errors->has('content_line_text'))
                @foreach ($errors->get('content_line_text') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection