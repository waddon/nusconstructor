@extends('layouts.admin')

@section('title')
    <title>Редагувати ЗОР</title>
@endsection

@section('content')

<div class='col-lg-12'>

    <h1>Редагувати {{$content_line->name_zmist}}</h1>


    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    {{ Form::model($content_line, ['route' => ['content-lines.update', $content_line->id_zmist], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name_zmist', 'Назва') }}
            {{ Form::text('name_zmist', null, ['class' => 'form-control' . ($errors->has('name_zmist') ? ' form-control-danger' : '')]) }}

            @if($errors->has('name_zmist'))
                @foreach ($errors->get('name_zmist') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('marking_zmist', 'Код вміння') }}
            {{ Form::text('marking_zmist', null, ['class' => 'form-control' . ($errors->has('marking_zmist') ? 'is-invalid' : '')]) }}

            @if($errors->has('marking_zmist'))
                @foreach ($errors->get('marking_zmist') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('id_galuz', 'Галузь', ['class' => 'form-control-label']) }}
            {{ Form::select('id_galuz', $branches, null, ['class' => 'form-control' . ($errors->has('id_galuz') ? 'is-invalid' : '')]) }}

            @if($errors->has('id_galuz'))
                @foreach ($errors->get('id_galuz') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('text_zmist', 'Опис') }}
            {{ Form::textarea('text_zmist', null, ['class' => 'form-control tinymce-val' . ($errors->has('text_zmist') ? ' is-invalid' : '')]) }}

            @if($errors->has('text_zmist'))
                @foreach ($errors->get('text_zmist') as $message)
                    <div class="invalid-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>
        {{ Form::submit('Редагувати', ['class' => 'btn btn-success']) }}

    {{ Form::close() }}

</div>

@endsection