@extends('layouts.admin')

@section('title')
    <title>Змістові лінії</title>
@endsection

@section('content')

    <h1>Змістові лінії</h1>
    <div class="col-lg-3">
        <div class="form-group">
            <a href="{{ route('content-lines.create') }}" class="btn btn-success">Додати змістову лінію</a>
        </div>
    </div>
    
    <table class="table">
        <thead>
            <tr>
                <th>Змістова лінія</th>
                <th>Код змістової лінії</th>
                <th>Галузь</th>
                {{-- <th>Код вміння</th> --}}
                <th>Операції</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($content_lines as $content_line)
                @if(Auth::user()->hasAnyPermission($content_line->getContentLineBranch()->marking_galuz, 'edit_content'))
                    <tr>
                        <td>{{ $content_line->name_zmist }}</td>
                        <td>{{ $content_line->marking_zmist }}</td>
                        <td>{{ $content_line->getContentLineBranch()->name_galuz }}</td>
                        {{-- <td>{!! $content_line->text_zmist !!}</td> --}}
                        <td>
                            <a href="{{ route('content-lines.edit', $content_line->id_zmist) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>
                            {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['content-lines.destroy', $content_line->id_zmist] ]) !!}
                                {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

@endsection