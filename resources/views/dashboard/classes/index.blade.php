@extends('layouts.admin')

@section('title')
    <title>Класи</title>
@endsection

@section('content')

<h1>Управління класами</h1>
<div class="col-lg-3">
    <div class="form-group">
        <a href="{{ route('classes.create') }}" class="btn btn-success">Додати клас</a>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Назва</th>
            <th>Цикл</th>
            <th>Операція</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($classes as $class)
        <tr>
            <td>{{ $class->name_class }}</td>
            <td>{{ $class->getClassCycle() }}</td>
            <td>
                <a href="{{ route('classes.edit', $class->id_class) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Редагувати</a>

                {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => ['classes.destroy', $class->id_class] ]) !!}
                    {!! Form::submit('Видалити', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<a href="{{ route('classes.create') }}" class="btn btn-success">Додати клас</a>

@endsection