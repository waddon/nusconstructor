@extends('layouts.admin')

@section('title')
    <title>Додати клас</title>
@endsection

@section('content')


<h1>Додати клас</h1>

<div class='col-lg-12'>

    {{ Form::open(array('route' => 'classes.index')) }}

        <div class="form-group {{ $errors->has('class_name') ? 'has-danger' : '' }}">
            {{ Form::label('class_name', 'Назва') }}
            {{ Form::text('class_name', null, array('class' => 'form-control')) }}

            @if($errors->has('cycle_name'))
                @foreach ($errors->get('cycle_name') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        <div class="form-group {{ $errors->has('cycle_id') ? 'has-danger' : '' }}">
            {{ Form::label('cycle_id', 'Цикл', ['class' => 'form-control-label']) }}
            {{ Form::select('cycle_id', $cycles, 0, ['class' => 'form-control' . ($errors->has('cycle_id') ? ' form-control-danger' : '')]) }}

            @if($errors->has('cycle_id'))
                @foreach ($errors->get('cycle_id') as $message)
                    <div class="form-control-feedback">{{ $message }}</div>
                @endforeach
            @endif
        </div>

        {{ Form::submit('Додати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection