@extends('layouts.admin')

@section('title')
    <title>Редагування рівня</title>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h1>Редагування {{$class->name_class}}</h1>

<div class='col-lg-12'>

    {{ Form::model($class, array('route' => array('classes.update', $class->id_class), 'method' => 'PUT')) }}

     <div class="form-group {{ $errors->has('name_class') ? 'has-danger' : '' }}">
        {{ Form::label('name_class', 'Назва') }}
        {{ Form::text('name_class', null, array('class' => 'form-control')) }}

        @if($errors->has('name_class'))
            @foreach ($errors->get('name_class') as $message)
                <div class="form-control-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    <div class="form-group {{ $errors->has('id_cikl') ? 'has-danger' : '' }}">
        {{ Form::label('id_cikl', 'Цикл', ['class' => 'form-control-label']) }}
        {{ Form::select('id_cikl', $cycles, null, ['class' => 'form-control' . ($errors->has('id_riven') ? ' form-control-danger' : '')]) }}

        @if($errors->has('id_cikl'))
            @foreach ($errors->get('id_cikl') as $message)
                <div class="form-control-feedback">{{ $message }}</div>
            @endforeach
        @endif
    </div>

    {{ Form::submit('Редагувати', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection