@extends('common.layout')

<style>
.video {position:relative;padding-bottom:56.25%;/*пропорции видео 16:9 */height:0;}

.video iframe {position:absolute;top:0;left:0;width:100%;height:100%;}
</style>

    @section('content')
        @if(isset($title))
            <h1>{{ $title }}</h1>
        @endif

        @if(isset($content))
            {!! $content !!}
        @endif

    @endsection

