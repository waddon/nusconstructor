@extends('common.layout-new')

    @section('content')

        <div class=" content-box-title">{{ $title or '' }}</div>
        <div class="content-box">
            <div class="select-zors dialog-content">
                <span>Галузь</span>
                <select name="galuz" id="galuz">
                    @foreach ($galuzes as $galuz)
                        <option value="{{ $galuz->id_galuz }}">{{ $galuz->name_galuz }}</option>
                    @endforeach                    
                </select>
                <span>Цикл</span>
                <select name="cikl" id="cikl">
                    @foreach ($cikls as $cikl)
                        <option value="{{ $cikl->id_cikl }}">{{ $cikl->name_cikl }}</option>
                    @endforeach                    
                </select>
            </div>

            <table class="zors">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Загальні результати навчання здобувачів освіти</td>
                        <td>Обов’язкові результати навчання здобувачів освіти</td>
                    </tr>
                </thead>
                <tbody id="view-zors">
                    <?php $temp='';?>
                    @foreach ($zors as $zor)
                        <?php if ($zor->umgroupe != $temp) { $temp = $zor->umgroupe;?>
                        <tr>
                            <td colspan="3" style="font-weight: bold;">{{ $zor->umgroupe }}</td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td>{{ $zor->key_zor }}</td>
                            <td>{{ $zor->name_umenie }}</td>
                            <td>{{ $zor->name_zor }}</td>
                        </tr>
                    @endforeach                    
                </tbody>
            </table>
        </div>

    @endsection


@section('scripts')
    <script>
        jQuery(document).ready(function(){
            var on_change = false;

            $( "#galuz, #cikl" ).change(function(){
                if (!on_change) {
                    on_change = true;                    
                    $.get('{{ route("get_zors") }}', {id_galuz: $( "#galuz" ).val(), id_cikl: $( "#cikl" ).val()} ,function(result){
                        result = JSON.parse(result);
                        $("#view-zors").empty();
                        temp = '';
                        result.forEach(function(item, i, result) {
                            if (item.umgroupe != temp) {
                                temp = item.umgroupe;
                                $("#view-zors").append('<tr><td colspan="3" style="font-weight: bold;">'+item.umgroupe+'</td></tr>');
                            }
                            $("#view-zors").append('<tr><td>'+item.key_zor+'</td><td>'+item.name_umenie+'</td><td>'+item.name_zor+'</td></tr>');
                        });                    
                        on_change = false;
                    });
                }

            });
        });
    </script>
@endsection

