@extends('common.layout-new')

    @section('content')

        <div class=" content-box-title">{{ $title or '' }}</div>
        <div class="content-box">
            <div class="select-zors dialog-content">
                <span>Цикл</span>
                <select name="cikl" id="cikl">
                    @foreach ($cikls as $cikl)
                        <option value="{{ $cikl->id_cikl }}">{{ $cikl->name_cikl }}</option>
                    @endforeach                    
                </select>
            </div>

            <div id="note">
                {!! $note->text_note !!}
            </div>

        </div>

    @endsection


@section('scripts')
    <script>
        jQuery(document).ready(function(){
            var on_change = false;

            $( "#cikl" ).change(function(){
                if (!on_change) {
                    on_change = true;                    
                    $.get('{{ route("get_note") }}', {id_cikl: $( "#cikl" ).val()} ,function(result){
                        result = JSON.parse(result);
                        // console.log(result);
                        $("#note").empty();
                        $("#note").append(result.text_note);
                        on_change = false;
                    });
                }

            });
        });
    </script>
@endsection

