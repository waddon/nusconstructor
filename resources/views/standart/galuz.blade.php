@extends('common.layout-new')

    @section('content')

        @if ( isset($galuzes) )
            <div class=" content-box-title">{{ $title or '' }}</div>
            <div class="content-box">
                <table class="galuzes">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Назва</td>
                            <td>Код</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($galuzes as $galuz)
                            <tr>
                                <td>{{ $galuz->id_galuz }}</td>
                                <td class="color{{ $galuz->id_galuz }} color-td action">
                                    <span class="galuz-title">{{ $galuz->name_galuz }}</span>
                                    <div class="galuz-info" style="display: none;">{!! $galuz->text_galuz !!}</div>
                                </td>
                                <td>{{ $galuz->marking_galuz }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif

    @endsection


    @section('scripts')
        <script>
            jQuery(document).ready(function(){

                $(".galuz-title").click(function(){

                    $(this).siblings(".galuz-info").slideToggle( "fast" );
                });

            });
        </script>
    @endsection
