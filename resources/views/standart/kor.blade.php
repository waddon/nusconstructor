@extends('common.layout-new')

    @section('content')

        <div class=" content-box-title">{{ $title or '' }}</div>
        <div class="content-box">
            <div class="select-zors dialog-content">
                <span>Галузь</span>
                <select name="galuz" id="galuz">
                    @foreach ($galuzes as $galuz)
                        <option value="{{ $galuz->id_galuz }}">{{ $galuz->name_galuz }}</option>
                    @endforeach                    
                </select>
                <span>Цикл</span>
                <select name="cikl" id="cikl">
                    @foreach ($cikls as $cikl)
                        <option value="{{ $cikl->id_cikl }}">{{ $cikl->name_cikl }}</option>
                    @endforeach                    
                </select>
            </div>

            <div class="card">
                <div class="card-header toggle-header">Пояснювальна записка</div>
                <div id="note" class="card-block toggle-content" style="display: none;">
                    {!! $galuznote->text_galuznote !!}
                </div>
            </div>

            <table class="kors">
                <thead>
                    <tr>
                        <td>Обов’язкові результати навчання здобувачів освіти</td>
                        <td>Очікувані результати навчання</td>
                    </tr>
                </thead>
                <tbody id="view-kors">
                    {!! $result !!}
                </tbody>
            </table>
        </div>

    @endsection


@section('scripts')
    <script>
        jQuery(document).ready(function(){
            var on_change = false;

            $( "#galuz, #cikl" ).change(function(){
                if (!on_change) {
                    on_change = true;                    
                    $.get('{{ route("get_kors") }}', {id_galuz: $( "#galuz" ).val(), id_cikl: $( "#cikl" ).val()} ,function(result){
                        result = JSON.parse(result);
                        $("#note").empty();
                        $("#note").append(result.galuznote.text_galuznote);
                        $("#view-kors").empty();
                        $("#view-kors").append(result.result);
                        on_change = false;
                    });
                }

            });
        });
    </script>
@endsection

