@extends('common.layout-new')

    @section('content')

        <div class=" content-box-title">{{ $title or '' }}</div>
        @if ( isset($result) )
            <div class="content-box">
                {!! $result !!}
            </div>
        @endif

    @endsection


