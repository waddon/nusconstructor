<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <link rel="icon" href="../../../../favicon.ico"> -->
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @section('title')
            <title>Адміністративна панель</title>
        @show
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <!-- jQuery-UI CSS -->
        <link rel="stylesheet" href="/assets/lib/jQueryUI/jquery-ui.min.css">
        <!-- toastmessage CSS -->
        <link rel="stylesheet" type="text/css" href="/assets/lib/toastmessage/css/jquery.toastmessage.css" />
        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="https://v4-alpha.getbootstrap.com/examples/dashboard/dashboard.css">
        <link rel="stylesheet" href="/assets/css/popup.css">
        <link rel="stylesheet" href="/assets/css/programs.css">
        <link rel="stylesheet" href="/assets/css/admin.css">
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <a class="navbar-brand" href="{{ route('dashboard') }}">Адміністративна панель</a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}">На головний сайт<span class="sr-only">(current)</span></a>
                        </li>
                    </ul>
                    <form class="form-inline mt-2 mt-md-0">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Пошук</button>
                    </form>
                </div>
            </nav>
        </header>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">

                    @can('edit_users')
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('users.index') }}">Користувачи</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('roles.index') }}">Ролі</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('permissions.index') }}">Дозволи</a>
                            </li>
                        </ul>
                    @endcan

                    @can('edit_content')
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('categories.index') }}">Категорії</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('pages.index') }}">Страницы</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('posts.index') }}">Записи</a>
                            </li>
                        </ul>
                    @endcan
                    
                    @can('edit_content')
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('levels.index') }}">Рівні</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('cycles.index') }}">Цикли</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('notes.index') }}">Описи освітніх програм циклів</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('classes.index') }}">Класи</a>
                            </li>
                        </ul>
                    @endcan
                    
                    @if(Auth::user()->hasAnyPermission(['edit_branch', 'edit_content']))
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('branches.index') }}">Галузі</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('expected-outcomes.index') }}">ЗОРи</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('content-lines.index') }}">Змістові лінії</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('specific-outcomes.index') }}">КОРи</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('skills.index') }}">Уміння</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('branchnotes.index') }}">Описи галузей</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('subjects.index') }}">Назви предметів</a>
                            </li>
                        </ul>
                    @endif
                    
                    @can('edit_content')
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('schools.index') }}">Школи</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('schooltypes.index') }}">Типи школ</a>
                            </li>
                        </ul>
                    @endcan
                    
                    @can('edit_content')
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('creates.index') }}">Роли создателей</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('tools.index') }}">Інструменти оцінювання</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('statuses.index') }}">Статуси</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('stypes.index') }}">Типи предметів</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('ptypes.index') }}">Типи планів\програм</a>
                            </li>
                        </ul>
                    @endcan
                    
                    @can('edit_menu')
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('menus.index') }}">Меню</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('menuitems.index') }}">Пункти меню</a>
                            </li>
                        </ul>
                    @endcan

                    @can('approve')
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('approve') }}">Узгодження</a>
                            </li>
                        </ul>
                    @endcan

                </nav>
                <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3" style="margin-bottom: 30px;">
                    @if(Session::has('flash_message'))
                        <div class="container">      
                            <div class="alert alert-success">
                                <em>{!! session('flash_message') !!}</em>
                            </div>
                        </div>
                    @endif
                    @if(Session::has('error_message'))
                        <div class="container">      
                            <div class="alert alert-danger">
                                <em>{!! session('error_message') !!}</em>
                            </div>
                        </div>
                    @endif
                    @section('content')
                        <h1>Dashboard</h1>
                    @show
                </main>
            </div>
        </div>
        <div style="display: none;">
            @section('dialogs')
            @show
        </div>

        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!--script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script-->
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" ></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/assets/lib/toastmessage/jquery.toastmessage.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script src="/tinymce/tinymce.min.js"></script>
        <script>
            var $rows = $('table tbody tr');
            var hexathis, current_package = {};
            
            $('#search').keyup(function() {
                var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
                
                $rows.show().filter(function() {
                    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();
            });

            tinymce.init({
                selector: ".tinymce-val",
                language: "uk_UA",
                theme: "modern",
                width: "100%",
                height: 300,
                plugins: [
                     "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                     "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                     "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
               ],
               toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
               toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
               image_advtab: true ,
               
               external_filemanager_path: "/filemanager/",
               filemanager_title: "Responsive Filemanager",
               filemanager_access_key: "s2yrxre2XExpkm4fcDtGLdaFVjWXScnaYtnGULh2efuE5u9DtXggmmjjQVKbrcY3",
               external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
            });
            
            $(document).ready(function(){

                $("#selector").change(function(){
                    $(location).attr('href', '{{route('menuitems.index')}}?id_menu=' + $(this).val());
                });
                
                $('.menuitem-title .name-menuitem').click(function(){
                    $(this).parent().siblings('.menuitem-content').slideToggle( "fast" );
                });

                $('.chevron-buttons .fa-chevron-up, .chevron-buttons .fa-chevron-down').click(function(){
                    if ($(this).hasClass('fa-chevron-up')) {
                        alert('Up');
                    } else {
                        alert('Down');
                    }
                });



                $( ".edit-package" ).click(function(){edit_package($(this));});

                function edit_package(sender){
                    temp_int = parseInt(sender.attr("data-id-package"));
                    var data = new FormData();
                    data.append( 'id_package', sender.attr("data-id-package") );
                    $.ajax ({
                        url: '{{ route("school-package-get") }}',
                        type: "POST",
                        dataType: 'json',
                        data: data,
                        processData: false,
                        contentType : false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (result) {
                            console.log(result);
                            $(".vam").empty();
                            $(".vam").append( result.vam_package );
                            tinyMCE.activeEditor.setContent("");
                            tinyMCE.execCommand('mceInsertContent', false, result.critical_package );
                            console.log(result.critical_package);
                            $( "#name_package" ).val( result.name_package );
                            //$( "#critical" ).append( result.critical_package );
                            plans_package = JSON.parse(result.plans_package);
                            var temp = '<table class="check"><thead><tr><td>Навчальний цикл</td><td>Назва плану</td><td>Статус плану</td><td class="action-buttons">Дії</td></tr></thead><tbody>';
                            for (key in plans_package) {
                                temp += '<tr class="list-of-plans"><td class="name-cikl" data-id-cikl="'+plans_package[key].id_cikl+'">'+plans_package[key].name_cikl+'</td><td class="name-plan" data-id-plan="'+plans_package[key].id_plan+'">'+plans_package[key].name_plan+'</td><td class="name-status" data-id-status="'+plans_package[key].id_status+'">'+plans_package[key].name_status+'</td><td><span class="popup-plan table-button" data-id-plan="'+plans_package[key].id_plan+'" data-toggle="tooltip" title="Переглянути план"><i class="fa fa-eye" aria-hidden="true"></i></span></td></tr>';
                            }
                            temp += '</tbody></table>';
                            $( "#tabs-2" ).html( temp );
                            $(".popup-plan").click(function(){
                                get_plan($(this).attr("data-id-plan"));
                            });
                            current_package.id_package = temp_int;
                            $( "#dialog-package" ).dialog( "open" );
                        },
                        error: function (result) {
                            $().toastmessage('showToast', {
                                text     : 'Error',
                                type     : 'error',
                            });
                        }
                    });
                }


                $( function() {
                    $( "#tabs" ).tabs();

                    $( "#dialog-package" ).dialog({
                        resizable: false,
                        width: "80%",
                        modal: true,
                        autoOpen: false,
                        show: {
                            effect: "fade",
                            duration: 100
                        },
                        hide: {
                            effect: "fade",
                            duration: 100
                        },
                        buttons: [
                            {
                                text: "Затвердити",
                                class: "btn btn-success",
                                click: function() {
                                    var data = new FormData();
                                    data.append( 'id_package', current_package.id_package );
                                    data.append( 'id_status', 7 );
                                    data.append( 'critical_package', tinyMCE.activeEditor.getContent() );
                                    $.ajax ({
                                        url: '{{ route("approve.package_approve") }}',
                                        type: "POST",
                                        dataType: 'json',
                                        data: data,
                                        processData: false,
                                        contentType : false,
                                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                        success: function (result) {
                                            $('[data-id-package="'+current_package.id_package+'"]').remove();
                                            $().toastmessage('showToast', {
                                                text     : 'Затверджено',
                                                type     : 'success',
                                            });
                                            console.log(result);
                                        },
                                        error: function (result) {
                                            $().toastmessage('showToast', {
                                                text     : 'Error',
                                                type     : 'error',
                                            });
                                        }
                                    });
                                    $( this ).dialog( "close" );
                                }
                            },
                            {
                                text: "Доопрацювання",
                                class: "btn btn-primary",
                                click: function() {
                                    var data = new FormData();
                                    data.append( 'id_package', current_package.id_package );
                                    data.append( 'id_status', 8 );
                                    data.append( 'critical_package', tinyMCE.activeEditor.getContent() );
                                    $.ajax ({
                                        url: '{{ route("approve.package_approve") }}',
                                        type: "POST",
                                        dataType: 'json',
                                        data: data,
                                        processData: false,
                                        contentType : false,
                                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                        success: function (result) {
                                            $('[data-id-package="'+current_package.id_package+'"]').remove();
                                            $().toastmessage('showToast', {
                                                text     : 'Повернуто на доопрацювання',
                                                type     : 'notice',
                                            });
                                            console.log(result);
                                        },
                                        error: function (result) {
                                            $().toastmessage('showToast', {
                                                text     : 'Error',
                                                type     : 'error',
                                            });
                                        }
                                    });
                                    $( this ).dialog( "close" );
                                }
                            },
                            {
                                text: "Скасувати",
                                class: "btn btn-warning",
                                click: function() {
                                    $( this ).dialog( "close" );
                                    $().toastmessage('showToast', {
                                        text     : 'Скасовано',
                                        type     : 'warning',
                                    });
                                }
                            }
                        ]

                    });
                });


            });
        </script>
@section('popup')
@show

    </body>
</html>