jQuery( document ).ready(function( $ ) {

    /* Подключение текстового редактора */
    tinymce.init({ selector:'textarea.tinymce' });


    /* открытие бокового меню */
    $('.icon-menu').click(function() {
        $('.left-menu').animate({ left: '0px' }, 100);
        jQuery('#mask').show();
    });
 
 
    /* закрытие бокового меню */
    $('.icon-close').click(function() {
        $('.left-menu').animate({ left: '-500px'}, 100);
        jQuery('#mask').hide();
    });


    /* ??? наверное, мобильное меню */
    $('.icon-menu-2').on('click', function(e) {
        e.preventDefault();
        $('.left-menu-2').slideToggle();
    });



    /* появляние/исчезание кнопки "вверх" */
    $(window).scroll(function() {
        if($(this).scrollTop() != 0) { 
            $('#GotoTop').fadeIn(); 
        } else { 
            $('#GotoTop').fadeOut(); 
        }
    });

    /* скролинг наверх при нажатии кнопки "вверх" */
    $('#GotoTop').click(function() {
        $('body,html').animate({scrollTop:0},800);
    });


    /* Создаем объекты Queryui и прочее */
    $( function() {
        
        $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        });

        
        $('[data-toggle="tooltip"]').tooltip()

    });



});
