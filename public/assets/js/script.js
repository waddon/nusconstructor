jQuery( document ).ready(function( $ ) {

    /* Тонкий скролл */
    var ps = new PerfectScrollbar('#scroll-box');
    function updateSize() {
        var width = parseInt($('#width').value, 10);
        var height = parseInt($('#height').value, 10);
        $('#scroll-box').style.width = width + 'px';
        $('#scroll-box').style.height = height + 'px';
        ps.update();
    }


    /* Уменьшаем ширину скролбокса. Изначально имеет ширину больше, чтобы не мигал скролинг */
    $('#scroll-box').css('width', '100%');


    /* Открывашка бокового меню */
    $('.sidebar-item.with-submenu').click(function(){
        if($(this).hasClass('open')) {
            $(this).removeClass('open');
        } else {
            $(this).addClass('open');
        }
    });


    $(".toggle-header").click(function(){
        $(this).siblings(".toggle-content").slideToggle( "slow" );
    });


    /* Создаем объекты Queryui и прочее */
    $( function() {
        $( "#tabs" ).tabs();
        
        $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        });

        
        //$('[data-toggle="tooltip"]').tooltip()

    });

});